<%@ include file="/taglibs.jsp"%>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
	var seleccionTextArea="";
	function onLoad(){
		fc_CambiaRequerimiento();
		fc_DesabilitarTodo();
		objMsg=document.getElementById("txhMsg");
		if ( objMsg.value == "OK" ){
			document.forms[0].imgAprobar01.disabled=true;
			alert(mstrAproboExito);
			fc_Regresar();
		}
		else
		if ( objMsg.value == "OKENVIAR" ){
			alert(mstrEnvioAprobExito);
			fc_Regresar();
		}
		else
		if ( objMsg.value == "OKRECHAZAR" ){
			alert(mstrRechazoSolExito);
			fc_Regresar();
		}
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
		}
		else if ( objMsg.value == "ERRORRESP"){
			alert(mstrNoAsigResponsables);
		}
		else if(  objMsg.value == "ERRORCANT"){
			 alert(mstrCantidadMayorCero);
		}
		else if(  objMsg.value == "ALMACEN_CERRADO"){
			 alert(mstrEstadoLogistica);
		}
	}
	function fc_DesabilitarTodo(){
		seleccion=document.getElementById("txhCodTipoRequerimiento").value;
		switch(seleccion){
			//bien
			case '0001':
				fc_DesabilitarCamposBien();
				break;
			//servicio
			case '0002':
				fc_DesabilitarCamposServicio();
				break;
		}
	}
	//desabilita todos los campos de la bandeja bien menos la cantidad
	//ALQD,19/08/08. HABILITAMOS EL CBOGASTO PARA PERMITIR CAMBIOS
	//ALQD,19/01/10. HABILITANDO cboCentroCosto PARA PERMITIR CAMBIOS
	function fc_DesabilitarCamposBien(){
//		document.getElementById("cboCentroCosto").disabled=true;
		document.getElementById("cboCentroCosto").disabled=false;
		
		tabla=document.getElementById("tablaBandeja");
		hijosTr=tabla.getElementsByTagName('tr');
		numeroHijoTabla = hijosTr.length-1;
		
		for(var i=0; i<numeroHijoTabla;i++){
			document.getElementById("txtCantidad"+i).disabled=false;
//			document.getElementById("cboGasto"+i).disabled=true;
			document.getElementById("cboGasto"+i).disabled=false;
			document.getElementById("txhFecha"+i).readOnly=true;
			document.getElementById("txtDescripcion").readOnly=true;
		}
	}
	//ALQD,19/01/10. HABILITANDO cboCentroCosto PARA PERMITIR CAMBIOS
	function fc_DesabilitarCamposServicio(){
		//document.getElementById("cboCentroCosto").disabled=true;
		document.getElementById("cboCentroCosto").disabled=false;
		document.getElementById("cboRequerimiento").disabled=true;
		document.getElementById("txtNombreServicio").readOnly=true;
		document.getElementById("cboServicio").disabled=true;
		document.getElementById("txtFechaAtencion").readOnly=true;
		document.getElementById("txtDescripcion").readOnly=true;
		//desabilitar boton enviar a arpobacion si existe un responsable
		/*if(document.getElementById("txhCodUsuarioResponsable").value == ""){
			document.getElementById("btnEnviarAprobacion").disabled=true;
		}*/
	}
	function fc_Refrescar(){
		document.getElementById("txhOperacion").value="REFRESCAR";
		document.getElementById("frmMain").submit();
	}
	function fc_TipoBien(param){
		activo.style.display = 'none';
		if (param == 'A'){
				activo.style.display = '';
		}
	}
	function fc_CambiaRequerimiento(){
		seleccion=document.getElementById("txhCodTipoRequerimiento").value;
		switch(seleccion){
			case '0001':
				trServicio01.style.display="none";
				trServicio02.style.display="none";
				trServicio03.style.display="none";
				tablaBandejaSolicitud.style.display="";
				tablaBotonesBien.style.display=""
				tablaBotonesServicioGeneral.style.display="none"
				tablaBotonesServicioMantenimiento.style.display="none"
				break;
			case '0002':
				trServicio01.style.display="";
				trServicio02.style.display="";
				trServicio03.style.display="";
				tablaBandejaSolicitud.style.display="none";
				tablaBotonesBien.style.display="none"
				if(document.getElementById("txhIndicadorGrupo").value=="0"){
					tablaBotonesServicioGeneral.style.display=""
					tablaBotonesServicioMantenimiento.style.display="none"
				}
				else{
					tablaBotonesServicioGeneral.style.display="none"
					tablaBotonesServicioMantenimiento.style.display=""
				}
				activo.style.display="";
				//********************************************************************
				codRequerimiento=document.getElementById("txhCodRequerimiento").value;
				codRequerimintoDetalle=document.getElementById("txhCodReqDetalleServicio").value;
				condicion="0";
				document.getElementById("iFrameGrilla").src="${ctx}/logistica/SreqDocumentosDetalle.html?txhCodRequerimiento="+codRequerimiento+
					"&txhCodDetRequerimiento="+codRequerimintoDetalle+"&txhCondicion="+condicion;;
				//********************************************************************
				break;
		}
		//ALQD,20/01/10 SETEANDO EL VALOR
		if(document.getElementById("txhIndInversion").value=="1"){
			document.getElementById("chkIndInversion").checked=true;
		}
		if(document.getElementById("txhIndEmpLogistica").value=="1"){
			if(document.getElementById("txhIndParaStock").value=="1"){
				document.getElementById("chkIndParaStock").checked=true;
			}
		}
	}
	function fc_MostrarDetalleRegistro(seleccion, codDetRequerimiento){
		//solo muestra el detalle descripcion y adjunto cuando sea tipo bien
		//if(document.getElementById("txhCodSubTipoRequerimiento").value=="0001"){
			activo.style.display = '';
			seleccionTextArea=seleccion;
			document.getElementById("txtDescripcion").value=document.getElementById('valHid'+seleccionTextArea).value;
			codRequeriminto=document.getElementById("txhCodRequerimiento").value;
		//ALQD,16/09/08. SE MOSTRARA EL CAMPO DESCRIPCION PERO NO ADJUNTOS
		if(document.getElementById("txhCodSubTipoRequerimiento").value=="0001"){
			document.getElementById("iFrameGrilla").src="${ctx}/logistica/SreqDocumentosDetalle.html?txhCodRequerimiento="+codRequeriminto+
				"&txhCodDetRequerimiento="+codDetRequerimiento+
				"&txhCondicion=0";
		}
	}
	function fc_Regresar(){
		codSede=document.getElementById("txhCodSede").value;
		window.location.href="${ctx}/logistica/bandejaAreqUsuario.html?txhCodSede="+codSede;
	}
	//ALQD,20/01/10. LEYENDO LOS NUEVOS CONTROLES
	function fc_AprobarBien(){
		form = document.forms[0];
		codUsuario=document.getElementById("txhCodUsuario").value;
		codRequerimiento=document.getElementById("txhCodRequerimiento").value;
		if(document.getElementById("chkIndInversion").checked==true){
			document.getElementById("txhIndInversion").value="1";
		}else document.getElementById("txhIndInversion").value="0";
		if(document.getElementById("txhIndEmpLogistica").value=="1"){
			if(document.getElementById("chkIndParaStock").checked==true){
				document.getElementById("txhIndParaStock").value="1";
			} else document.getElementById("txhIndParaStock").value="0";
		} else document.getElementById("txhIndParaStock").value="0";
		indInversion=document.getElementById("txhIndInversion").value;
		indParaStock=document.getElementById("txhIndParaStock").value;
		if(fc_DatosBandejaSolReqBien()){
			if(confirm("Seguro de aprobar el requerimiento?")){
				form.imgAprobar01.disabled=true;
				document.getElementById("txhOperacion").value="APROBARBIEN";
				document.getElementById("frmMain").submit();
			}else
				form.imgAprobar01.disabled=false;
		}
	}
	//ALQD,20/01/10. LEYENDO LOS NUEVOS CONTROLES
	function fc_AprobarServicio(){
		form = document.forms[0];
		if(document.getElementById("chkIndInversion").checked==true){
			document.getElementById("txhIndInversion").value="1";
		}else document.getElementById("txhIndInversion").value="0";
		indInversion=document.getElementById("txhIndInversion").value;
		indParaStock="0";
		if(confirm(mstrConfAprobarRequerimiento)){
			form.imgAprobar02.disabled=true;
			codUsuario=document.getElementById("txhCodUsuario").value;
			codRequerimiento=document.getElementById("txhCodRequerimiento").value;
			document.getElementById("txhOperacion").value="APROBARSERVICIO";
			document.getElementById("frmMain").submit();
		}else
			form.imgAprobar02.disabled=false;
	}
	//ALQD,20/01/10. LEYENDO LOS NUEVOS CONTROLES
	function fc_AprobarServicioMantenimiento(){
		form = document.forms[0];
		if(document.getElementById("chkIndInversion").checked==true){
			document.getElementById("txhIndInversion").value="1";
		}else document.getElementById("txhIndInversion").value="0";
		indInversion=document.getElementById("txhIndInversion").value;
		indParaStock="0";
	if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)
	   {
		if(document.getElementById("txhCodUsuarioResponsable").value!=""){
			if(confirm(mstrConfAprobarRequerimiento)){
				form.imgAprobar03.disabled=true;
				codUsuario=document.getElementById("txhCodUsuario").value;
				codRequerimiento=document.getElementById("txhCodRequerimiento").value;
				document.getElementById("txhOperacion").value="APROBARSERVICIO";
				document.getElementById("frmMain").submit();
			}else
				form.imgAprobar03.disabled=false;
		}
		else{
			alert(mstrAsignarResponsable);
			return;
		}
	  }
	  else alert(mstrEstadoLogistica);
	}
	//ALQD,27/01/09. VALIDANDO INGRESAR RESPONSABLE ANTES DE ENVIAR
	//ALQD,20/01/10. LEYENDO LOS NUEVOS CONTROLES
	function fc_EnviarAprobacionMantenimiento(){
		form = document.forms[0];
		if(document.getElementById("chkIndInversion").checked==true){
			document.getElementById("txhIndInversion").value="1";
		}else document.getElementById("txhIndInversion").value="0";
		indInversion=document.getElementById("txhIndInversion").value;
		indParaStock="0";
		if(document.getElementById("txhCodUsuarioResponsable").value!=""){
			if(document.getElementById("txhBanderaEnviar").value=="0")
			  {	if(confirm(mstrConfEnviarCoti)){
				  form.btnEnviarAprobacion.disabled=true;
					codUsuario=document.getElementById("txhCodUsuario").value;
					codRequerimiento=document.getElementById("txhCodRequerimiento").value;
					document.getElementById("txhOperacion").value="ENVIARAPROBACION";
					document.getElementById("frmMain").submit();
				}else
					form.btnEnviarAprobacion.disabled=false;
			  }
			else alert(mstrAprobacionYaEnviada);
		}
		else{
			alert("Debe Asignar un responsable.");
			return;
		}
	}
	function fc_Rechazar(){
		form = document.forms[0];
		if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)
	  	{
		  if(confirm(mstrConfirRechazarApro)){
			   form.imgRechazar01.disabled=true;
			   form.imgRechazar02.disabled=true;
			   form.imgRechazar03.disabled=true;
			codUsuario=document.getElementById("txhCodUsuario").value;
			codRequerimiento=document.getElementById("txhCodRequerimiento").value;
			document.getElementById("txhOperacion").value="RECHAZAR";
			document.getElementById("frmMain").submit();
		  }else{
			  form.imgRechazar01.disabled=false;
			  form.imgRechazar02.disabled=false;
			  form.imgRechazar03.disabled=false;
		  }
	  	}
	  	else alert(mstrEstadoLogistica);
	}
	function fc_AsignarResponsable(){
    	codUsuario=document.getElementById("txhCodUsuario").value;
    	codRequerimiento=document.getElementById("txhCodRequerimiento").value;
    	codSede=document.getElementById("txhCodSede").value;
    	nroReq = document.getElementById("txtNroRequerimiento").value;
    	
    	Fc_Popup("${ctx}/logistica/log_Aprobacion_Responsable.html?txhCodUsuario="+codUsuario+
				"&txhCodRequerimiento="+codRequerimiento+
				"&txhCodSede="+codSede+"&txhNroReq="+nroReq,850,420);
	}
	function fc_DatosBandejaSolReqBien(){
		if(fc_ValidarCantidadVacio()==true){
			alert(mstrDebeIngrCantidad);
			return false;
		}
		if(fc_ValidarUnCampoMayorCero()==true){
			alert(mstrCampoMayorCero);
			return false;
		}
		else{
			tabla=document.getElementById("tablaBandeja");
			hijosTr=tabla.getElementsByTagName('tr');
			numeroHijoTabla = hijosTr.length-1;
			
			var cadena="";
			//ALQD. aqui debo modificar para permitir cambios en el tipo de gasto
			for(var i=0; i<numeroHijoTabla;i++){
				if(i==0){
					cadena=document.getElementById("txhCodigo"+i).value+"|"+document.getElementById("txtCantidad"+i).value+"|"+document.getElementById("cboGasto"+i).value;
				}
				else{
					cadena=cadena+"$"+document.getElementById("txhCodigo"+i).value+"|"+document.getElementById("txtCantidad"+i).value+"|"+document.getElementById("cboGasto"+i).value;
				}
			}
			document.getElementById("txhCadena").value=cadena;
			
			return true;
		}
	}
	function fc_ValidarCantidadVacio(){
		tabla=document.getElementById("tablaBandeja");
		hijosTr=tabla.getElementsByTagName('tr');
		numeroHijoTabla = hijosTr.length-1;
		
		for(var i=0; i<numeroHijoTabla;i++){
			cantidad=document.getElementById("txtCantidad"+i).value;
			if(cantidad=="" || fc_Trim(cantidad)==""){
				document.getElementById("txtCantidad"+i).focus();
				return true;
				
			}
		}
		return false;
	}
	function fc_ValidarUnCampoMayorCero(){
		tabla=document.getElementById("tablaBandeja");
		hijosTr=tabla.getElementsByTagName('tr');
		numeroHijoTabla = hijosTr.length-1;
		band=true;
		
		for(var i=0; i<numeroHijoTabla;i++){
			cantidad=document.getElementById("txtCantidad"+i).value;
			valor=parseFloat(cantidad,10);
			if(valor > 0){
				band=false;
				return band;
			}
		}
		return true;
	}
//ALQD,28/08/08. A�ADIENDO FUNCION PARA VALIDAR LA CANTIDAD DE DECIMALES INGRESADOS
	function fc_validaDecimalOnBlur(posicion, numDecimales){
		var nom = "txtCantidad"+posicion;
		var numero = parseFloat(document.getElementById(""+nom).value);
		var numDec = parseInt(document.getElementById(""+nom).value);
		var parteDecimal = String(numero - numDec);
		if(parteDecimal.length>numDecimales+2){
			alert("No puede colocar m�s de "+numDecimales+" decimal(es) a la cantidad para este producto.");
			document.getElementById(""+nom).focus();
		}
	}
	</script>
</head>

<body >
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/AreqBienesServicios.html" >
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codUsuarioResponsable" id="txhCodUsuarioResponsable" />
	<form:hidden path="cadena" id="txhCadena" />
	<form:hidden path="estado" id="txhEstado" />
	
	<!-- para trabajar con la bd -->
	<form:hidden path="codSede" id="txhCodSede" />
	<form:hidden path="codRequerimiento" id="txhCodRequerimiento" />
	<form:hidden path="codTipoCentroCosto" id="txhCodTipoCentroCosto" />
	<form:hidden path="codTipoRequerimiento" id="txhCodTipoRequerimiento" />
	<form:hidden path="codSubTipoRequerimiento" id="txhCodSubTipoRequerimiento" />
	<!--/para trabajar con la bd -->
	<form:hidden path="codReqDetalleServicio" id="txhCodReqDetalleServicio" />
	<form:hidden path="codDetRequerimiento" id="txhCodDetRequerimiento" />
	
	<form:hidden path="indicadorGrupo" id="txhIndicadorGrupo" />
	<form:hidden path="banderaEnviar" id="txhBanderaEnviar" />
	<form:hidden path="estadoAlmacen" id="txhEstadoAlmacen"/>
	<form:hidden path="consteEstadoAlmacen" id="txhConsteEstadoAlmacen"/>
	<form:hidden path="codPerfil" id="txhCodPerfil"/>
	<form:hidden path="jefeLog" id="txhJefeLog"/>
	<!-- ALQD,31/10/08. AGREGANDO NUEVA PROPIEDAD -->
	<form:hidden path="indInversion" id="txhIndInversion"/>
	<form:hidden path="indParaStock" id="txhIndParaStock"/>
	<form:hidden path="indEmpLogistica" id="txhIndEmpLogistica"/>
	<!-- title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo">
		 		<font style="">
		 			<c:if test='${control.codTipoRequerimiento=="0001"}'>
		 				<c:if test='${control.codSubTipoRequerimiento=="0002"}'>
					 		Aprobaci�n de Requerimiento de Bienes Consumibles
					 		<c:if test="${control.indInversion=='1'}"> - Tipo Inversi�n</c:if>
					 		<c:if test="${control.indParaStock=='1'}"> - Para Stock</c:if>
					 	</c:if>
				 		<c:if test='${control.codSubTipoRequerimiento=="0001"}'>
					 		Aprobaci�n de Requerimiento de Bienes Activos
					 	</c:if>
				 	</c:if>
				 	<c:if test='${control.codTipoRequerimiento=="0002"}'>
					 	Aprobacion de Requerimiento de Servicios
					 	<c:if test="${control.indInversion=='1'}"> - Tipo Inversi�n</c:if>
				 	</c:if>
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<!-- cabecera -->
	<!-- ALQD,20/01/10.PERMITIR MODIFICAR CCOSTO, IND_INVERSION Y P_STOCK -->
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2" border="0">
		<tr>
			<td width="15%">&nbsp;&nbsp;&nbsp;Nro. Requerimiento :</td>
			<td width="35%">
			
				<form:input path="nroRequerimiento" id="txtNroRequerimiento"
					cssClass="cajatexto_1" cssStyle="width: 50%" readonly="true"/>
					
			</td>
			<td width="15%">Fec. Requerimiento :</td>
			<td width="35%">
			
				<form:input path="fechaRequerimiento" id="txtFechaRequerimiento"
					cssClass="cajatexto_1" cssStyle="width: 50%" readonly="true"/>
				
			</td>
		</tr>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;Centro de Costo :</td>
			<td>
				<form:select path="cboTipoCentroCosto" id="cboCentroCosto" cssStyle="width:80%" cssClass="combo_o">
					<form:option value="-1">-- Seleccione --</form:option>
					<c:if test="${control.listaCentroCosto!=null}">
					<form:options itemValue="codTecsup" itemLabel="descripcion"
						items="${control.listaCentroCosto}" />
					</c:if>
				</form:select>
			</td>
			<td>Usuario Solicitante :</td>
			<td>
				<form:input path="usuSolicitante" id="txtUsuSolicitante"
					cssClass="cajatexto_1" cssStyle="width: 50%" readonly="true"/>
			</td>
		</tr>
		<tr id="trServicio01">
			<td>&nbsp;&nbsp;&nbsp;Tipo de &nbsp;&nbsp;&nbsp;Requerimiento :</td>
			<td>
				<form:select path="cboTipoRequerimiento" id="cboRequerimiento" cssStyle="width:50%" cssClass="combo_o" 
					onchange="javascript:fc_CambiaRequerimiento();">
					<form:option value="-1">--Seleccione--</form:option>
					<c:if test="${control.listaRequerimiento!=null}">
						<form:options itemValue="codSecuencial" itemLabel="descripcion" 
							items="${control.listaRequerimiento}" />
					</c:if>
				</form:select>
			</td>
			<td>Tipo Servicio :</td>
			<td>
				<form:select path="cboTipoServicio" id="cboServicio" cssStyle="width:80%" cssClass="cajatexto_o">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaServicio!=null}">
						<form:options itemValue="codSecuencial" itemLabel="descripcion" 
							items="${control.listaServicio}" />
					</c:if>
				</form:select>
			</td>
		</tr>
		<tr id="trServicio02">
			<td>&nbsp;&nbsp;&nbsp;Nombre del Servicio :</td>
			<td colspan="3">
				<form:input path="nombreServicio" id="txtNombreServicio"
					cssClass="cajatexto_o" cssStyle="width: 80%"/>
			</td>
		</tr>
		<tr id="trServicio03">
			<td>&nbsp;&nbsp;&nbsp;Fec. Atenci�n :</td>
			<td>
				<form:input path="fechaAtencion" id="txtFechaAtencion"
					cssClass="cajatexto_o" cssStyle="width: 70px" readonly="true"
					onkeypress="fc_ValidaFecha('txtFechaAtencion');"
					onblur="fc_FechaOnblur('txtFechaAtencion');"/>
			</td>
			<td>Usuario Asignado :</td>
			<td>
				<form:input path="usuAsignado" id="usuAsignado"
					cssClass="cajatexto_o" cssStyle="width: 170px" readonly="true"/>
			</td>
		</tr>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;Tipo inversi�n 
			<input type="checkbox" id="chkIndInversion" />
			</td>
			<td></td>
			<td>
				<c:if test="${control.codTipoRequerimiento=='0001'}">
				<c:if test="${control.indEmpLogistica=='1'}">Para Stock
				<input type="checkbox" id="chkIndParaStock" />
				</c:if>
				</c:if>
			</td>
			<td></td>
		</tr>
	</table>
	<!-- cuerpo de la pagina -->
	<table cellpadding="0" cellspacing="0" id="tablaBandejaSolicitud" 
		style="width:97%;margin-top:6px;margin-left:9px" >
		<tr>
			<td>
				<div style="overflow: auto; height: 220px">
				<table cellpadding="0" cellspacing="1" style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandeja">
					<tr>
						<td class="grilla" width="8%">C�digo</td>
						<td class="grilla" width="32%">Producto</td>
						<td class="grilla" width="8%">Uni. Med.</td>
						<td class="grilla" width="10%">Cantidad</td>
						<td class="grilla" width="5%">Precio<br/>Unitario</td>
						<td class="grilla" width="10%">Precio<br/>Total</td>
						<td class="grilla" width="10%">Tipo de Gasto</td>
						<td class="grilla" width="10%">Fecha Entrega</td>
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaSolicitudDetalle}"  >
					<tr class="texto" style="cursor: pointer;" ondblclick="fc_MostrarDetalleRegistro('<c:out value="${loop.index}"/>','<c:out value="${lista.codigo}" />');">
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.codigoBien}" />
							<input type="hidden"
								id='valHid<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.descripcion}" />'>
							<input type="hidden"
								id='txhCodigo<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codigo}" />'>
						</td>
						<td align="left" class="tablagrilla" style="width: 32%">
							<input type="hidden"
								id='txhCodigoBien<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codigoBien}" />'>
							<c:out value="${lista.producto}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.unidadMedida}" />
						</td>
<!-- ALQD,28/08/08. VALIDAMOS CANTIDAD DE DECIMALES A INGRESAR POR EL TIPO DE UNIDAD
								onblur="fc_ValidaDecimalOnBlur(this.id,'10','2');"
 -->
						<td align="center" class="tablagrilla" style="width: 10%">
							<input type=text value='<c:out value="${lista.cantidad}" />'
								id='txtCantidad<c:out value="${loop.index}"/>'
								maxlength="10"
								onkeypress="fc_ValidaNumeroDecimal();"
								onblur="fc_validaDecimalOnBlur('<c:out value="${loop.index}" />',
								'<c:out value="${lista.cantDecimales}" />');"
								class="cajatexto" size="12" style="text-align: right;">
						</td>
						<td align="right" class="tablagrilla" style="width: 5%">
							<c:out value="${lista.precioUnitario}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.total}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<select id='cboGasto${loop.index}' style='width:100%' class='cajatexto_o'>
								<option value="-1">--Seleccione--</option>
								<c:if test="${control.listaGasto!=null}">
									<c:forEach var="gasto" items="${control.listaGasto}"  >
										<option value="${gasto.codSecuencial}"
										<c:if test="${lista.tipoGasto==gasto.codSecuencial}">
											<c:out value="selected" />
										</c:if>>
											<c:out value="${gasto.descripcion}" />
										</option>
									</c:forEach>
								</c:if>
							</select>
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<input id="txhFecha<c:out value="${loop.index}"/>"
								value='<c:out value="${lista.fechaEntrega}" />'
								maxlength="10"
								onkeypress="fc_ValidaFecha('txhFecha<c:out value="${loop.index}"/>');"
								onblur="fc_FechaOnblur('txhFecha<c:out value="${loop.index}"/>');"
								class="cajatexto_o" size="10" />
						</td>
					</tr>
					</c:forEach>
				</table>
				</div>
			</td>
		</tr>
	</table>
	<div id="activo" name="activo" style="display:none">
	<table cellpadding="0" cellspacing="0" style="width:97%;margin-top:6px;margin-left:9px" border="0" bordercolor="red">
		<tr valign="top">
			<td width="50%" align="left">
				<table cellpadding="0" cellspacing="0"  >
					 <tr>
					 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
					 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="285px" class="opc_combo">Descripci�n</td>
					 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
					 </tr>
				 </table>
				<table cellpadding="0" cellspacing="0" class="tabla" style="width:99%; margin-top: 3px">
					<tr>
						<td>
							<form:textarea path="descripcion" id='txtDescripcion' cssClass="cajatexto_o" 
								cssStyle="width:98%;height:75px;"/>
						</td>
					</tr>
				</table>
			</td>
			<td width="50%">
				<table cellpadding="0" cellspacing="0"  >
					 <tr>
					 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
					 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="300px" class="opc_combo">Documentos Relacionados</td>
					 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
					 </tr>
				 </table>
				<table cellpadding="0" cellspacing="0" style="width:100%; margin-top: 4px" >
					<tr>
						<td>
							<iframe id="iFrameGrilla" name="iFrameGrilla" frameborder="0" height="80px" width="100%"></iframe>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	<br>
	<table align="center" id="tablaBotonesBien" style="display:none">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar01','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar01" style="cursor:pointer;" onclick="fc_Regresar();">
				</a>
			<td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAprobar01','','${ctx}/images/botones/aprobar2.jpg',1)">
					<img alt="Aprobar" src="${ctx}/images/botones/aprobar1.jpg" id="imgAprobar01" style="cursor:pointer;" onclick="fc_AprobarBien();">
				</a>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRechazar01','','${ctx}/images/botones/rechazar2.jpg',1)">
					<img alt="Rechazar" src="${ctx}/images/botones/rechazar1.jpg" id="imgRechazar01" style="cursor:pointer;" onclick="fc_Rechazar();">
				</a>
			</td>
		</tr>
	</table>
	<table align="center" id="tablaBotonesServicioGeneral" style="display:none">
		<tr>
			<td align=center valign="top">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar02','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar02" style="cursor:pointer;" onclick="fc_Regresar();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAprobar02','','${ctx}/images/botones/aprobar2.jpg',1)">
				<img alt="Aprobar" src="${ctx}/images/botones/aprobar1.jpg" id="imgAprobar02" style="cursor:pointer;" onclick="fc_AprobarServicio();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRechazar02','','${ctx}/images/botones/rechazar2.jpg',1)">
				<img alt="Rechazar" src="${ctx}/images/botones/rechazar1.jpg" id="imgRechazar02" style="cursor:pointer;" onclick="fc_Rechazar();"></a></td>
		</tr>
	</table>
	<table cellpadding="2" cellspacing="2" align="center" id="tablaBotonesServicioMantenimiento" style="display:none"
		border="0" bordercolor="red">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar03','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar03" style="cursor:pointer;" onclick="fc_Regresar();">
				</a>
			</td>
			<td>
			    <c:if test="${control.banderaEnviar=='0'}">
			      <c:if test="${control.jefeLog=='1'}">
			   		<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnEnviarAprobacion','','${ctx}/images/botones/enviar2.jpg',1)">
						<img alt="Enviar a Aprobaci�n" src="${ctx}/images/botones/enviar1.jpg" id="btnEnviarAprobacion" 
					 		 style="cursor:pointer;" onclick="fc_EnviarAprobacionMantenimiento();">
					</a>
				  </c:if>
			    </c:if>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAprobar03','','${ctx}/images/botones/aprobar2.jpg',1)">
					<img alt="Aprobar" src="${ctx}/images/botones/aprobar1.jpg" id="imgAprobar03" style="cursor:pointer;" onclick="fc_AprobarServicioMantenimiento();">
				</a>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRechazar03','','${ctx}/images/botones/rechazar2.jpg',1)">
					<img alt="Rechazar" src="${ctx}/images/botones/rechazar1.jpg" id="imgRechazar03" style="cursor:pointer;" onclick="fc_Rechazar();">
				</a>
			</td>
			<td>
			    <c:if test="${control.jefeLog=='1'}">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAsignarRes','','${ctx}/images/botones/asignarres2.jpg',1)">
					<img alt="Asignar Responsable" src="${ctx}/images/botones/asignarres1.jpg" id="imgAsignarRes" style="cursor:pointer;" onclick="fc_AsignarResponsable();">
				</a>
				</c:if>
			</td>
		</tr>
	</table>
</form:form>
</body>