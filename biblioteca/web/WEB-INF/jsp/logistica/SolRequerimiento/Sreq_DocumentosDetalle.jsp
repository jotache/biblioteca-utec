<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
		function onLoad(){
			strMsg = document.getElementById("txhMsg").value;

			if ( strMsg == "OK") alert(mstrElimino);
			else if ( strMsg == "ERROR") alert(mstrNoElimino);
			
			document.getElementById("txhMsg").value = "";
			
			if(document.getElementById("txhCondicion").value=="0"){
				document.getElementById("tdBotones").style.display="none";
			}
		}
		function fc_Ocultar(){
			alert("");
		}
		function fc_Agregar()
		{
			var url = "${ctx}/logistica/bandejaSreqUsuarioAdjuntarDocumentos.html";
			url = url + "?txhCodRequerimiento=" + document.getElementById("txhCodRequerimiento").value;
			url = url + "&txhCodDetRequerimiento=" + document.getElementById("txhCodDetRequerimiento").value;
			
			Fc_Popup(url,500,150,"");
		}
		
		function fc_Elimina()
		{

			if (document.getElementById("txhCodAdjunto").value == "" )
			{
				alert(mstrSeleccione);
				return;
			}
			if (!confirm(mstrSeguroEliminar1)) return;
			
			document.getElementById("txhAccion").value ="ELIMINAR";
			document.getElementById("frmMain").submit();
		}
		
		function fc_Refrescar()
		{
			document.getElementById("txhAccion").value ="";
			document.getElementById("txhMsg").value ="";
			document.getElementById("frmMain").submit();
		}
		
		function fc_seleccionarRegistro(codDetalle)
		{
			document.getElementById("txhCodAdjunto").value = codDetalle;
		}
		function fc_VerDocumento(strNomDoc)
		{
			if ( strNomDoc != "")
			{
<%-- 				strRuta = "<%=(( request.getAttribute("msgRutaServer")==null)?"": request.getAttribute("msgRutaServer"))%>"; --%>
				//if ( strRuta != "")
				//{
					//window.open(strRuta + strNomDoc);
					
					document.getElementById("txhAccion").value = "OPEN_DOCUMENTO";
					document.getElementById("txhDocumentDownload").value = strNomDoc;
					document.getElementById("frmMain").submit();
				//}
			} 
			else
			{
				alert(mstrNoExisteDocAdjunto);
			}
		}
	</script>
</head>

<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/SreqDocumentosDetalle.html" >
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codUsuario" id="txhUsuario"/>
	<form:hidden path="codRequerimiento" id="txhCodRequerimiento"/>
	<form:hidden path="codDetRequerimiento" id="txhCodDetRequerimiento"/>
	<form:hidden path="codDocAdjunto" id="txhCodAdjunto"/>
	<form:hidden path="documentDownload" id="txhDocumentDownload"/>
	
	<form:hidden path="condicion" id="txhCondicion"/>
	
	<table cellpadding="0" cellspacing="0" style="width:100%" border="0" bordercolor="blue">
		<tr>
			<td>
				<display:table name="sessionScope.listaDocumentos" cellpadding="0" cellspacing="1"
				decorator="com.tecsup.SGA.bean.DocumentosDetalleDecorator"  requestURI=""
				style="border: 1px solid #048BBA;width:100%;">
					<display:column property="rbtSelArchivo" title="Sel"
						headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
					<display:column property="dscTipoAdjunto" title="Tipo Adjunto" headerClass="grilla"
						class="tablagrilla" style="text-align:left;width:90%"/>
					<display:column property="imgVerArchivo" title="Ver" headerClass="grilla"
						class="tablagrilla" style="text-align:center;width:5%"/>
					
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
			</td>
			<td width="30px" valign="middle" align="right" id="tdBotones">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/agregar2.jpg',1)">
				<img src="${ctx}/images/iconos/agregar1.jpg" style="cursor:pointer;" id="imgagregar" onclick="fc_Agregar();"
				alt="Agregar Solicitud"></a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitar','','${ctx}/images/iconos/quitar2.jpg',1)">
				<img src="${ctx}/images/iconos/quitar1.jpg" style="cursor:pointer;" id="imgquitar" onclick="fc_Elimina();"
				alt="Eliminar Solicitud"></a>
			</td>
		</tr>
	</table>
</form:form>