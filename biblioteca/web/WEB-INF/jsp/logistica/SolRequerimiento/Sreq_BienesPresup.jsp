<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">	
	var seleccionTextArea="";


	 function functionpermiteNumerosPunto() {			
			var valido = "0123456789.";                     
		      if((window.event.keyCode == 209) || (window.event.keyCode == 241)){            
		            var intEncontrado = 0;
		            //convierte la � en �
		            window.event.keyCode = 209;
		      }
		      else{          
		            var ch_Caracter = String.fromCharCode(window.event.keyCode).toUpperCase();
		            var intEncontrado = valido.indexOf(ch_Caracter);            
		            if (intEncontrado == -1)
		            {           
		                 window.event.keyCode = 0;
		            }
		            else
		            {
		                 window.event.keyCode = ch_Caracter.charCodeAt();
		            }
		      }
		}
			
	//ALQD,19/08/08. TECLA ENTER PERMITE BAJAR
	function fc_permiteNumerosValEnter(posObjEvt){

	if(window.event.keyCode ==13 ){
		var nom = "txtCantidad"+posObjEvt;	
		
			if(Number(posObjEvt)<Number(document.getElementById("txhTamListaSolicitudDetalle").value))
				document.getElementById(""+nom).focus();		
			return true;
		}
	functionpermiteNumerosPunto();
	}
	//VALIDA # DE DECIMALES PERMITIDOS EN LA CANTIDAD
	function fc_validaDecimalOnBlur(posicion, numDecimales){
		var nom = "txtCantidad"+posicion;
		var numerosold = document.getElementById(""+nom).value;
		var numero = parseFloat(document.getElementById(""+nom).value);

		var aux = parseFloat(numerosold);
		//alert(aux.toString());
		var numeros = aux.toString();
		//alert(parseFloat( numeros.toString().indexOf(".") ));
		
		if (numeros.indexOf(".")>=0 ){
			var ind = numeros.indexOf(".");									
			var numDec = parseInt(document.getElementById(""+nom).value);
			var parteDecimal = numeros.substring(ind+1);

			/*alert('ctrl:'+posicion+'\n'+
					'numDecimales permitidos:'+numDecimales+'\n'+
					'posici�n punto:'+ind+'\n'+
					'numero:'+numero+'\n'+
					'numDec:'+numDec+'\n'+
					'parteDecimal:'+parteDecimal);*/
			
			if (numDecimales=='0' && (parteDecimal.length>0 || parteDecimal=='') ){
				alert("Este campo no admite decimales.");
				document.getElementById(""+nom).focus();				
				return false;
			}			
					
			if(parteDecimal.length>numDecimales){
				alert("No puede colocar m�s de "+numDecimales+" decimal(es) a la cantidad para este producto.");
				document.getElementById(""+nom).focus();
			}			
		}		

	}
	
	function onLoad(){	 
	
		if(document.getElementById("txhIndTitulo").value=="0")
		{   a1.style.display="";
		    a2.style.display="none";
		   
		}
		else{ if(document.getElementById("txhIndTitulo").value=="1")
			{   a1.style.display="none";
		        a2.style.display="";
		       
			}
		}
		
		
		fc_CambiaRequerimiento();
		
		//MODIFICAR o VISUALIZAR
		if(document.getElementById("txhCodRequerimiento").value!=""){			
			fc_CambiaRequerimiento();
			document.getElementById("cboRequerimiento").disabled=true;						
			if(document.getElementById("cboRequerimiento").value=="0001"){
				fc_AsignaTipoBien();
			}
			/*
			document.getElementById("rbtConsumible").disabled = true;
			document.getElementById("rbtActivo").disabled = true;			
			document.getElementById("chkIndInversion").disabled = true;
			if(document.getElementById("txhIndEmpLogistica").value=="1"){
				document.getElementById("chkIndParaStock").disabled=true;
			}
			if(document.getElementById("txhEstado").value!="0001"){
				fc_DesabilitarTodo();
			}
			*/
		}   			
		//***************************************		
		objMsg=document.getElementById("txhMsg");
		
		if ( objMsg.value == "OK_GRABAR" ){		
			alert(mstrGrabar);
		}		
		else if ( objMsg.value == "ERROR_GRABAR" ){
			alert(mstrProblemaGrabar);
		}
		else if ( objMsg.value == "OK" ){		
			alert(mstrExitoEnviarSolicitud);
			fc_DesabilitarTodo();
			fc_Regresar();
		}		
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
		}
		else if ( objMsg.value == "ERROR_PROCESO" ){
			alert(mstrErrorProcesarReq);		
		}
		else if ( objMsg.value == "ERROR_INGRESO" ){
			alert(mstrLlenarCamposDetalle);		
		}
		else if ( objMsg.value == "ERROR_NO_RESP" ){
			alert(mstrNoEnviarByNoResponsable);		
		}
		else if ( objMsg.value == "ALMACEN_CERRADO" ){
			alert(mstrEstadoLogistica);		
		}
		else if ( objMsg.value == "OK_ELIMINAR" ){
			alert(mstrElimino);	
			fc_Regresar();	
		}
		else if ( objMsg.value == "ERROR_ELIMINAR" ){
			alert(mstrNoElimino);		
		}
		else if ( objMsg.value == "NO_SE_ELIMINA" ){
			alert(mstrNoElim);		
		}
		
		//document.getElementById("txhOperacion").value="";		
		document.getElementById("txhMsg").value="";
		//***************************************		
	}
	function fc_DesabilitarTodo(){		
		seleccion=document.getElementById("cboRequerimiento").value;
				
		switch(seleccion){
			//bien
			case '0001':
				fc_DesabilitarCamposBien();
				break;
			//servicio
			case '0002':
				fc_DesabilitarCamposServicio();
				break;
		}
		fc_DesabilitaBotones();
		fc_OcultarBotones();
	}
	function fc_DesabilitarCamposBien(){
		/*
		document.getElementById("cboCentroCosto").disabled=true;
		document.getElementById("rbtActivo").disabled=true;
		document.getElementById("rbtConsumible").disabled=true;
		document.getElementById("chkIndInversion").disabled=true;
		if(document.getElementById("txhIndEmpLogistica").value=="1"){
			document.getElementById("chkIndParaStock").disabled=true;
		}
		*/
		
		/*tabla=document.getElementById("tablaBandeja");
		hijosTr=tabla.getElementsByTagName('tr');		
		numeroHijoTabla = hijosTr.length-1;*/
		
		numeroHijoTabla=document.getElementById("txhTamListaSolicitudDetalle").value;
		
		for(var i=0; i<numeroHijoTabla;i++){
			document.getElementById("txtCantidad"+i).readOnly=true;
			document.getElementById("cboGasto"+i).disabled=true;
			document.getElementById("txhFecha"+i).readOnly=true;
			document.getElementById("txtDescripcion").readOnly=true;					
		}
	}
	function fc_DesabilitarCamposServicio(){
		document.getElementById("cboCentroCosto").disabled=true;
		document.getElementById("txtNombreServicio").readOnly=true;
		document.getElementById("cboServicio").disabled=true;
		document.getElementById("txtFechaAtencion").readOnly=true;
		document.getElementById("txtDescripcion").readOnly=true;
	}
	function fc_DesabilitaBotones(){
		document.getElementById("botonEnviar").disabled=true;
		document.getElementById("botonGrabar").disabled=true;
		document.getElementById("imgFechaAtencion").disabled=true;
		//desabilitamos los botones de fecha de entrega de la bandeja en caso sea tipo bien
		numeroHijoTabla=document.getElementById("txhTamListaSolicitudDetalle").value;
		for(var i=0; i<numeroHijoTabla;i++){
			document.getElementById("imgFec"+i).disabled=true;
		}
	}
	function fc_Refrescar(){
		document.getElementById("txhOperacion").value="REFRESCAR";
		document.getElementById("frmMain").submit();
	}
	function fc_AsignaTipoBien(){
		/*		
		if(document.getElementById("txhCodSubTipoRequerimiento").value==document.getElementById("txhCodTipoBienActivo").value){
			document.getElementById("rbtActivo").checked=true;		
			document.getElementById("chkIndInversion").style.display="none";
			document.getElementById("lblIndInversion").style.display="none";
			if(document.getElementById("txhIndEmpLogistica").value=="1"){
				document.getElementById("chkIndParaStock").style.display="none";
				document.getElementById("lblIndParaStock").style.display="none";
			}												
		}
		else{			
			//document.getElementById("rbtConsumible").checked=true;			
		}
		
		//MODIFICADO RNAPA 19/08/2008		
		if(document.getElementById("txhIndInversion").value=="1"){
			document.getElementById("chkIndInversion").checked=true;
		}
		
		if(document.getElementById("txhIndEmpLogistica").value=="1"){
			if(document.getElementById("txhIndParaStock").value=="1"){
				document.getElementById("chkIndParaStock").checked=true;
			}
		}
		*/
	}	
	
	function fc_CambiaRequerimiento(){		
		seleccion=document.getElementById("cboRequerimiento").value;		
		switch(seleccion){
			case '0001':
				tdTipoBien01.style.display="";
				tdTipoBien02.style.display="";
				tdTipoServicio01.style.display="none";
				tdTipoServicio02.style.display="none";
				trServicio01.style.display="none";
				trServicio02.style.display="none";
				document.getElementById("cboRequerimiento").value='0001';
				tablaBandejaSolicitud.style.display="";
				activo.style.display="none";
				document.getElementById("txtDescripcion").className="cajatexto";
				document.getElementById("txhIndTitulo").value="0";
				  a1.style.display="";
		    	  a2.style.display="none";
				
				break;
			case '0002':
				tdTipoBien01.style.display="none";
				tdTipoBien02.style.display="none";
				tdTipoServicio01.style.display="";
				tdTipoServicio02.style.display="";				
				trServicio01.style.display="";
				trServicio02.style.display="";								
				document.getElementById("cboRequerimiento").value='0002';
				tablaBandejaSolicitud.style.display="none";
				activo.style.display="";
				document.getElementById("txhIndTitulo").value="1"
				a1.style.display="none";
		    	a2.style.display="";
				//********************************************************************				
				codRequerimiento=document.getElementById("txhCodRequerimiento").value;
				codRequerimintoDetalle=document.getElementById("txhCodReqDetalleServicio").value;
				
				if(codRequerimiento!="" && codRequerimiento!=null){				
					if(document.getElementById("txhEstado").value!="0001"){
						condicion="0";						
					}
					else{
						condicion="1";				
					}				
					document.getElementById("iFrameGrilla").src="${ctx}/logistica/SreqDocumentosDetalle.html?txhCodRequerimiento="+codRequerimiento+
						"&txhCodDetRequerimiento="+codRequerimintoDetalle+"&txhCondicion="+condicion;
				}
				document.getElementById("txtDescripcion").className="cajatexto_o";	
				document.getElementById("txhIndTitulo").value="1"			
				break;
		}
	}
	function fc_SeleccionarRegistro(codProducto,seleccion){		
		document.getElementById("txhSeleccion").value=codProducto;
		document.getElementById("txhCodDetRequerimiento").value=codProducto;
	}
	function fc_MostrarDetalleRegistro(seleccion, codDetRequerimiento){		
		activo.style.display = 'none';
		
		seleccionTextArea=seleccion;
		//if(document.getElementById("rbtActivo").checked==true){			
			activo.style.display = '';			
			document.getElementById("txtDescripcion").value=document.getElementById('valHid'+seleccionTextArea).value;				
		    document.getElementById("txtDescripcion").focus();
		//}
		codRequeriminto=document.getElementById("txhCodRequerimiento").value;
		if(document.getElementById("txhEstado").value!="0001"){
			condicion=0;	
		}
		else{
			condicion=1;
		}
		//ALQD,16/09/08. SOLO SE MOSTRARA EL CAMPO DESCRIPCION PERO NO ADJUNTOS
		if(document.getElementById("rbtActivo").checked==true){			
		document.getElementById("iFrameGrilla").src="${ctx}/logistica/SreqDocumentosDetalle.html?txhCodRequerimiento="+codRequeriminto+
			"&txhCodDetRequerimiento="+codDetRequerimiento+"&txhCondicion="+condicion;
		}
	}
	function fc_AsignarDescripcion(){		
		if(document.getElementById("cboRequerimiento").value=='0001'){
			document.getElementById('valHid'+seleccionTextArea).value=document.getElementById("txtDescripcion").value;
			//seleccionTextArea="";
		}		
	}
	function fc_Enviar(){
		form = document.forms[0];
		if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value){
			if(document.getElementById("cboCentroCosto").value=="-1"){
				alert(mstrSelCeco);
				document.getElementById("cboCentroCosto").focus();
				return;
			}
			if(document.getElementById("cboRequerimiento").value=="-1"){
				alert(mstrSelTipoReq);
				document.getElementById("cboRequerimiento").focus();
				return;
			}			
			document.getElementById("txhCodTipoRequerimiento").value=document.getElementById("cboRequerimiento").value;
			document.getElementById("txhCodTipoCentroCosto").value=document.getElementById("cboCentroCosto").value;

			if(document.getElementById("cboRequerimiento").value=='0001') //Tipo Req = BIEN
				document.getElementById("codSubTipoRequerimiento").value="";
			else
				document.getElementById("codSubTipoRequerimiento").value=document.getElementById("cboServicio").value;
			
			/*
			if(document.getElementById("cboRequerimiento").value=='0001'){
				if(document.getElementById("rbtConsumible").checked==true){
					document.getElementById("codSubTipoRequerimiento").value=document.getElementById("txhCodTipoBienConsumible").value;
				}
				else{
					document.getElementById("codSubTipoRequerimiento").value=document.getElementById("txhCodTipoBienActivo").value;
				}
			}
			else{
				document.getElementById("codSubTipoRequerimiento").value=document.getElementById("cboServicio").value;
			}
			*/
			
			//**********************************************************
			seleccion=document.getElementById("cboRequerimiento").value;		
			switch(seleccion){
				//bien
				case '0001':
					if(fc_ValidaFechaMayorActualBien()){
						return false;
					}								
					fc_DatosBandejaSolReqBien();					
					//CCORDOVA 13/06/2008
					numRegistros=document.getElementById("txhTamListaSolicitudDetalle").value;
					if(numRegistros == 0){
						alert(mstrIngresaRegEnviarCot);
						return;
					}
					if(confirm(mstrSegEnviarAprobacion)){
						form.botonEnviar.disabled=true;
						document.getElementById("txhOperacion").value="ENVIAR";
						document.getElementById("frmMain").submit();
						
					}else
						form.botonEnviar.disabled=false;
					break;
				//servicio
				case '0002':
					tipoServicio = document.getElementById("cboServicio").value;
					if(tipoServicio == ""){
						alert(mstrSelTipoServicio);
						document.getElementById("cboServicio").focus();
						return;
					}
					nombreServicio = document.getElementById("txtNombreServicio").value;
					if(nombreServicio == "" || fc_Trim(nombreServicio) == ""){
						alert(mstrIngreNombreServ);
						document.getElementById("txtNombreServicio").focus();
						return;
					}				
					fechaAtencion = document.getElementById("txtFechaAtencion").value;
					if(fechaAtencion == "" || fc_Trim(fechaAtencion) == ""){
						alert(mstrIngreFechaAten);
						document.getElementById("txtFechaAtencion").focus();
						return;
					}
					descripcion = document.getElementById("txtDescripcion").value;
					if(descripcion == "" || fc_Trim(descripcion) == ""){
						alert(mstrIngreDescripServ);
						document.getElementById("txtDescripcion").focus();
						return;
					}
					if(fc_ValidaFechaMayorActualServicio()){
						return false;
					}				
					//CCORDOVA 13/06/2008
					if(confirm(mstrSegEnviarAprobacion)){
						form.botonEnviar.disabled=true;
						document.getElementById("txhOperacion").value="ENVIAR";
						document.getElementById("frmMain").submit();						
					}else
						form.botonEnviar.disabled=false;
					break;
			}
			//**********************************************************
			/*alert("centro costo="+document.getElementById("txhCodTipoCentroCosto").value+
				"\ntipo requerimiento="+document.getElementById("txhCodTipoRequerimiento").value+
				"\nsub tipo requerimiento="+document.getElementById("codSubTipoRequerimiento").value);*/
		  }
		  else alert(mstrEstadoLogistica);
	
	 
	}	
	function fc_Grabar(){
				
		form = document.forms[0];		
		if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value){
			if(document.getElementById("cboCentroCosto").value=="-1"){
				alert(mstrSelCeco);
				//document.getElementById("cboCentroCosto").focus();
				document.getElementById("icoBuscar").focus(); 
				return;
			}
			if(document.getElementById("cboRequerimiento").value=="-1"){
				alert(mstrSelTipoReq);
				document.getElementById("cboRequerimiento").focus();
				return;
			}
			
			document.getElementById("txhCodTipoRequerimiento").value=document.getElementById("cboRequerimiento").value;
			document.getElementById("txhCodTipoCentroCosto").value=document.getElementById("cboCentroCosto").value;
			//POR DEFECTO EL INDICADOR ES 0 PARA LOS DOS CASOS
			//BIEN O SERVICIO
			document.getElementById("txhIndInversion").value="0";
			//ALQD,31/10/08. SOLO PARA BIEN
			if (tdTipoBien02.style.display=""){
				//document.getElementById("chkIndParaStock").value="0";
			}

			document.getElementById("codSubTipoRequerimiento").value=""; //En Bienes = NULL.
			
			
			if(document.getElementById("cboRequerimiento").value=='0001'){
				/*
				if(document.getElementById("rbtConsumible").checked==true){
					document.getElementById("codSubTipoRequerimiento").value=document.getElementById("txhCodTipoBienConsumible").value;
					//MODIFICADO RNAPA 19/08/2008
					if(document.getElementById("chkIndInversion").checked==true){
						document.getElementById("txhIndInversion").value="1";
					}
					//ALQD,31/10/08.SETEANDO EL NUEVO CONTROL
					if(document.getElementById("txhIndEmpLogistica").value=="1"){
						if(document.getElementById("chkIndParaStock").checked==true){
							document.getElementById("txhIndParaStock").value="1";
						}
					}
					else {
					document.getElementById("txhIndParaStock").value="0";
					}
				}
				
				else{
					document.getElementById("codSubTipoRequerimiento").value=document.getElementById("txhCodTipoBienActivo").value;
				}
				*/
			}
			else{
				document.getElementById("codSubTipoRequerimiento").value=document.getElementById("cboServicio").value;
			}
			//**********************************************************
			seleccion=document.getElementById("cboRequerimiento").value;		
			switch(seleccion){
				//bien
				case '0001':
					if(fc_ValidaFechaMayorActualBien()){
						return false;
					}								
					fc_DatosBandejaSolReqBien();				
					if(confirm(mstrConfiGrabarDatos)){						
						form.botonGrabar.disabled=true;
						document.getElementById("txhOperacion").value="GRABAR";		
						document.getElementById("frmMain").submit();						
					}else
						form.botonGrabar.disabled=false;
					break;
				//servicio
				case '0002':
					tipoServicio = document.getElementById("cboServicio").value;
					if(tipoServicio == ""){
						alert(mstrSelTipoServicio);
						document.getElementById("cboServicio").focus();
						return;
					}
					nombreServicio = document.getElementById("txtNombreServicio").value;
					if(nombreServicio == "" || fc_Trim(nombreServicio) == ""){
						alert(mstrIngreNombreServ);
						document.getElementById("txtNombreServicio").focus();
						return;
					}				
					fechaAtencion = document.getElementById("txtFechaAtencion").value;
					if(fechaAtencion == "" || fc_Trim(fechaAtencion) == ""){
						alert(mstrIngreFechaAten);
						document.getElementById("txtFechaAtencion").focus();
						return;
					}
					descripcion = document.getElementById("txtDescripcion").value;
					if(descripcion == "" || fc_Trim(descripcion) == ""){
						alert(mstrIngreDescripServ);
						document.getElementById("txtDescripcion").focus();
						return;
					}
					if(fc_ValidaFechaMayorActualServicio()){
						return false;
					}
					/*if ( document.getElementById("cboTipoGastoServicio").value == "")
					{
						alert("Debe seleccionar el tipo de gasto.");
						return false;
					}*/
					if(confirm(mstrSeguroGrabar)){
						
						form.botonGrabar.disabled=true;
						document.getElementById("txhOperacion").value="GRABAR";		
						document.getElementById("frmMain").submit();
						
					}else
						form.botonGrabar.disabled=false;
					
					break;
			}
			//**********************************************************
			/*alert("centro costo="+document.getElementById("txhCodTipoCentroCosto").value+
				"\ntipo requerimiento="+document.getElementById("txhCodTipoRequerimiento").value+
				"\nsub tipo requerimiento="+document.getElementById("codSubTipoRequerimiento").value);*/
		  }
		  else alert(mstrEstadoLogistica);				
	}
	function fc_DatosBandejaSolReqBien(){
		/*tabla=document.getElementById("tablaBandeja");
		hijosTr=tabla.getElementsByTagName('tr');		
		numeroHijoTabla = hijosTr.length-1;*/
		
		var cadenaCodigo="";
		var cadenaCodBien="";
		var cadenaCantidad="";
		var cadenaTipoGasto="";
		var cadenaFechaEntrega="";
		var cadenaDescripcion="";
		
		numeroHijoTabla=document.getElementById("txhTamListaSolicitudDetalle").value;
		
		for(var i=0; i<numeroHijoTabla;i++){
			if(i==0){
				cadenaCodigo=document.getElementById("txhCodigo"+i).value;
				cadenaCodBien=document.getElementById("txhCodigoBien"+i).value;
				if(document.getElementById("txtCantidad"+i).value==""){					
					cadenaCantidad="$";
				}
				else{
					cadenaCantidad=document.getElementById("txtCantidad"+i).value;					
				}				
				cadenaTipoGasto=document.getElementById("cboGasto"+i).value;
				if(document.getElementById("txhFecha"+i).value==""){					
					cadenaFechaEntrega="$";
				}
				else{
					cadenaFechaEntrega=document.getElementById("txhFecha"+i).value;
				}
				if(document.getElementById("valHid"+i).value==""){
					cadenaDescripcion="$";
				}
				else{
					cadenaDescripcion=document.getElementById("valHid"+i).value;
				}				
			}
			else{
				cadenaCodigo=cadenaCodigo+"|"+document.getElementById("txhCodigo"+i).value;
				cadenaCodBien=cadenaCodBien+"|"+document.getElementById("txhCodigoBien"+i).value;
				if(document.getElementById("txtCantidad"+i).value==""){					
					cadenaCantidad=cadenaCantidad+"|$";
				}
				else{
					cadenaCantidad=cadenaCantidad+"|"+document.getElementById("txtCantidad"+i).value;					
				}												
				cadenaTipoGasto=cadenaTipoGasto+"|"+document.getElementById("cboGasto"+i).value;
				if(document.getElementById("txhFecha"+i).value==""){					
					cadenaFechaEntrega=cadenaFechaEntrega+"|$";
				}
				else{
					cadenaFechaEntrega=cadenaFechaEntrega+"|"+document.getElementById("txhFecha"+i).value;
				}
				if(document.getElementById("valHid"+i).value==""){
					cadenaDescripcion=cadenaDescripcion+"|$";
				}
				else{
					cadenaDescripcion=cadenaDescripcion+"|"+document.getElementById("valHid"+i).value;
				}
			}
		}
		document.getElementById("txhCadenaCodigo").value=cadenaCodigo;
		document.getElementById("txhCadenaCodBien").value=cadenaCodBien;
		document.getElementById("txhCadenaCantidad").value=cadenaCantidad;
		document.getElementById("txhCadenaCodTipoGasto").value=cadenaTipoGasto;
		document.getElementById("txhCadenaFechaEntrega").value=cadenaFechaEntrega;
		document.getElementById("txhCadenaDescripcion").value=cadenaDescripcion;
		/*alert("cadena codigo="+cadenaCodigo+
			"\ncadena codBien="+cadenaCodBien+
			"\ncadena cantidad="+cadenaCantidad+
			"\ncadena tipogasto="+cadenaTipoGasto+
			"\ncadena fecha entrega="+cadenaFechaEntrega+
			"\ncadena descripcion="+cadenaDescripcion);*/
	}	
	function fc_DatosBandejaSolReqServicio(){		
		/*alert("nombre="+document.getElementById("txtNombreServicio").value+
			"\nfechaEntrega="+document.getElementById("txtFechaAtencion").value+
			"\ndescripcion="+document.getElementById("txtDescripcion").value);*/
	}
	function fc_ValidaFechaMayorActualBien(){		
		/*tabla=document.getElementById("tablaBandeja");
		hijosTr=tabla.getElementsByTagName('tr');		
		numeroHijoTabla = hijosTr.length-1;*/	
		
		numeroHijoTabla=document.getElementById("txhTamListaSolicitudDetalle").value;
		
		for(var i=0; i<numeroHijoTabla;i++){
			fecha=document.getElementById("txhFecha"+i).value;			
			if(fecha!=""){
				fechaActual=document.getElementById("txhFechaActual").value;									
				if (Fc_RestaFechas(fechaActual,"dd/MM/yyyy",fecha,"dd/MM/yyyy") < 0 ){
					alert(mstrFechaEntreMayorAct);
					document.getElementById("txhFecha"+i).focus();					
					return true;
				}
			}				
		}
		return false;
	}
	function fc_ValidaFechaMayorActualServicio(){
		fecha=document.getElementById("txtFechaAtencion").value;			
		if(fecha!=""){
			fechaActual=document.getElementById("txhFechaActual").value;									
			if (Fc_RestaFechas(fechaActual,"dd/MM/yyyy",fecha,"dd/MM/yyyy") < 0 ){
				alert(mstrFechaAtenMayorAct);
				document.getElementById("txtFechaAtencion").focus();					
				return true;
			}
		}
		return false;
	}
	function fc_AgregarProducto(){
		if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)		
		  {	
		     if(fc_ValidarEstado()){			
				codUsuario=document.getElementById("txhCodUsuario").value;
				codRequerimiento=document.getElementById("txhCodRequerimiento").value;
				codSede=document.getElementById("txhCodSede").value;

				//alert("TipoBienConsumible:"+document.getElementById("txhCodTipoBienConsumible").value);
				//alert("TipoBienActivo:"+document.getElementById("txhCodTipoBienActivo").value);
				
				codTipoBien = ""; //El filtro de familias es realizado en la ventana de busqueda de productos
				/*
				if ( document.getElementById("rbtConsumible").checked )
					codTipoBien = document.getElementById("txhCodTipoBienConsumible").value;
				else
					codTipoBien = document.getElementById("txhCodTipoBienActivo").value;
				*/
				Fc_Popup("${ctx}/logistica/log_Busqueda_ProductosPresup.html?txhCodUsuario="+codUsuario+
					"&txhCodRequerimiento="+codRequerimiento+
					"&txhCodSede="+codSede+
					"&txhCodTipoBien="+codTipoBien,850,470);		
			  }
			}
		  else alert(mstrEstadoLogistica);				
	}
	function fc_QuitarArchivo(){
		fc_ValidarEstado();
	}
	function fc_AdjuntarArchivo(){
		fc_ValidarEstado();
	}
	function fc_QuitarProducto(){				
	  if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)		
		 {	
			if(fc_ValidarEstado()){
				if (document.getElementById("txhSeleccion").value != ""){
					if( confirm(mstrEliminar) ){
						document.getElementById("txhSeleccion").value="";
						document.getElementById("txhOperacion").value = "QUITAR";
						document.getElementById("frmMain").submit();
					}
				}
				else{
					alert(mstrSeleccion);
					return false;
				}
			}
		}
		else alert(mstrEstadoLogistica);	
	}
	function fc_ValidarEstado(){
		if(document.getElementById("txhCodRequerimiento").value==""){
			alert(mstrGuardarDatosCabecera);
			return false;
		}
		else{
			return true;			
		}
	}
	function fc_Regresar(){
		codSede=document.getElementById("txhCodSede").value;
		if(document.getElementById("txhTipoProcedencia").value=="0") 
		window.location.href="${ctx}/logistica/bandejaSreqPresupuestado.html?txhCodSede="+codSede;
		//else if(document.getElementById("txhTipoProcedencia").value=="1")
		     //window.location.href="${ctx}/logistica/AtenRequerimientosBandejaMantenimiento.html?txhCodSede="+codSede;
	}
	function fc_OcultarBotones(){
		tdBotones.style.display="none";
	}	
	function fc_Calendario(seleccion){		
		Calendar.setup({
			inputField     :    "txhFecha"+seleccion,
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgFec"+seleccion,
			singleClick    :    true
		});
	}
	function ismaxlength(obj,tamanio){		
		var mlength=parseInt(tamanio);
		if (obj.value.length>mlength){
			obj.value=obj.value.substring(0,mlength)
		}
	}
	
	function fc_Cancelar(){
	//alert("Tam<<"+document.getElementById("txhTamListaSolicitudDetalle").value+"<<");
	
	if(document.getElementById("cboRequerimiento").value==document.getElementById("txhConsteCodBien").value) 
	 {	 if(document.getElementById("txhTamListaSolicitudDetalle").value=="0" && 
	        (document.getElementById("txhOperacion").value=="GRABAR" || document.getElementById("txhOperacion").value=="QUITAR"))   
			{ if(confirm(mstrConfSeguroContinuar))
			  {
			  	document.getElementById("txhOperacion").value = "CANCELAR";
			  	document.getElementById("frmMain").submit();
			  }
			}
		  else  fc_Regresar();
		  	    /*if(document.getElementById("txhTamListaSolicitudDetalle").value!="0")  
		             alert("Tama�o > 1");
		        else if(document.getElementById("txhOperacion").value!="REFRESCAR") 
		             alert("Operacion: "+document.getElementById("txhOperacion").value);*/
	 }
	 else fc_Regresar();
	}
	
	
	/*  <c:if test='${control.indTitulo=="0"}'>
					Registro de Requerimiento de Bien		 		
		</c:if>
				
		<c:if test='${control.indTitulo=="1"}'>
					Registro de Requerimiento de Servicio
		</c:if>
	*/
	function fc_TipoBien(param){
		activo.style.display = 'none';
		if (param == 'A'){
				activo.style.display = '';
		}
		document.getElementById("chkIndInversion").style.display="";
		document.getElementById("lblIndInversion").style.display="";
//ALQD,31/10/08. MOSTRANDO EL NUEVO CONTROL 
		if(document.getElementById("txhIndEmpLogistica").value=="1"){
			document.getElementById("chkIndParaStock").style.display="";
			document.getElementById("lblIndParaStock").style.display="";
		}
	}
	//CCORDOVA 14/06/2008
	function Fc_BuscarCeco()
	{
		Fc_Popup("${ctx}/logistica/log_Buscar_CentroCosto.html?txhNomControl=cboCentroCosto"+
			"&txhCodSede=" + document.getElementById("txhCodSede").value 
			,450,400);
	}
	function fc_TipoActivo(){
		//document.getElementById("divIndInversion").style.display=""
		document.getElementById("chkIndInversion").style.display="none";
		document.getElementById("lblIndInversion").style.display="none";
//ALQD,31/10/08. OCULTANDO EL NUEVO CONTROL 
		if(document.getElementById("txhIndEmpLogistica").value=="1"){
			document.getElementById("chkIndParaStock").style.display="none";
			document.getElementById("lblIndParaStock").style.display="none";
		}
	}

	function fc_seleccionarContenido(objeto){
		var num=document.getElementById(""+objeto).select();
	}
	function fc_llenarValor(objeto, posicion){
		var numeroHijoTabla=document.getElementById("txhTamListaSolicitudDetalle").value;
		var nomOld = objeto+(posicion-1);
		for(var i=posicion; i<numeroHijoTabla;++i){
			var nomNew = objeto+i;
			document.getElementById(nomNew).value=document.getElementById(nomOld).value;
		}
	}
	function fc_Exportar(){
		var url="?tCodRep=0007" +  
			"&tCodSede=" + fc_Trim(document.getElementById("txhcodSede").value) +
			"&tCodTipoReq=" + fc_Trim(document.getElementById("txhCodTipoRequerimiento").value) +
			"&tCodTipoBien=" + fc_Trim(document.getElementById("txhCodSubTipoRequerimiento").value) +  
			"&tCodTipoServicio=" + fc_Trim(document.getElementById("txhCodSubTipoRequerimiento").value) + 
			"&tCodUsu=" + fc_Trim(document.getElementById("txhCodUsuario").value) + 
			"&tCodEstadoReq=" + "-1" +
			"&tFecIni=" + "" +
			"&tFecFin=" + "" +
			"&tUSol=" + fc_Trim(document.getElementById("txhCodRequerimiento").value) ;
		url="${ctx}/logistica/reportesLogistica.html"+url;
		window.open(url,"Reportes","resizable=yes, menubar=yes");
		
	}
	</script>
</head>

<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/SreqBienesPresup.html" >
	
	<form:hidden path="estado" id="txhEstado" />
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="seleccion" id="txhSeleccion" />
	<form:hidden path="fechaActual" id="txhFechaActual" />
	<!-- estos codigos se obtienen de la BD -->
	<form:hidden path="codTipoBienConsumible" id="txhCodTipoBienConsumible" />
	<form:hidden path="codTipoBienActivo" id="txhCodTipoBienActivo" />
	<!-- para trabajar con la bd -->
	<form:hidden path="codSede" id="txhCodSede" />
	<form:hidden path="codRequerimiento" id="txhCodRequerimiento" />
	<form:hidden path="codTipoCentroCosto" id="txhCodTipoCentroCosto" />
	<form:hidden path="codTipoRequerimiento" id="txhCodTipoRequerimiento" />
	<form:hidden path="codSubTipoRequerimiento" id="txhCodSubTipoRequerimiento" />
	<!-- para actualizar los datos de la bandeja de bienes-->
	<form:hidden path="cadenaCodigo" id="txhCadenaCodigo" />
	<form:hidden path="cadenaCodBien" id="txhCadenaCodBien" />
	<form:hidden path="cadenaCantidad" id="txhCadenaCantidad" />
	<form:hidden path="cadenaCodTipoGasto" id="txhCadenaCodTipoGasto" />
	<form:hidden path="cadenaFechaEntrega" id="txhCadenaFechaEntrega" />
	<form:hidden path="cadenaDescripcion" id="txhCadenaDescripcion" />
	<!--/para trabajar con la bd -->
	<form:hidden path="codReqDetalleServicio" id="txhCodReqDetalleServicio" />
	<form:hidden path="codDetRequerimiento" id="txhCodDetRequerimiento" />	
	<form:hidden path="codRequerimientoOri" id="txhCodRequerimientoOri" />
	
	<form:hidden path="tamListaSolicitudDetalle" id="txhTamListaSolicitudDetalle" />
	
	<form:hidden path="indTitulo" id="txhIndTitulo" />
	<form:hidden path="estadoAlmacen" id="txhEstadoAlmacen"/>
	<form:hidden path="consteEstadoAlmacen" id="txhConsteEstadoAlmacen"/>
	<form:hidden path="consteCodBien" id="txhConsteCodBien"/>
	<form:hidden path="tipoProcedencia" id="txhTipoProcedencia"/>
	
	<form:hidden path="indInversion" id="txhIndInversion"/>
	<!-- ALQD,31/10/08. AGREGANDO NUEVA PROPIEDAD -->
	<form:hidden path="indParaStock" id="txhIndParaStock"/>
	<form:hidden path="indEmpLogistica" id="txhIndEmpLogistica"/>
	<!-- title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo">
		 	<font style="display: none;" id="a1">
		 		
					Registro de Requerimiento de Bien..		 		
			</font>
		 	<font style="display: none;" id="a2">
		 		    Registro de Requerimiento de Servicio..
			</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<!-- cabecera -->	
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2" border="0">
		<tr>
			<td width="15%">Nro. Requerimiento:</td>
			<td width="20%">
			
				<form:input path="nroRequerimiento" id="txtNroRequerimiento"													 
					cssClass="cajatexto_1" cssStyle="width: 50%" readonly="true"/>
			</td>
			<td width="15%">Nro Asociado:</td>
			<td width="30%">
				<form:input path="nroAsociado" id="txtNroAsociado"													 
					cssClass="cajatexto_1" cssStyle="width: 50%" readonly="true"/>
			</td>
			<td width="20%">
				<a onmouseout="MM_swapImgRestore()"
					onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/iconos/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/iconos/grabar1.jpg" id="imgAgregar"  
					style="cursor:pointer; vertical-align: middle" onclick="javascript:fc_Grabar();"></a>

	
				<c:if test="${control.codRequerimiento=='' || control.codRequerimiento==null}">
					<c:out value="<<<Grabar"></c:out>
				</c:if>
			    
			</td>
		</tr>
		<tr>
			<td width="15%">Fec. Requerimiento:</td>
			<td width="20%">			
				<form:input path="fechaRequerimiento" id="txtFechaRequerimiento"					
					cssClass="cajatexto_1" cssStyle="width: 50%" readonly="true"/>				
			</td>
			<td width="15%">Usuario Solicitante:</td>
			<td width="30%">				
				<form:input path="usuSolicitante" id="txtUsuSolicitante"
					cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>
			</td>
			<td colspan="1">
				<c:if test="${control.codRequerimiento=='' || control.codRequerimiento==null}">
					<c:out value="Grabe para a�adir materiales"></c:out>
				</c:if>
			</td>
		</tr>
		<tr>
			<td>Centro de Costo:</td>
			<td colspan="3" style="vertical-align: middle">
				<form:select path="cboTipoCentroCosto" id="cboCentroCosto" cssStyle="width:405px" 
					cssClass="combo_o" >
					<form:option value="-1">-- Seleccione --</form:option>
					<c:if test="${control.listaCentroCosto!=null}">
					<form:options itemValue="codTecsup" itemLabel="descripcion" 
						items="${control.listaCentroCosto}" />
					</c:if>
				</form:select>
				&nbsp;
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
				<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:Fc_BuscarCeco();" alt="Buscar" style="cursor:pointer;" id="icoBuscar"  ></a>
			</td>
		</tr>
		<tr>
			<td>Tipo de Requerimiento:</td>
			<td>
				<form:select path="cboTipoRequerimiento" id="cboRequerimiento" cssStyle="width:50%" cssClass="combo_o" 
					onchange="javascript:fc_CambiaRequerimiento();">
					<form:option value="-1">--Seleccione--</form:option>
					<c:if test="${control.listaRequerimiento!=null}">
						<form:options itemValue="codSecuencial" itemLabel="descripcion" 
							items="${control.listaRequerimiento}" />
					</c:if>
				</form:select>				
			</td>
			<td id="tdTipoBien01">Tipo de Bien:</td>
			<td id="tdTipoBien02" colspan="2">
				<%-- 
				<input type="radio" id="rbtConsumible" name="radioTipoBien" checked onclick="fc_TipoBien();">Consumible&nbsp;
				<input type="radio" id="rbtActivo" name="radioTipoBien" onclick="fc_TipoActivo();">Activo&nbsp;&nbsp;&nbsp;				
				<input type="checkbox" id="chkIndInversion" />&nbsp;<label id="lblIndInversion">Inversi�n</label>
				<c:if test="${control.indEmpLogistica=='1'}">
				<input type="checkbox" id="chkIndParaStock" />&nbsp;<label id="lblIndParaStock">Para Stock</label>
				</c:if>
				--%>
			</td>
			<td style="display: none" id="tdTipoServicio01">Tipo Servicio:</td>
			<td style="display: none" id="tdTipoServicio02">
				<form:select path="cboTipoServicio" id="cboServicio" cssStyle="width:80%" cssClass="cajatexto_o">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaServicio!=null}">
						<form:options itemValue="codSecuencial" itemLabel="descripcion" 
							items="${control.listaServicio}" />
					</c:if>
				</form:select>
			</td>
		</tr>		
		<tr id="trServicio02" style="display:none;">
			<td>Fec. Atenci�n:</td>
			<td>
				<form:input path="fechaAtencion" id="txtFechaAtencion"
					cssClass="cajatexto_o" cssStyle="width: 70px"
					maxlength="10" 				
					onkeypress="fc_ValidaFecha('txtFechaAtencion');"
					onblur="fc_FechaOnblur('txtFechaAtencion');"/>
					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaAtencion','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaAtencion" alt="Calendario" style='cursor:hand;' align="absmiddle">
				</a>			
			</td>
			<td style="display:none">Tipo Gasto:</td>
			<td style="display:none">
				<form:select path="codTipoGastoServicio" id="cboTipoGastoServicio" cssStyle="width:80%" cssClass="cajatexto_o">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listTipoGastoServicio!=null}">
						<form:options itemValue="codSecuencial" itemLabel="descripcion" 
							items="${control.listTipoGastoServicio}" />
					</c:if>
				</form:select>
			</td>
		</tr>
		<tr id="trServicio01" style="display:none;">
			<td>Nombre del Servicio:</td>
			<td colspan="3">
				<form:input path="nombreServicio" id="txtNombreServicio"
					cssClass="cajatexto_o" cssStyle="width: 80%" maxlength="255"/>				
			</td>
		</tr>
	</table>		
	<!-- cuerpo de la pagina -->
	<table cellpadding="0" cellspacing="0" id="tablaBandejaSolicitud" border="0" bordercolor="red"
		style="width:97%;margin-top:6px;margin-left:9px" >
		<tr>
			<td>
				<div style="overflow: auto; height: 180px;">
				<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandeja">
					<tr>
						<td class="grilla" width="4%">Sel.</td>
						<td class="grilla" width="8%">C�digo</td>
						<td class="grilla" width="28%">Producto</td>
						<td class="grilla" width="8%">Uni. Med.</td>
						<td class="grilla" width="10%">Cantidad</td>
						<td class="grilla" width="5%">Precio<br/>Unitario</td>
						<td class="grilla" width="10%">Precio<br/>Total</td>
						<td class="grilla" width="10%">Tipo de Gasto</td>
						<td class="grilla" width="12%">Fecha Entrega <br> (DD/MM/YYYY)</td>							
<!-- ALQD,29/10/08. A�ADIENDO DOS COLUMNAS MAS -->
						<c:if test="${control.estado>'0004'}"><td class="grilla" width="10%">Cant.<br>Recibida</td></c:if>
						<c:if test="${control.estado>'0004'}"><td class="grilla" width="10%">Cant<br>Devuelta</td></c:if>
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaSolicitudDetalle}"  >
					<tr class="texto" style="cursor: pointer; width: 4%; " ondblclick="fc_MostrarDetalleRegistro('<c:out value="${loop.index}"/>','<c:out value="${lista.codigo}" />');">
						<td align="center" class="tablagrilla" style="width: 4%; ">
							<input type="radio" name="producto"
								value='<c:out value="${lista.codigo}" />'
								id='val<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro(this.value,'<c:out value="${loop.index}"/>');">
							<input type="hidden" 
								id='valHid<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.descripcion}" />'>
							<input type="hidden" 
								id='txhCodigo<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codigo}" />'>								
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">							
							<c:out value="${lista.codigoBien}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 28%">
							<input type="hidden"
								id='txhCodigoBien<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codigoBien}" />'>
							<c:out value="${lista.producto}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.unidadMedida}" />
						</td>
<!-- ALQD,19/08/08. ENTER PARA BAJAR,VALIDAMOS CANT. DE DECIMALES INGRESADOS -->
						<td align="center" class="tablagrilla" style="width: 10%;">
							<input type=text value='<c:out value="${lista.cantidad}" />'
								id='txtCantidad<c:out value="${loop.index}"/>'
								maxlength="10"
								onfocus="fc_seleccionarContenido('txtCantidad'+'<c:out value="${loop.index}" />')"
								onkeypress="fc_permiteNumerosValEnter('<c:out value="${loop.index+1}" />')"
								onblur="fc_validaDecimalOnBlur('<c:out value="${loop.index}" />',
								'<c:out value="${lista.cantDecimales}" />')"
								class="cajatexto_o" size="12" style="text-align: right;">
								<!-- onkeypress="fc_ValidaNumeroDecimal();"
								onblur="fc_ValidaDecimalOnBlur(this.id,'10','2');" -->								
						</td>
						<td align="right" class="tablagrilla" style="width: 5%">
							<c:out value="${lista.precioUnitario}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 10%">						
							<c:out value="${lista.total}" />							
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<select id='cboGasto${loop.index}' style='width:100%' class='cajatexto_o'
							onChange="fc_llenarValor('cboGasto',<c:out value="${loop.index+1}" />)">								
								<option value="0000">--Seleccione--</option>
								<c:if test="${control.listaGasto!=null}">
									<c:forEach var="gasto" items="${control.listaGasto}"  >									
										<option value="${gasto.codSecuencial}"
										<c:if test="${lista.tipoGasto==gasto.codSecuencial}">
											<c:out value="selected" />
										</c:if>>										
											<c:out value="${gasto.descripcion}" />
										</option>										
									</c:forEach>
								</c:if>
							</select>							
						</td>
						<td align="center" class="tablagrilla" style="width: 12%">
							<input id="txhFecha<c:out value="${loop.index}"/>"
								value='<c:out value="${lista.fechaEntrega}" />'
								maxlength="10"
								onkeypress="fc_ValidaFecha('txhFecha<c:out value="${loop.index}"/>');"
								onblur="fc_FechaOnblur('txhFecha<c:out value="${loop.index}"/>');" 
								class="cajatexto_o" size="10" />
								
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFec<c:out value="${loop.index}"/>','','${ctx}/images/iconos/calendario2.jpg',1)">
								<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFec<c:out value="${loop.index}"/>" alt="Calendario" style='cursor:hand;' align="absmiddle" onclick="fc_Calendario('<c:out value="${loop.index}"/>');">
							</a>
						</td>							
						<c:if test="${control.estado>'0004'}">
						<td align="center" class="tablagrilla" width="10%">
							<input type=text value='<c:out value="${lista.totCantRecibida}" />'
								id='txtCantRecibida<c:out value="${loop.index}"/>'
								maxlength="10" class="cajatexto_o" size="12" style="text-align: right;">
						</td></c:if>
						<c:if test="${control.estado>'0004'}">
						<td align="center" class="tablagrilla" width="10%">
							<input type=text value='<c:out value="${lista.totCantDevuelta}" />'
								id='txtCantDevuelta<c:out value="${loop.index}"/>'
								maxlength="10" class="cajatexto_o" size="12" style="text-align: right;">
						</td></c:if>
					</tr>
					</c:forEach>
					<c:if test='${control.tamListaSolicitudDetalle=="0"}'>
						<tr class="tablagrilla">
							<td align="center" colspan="9" height="20px">No se encontraron registros
							</td>
						</tr>					 		
					</c:if>
				</table>
				</div>
			</td>
			<td width="30px" valign="middle" align="right" id="tdBotones">			
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/agregar2.jpg',1)">
				<img src="${ctx}/images/iconos/agregar1.jpg" style="cursor:pointer;" id="imgagregar" onclick="fc_AgregarProducto();" 
				alt="Agregar Producto"></a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitar','','${ctx}/images/iconos/quitar2.jpg',1)">
				<img src="${ctx}/images/iconos/quitar1.jpg" style="cursor:pointer;" id="imgquitar" onclick="fc_QuitarProducto();"
				alt="Eliminar Producto"></a>
			</td>
		</tr>
	</table>

	<div id="activo" name="activo" style="display:none">
	<table cellpadding="0" cellspacing="0" style="width:97%;margin-top:6px;margin-left:9px" border="0" bordercolor="red">
		<tr valign="top">
			<td width="50%" align="left">
				<table cellpadding="0" cellspacing="0"  >
					 <tr>
					 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
					 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="285px" class="opc_combo">Descripci�n</td>
					 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
					 </tr>
				 </table>
				<table cellpadding="0" cellspacing="0" class="tabla" style="width:99%; margin-top: 3px">
					<tr>
						<td>							
							<form:textarea path="descripcion" id='txtDescripcion' cssClass="cajatexto" 
								cssStyle="width:98%;height:75px;" onblur="fc_AsignarDescripcion();"
								onkeyup="return ismaxlength(this,'4000')"/>
						</td>	
					</tr>
				</table>				
			</td>
			<td width="50%">
				<table cellpadding="0" cellspacing="0"  >
					 <tr>
					 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
					 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="300px" class="opc_combo">Documentos Relacionados</td>
					 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
					 </tr>
				 </table>
				<table cellpadding="0" cellspacing="0" style="width:100%; margin-top: 4px" >
					<tr>
						<td>
							<iframe id="iFrameGrilla" name="iFrameGrilla" frameborder="0" height="80px" width="100%"></iframe>
						</td>							
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>	
	<br>
	<table cellpadding="2" cellspacing="2" align="center" width="100%">
		
		<tr>
			<td align=center>
<!-- ALQD,19/08/08. A�ADIENDO BOTON EXPORTAR -->
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgExportar','','${ctx}/images/botones/exportar2.jpg',1)">
				<img alt="Exportar a Excel" src="${ctx}/images/botones/exportar1.jpg" id="imgExportar" style="cursor:pointer;" onclick="fc_Exportar();"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonGrabar','','${ctx}/images/botones/grabar2.jpg',1)">				
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="botonGrabar" style="cursor:pointer" onclick="javascript:fc_Grabar();"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonEnviar','','${ctx}/images/botones/enviar2.jpg',1)">
				<img alt="Enviar" src="${ctx}/images/botones/enviar1.jpg" id="botonEnviar" style="cursor:pointer" onclick="javascript:fc_Enviar();"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonRegresar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Regresar" src="${ctx}/images/botones/cancelar1.jpg" id="botonRegresar" onclick="javascript:fc_Cancelar();" style="cursor:pointer;"></a>
			</td>
		</tr>
	</table>
</form:form>
<script type="text/javascript">

	document.getElementById("imgAgregar").style.display = "none";
	document.getElementById("botonGrabar").style.display = "none";
	document.getElementById("botonEnviar").style.display = "none";

	if (document.getElementById("txhCodRequerimiento").value == "")
		document.getElementById("imgAgregar").style.display = "inline-block";
	else {
		document.getElementById("botonGrabar").style.display = "inline-block";
		document.getElementById("botonEnviar").style.display = "inline-block";
	}

	
	Calendar.setup({
		inputField     :    "txtFechaAtencion",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFechaAtencion",
		singleClick    :    true
	});		
</script>
</body>