<%@ include file="/taglibs.jsp"%>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">	
	function onLoad(){		
		objMsg=document.getElementById("txhMsg");
		if( objMsg.value == "OK_CONFIRMAR" ){
			alert(mstrDevolucionConfirmarExito);
			fc_Regresar();
		}		
		else if( objMsg.value == "ERROR_CONFIRMAR" ){
			alert(mstrProblemaGrabar);
		}
		else if( objMsg.value == "OK_RECHAZAR" ){
				alert(mstrDevolucionConfirmarRechazo);
				fc_Regresar();
		}
		else if( objMsg.value == "ERROR_RECHAZAR" )
			 	alert(mstrProblemaGrabar);
			 else  if(objMsg.value== "ALMACEN_CERRADO")
				    alert(mstrEstadoLogistica);
				   
				
		document.getElementById("txhMsg").value="";
		document.getElementById("txtMotivoDevolucion").readOnly=true;
	}
	function fc_Regresar(){
		codSede=document.getElementById("txhCodSede").value;
		window.location.href="${ctx}/logistica/RecepcionBienesDevueltos.html?txhCodSede="+codSede+
		"&txhCodUsuario=" + document.getElementById("txhCodUsuario").value;
	}	
	function fc_Confirmar(){
	if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)		
	{
		if(confirm(mstrSeguroRealizarDevolucion)){
			document.getElementById("txhOperacion").value="CONFIRMAR";		
			document.getElementById("frmMain").submit();
		}
		}
	  else alert(mstrEstadoLogistica);
	}
	
	function fc_Rechazar(){
	if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)		
	{
		if(confirm(mstrSeguroRechazarDevolucion)){
			document.getElementById("txhOperacion").value="RECHAZAR";		
			document.getElementById("frmMain").submit();
		}
	}
	  else alert(mstrEstadoLogistica);
		
	}
	
	function fc_NumeroSerie(srtIndice){
	 Fc_Popup("${ctx}/logistica/log_Numeros_Series_Devolucion.html?txhCodUsuario=" +
		document.getElementById("txhCodUsuario").value + "&txhCodBien=" +
		document.getElementById("txhCodBien"+srtIndice).value + "&txhCodReq=" + 
		document.getElementById("txhCodRequerimiento").value +
		"&txhCodReqDet=" + document.getElementById("txhCodReqDet"+srtIndice).value + 
		"&txhCodDev=" + document.getElementById("txhCodDevolucion").value +
		"&txhCodDevDet=" + document.getElementById("txhCodDevDet"+srtIndice).value +
		"&txhTipoProcedencia=0",500,200);
	}
	</script>
</head>

<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/DevolverBienes.html" >
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="msg" id="txhMsg" />
<form:hidden path="codUsuario" id="txhCodUsuario" />
<form:hidden path="codSede" id="txhCodSede" />
	
<form:hidden path="codDevolucion" id="txhCodDevolucion" />
	
<form:hidden path="codSubTipoRequerimiento" id="txhCodSubTipoRequerimiento" />
<form:hidden path="codTipoBienActivo" id="txhCodTipoBienActivo" />
<form:hidden path="codTipoBienConsumible" id="txhCodTipoBienConsumible" />
	
<form:hidden path="tamListaBienes" id="txhTamListaBienes" />
<form:hidden path="codTipoBien" id="txhCodTipoBien" />
<form:hidden path="codRequerimiento" id="txhCodRequerimiento" />
<form:hidden path="codEstado" id="txhCodEstado" />
<form:hidden path="estadoAlmacen" id="txhEstadoAlmacen"/>
<form:hidden path="consteEstadoAlmacen" id="txhConsteEstadoAlmacen"/>
	
	<!-- title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo">
		 		<font style="">
		 			Devoluci�n de Bienes
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<!-- cabecera -->	
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="0" >
		<tr>
			<td width="20%">Nro. Devolucion:</td>
			<td width="30%">
				<form:input path="nroDevolucion" id="txtNroDevolucion"													 
					cssClass="cajatexto_1" cssStyle="width: 40%" readonly="true"/>
					
			</td>
			<td width="20%">Fec. Devolucion:</td>
			<td width="30%">
				<form:input path="fechaDevoluci�n" id="txtFechaDevoluci�n"					
					cssClass="cajatexto_1" cssStyle="width: 40%" readonly="true"/>
			</td>
		</tr>
		<tr>
			<td>Centro de Costo:</td>
			<td>
				<form:input path="centroCosto" id="txtCentroCosto"					
					cssClass="cajatexto_1" cssStyle="width: 50%" readonly="true"/>
			</td>				
			<td>Usuario Solicitante:</td>
			<td>				
				<form:input path="usuSolicitante" id="txtUsuSolicitante"
					cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>				
			</td>
		</tr>
		<tr>
			<td>Tipo de Requerimiento:</td>
			<td>
				<form:input path="tipoRequerimiento" id="txtTipoRequerimiento"
					cssClass="cajatexto_1" cssStyle="width: 50%" readonly="true"/>				
			</td>
			<td>Tipo Bien:</td>			
			<td>
				<input type="radio" id="rbtConsumible" name="radioTipoBien"  
				  <c:if test="${control.codTipoBien==control.codTipoBienConsumible}"><c:out value=" checked=checked " /></c:if> disabled="disabled">
				  Consumible&nbsp;
				<input type="radio" id="rbtActivo" name="radioTipoBien" 
				   <c:if test="${control.codTipoBien==control.codTipoBienActivo}"><c:out value=" checked=checked " /></c:if> disabled="disabled">
				   Activo
			</td>
		</tr>		
		<tr>
			<td>Nro. Req. Asociado:</td>
			<td>
				<form:input path="nroReqAsociado" id="txtNroReqAsociado"
					cssClass="cajatexto_1" cssStyle="width: 50%"/>				
			</td>
		</tr>						
	</table>
	<!-- bandeja de la pagina -->
	<table cellpadding="0" cellspacing="0" id="tablaBandejaSolicitud" 
		style="width:97%;margin-top:6px;margin-left:9px" >
		<tr>
			<td>
				<div style="overflow: auto; height: 150px">				
				<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandeja">
					<tr>						
						<td class="grilla" width="10%">Codigo</td>
						<td class="grilla" width="25%">Producto</td>
						<td class="grilla" width="10%">Uni. Med.</td>
						<td class="grilla" width="15%">Cantidad</td>
						<td class="grilla" width="10%">Numero Serie</td>																			
					</tr>									
					<c:forEach varStatus="loop" var="lista" items="${control.listaBienes}"  >
					<tr class="texto">						
						<td align="center" class="tablagrilla" style="width: 8%">							
							<c:out value="${lista.codBien}" />							
						</td>
						<td align="left" class="tablagrilla" style="width: 32%">
							<c:out value="${lista.dscBien}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.dscUnidad}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.cantidad}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">							
							<input type="hidden" 
								id='txhCodReqDet<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codRequerimientoDetalle}" />'>
							<input type="hidden" 
								id='txhCodBien<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codBien}" />'>
							<input type="hidden" 
								id='txhCodDevDet<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codDevolucionDetalle}" />'>
							<c:if test="${lista.indiceNumeroSerie=='1'}">
							<img src="${ctx}/images/iconos/agregar_on.gif" onclick="javascript:fc_NumeroSerie('<c:out value="${loop.index}"/>');"
							     style="cursor:hand" alt="N�mero Serie">
							</c:if>
						</td>													
					</tr>
					</c:forEach>
					<c:if test='${control.tamListaBienes=="0"}'>
						<tr class="tablagrilla">
							<td align="center" colspan="6" height="20px">No se encontraron registros
							</td>
						</tr>					 		
					</c:if>
				</table>
				</div>
			</td>			
		</tr>
	</table>
	<table cellpadding="0" cellspacing="4" width="98%" border="0" bordercolor="blue" class="tabla2" style="margin-left:6px;">
		<tr>
			<td width="100%">
				<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
					<tr>
						<td class="">Motivo de Devoluci�n</td>	
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" class="tablagrilla" style="width:100%;border: 1px solid #048BBA">
					<tr>
						<td>
							<form:textarea path="motivoDevolucion" id="txtMotivoDevolucion" cssClass="cajatexto_o" cssStyle="width:99.5%;height:40px;" />
						</td>
					</tr>
				</table>							
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="4" width="98%" border="0" bordercolor="blue" class="tabla2" style="margin-left:6px;">
		<tr>
			<td width="100%">
				<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
					<tr>
						<td class="">Observaciones</td>	
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" class="tablagrilla" style="width:100%;border: 1px solid #048BBA">
					<tr>
						<td>
							<form:textarea path="observaciones" id="txtObservaciones" cssClass="cajatexto" cssStyle="width:99.5%;height:40px;"/>							
						</td>
					</tr>
				</table>							
			</td>
		</tr>
	</table>	
	
	<br>
	
	<table align="center">
		<tr>
			<td align=center valign="top">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar02','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar02" style="cursor:pointer;" onclick="fc_Regresar();">
				</a>
			<td>
			<c:if test="${control.codEstado=='0002'}">
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgConfirmar02','','${ctx}/images/botones/confirmardev2.jpg',1)">
					<img alt="Confirmar Devoluci�n" src="${ctx}/images/botones/confirmardev1.jpg" id="imgConfirmar02" style="cursor:pointer;" onclick="fc_Confirmar();">
				</a>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRechazar','','${ctx}/images/botones/rechazar2.jpg',1)">
					<img alt="Rechazar" src="${ctx}/images/botones/rechazar1.jpg" id="imgRechazar" style="cursor:pointer;" onclick="fc_Rechazar();">
				</a>
				<td>				
		    </c:if>
		</tr>
	</table>
		
</form:form>
</body>