<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

	<script language=javascript>
		function onLoad()
		{
		 document.getElementById("txhCodOrden").value="";
		}
		function Fc_Limpiar(){
		document.getElementById("txtNroOrden").value = "";
		document.getElementById("txhFechaEmi").value = "";
		document.getElementById("txhFechaEmision2").value = "";
		document.getElementById("txhFechaEmision3").value = "";
		document.getElementById("txhFechaEmision4").value = "";
		document.getElementById("txtCodProveedor").value = "";
		document.getElementById("txtProveedor").value = "";
		document.getElementById("txtComprador").value = "";
		document.getElementById("txtNroGuia").value = "";
		}
		
		 //1 q la fecha inicial es mayor o igual ala fecha final
       	 //0 es vacia las 2 fechas o la fecha final es mayor q la fecha incial
		function fc_Validar_Fechas(srtFechaInicio, srtFechaFin)
	   {
		   var num=0;
		   var srtFecha="0";
		  if( srtFechaInicio!="" && srtFechaFin!="")
		  {   
			    num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
			      //alert(num);
			     if(num==1) srtFecha="0";
			     else srtFecha="1";	    
		  }	 
	      return srtFecha;
	   }
	
		function Fc_Buscar(){
		var fec1=fc_Validar_Fechas(document.getElementById("txhFechaEmi").value,
		document.getElementById("txhFechaEmision2").value);
		var fec2=fc_Validar_Fechas(document.getElementById("txhFechaEmision3").value,
		document.getElementById("txhFechaEmision4").value);
		
		if(fec1=="0" && fec2=="0")
		 { document.getElementById("txhOperacion").value = "BUSCAR";
		   document.getElementById("frmMain").submit();
		 }
		 else{ if(fec1!="0") alert("La fecha inicio de Nro. Orden debe ser menor a la fecha fin.");
		 	   else if(fec2!="0") alert("La fecha inicio de Nro. Gu�a debe ser menor a la fecha fin.");
		 }		
		}
			
		function fc_SelBienAlmacen(strCod){
		document.getElementById("txhCodOrden").value=strCod;
		}
		
		function Fc_RegEntrega(){
			if(document.getElementById("txhCodOrden").value!=""){
				srtCodSede=document.getElementById("txhSede").value;
				srtCodOrden=document.getElementById("txhCodOrden").value;
				srtCodUsuario=document.getElementById("txhCodUsuario").value;
				   location.href="${ctx}/logistica/RecBieRegistrarEntregaBienes.html?txhCodSede="+srtCodSede+
				     "&txhCodOrden="+srtCodOrden+"&txhCodUsuario="+srtCodUsuario;
			}
			else alert(mstrSeleccione);
		}
		
		function fc_Regresar()
		{
			document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhSede").value;
		}
	function fc_IndPendiente(param){
	//ALQD,21/01/09. NUEVA FUNCION
		if(param=="1") 
		  {  if(document.getElementById("txhValSelec1").value=="1")
		    	 document.getElementById("txhValSelec1").value="0";
		     else
		     document.getElementById("txhValSelec1").value=param;
	      }
	}
		
</script>
</head>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/RecepcionBienes.html">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="sede" id="txhSede"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codOrden" id="txhCodOrden"/>
<form:hidden path="codOpciones" id="txhCodOpciones"/>
<form:hidden path="codPerfil" id="txhCodPerfil"/>
<form:hidden path="estadoAlmacen" id="txhEstadoAlmacen"/>
<form:hidden path="consteEstadoAlmacen" id="txhConsteEstadoAlmacen"/>
<form:hidden path="indIngPorConfirmar" id="txhValSelec1"/>
	
<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
</table>
		
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		<tr>
			<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			<td background="${ctx}/images/Evaluaciones/centro.jpg" width="770px" class="opc_combo">
				<font style="">Almac�n-Recepci�n de Bienes</font></td>
			<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" style="margin-top:6px;width:96% ;margin-left: 9px;" 
 	        background="${ctx}/images/Logistica/back.jpg" class="tabla" border="0" bordercolor="red">
		<tr>
			<td class="" width="8%">&nbsp;&nbsp;Nro. Orden :</td>
			<td width="34%"><form:input path="nroOrden" id="txtNroOrden" 
							onkeypress="fc_ValidaNroOrden();" 
						    onblur="fc_ValidaNroOrdenOnblur(this, 'Nro. Orden.');"
							cssClass="cajatexto" cssStyle="width:90px" maxlength="15" />
			</td>
			<td class="" width="10%">Fec. Emisi�n :</td>
			<td width="48%" colspan="2">
				<form:input path="fechaEmision" id="txhFechaEmi"
					onkeypress="fc_ValidaFecha('txhFechaEmi');"
					onblur="fc_FechaOnblur('txhFechaEmi');" 
					cssClass="cajatexto" size="10" />
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecEmi','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecEmi" alt="Calendario" style='cursor:hand;' align="middle"></a> - &nbsp;
				<form:input path="fechaEmision2" id="txhFechaEmision2"
					onkeypress="fc_ValidaFecha('txhFechaEmision2');"
					onblur="fc_FechaOnblur('txhFechaEmision2');" 
					cssClass="cajatexto" size="10" />
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecEmi2','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecEmi2" alt="Calendario" style='cursor:hand;' align="middle"></a>
			</td>
		</tr>
		<tr>
			<td class="">&nbsp;&nbsp;Proveedor :</td>
			<td><form:input path="codProveedor" id="txtCodProveedor" 
				onkeypress="fc_ValidaNumerico2();" maxlength="11"
				onblur="fc_ValidaLongitudRuc('txtCodProveedor');"
				cssClass="cajatexto" cssStyle="width:65px"/>&nbsp;
				<form:input path="proveedor" id="txtProveedor" 
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaSoloLetrasFinal1(this,'Proveedor');"
					cssClass="cajatexto" cssStyle="width:230px" maxlength="50"/></td>
			<td class="">Comprador :</td>
			<td colspan="2"><form:input path="comprador" id="txtComprador" 
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaSoloLetrasFinal1(this,'Comprador');"
					cssClass="cajatexto" cssStyle="width:230px" maxlength="50"/>
			</td>									
		</tr>
		<tr>
			<td class="" width="8%">&nbsp;&nbsp;Nro. Gu�a :</td>
			<td width="34%"><form:input path="nroGuia" id="txtNroGuia" 
							onkeypress="fc_ValidaNumerosGuion();"
							onblur="fc_ValidaNumeroGuionOnblur(this, 'Nro. Gu�a');"
							cssClass="cajatexto" cssStyle="width:90px" maxlength="15" />
			</td>
			<td class="" width="10%">Fec. Emisi�n :</td>
			<td width="38%">
				<form:input path="fechaEmision3" id="txhFechaEmision3"
					onkeypress="fc_ValidaFecha('txhFechaEmision3');"
					onblur="fc_FechaOnblur('txhFechaEmision3');" 
					cssClass="cajatexto" size="10" />
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecEmi3','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecEmi3" alt="Calendario" style='cursor:hand;' align="middle"></a> - &nbsp;
				<form:input path="fechaEmision4" id="txhFechaEmision4"
					onkeypress="fc_ValidaFecha('txhFechaEmision4');"
					onblur="fc_FechaOnblur('txhFechaEmision4');" 
					cssClass="cajatexto" size="10" />
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecEmi4','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecEmi4" alt="Calendario" style='cursor:hand;' align="middle"></a>
				<input Type="checkbox" name="checkbox1" align="middle"
				    <c:if test="${control.indIngPorConfirmar==control.consteIndIngPorConfirmar}"><c:out value=" checked=checked " /></c:if>
					id="checkbox1" onclick="javascript:fc_IndPendiente('1');">Por Confirmar&nbsp;
		    </td>
			<td align="right" width="10%">
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" onclick="javascript:Fc_Limpiar();" alt="Limpiar" style="cursor:hand" id="icoLimpiar"></a>&nbsp;
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:Fc_Buscar();" alt="Buscar" style="cursor:hand" id="icoBuscar"></a>&nbsp;
			</td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" style="margin-top: 6px;margin-left: 9px;"
		name="TablaBandejaBien" width="98.5%" border="0" bordercolor="red">
		<tr><td>
			<div style="overflow: auto; height: 280px;width:100%">									
				<display:table name="sessionScope.listCentroCostos" cellpadding="0" cellspacing="1"
					decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
					requestURI="" style="border: 1px solid #048BBA;width:97.5%"> 
					<display:column property="rbtSelBienAlmacen" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
					<display:column property="nroOrden" title="Nro. Orden" headerClass="grilla" class="tablagrilla" style="align:center;width:150px"/>
					<display:column property="fecEmision" title="Fec. Emisi�n" headerClass="grilla" class="tablagrilla" style="align:center;width:180px"/>
					<display:column property="dscRazonSocial" title="Proveedor" headerClass="grilla" class="tablagrilla" style="align:left;width:300px"/>
					<display:column property="dscMoneda" title="Moneda" headerClass="grilla" class="tablagrilla" style="align:center;width:180px"/>
					<display:column property="montoTotal" title="Monto" headerClass="grilla" class="tablagrilla" style="align:right;width:220px"/>
					<display:column property="dscComprador" title="Comprador" headerClass="grilla" class="tablagrilla" style="align:left;width:220px"/>
					
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
				</display:table>
				<% request.getSession().removeAttribute("listCentroCostos"); %>
			</div>
			</td>
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align="center">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('regEntrega','','${ctx}/images/botones/regentrega2.jpg',1)">
					<img alt="Registrar Entrega" src="${ctx}/images/botones/regentrega1.jpg" id="regEntrega" style="cursor:pointer;" onclick="Fc_RegEntrega();">
				</a>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('cerrarOrden','','${ctx}/images/botones/cerrarorden2.jpg',1)">
					<img alt="Cerrar Orden" src="${ctx}/images/botones/cerrarorden1.jpg" id="cerrarOrden" style="cursor:pointer;" onclick="">
				</a>
			</td>			
		</tr>
	</table>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txhFechaEmi",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecEmi",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txhFechaEmision2",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecEmi2",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txhFechaEmision3",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecEmi3",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txhFechaEmision4",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecEmi4",
		singleClick    :    true
	});
	</script>