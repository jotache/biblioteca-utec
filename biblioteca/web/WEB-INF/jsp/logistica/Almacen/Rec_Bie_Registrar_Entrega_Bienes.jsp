<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	function onLoad(){
	//alert(document.getElementById("txhCodUsuario").value);
	}
	
	function fc_Buscar(){
		document.getElementById("txhOperacion").value = "BUSCAR";
		document.getElementById("frmMain").submit();
	}
	
	function fc_Ver_Guia(){
	if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)		
	{
	 if(document.getElementById("txhCodDocumento").value!=""){	
	   /*if(document.getElementById("txhIndiceModificacion").value=="1")	
	 	{*/ srtCodSede=document.getElementById("txhCodSede").value;
	 	  srtCodCtaPago=document.getElementById("txhCodDocumento").value;
	 	  srtCodUsuario=document.getElementById("txhCodUsuario").value;
	 	  //alert(srtCodUsuario);
	  	  document.getElementById("iFrameDocumentosPago").src="${ctx}/logistica/RecBieRegistrarEntregaBienesIframe.html?txhCodSede="
	  	  +srtCodSede + "&txhCodCtaPago="+srtCodCtaPago+ "&txhCodUsuario="+srtCodUsuario +
	  	  "&txhIndModi="+document.getElementById("txhIndiceModificacion").value;
	  	  TablaDocumentoPago.style.display='inline-block';
	   /* }
	   else alert(mstrNoModificarGuiaRemision);*/
	 }
	 else alert(mstrSeleccione);
	}
		  else alert(mstrEstadoLogistica);
	}
	
	function fc_Cerrar(){
	}
	
	function fc_Regresar(){
	srtCodSede=document.getElementById("txhCodSede").value;
	location.href="${ctx}/logistica/RecepcionBienes.html?txhCodSede="+srtCodSede;
	}
	
	function fc_SeleccionarRegistro(srtCodDoc, srtIndiceModificacion){

	document.getElementById("txhCodDocumento").value=srtCodDoc;
	document.getElementById("txhIndiceModificacion").value=srtIndiceModificacion;
	}
	
	function fc_Anular(){
	
	}
	
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/RecBieRegistrarEntregaBienes.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codProveedor" id="txhCodProveedor"/>
<form:hidden path="codPerfil" id="txhCodPerfil"/>
<form:hidden path="codOpciones" id="txhCodOpciones"/>
<form:hidden path="codOrden" id="txhCodOrden"/>
<form:hidden path="codEstado" id="txhCodEstado"/>
<form:hidden path="codDocumento" id="txhCodDocumento"/>
<form:hidden path="indiceModificacion" id="txhIndiceModificacion"/>
<form:hidden path="estadoAlmacen" id="txhEstadoAlmacen"/>
<form:hidden path="consteEstadoAlmacen" id="txhConsteEstadoAlmacen"/>

<div style="overflow: auto; height: 490px;width:100%">
<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" 
         style="margin-left:9px; margin-top: 7px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="765px" class="opc_combo">
		 		<font style="">Registrar Entrega de Bienes</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
</table>
		
<table class="tabla" background="${ctx}/images/Evaluaciones/back.jpg" 
		style="width:96%;margin-top:6px; margin-left:9px;" cellspacing="2" cellpadding="0">
		<tr>
				<td class="">&nbsp;&nbsp;Nro. Orden :</td>
				<td><form:input path="nroOrden" id="nroOrden" cssClass="cajatexto_1" readonly="true"
								cssStyle="width:100px; text-align: right;" maxlength="10" />
				</td>
				<td>Fec. Emisi�n :</td>
				<td><form:input path="fecEmision" id="fecEmision" cssClass="cajatexto_1" readonly="true"
								cssStyle="width:70px; text-align: right;" maxlength="10"/>
				</td>
			</tr>
			<tr>
				<td class="">&nbsp;&nbsp;Proveedor :</td>
				<td>
					<form:input path="txtNroRucProveedor" id="txtNroRucProveedor" cssClass="cajatexto_1"
								readonly="true" cssStyle="width:100px; text-align:right;" maxlength="10" />&nbsp;
					<form:input path="txtNombreProveedor" id="txtNombreProveedor" cssClass="cajatexto_1"
								 readonly="true" cssStyle="width:230px" maxlength="10" />				
				</td>
				<td class="">Atencion a :</td>
				<td><form:input path="txtAtencion" id="txtAtencion" cssClass="cajatexto_1"
								readonly="true" cssStyle="width:230px" maxlength="10" />
				</td>		
			</tr>			
			<tr>
				<td class="">&nbsp;&nbsp;Monto :</td>
				<td>
					<form:input path="txtMonto" id="txtMonto" cssClass="cajatexto_1" 
								readonly="true"	cssStyle="width:100px; text-align:right;" maxlength="10" />
				</td>
				<td>Comprador :</td>
				<td>
					<form:input path="txtComprador" id="txtComprador" cssClass="cajatexto_1"
								readonly="true"	cssStyle="width:230px" maxlength="10" />
				</td>
				<td class="">&nbsp;</td>
				<td>&nbsp;</td>			
			</tr>		
		</table>
		
		<table cellpadding="0" cellspacing="0" ID="Table2" style="margin-left: 9px; margin-top: 6px;">
			<tr height="25px">
				
				<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 		<td background="${ctx}/images/Evaluaciones/centro.jpg" width="740px" class="opc_combo">
		 		<font style="">Consulta de Gu�a de Remisi�n Asociadas</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0"  ID="Table1" 
		              style="width:96%;margin-top:6px; margin-left:9px;">
			<tr>
				<td width="97%">
				   <div style="overflow: auto; height: 110px">
					<table cellpadding="0" cellspacing="1" border="0" bordercolor="white"
					 style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA">
						<tr>
							<td class="grilla" width="4%">Sel.</td>
							<td class="grilla" width="15%">Nro. Gu�a</td>
							<td class="grilla" width="10%">Fec. Emisi�n Gu�a</td>
							<td class="grilla" width="15%">Nro. Factura</td>							
							<td class="grilla" width="15%">Fec. Emisi�n Factura</td>
							<td class="grilla" width="15%">Monto Factura</td>
							<td class="grilla" width="15%">Estado Gu�a</td>
						</tr>
				   <c:forEach varStatus="loop" var="lista" items="${control.listaSuperior}"  >
					<tr class="texto">
					   <td align="center" class="tablagrilla" style="width: 5%">
							<input type="radio" name="producto"
								id='valBa<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro('<c:out value="${lista.codCtaPago}" />','<c:out value="${lista.indModificacion}" />');">
							
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">							
							<c:out value="${lista.nroGuia}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.fecEmisionGuia}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.nroCtaPago}" />							
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">						
							<c:out value="${lista.fecEmisionCta}" />							
						</td>
						<td align="right" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.dscMontoCta}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 15%">
							<c:out value="${lista.dscEstado}" /> 					
						</td>
																	
					</tr>
					</c:forEach>
					</table>
					</div>
				</td>
				<td align="right" width="3%">
					<img src="${ctx}/images/iconos/modificar_on.gif" onclick="javascript:fc_Ver_Guia();" style="cursor:hand" alt="Ver Detalle">					
				</td>
			</tr>
			
		</table>
		<table align="center" style="margin-top: 6px;">
		
		<tr>
			<td>
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar01','','${ctx}/images/botones/regresar2.jpg',1)">
			    <img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar01" style="cursor:pointer;" onclick="javascript:fc_Regresar();"></a>
			</td>
			
		</tr>
		</table>
		
		<table id="TablaDocumentoPago" name="TablaDocumentoPago" style="display: none; margin-left: 9px; margin-top: 6px;"
		        border="0" bordercolor="red" width="96%">
		<tr>
			<td>
				<iframe id="iFrameDocumentosPago" name="iFrameDocumentosPago" frameborder="0"  height="340px" width="100%">
				</iframe>
			</td>
		</tr>	
		</table>
</div>
</form:form>
</body>
</html>