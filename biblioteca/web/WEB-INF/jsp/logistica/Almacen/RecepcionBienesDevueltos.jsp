<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

	<script language=javascript>
	var codDev = "";
		function onLoad()
		{
		
		}
	function fc_Limpia(){
	document.getElementById("txhFechaDevolucionIni").value = "";
	document.getElementById("txhFechaDevolucionFin").value = "";
	document.getElementById("txhFechaDevolucionFin").value = "";
	document.getElementById("codCentro").value = "";
	document.getElementById("txtSolicitante").value = "";
	document.getElementById("codEstado").value = "";
	}
	
	function fc_Regresar()
		{
			document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhSede").value;
		}
		
	function fc_Buscar(){
	srtFechaInicio=document.getElementById("txhFechaDevolucionIni").value;
	srtFechaFin=document.getElementById("txhFechaDevolucionFin").value;
	     var num=0;
	     var srtFecha="0";
	     if( srtFechaInicio!="" && srtFechaFin!="")
	       {   
	           num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
	           //num=1: si el primer parametro es mayor al segundo parametro
	           //num=0: si el segundo parametro es mayor o igual al primer parametro
	           //alert(num);
	           if(num==1) 	srtFecha="0";
	           else srtFecha="1";	    
	       }
	     
	    if(srtFecha=="0")   
	    	{	
	    	    document.getElementById("txhOperacion").value = "BUSCAR";
				document.getElementById("frmMain").submit();
	    	}
	else alert(mstrFecha);
		/*document.getElementById("txhOperacion").value="BUSCAR";
		document.getElementById("frmMain").submit();*/
	}
		
	function fc_ConfirmarDevolucion(){
	
	 if(document.getElementById("txhCodRequerimiento").value!="")
	  {	if(confirm(mstrSeguroRealizarDevolucion)){  
			codDevolucion = document.getElementById("txhCodDevolucion").value; 
			codSede= document.getElementById("txhSede").value;
			codUsuario= document.getElementById("txhCodUsuario").value;	
			window.location.href="${ctx}/logistica/DevolverBienes.html"+
					"?txhCodDevolucion="+codDevolucion+
					"&txhCodSede="+codSede+
					"&txhCodUsuario="+codUsuario;
		 }
	  }
		else alert(mstrSeleccion);	
	}
	
	function fc_SelBienAlmacen(strCodDev,strCodReq){
	document.getElementById("txhCodDevolucion").value=strCodDev;
	document.getElementById("txhCodRequerimiento").value=strCodReq;
	}
	
</script>
</head>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/RecepcionBienesDevueltos.html">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="sede" id="txhSede"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codTipoBien" id="txhCodTipoBien"/>
<form:hidden path="codRequerimiento" id="txhCodRequerimiento"/>
<form:hidden path="codDevolucion" id="txhCodDevolucion"/>
<input type="hidden" name="hid_Aprobacion" id="hid_Aprobacion" value="1"/>
		
	<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="765px" class="opc_combo">
		 		<font style="">Recepción de Bienes Devueltos</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
		
		<table cellpadding="1" cellspacing="0" style="margin-top:10px;width:96% ;margin-left: 9px; margin-top: 6px;" 
 		        background="${ctx}/images/Logistica/back.jpg" class="tabla" border="0" bordercolor="red">
			<tr><td class="">&nbsp;&nbsp;Fec. Devolución:</td>
				<td>
				<form:input path="fechaDevolucionIni" id="txhFechaDevolucionIni"
					onkeypress="fc_ValidaFecha('txhFechaDevolucionIni');"
					onblur="fc_FechaOnblur('txhFechaDevolucionIni');" 
					cssClass="cajatexto" size="10" maxlength="10"/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecIni" alt="Calendario" style='cursor:hand;' align="absmiddle"></a>&nbsp;
				<form:input path="fechaDevolucionFin" id="txhFechaDevolucionFin"
					onkeypress="fc_ValidaFecha('txhFechaDevolucionFin');"
					onblur="fc_FechaOnblur('txhFechaDevolucionFin');" 
					cssClass="cajatexto" size="10" maxlength="10"/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecFin" alt="Calendario" style='cursor:hand;' align="absmiddle"></a>&nbsp;
			</td><td class="">Centro Costo:</td>
				<td colspan="2">
					<form:select path="codCentro" id="codCentro" cssClass="cajatexto"
					cssStyle="width:250px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listCentroCosto!=null}">
					<form:options itemValue="codTecsup" itemLabel="descripcion"
					items="${control.listCentroCosto}" />
					</c:if>
					</form:select>
				</td>
			</tr><tr><td class="">&nbsp;&nbsp;U. Solicitante :</td>
				<td>
				<form:input path="solicitante" id="txtSolicitante" 
					onkeypress="fc_ValidaTextoEspecial();"
								onblur="fc_ValidaSoloLetrasFinal1(this,'Comprador');"
								cssClass="cajatexto" cssStyle="width:230px" maxlength="50"/>
				</td>	
				<td class="">Estado</td>
				<td><form:select path="codEstado" id="codEstado" cssClass="cajatexto"
					cssStyle="width:150px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listEstado!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
					items="${control.listEstado}" />
					</c:if>
					</form:select>
				</td>
				<td align="right">
							
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
							<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
								 style="cursor: pointer;" 
								 onclick="javascript:fc_Limpia();" id="imgLimpiar"></a>&nbsp;
							
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
							<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle" 
								 style="cursor: pointer; margin-right: 60px" onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
				</td>
			</tr>
		</table>
		
		<table cellpadding="0" cellspacing="0"  style="margin-left: 9px; margin-top: 6px;" ID="Table1" 
		     width="100%" border="0" bordercolor="red"> 
			<tr>
				<td>
					<table cellpadding="0" cellspacing="1" class="" bordercolor="red" border="0"
					       style="border: 0px solid #048BBA;width:98.5%;">
			 		<tr><td>
						<div style="overflow: auto; height: 280px;width:100%">									
						<display:table name="sessionScope.listBienesDevueltos" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
							requestURI="" style="border: 1px solid #048BBA;width:97.5%"> 
							<display:column property="rbtSelBienAlmacenDev" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
							<display:column property="nroReq" title="Nro. Req" headerClass="grilla" class="tablagrilla" style="text-align:center;width:10%"/>
							<display:column property="dscCeco" title="Centro Costo" headerClass="grilla" class="tablagrilla" style="text-align:left;width:25%"/>
							<display:column property="dscTipoBien" title="Tipo Bien" headerClass="grilla" class="tablagrilla" style="text-align:left;width:20%"/>
							<display:column property="cantDevuelta" title="Cantidad <br>Devuelta" headerClass="grilla" class="tablagrilla" style="text-align:right;width:10%"/>
							<display:column property="fecDevolucion" title="Fecha <br>Devolución" headerClass="grilla" class="tablagrilla" style="text-align:center;10%"/>
							<display:column property="dscEstado" title="Estado" headerClass="grilla" class="tablagrilla" style="text-align:left;width:20%"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
						</display:table>
						<% request.getSession().removeAttribute("listBienesDevueltos"); %>
				</div>
				</td>
			</tr> 
		</table>
		<table><tr height="5px"><td></td></tr></table>
		<table align="center">
			<tr>
				<td align="center" style="margin-top: 15px;">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('conDevolucion','','${ctx}/images/botones/confirmardev2.jpg',1)">
						<img alt="Confirmar Devolución" src="${ctx}/images/botones/confirmardev1.jpg" id="conDevolucion" style="cursor:pointer;" onclick="fc_ConfirmarDevolucion();">
					</a>					
				</td>
			</tr>			
		</table>	
</form:form>
	<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txhFechaDevolucionIni",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txhFechaDevolucionFin",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
