<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	function onLoad(){
	if(document.getElementById("txhMsg").value=="OK_GRABAR")
	  {  		  
		  alert(mstrSeGraboConExito);
	  }
	  else{ if(document.getElementById("txhMsg").value=="ERROR_GRABAR")
	           alert(mstrProblemaGrabar);
	        else{ if(document.getElementById("txhMsg").value=="OK_CERRAR")
				  { 
	        		 document.forms[0].imgCerrarGuia.disabled=true; 
				  	 alert(mstrSeGraboConExito);
	  				 parent.fc_Buscar();
					  }
					  else{ if(document.getElementById("txhMsg").value=="ERROR_CERRAR")
	           		           		alert(mstrProblemaGrabar);
	           		         else{ if(document.getElementById("txhMsg").value=="ERROR_CANT_ASIG")
	           		           		alert(mstrNoCantidadAsignada);
	           		           	   else{ if(document.getElementById("txhMsg").value=="ERROR_CANT_CORRE")
	           		          				 alert(mstrNoCantidadCorrecta);
	           		          			 else{ if(document.getElementById("txhMsg").value=="ERROR_CERRAR_GUIA")
	           		           					alert(mstrProblemasCerrarGuia);
	           		           				   else if(document.getElementById("txhMsg").value== "ALMACEN_CERRADO")
												alert(mstrEstadoLogistica);
											 }
	           		           		}
	           		         }
	        	   }
	  }
	
	 }
	 document.getElementById("txhMsg").value="";
	}
	function fc_Cerrar_Guia(){
		form = document.forms[0];
		if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)
		{
			if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)
			{
			  if(confirm(mstrSeguroCerrarGuia)){
				  form.imgCerrarGuia.disabled=true;
				  document.getElementById("txhOperacion").value = "CERRAR";
				  document.getElementById("frmMain").submit();
			   }else
				   form.imgCerrarGuia.disabled=false;
			}
			else alert(mstrEstadoLogistica);
		 }
		else alert(mstrEstadoLogistica);
	}
	
	function fc_Guardar(){
		form = document.forms[0];
		if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)
		{
			if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)
			{
					fc_Encadenar();
					var ffw=0;
					  if(confirm(mstrSeguroGrabarGuia)){
						  form.imgGrabar01.disabled=true;
					  	  document.getElementById("txhOperacion").value = "GUARDAR";
						  document.getElementById("frmMain").submit();
					  }else
						  form.imgGrabar01.disabled=false;
			 }
				else alert(mstrEstadoLogistica);
		}
		else alert(mstrEstadoLogistica);
	}
	
	function fc_Cancelar(){
	parent.fc_Regresar();
	}
	
	function fc_Agregar(indice){
	srtCodBien=document.getElementById("codBien"+indice).value;
	srtCodDocPago=document.getElementById("codDocPago"+indice).value;
	srtPrecioUnitario=document.getElementById("precioCompra"+indice).value;
	srtCantidadEntregada=document.getElementById("cantEntregadaHi"+indice).value;
	srtCodDetalle=document.getElementById("codDetalle"+indice).value;
	Fc_Popup("${ctx}/logistica/RegistrarNumeroSeries.html?txhCodUsuario=" +
		document.getElementById("txhCodUsuario").value + "&txhCodSede=" +
		document.getElementById("txhCodSede").value + "&txhCodBien=" + srtCodBien +
		"&txhCodDocPago=" + srtCodDocPago + "&txhPrecioUnitario=" + srtPrecioUnitario +
		"&txhCantEntregada=" + srtCantidadEntregada +
		"&txhCodDetalle=" + srtCodDetalle,500,200);
	}
	
	function fc_CalcularCantidad(srtIndice){
	 
	 if(document.getElementById("canEntregada"+srtIndice).value!="")
		{ 
		  srtCanSolicitada=document.getElementById("canFac"+srtIndice).value;
		  srtCanFacturada=document.getElementById("canEntregada"+srtIndice).value;
		  var canFacturada=parseFloat(srtCanFacturada);
		  var canSolicitada=parseFloat(srtCanSolicitada);
		  if(canSolicitada>=canFacturada)
			 {if(document.getElementById("total").value=="")
			    document.getElementById("total").value="0";
			    
			 srtPrecioUnitario=document.getElementById("preUni"+srtIndice).value;
			 var precioUnitario=parseFloat(srtPrecioUnitario);
			 
			 var suma=Math.round(precioUnitario*canFacturada*1000)/1000;
			 document.getElementById("subTotal"+srtIndice).value=suma.toString();
			 Suma_Total();
			 }
			 else{ alert(mstrGuiaExcedeCantidad);
		           document.getElementById("canEntregada"+srtIndice).focus();
		 }
		}
	 else{
	       document.getElementById("subTotal"+srtIndice).value="";
	       Suma_Total();
	 }
	}
	
	function Suma_Total(){
	
	 srtLong=parseInt(document.getElementById("txhLongitud").value);
	 var redSun = 0;
	    for ( a = 0; a< srtLong; a++ ) {
 			  
 			  if(document.getElementById("subTotal"+a).value!="")
 			   { redSun=parseFloat(document.getElementById("subTotal"+a).value)+redSun;
 			   } 
			}
	  
	  document.getElementById("total").value=redSun.toString();
	}
	
	function fc_Encadenar(){
	 srtLong=parseInt(document.getElementById("txhLongitud").value);
	 var cadCantidades="";
	 var cadObservaciones="";
	 var cadCodDocDetalle="";
	  for ( a = 0; a< srtLong; a++ ) {
	        
	        if(a==0)
	         {	cadCantidades=document.getElementById("canEntregada"+a).value ;
	            cadObservaciones=document.getElementById("obs"+a).value ;
	            cadObsercadCodDocDetallevaciones=document.getElementById("codDetalle"+a).value ;
	         }
	       	else
	        {cadCantidades=cadCantidades + "|" + document.getElementById("canEntregada"+a).value;
	         cadObservaciones=cadObservaciones + "|" + document.getElementById("obs"+a).value;
	         cadObsercadCodDocDetallevaciones=cadObsercadCodDocDetallevaciones + "|" +
	                                          document.getElementById("codDetalle"+a).value;
	        }
	        
	  }
	  cadCantidades=fc_Trim(cadCantidades);
	  cadObservaciones=fc_Trim(cadObservaciones);
	  cadObsercadCodDocDetallevaciones=fc_Trim(cadObsercadCodDocDetallevaciones);
	
	 document.getElementById("txhCadCantidades").value=cadCantidades;
	 document.getElementById("txhCadObservaciones").value=cadObservaciones;
	 document.getElementById("txhCadCodDocDetalle").value=cadObsercadCodDocDetallevaciones;
	}
	
	function fc_ValidarFechas(){
	
	srtFechaBD=document.getElementById("txhFechaBD").value;
	
	srtFechaFin=document.getElementById("txtFecInicio1").value;
	var num=0;
	num=fc_ValidaFechaIniFechaFin(srtFechaBD,srtFechaFin);
	//si es 0 la fecEmision es mayor igual ala fechaBD
	
	var mad=0;
	srtFechaVcto=document.getElementById("txtFecInicio2").value;
	mad=fc_ValidaFechaIniFechaFin(srtFechaVcto,srtFechaFin);
	//si es 1 la fecVecto es mayor ala fecEmision
	
	var wild=0;
	srtFechaFinDos=document.getElementById("txtFecFinal1").value;
	wild=fc_ValidaFechaIniFechaFin(srtFechaBD,srtFechaFinDos);
	//si es 0 la fecEmision es mayor igual ala fechaBD
	
	if( (num==0)&&(mad==1)&&(wild==0) )
	 {  return 1;
	 }
	 else{ if(num==1) alert(mstrFecEmisionGuiaMenorActual);
	       else{ if(num==2) alert(mstrFecEmisionGuiaNoVacia);
	             else{ if(mad==0) alert(mstrFecEmisionFactMenorActual);
	                   else{ if(mad==2) alert(mstrFecEmisionNoVacia);
	                         else{ if(wild==1) alert(mstrFecVctoMayorFecEmision);
	                               else alert(mstrFecVctoNoVacia);
	                         }
	                   }
	             }
	       }
	  
	  return 0;
	 }
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/RecBieRegistrarEntregaBienesIframe.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codPerfil" id="txhCodPerfil"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codOpciones" id="txhCodOpciones"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="codOrden" id="txhCodOrden"/>
<form:hidden path="codDocumento" id="txhCodDocumento"/>
<form:hidden path="longitud" id="txhLongitud"/>
<form:hidden path="cadCantidades" id="txhCadCantidades"/>
<form:hidden path="cadObservaciones" id="txhCadObservaciones"/>
<form:hidden path="cadCodDocDetalle" id="txhCadCodDocDetalle"/>
<form:hidden path="fechaBD" id="txhFechaBD"/>
<form:hidden path="estadoAlmacen" id="txhEstadoAlmacen"/>
<form:hidden path="consteEstadoAlmacen" id="txhConsteEstadoAlmacen"/>
<form:hidden path="indModi" id="txhIndModi"/>

<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-top: 7px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="740px" class="opc_combo">
		 		<font style="">Gu�a de Remisi�n</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	 
	 <table class="tabla" background="${ctx}/images/Evaluaciones/back.jpg" border="0" bordercolor="blue"
					style="width:98%;margin-top:6px;height:65px" cellspacing="2" cellpadding="0">
				<tr>
					
					<td class="">&nbsp;&nbsp;&nbsp;Nro. Gu�a :</td>
					<td width="15%"><form:input path="nroGuia" id="nroGuia" cssClass="cajatexto_1"
								readonly="true" cssStyle="width:90px; text-align: right;" maxlength="10" />
					</td>
					<td class="">Fecha Emisi�n :</td>
					<td ><form:input path="fecInicio1" id="txtFecInicio1" cssClass="cajatexto_1"
								readonly="true" cssStyle="width:70px; text-align: right;" maxlength="10"/>&nbsp;
					</td>
					<td class="">Fecha Recepci�n :</td>
					<td ><form:input path="fecFinal2" id="txtFecFinal1" cssClass="cajatexto_1"
								readonly="true" cssStyle="width:70px; text-align: right;" maxlength="10"/>&nbsp;
					</td>
				</tr>
				
				<tr>
					<td class="" width="10%">&nbsp;&nbsp;&nbsp;Nro. Factura:</td>
					<td width="25%">
						<form:input path="nroFactura" id="nroFactura" cssClass="cajatexto_1"
								  readonly="true" cssStyle="width:90px; text-align: right;" maxlength="10"/>
					</td>
					
					<td class="" width="10%">Fecha Emisi�n :</td>
					<td width="25%"><form:input path="fecInicio2" id="txtFecInicio2" cssClass="cajatexto_1"
								readonly="true" cssStyle="width:70px; text-align: right;" maxlength="10"/>&nbsp;
					</td>
					<td class="" width="13%">Fecha Vencimiento :</td>
					<td width="27%">
						<form:input path="fecFinal1" id="txtFecFinal1" cssClass="cajatexto_1"
								readonly="true" cssStyle="width:70px; text-align: right;" maxlength="10"/>&nbsp;
					</td>
				</tr>
			</table>
			
	<div style="overflow: auto; height: 180px;width:98%">
		<table border="0" cellpadding="0" cellspacing="0"  ID="AgregarBandeja"
		       bordercolor="red"  style="width:98%;margin-top:6px;" >
			<tr>
				<td width="100%">
				  
					<table cellpadding="0" cellspacing="1" border="0" bordercolor="red"
					 style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" ID="Table5">
						<tr>
							<td class="grilla" width="24%">Producto</td>
							<td class="grilla" width="9%">Cantidad<br>Solicitada</td>
							<td class="grilla" width="9%">Cantidad<br>Facturada</td>
							<td class="grilla" width="9%">Precio<br>Unitario</td>
							<td class="grilla" width="9%">Cantidad<br>Entregada</td>
							<td class="grilla" width="5%">Unidad</td>
							<td class="grilla" width="15%">SubTotal<br>Entregada</td>
							<td class="grilla" width="15%">Obs.</td>
							<td class="grilla" width="10%">Ing. Activo</td>
						</tr>
				<c:forEach varStatus="loop" var="lista" items="${control.listaBandeja}"  >
					<tr class="texto">
					   
						<td align="left" class="tablagrilla" style="width: 24%">							
							&nbsp;&nbsp;&nbsp;<c:out value="${lista.dscBien}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 9%">
							
							<input type="text" id="canSol<c:out value="${loop.index}"/>" name="canSol<c:out value="${loop.index}"/>"
							 style="text-align: right;" size="5" class="cajatexto_1" value="<c:out value="${lista.canSolicitada}" />" readonly />
							 <input type="hidden" id="codDetalle<c:out value="${loop.index}"/>" name="codDetalle<c:out value="${loop.index}"/>"
							 size="5" class="texto_moneda1" value="<c:out value="${lista.codDocPagoDet}" />" />
							 <input type="hidden" id="codDocPago<c:out value="${loop.index}"/>" name="codDocPago<c:out value="${loop.index}"/>"
							 size="5" class="texto_moneda1" value="<c:out value="${lista.codDocPago}" />" />
							 <input type="hidden" id="codBien<c:out value="${loop.index}"/>" name="codBien<c:out value="${loop.index}"/>"
							 size="5" class="texto_moneda1" value="<c:out value="${lista.codBien}" />" />
						</td>
						<td align="center" class="tablagrilla" style="width: 9%">
						  <input type="text" style="text-align: right;" id="canFac<c:out value="${loop.index}"/>" name="canFac<c:out value="${loop.index}"/>" readonly
							 size="10" class="cajatexto_1" value="<c:out value="${lista.canFacturada}" />" />
						</td>
						<td align="left" class="tablagrilla" style="width: 9%">
							<input type="text" id="preUni<c:out value="${loop.index}"/>" name="preUni<c:out value="${loop.index}"/>" align="center"
							 size="10" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.dscPrecioUnitario}" />" readonly />
						     <input type="hidden" id="precioCompra<c:out value="${loop.index}"/>" name="precioCompra<c:out value="${loop.index}"/>"
							 size="5" class="texto_moneda1" value="<c:out value="${lista.precioCompra}" />" />
						</td>
						<td align="center" class="tablagrilla" style="width: 9%">
						  <input type="text" style="text-align: right;" id="canEntregada<c:out value="${loop.index}"/>" name="canEntregada<c:out value="${loop.index}"/>" maxlength="10"
							 size="10" class="cajatexto" value="<c:out value="${lista.canEntregada}" />" onblur="fc_ValidaDecimalOnBlur('canEntregada<c:out value="${loop.index}"/>','3','3');fc_CalcularCantidad('<c:out value="${loop.index}"/>');" onkeypress="fc_PermiteNumerosPunto();" />							
						  <input type="hidden" id="cantEntregadaHi<c:out value="${loop.index}"/>" name="cantEntregadaHi<c:out value="${loop.index}"/>"
							 size="5" class="texto_moneda1" value="<c:out value="${lista.canEntregada}" />" />
						</td>

						<!-- jhpr 2008-09-25 -->
						<td align="center" class="tablagrilla" style="width: 5%">
							<input type="text" style="text-align: right;" id="unidad<c:out value="${loop.index}"/>" name="unidad<c:out value="${loop.index}"/>"
							 size="5" class="cajatexto_1" value="<c:out value="${lista.unidadMedida}" />" readonly/>
						</td>
						
						<td align="center" class="tablagrilla" style="width: 15%">
							<input type="text" style="text-align: right;" id="subTotal<c:out value="${loop.index}"/>" name="subTotal<c:out value="${loop.index}"/>"
							 size="10" class="cajatexto_1" value="<c:out value="${lista.dscSubTotal}" />" readonly/>
						</td>
						<td align="center" class="tablagrilla" style="width: 15%">
							<textarea cols="40" rows="2" class="cajatexto" maxlength="10"
							id="obs<c:out value="${loop.index}"/>" name="obs<c:out value="${loop.index}"/>"	><c:out value="${lista.dscObservacion}" /></textarea>
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:if test="${lista.indActivo=='1'}">
							<img src="${ctx}/images/iconos/agregar_on.gif" onclick="javascript:fc_Agregar('<c:out value="${loop.index}"/>');" style="cursor:hand" alt="Ingresar Activo">
							</c:if>
						</td>
					</tr>
				</c:forEach>
				
					</table>
					
				</td>
				<td></td>
			</tr>
			
			<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" bordercolor="white" width="78%"
						    style="margin-top: 6px;" ID="Table5">
							<tr>
								<td width="35%" class="texto_bold" align="right">TOTAL :</td>
								<td width="10%" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;
									<form:input path="total" id="total" cssClass="cajatexto_1"
										readonly="true"
										cssStyle="width:63px ; text-align:right" maxlength="10" />
								</td>
							
							</tr>
						</table>	
					</td>
				</tr>
		</table>
	</div>
		<table align=center id="botones" style="margin-top: 6px;">
			<tr>
				<td align="center">
				  <c:if test="${control.indModi=='1'}">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar01','','${ctx}/images/botones/grabar2.jpg',1)">
						<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imgGrabar01" style="cursor:pointer;" onclick="javascript:fc_Guardar();">
					</a>
					
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCerrarGuia','','${ctx}/images/botones/cerrarguia2.jpg',1)">
						<img alt="Cerrar Gu�a" src="${ctx}/images/botones/cerrarguia1.jpg" id="imgCerrarGuia" style="cursor:pointer;" onclick="javascript:fc_Cerrar_Guia();">
					</a>
					
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCancelar01','','${ctx}/images/botones/cancelar2.jpg',1)">
						<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgCancelar01" style="cursor:pointer;" onclick="javascript:fc_Cancelar();">
					</a>
				  </c:if>
				</td>
			</tr>
		</table>

</form:form>

</body>
</html>