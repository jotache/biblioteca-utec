<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Producto'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td>&nbsp;&nbsp;</td>
	<td><img src="${ctx}/images/logoTecsup.jpg"></td>		
</tr>
<tr class="texto_bold">
	<td></td>	
</tr>
<tr >
	<td></td>
	<td class="texto_bold" colspan="2">TOMA DE INVENTARIO</td>
	<td colspan="5">
	</td>
	<td align="right" class="texto_bold">Fecha :</td>
	<td class="texto">${model.fechaRep}</td>		
</tr>
<tr>
	<td></td>
	<td class="texto_bold" colspan="2">
	LISTA DE PRODUCTOS - SEDE
	<%
	if(request.getSession().getAttribute("codSede")!=null){
		String codSede=(String)request.getSession().getAttribute("codSede");
		if(codSede.equals("A")){%>
			AREQUIPA
		<%
		}
		else if(codSede.equals("T")){%>
			TRUJILLO
		<%	
		
		}
		else if(codSede.equals("L")){%>
			LIMA
		<%
		}
		request.getSession().removeAttribute("codSede");
	}
	%>	
	</td>
	<td colspan="5">
	</td>
	<td align="right" class="texto_bold">Hora :</td>
	<td class="texto">${model.horaRep}</td>	
</tr>
<tr>
	<td></td>
	<td class="texto_bold" align="left">${model.fechaActRep}</td>	
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
				<tr>
					<td class="cabecera_grilla" colspan="1">Familia</td>
					<td class="cabecera_grilla">Sub-Familia</td>
					<td class="cabecera_grilla">Ubicaci�n</td>
					<td class="cabecera_grilla">C�digo</td>
					<td class="cabecera_grilla">Descripci�n</td>
					<td class="cabecera_grilla">Unidad</td>
					<td class="cabecera_grilla">P. Unitario<br>(S/.)</td>
					<td class="cabecera_grilla">Stock<br>F�sico</td>
					<td class="cabecera_grilla">Cantidad<br>Inventario</td>
					<td class="cabecera_grilla">Diferencia</td>				
					<td class="cabecera_grilla">Tot.Dif.<br>(S/.)</td>				
				</tr>
				<%  
				System.out.println("entroooo");
				if(request.getSession().getAttribute("consulta")!=null){
					List consulta = (List) request.getSession().getAttribute("consulta"); 
					for(int i=0;i<consulta.size();i++){
						Producto obj  = (Producto) consulta.get(i);
				%>
					<tr class="texto_grilla">
						<td style="text-align:left"><%=obj.getDscFamilia()==null?"":obj.getDscFamilia()%></td>
						<td style="text-align:left"><%=obj.getDscSubFamilia()==null?"":obj.getDscSubFamilia()%></td>
						<td style="text-align:left"><%=obj.getDscUbicacion()==null?"":obj.getDscUbicacion()%></td>
						<td style="text-align:right"><%=obj.getCodProducto()==null?"":obj.getCodProducto()%></td>
						<td style="text-align:left"><%=obj.getNomProducto()==null?"":obj.getNomProducto()%></td>
						<td style="text-align:center"><%=obj.getDscUnidad()==null?"":obj.getDscUnidad()%></td>
					
						<td style="text-align:right"><%=obj.getPreUnitario()==null?"":obj.getPreUnitario()%></td>
						<td style="text-align:right"><%=obj.getCantidad()==null?"":obj.getCantidad()%></td>
						<td style="text-align:right"><%=obj.getCantidadInventario()==null?"":obj.getCantidadInventario()%></td>
						<td style="text-align:right"><%=obj.getCantidadDiferencia()==null?"":obj.getCantidadDiferencia()%></td>
						<td style="text-align:right"><%=obj.getTotDiferencia()==null?"":obj.getTotDiferencia()%></td>						
					</tr>
				<%
					}
				}
				request.removeAttribute("consulta"); 
				%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>