<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>
	var strExtensionXLS = "<%=(String)request.getAttribute("strExtensionXLS")%>";
	var strExtensionXLSX = "<%=(String)request.getAttribute("strExtensionXLSX")%>";
	function onLoad(){
		msg = document.getElementById("txhMsg").value;	
		//alert(msg);
		if(msg == "OK_ABRIR_ALMACEN"){
			alert(mstrAbrirAlmacen);
		}
		else
		if(msg == "ERROR_ABRIR_ALMACEN"){
			alert(mstrProblemaAbrir);
		}
		else
		if(msg == "ACTUALIZACION_ABRIR_ALMACEN"){
			alert(mstrActAbrirAlmacen);
		}
		else
		if(msg == "OK_CERRAR_ALMACEN"){
			alert(mstrCerrarAlmacen);
		}
		else
		if(msg == "ERROR_CERRAR_ALMACEN"){
			alert(mstrProblemaCerrar);
		}
		else
		if(msg == "OK_ENVIAR_INVENTARIO"){
			alert(mstrEnviarInventario);
		}
		else
		if(msg == "ERROR_ENVIAR_INVENTARIO"){
			alert(mstrProblemaEnviarInventario);
		}
		else
		if(msg == "ACTUALIZACION_ENVIAR_INVENTARIO"){
			alert(mstrActEnviarInventario);
		}
		else
		if(msg == "OK_APROBAR_INVENTARIO"){
			alert(mstrAprobarInventario);
		}
		else
		if(msg == "ERROR_APROBAR_INVENTARIO"){
			alert(mstrProblemaAprobarInventario);
		}
		else
		if(msg == "ACTUALIZACION_APROBAR_INVENTARIO"){
			alert(mstrActAprobarInventario);
		}
		else
		if(msg == "OK_RECHAZAR_INVENTARIO"){
			alert(mstrRechazarInventario);
		}
		else
		if(msg == "ERROR_RECHAZAR_INVENTARIO"){
			alert(mstrProblemaRechazarInventario);
		}
		else
		if(msg == "ACTUALIZACION_RECHAZAR_INVENTARIO"){
			alert(mstrActRechazarInventario);
		}
		else
		if(msg == "OK_REGISTRAR_INVENTARIO"){
			alert(mstrRegistrarInventario);
		}
		else
		if(msg == "ALMACEN_ABIERTO"){
			alert(mstrAlmacenAbierto);
		}
		else
		if(msg == "ACTUALIZACION_REGISTRAR_INVENTARIO"){
			alert(mstrActRegistrarInventario);
		}
		else
		if(msg == "ERROR_REGISTRAR_INVENTARIO"){
			alert(mstrProblemaRegistrarInventario);
		}
		document.getElementById("txhMsg").value="";
		document.getElementById("txhOperacion").value="";		
	}

	function fc_Valida(){
		var strExtension;
		var lstrRutaArchivo;
		if(fc_Trim(document.getElementById("txtCV").value) == ""){
				alert(mstrInforme);
					return false;
		}
		else{
			if(fc_Trim(document.getElementById("txtCV").value) != ""){
				lstrRutaArchivo = document.getElementById("txtCV").value;				
				document.getElementById("txhArchivo").value=lstrRutaArchivo;
				strExtension = "";
				strExtension = lstrRutaArchivo.toLowerCase().substring(lstrRutaArchivo.length - 3);
			
				nombre = (lstrRutaArchivo.substring(lstrRutaArchivo.lastIndexOf("\\"))).toLowerCase();				 
			
				if (strExtension != strExtensionXLS && strExtension != strExtensionXLSX){
					alert(mstrExtensionXLS);
					return false;
				}
				else{
					document.getElementById("txhExtCv").value = strExtension;
				}
				document.getElementById("txhExtCv").value = strExtension;				
			}
		}
		return true;
	}
		
	function fc_GrabarArchivos(){
		estadoAlmacen = document.getElementById("txhEstadoAlmacen").value;
		if(estadoAlmacen!=""){
			if(estadoAlmacen=="0"){	
				if (fc_Valida()){
					if ( fc_Trim(document.getElementById("txhExtCv").value) !== ""){
						if (confirm(mstrSeguroRegistrarInventario)){
							document.getElementById("txhOperacion").value="REGISTRAR_INVENTARIO";
							document.getElementById("frmMain").submit();			
						}
					}
				}
			}
			else{
				alert(mstrAlmacenCerrado);
			}
		}		
	}
	function fc_Regresar(){
		document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhCodSede").value;
	}	
	function Fc_EnviarInventario(){
		if(confirm(mstrSeguroEnviarInventario)){
			document.getElementById("txhOperacion").value="ENVIAR_INVENTARIO";
			document.getElementById("frmMain").submit();
		}
	}	
	function Fc_AprobarInventario(){
		if(confirm(mstrSeguroAprobarInventario)){
			document.getElementById("txhOperacion").value="APROBAR_INVENTARIO";
			document.getElementById("frmMain").submit();
		}
	}	
	function Fc_RechazarInventario(){
		if(confirm(mstrSeguroRechazarInventario)){
			document.getElementById("txhOperacion").value="RECHAZAR_INVENTARIO";
			document.getElementById("frmMain").submit();
		}
	}	
	function fc_AbrirAlmacen(){
		estadoAlmacen = document.getElementById("txhEstadoAlmacen").value;
		if(estadoAlmacen!=""){
			if(estadoAlmacen=="0"){
				if(confirm(mstrSeguroAbrirAlmacen)){
					document.getElementById("txhOperacion").value="ABRIR_ALMACEN";
					document.getElementById("frmMain").submit();
				}
			}
			else{
				alert(mstrEstadoAlmacenAbierto);
			}
		}		
	}	
	function fc_CerrarAlmacen(){
		estadoAlmacen = document.getElementById("txhEstadoAlmacen").value;
		if(estadoAlmacen!=""){
			if(estadoAlmacen=="1"){
				if(confirm(mstrSeguroCerrarAlmacen)){
					document.getElementById("txhOperacion").value="CERRAR_ALMACEN";
					document.getElementById("frmMain").submit();
				}
			}
			else{
				alert(mstrEstadoAlmacenCerrado);
			}
		}
	}
	function fc_ImprimeTicket(){		
		codSede=document.getElementById("txhCodSede").value;
		window.open("${ctx}/logistica/actualizarInventario.html?txhCodSede="+codSede+"&txhOpcion=0","Reportes","resizable=yes, menubar=yes");
	}
	function fc_VerInventario(){
		estadoAlmacen = document.getElementById("txhEstadoAlmacen").value;
		if(estadoAlmacen!=""){
			if(estadoAlmacen=="0"){	
				codSede=document.getElementById("txhCodSede").value;
				window.open("${ctx}/logistica/actualizarInventario.html?txhCodSede="+codSede+"&txhOpcion=1","Reportes","resizable=yes, menubar=yes");
			}
			else{
				alert(mstrAlmacenCerrado);
			}
		}
	}	
</script>
</head>
 <form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/ActualizarInventarioFisico.html" 
		enctype="multipart/form-data" method="post">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="extCv" id="txhExtCv"/>
	<form:hidden path="codSede" id="txhCodSede"/>
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="msg" id="txhMsg"/>	
	
	<form:hidden path="archivo" id="txhArchivo"/>
	<form:hidden path="estadoAlmacen" id="txhEstadoAlmacen"/>
	
	<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>
	<br>
	<br>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:0px;margin-top:7px" align="center">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="710px" class="opc_combo">
		 	<font>
		 		Actualizar Inventario F�sico
			</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<table class="tabla" style="width:90%;height: 50px;margin-top:6px;margin-left:0px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2" border="0" bordercolor="red" align="center">
			<tr>
				<td align="center">
					Adjuntar Archivo:&nbsp;&nbsp;&nbsp;
					<input type="file" name="txtCV" class="cajatexto_1" style="width:400px" id="txtCV">
				</td>
			</tr>			
	</table>
	<table cellpadding="2" cellspacing="2" border="0" bordercolor="red" align="center">		
		<tr>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgAbrir','','${ctx}/images/botones/abriralmacen2.jpg',1)">
				<img src="${ctx}/images/botones/abriralmacen1.jpg" style="cursor:hand;" onclick="javascript:fc_AbrirAlmacen();" alt="Abrir Almacen" id="imgAbrir"></a>
			</td>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgCerrar','','${ctx}/images/botones/cerraralmacen2.jpg',1)">
				<img src="${ctx}/images/botones/cerraralmacen1.jpg" style="cursor:hand;" onclick="javascript:fc_CerrarAlmacen();" alt="Cerrar Almacen" id="imgCerrar"></a>				
			</td>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgImprime','','${ctx}/images/botones/imprime2.jpg',1)">
				<img src="${ctx}/images/botones/imprime1.jpg" style="cursor:hand;" onclick="javascript:fc_ImprimeTicket();" alt="Imprime Ticket's" id="imgImprime"></a>
			</td>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgVer','','${ctx}/images/botones/verinventario2.jpg',1)">
				<img src="${ctx}/images/botones/verinventario1.jpg" style="cursor:hand;" onclick="javascript:fc_VerInventario();" alt="Ver Inventario" id="imgVer"></a>
			</td>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgRegistrar','','${ctx}/images/botones/registrarinv2.jpg',1)">
				<img src="${ctx}/images/botones/registrarinv1.jpg" style="cursor:hand;" onclick="javascript:fc_GrabarArchivos();" alt="Registrar Inventario" id="imgRegistrar"></a>
			</td>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgEnviar','','${ctx}/images/botones/enviar2.jpg',1)">
				<img src="${ctx}/images/botones/enviar1.jpg" style="cursor:hand;" onclick="javascript:Fc_EnviarInventario();" alt="Enviar" id="imgEnviar"></a>
			</td>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgAprobar','','${ctx}/images/botones/aprobar2.jpg',1)">
				<img src="${ctx}/images/botones/aprobar1.jpg" style="cursor:hand;" onclick="javascript:Fc_AprobarInventario();" alt="Aprobar" id="imgAprobar"></a>
			</td>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgRechazar','','${ctx}/images/botones/rechazar2.jpg',1)">
				<img src="${ctx}/images/botones/rechazar1.jpg" style="cursor:hand;" onclick="javascript:Fc_RechazarInventario();" alt="Rechazar" id="imgRechazar"></a>
			</td>
		</tr>
	</table>
</form:form>	

