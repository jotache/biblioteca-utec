<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>
		
		function onLoad()
		{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{	
			objMsg.value = "";
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);  
		}
		else if(objMsg.value == "DOBLE"){
		alert(mstrRegistroDoble2);
		}
	
		if ( objMsg.value == "EOK" )
		{
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			alert(mstrElimino);
			window.close();
		}		
		else{ if ( objMsg.value == "EERROR" )
			 {
			  objMsg.value = "";
			  document.getElementById("txhMsg").value = "";
			  alert(mstrProbEliminar);			
			 }
			 else{ if(document.getElementById("txhMsg").value== "ALMACEN_CERRADO")
				   { alert(mstrEstadoLogistica);
				    document.getElementById("txhMsg").value = "";
				   }
				}
			}
		}		
		
	function fc_Grabar(){
	if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)		
	{
			var i = 0;
			cadIndicador = "";
			cadDesripcion = "";
		while (true)
			{
		objIndicador = document.getElementById("txtnroSerie" + i);
		objDescripcion = document.getElementById("txtcodIng" + i);
		if ( objIndicador == null ){ break; }
		
		cadIndicador = cadIndicador + objIndicador.value + "|";
		cadDesripcion = cadDesripcion + objDescripcion.innerHTML + "|";
		i++;
		}
	//	alert("tot: "+i);
		document.getElementById("txhTotPintar").value = i;
		document.getElementById("txhnroSerie").value = cadIndicador;
		document.getElementById("txhcodIng").value = cadDesripcion;
	//	alert(document.getElementById("txhnroSerie").value);
				if (confirm(mstrSeguroGrabar)){
					document.getElementById("txhOperacion").value="GRABAR";
					document.getElementById("frmMain").submit();			
				}
	 }
		  else alert(mstrEstadoLogistica);			
	}
		
	function fc_sel(pos){
	cadDesripcion = "";
	objDescripcion = document.getElementById("txtcodIng" + pos);
	
	document.getElementById("txhcodIngE").value = objDescripcion.innerHTML ;
	}
	function fc_Eliminar(){
	if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)		
	{
		if(document.getElementById("txhcodIngE").value!=""){
		if( confirm(mstrEliminar) ){
				document.getElementById("txhOperacion").value = "ELIMINAR";
				document.getElementById("frmMain").submit();
			document.getElementById("txhOperacion").value = "";
				}
			}
			else{
			alert(mstrSeleccion);
		}
	 }
	  else alert(mstrEstadoLogistica);
	
	}	 
</script>

</HEAD>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/RegistrarNumeroSeries.html">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="falta" id="txhFalta"/>
<form:hidden path="codIng" id="txhcodIng"/>
<form:hidden path="codIngE" id="txhcodIngE"/>
<form:hidden path="nroSerie" id="txhnroSerie"/>
<form:hidden path="preUni" id="txhpreUni"/>
<form:hidden path="codUsuario" id="txhcodUsuario"/>
<form:hidden path="codBien" id="txhcodBien"/>
<form:hidden path="codDocPago" id="txhcodDocPago"/>
<form:hidden path="totPintar" id="txhTotPintar"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codDetalle" id="txhCodDetalle"/>
<form:hidden path="cantEntregada" id="txhCantEntregada"/>
<form:hidden path="estadoAlmacen" id="txhEstadoAlmacen"/>
<form:hidden path="consteEstadoAlmacen" id="txhConsteEstadoAlmacen"/>
		
<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" 
		      style="margin-left:3px;margin-top:7px;margin-left:9px">
				 <tr>
					 <td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 			 <td background="${ctx}/images/Evaluaciones/centro.jpg" width="290px" class="opc_combo">
		 			 	<font style="">
		 			 	Registrar N�meros de Series del Activo					 	
		 				</font>
		 			</td>
		 <td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
				 </tr>
</table>
		 
			 
<div style="overflow: auto;width: 97%;height: 100px;" >
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" 
		   style="width:94%;height:20px; margin-top: 3px; margin-left: 9px;">
		<tr>
			<td colspan="2">
			
				<table cellpadding="0" cellspacing="0" border="1" bordercolor="white" class="tabla" width="100%">
			
				<tr>
						<td class="grilla" width="4%" align="center">Sel.</td>
						<td class="grilla" width="4%" align="center">Nro.</td>
						<td class="grilla" width="12%" align="center">Nro. Serie</td>
					</tr>
				<%int i = 0;%>
				
				<c:forEach var="objCurrent" items="${control.listSeleccion}"
						varStatus="loop">
						
						<c:set var="tam" value="${control.totPintar+1}"  />
						
					<tr  class="tablagrilla" >
					<td width="4%" align="center">									
							 <input type="radio" name="rdoSel" id="rdoSel" 
							 onclick="fc_sel('<c:out value="${loop.index}"/>')" >									
						</td>
					<td width="4%" align="center">
					<label><%=i + 1%></label>
					</td>
						<td align="center">									
						<input type="text" class="cajatexto_o" style="width:100px" maxlength="20" onkeypress="fc_ValidaTextoNumeroGuionEspecial();" size="20" 
							 value="<c:out value="${objCurrent.nroSerie}"/>"  
							id="txtnroSerie<c:out value="${loop.index}"/>" 
							name="txtnroSerie<c:out value="${loop.index}"/>" onblur="fc_ValidaNombreAutorOnblur(this.id,'nro. serie');">	
								</td><td style="display:none">
								<label id="txtcodIng${loop.index}" style="HEIGHT:13px;width:175px;display:none">${objCurrent.codIngreso}</label>
					</td></tr><%i++; %>
					
					</c:forEach>
					
				</table>
				
			</td>			
		</tr>				
	</table>
	</div>
	<br>
	<table align=center style="margin-top:3px;">
			<tr>
			<td>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
			<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:fc_Grabar();" alt="Grabar" id="btnGrabar"></a>
				</td>
				<td>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar2','','${ctx}/images/botones/eliminar2.jpg',1)">
			<img src="${ctx}/images/botones/eliminar1.jpg" style="cursor:hand;" onclick="javascript:fc_Eliminar();" alt="Grabar2" id="btnGrabar2"></a>
				</td>
				<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
				<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" alt="cancelar" id="btnCancelar"></a>
			</td>
			</tr>
		</table>	
</form:form>


