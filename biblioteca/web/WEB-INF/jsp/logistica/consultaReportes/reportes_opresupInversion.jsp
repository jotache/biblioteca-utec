<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.ReportesLogistica'%>
<%@page import='java.math.BigDecimal'%>
<%@page contentType="application/vnd.ms-excel"%>
<%--@ page language="java" errorPage="/error.jsp"--%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator"
	prefix="html"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 
<html>
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.79in .40in .79in .40in;
	mso-header-margin:0cm;
	mso-footer-margin:0cm;}
-->
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
}

.texto_grilla_c {
	FONT-SIZE: 10px; */
	COLOR: #636563; */
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

	.detalleNro {
		FONT-SIZE: 10px;
		/*COLOR: #636563;*/
		COLOR: #000000;
		FONT-FAMILY: Verdana;
		HEIGHT: 18px;
		CURSOR: hand;
		mso-style-parent:style0;
		mso-number-format:Standard;
	}
	
	.detalleNro2 {
		FONT-SIZE: 10px;
		/*COLOR: #636563;*/
		COLOR: #000000;
		FONT-FAMILY: Verdana;
		HEIGHT: 18px;
		CURSOR: hand;
		mso-style-parent:style0;
		mso-number-format:"0\.000";
	}	
	
</style>
<body>

<table class="tabla" width="896">
	<tr>
		<td class="texto_bold" colspan="2">&nbsp;</td>
		<td></td>
		<td></td>
		<td></td><td></td><td></td><td></td><td></td><td></td>
		<td class="texto_bold">${model.FECHA_ACTUAL}</td>
	</tr>

	<tr>
		<td class="texto_bold" colspan="4">TECSUP-${model.NOM_SEDE}</td>

		<td>&nbsp;</td><td></td><td></td><td></td><td></td><td></td>
		<td class="texto_bold">${model.HORA_ACTUAL}</td>
	</tr>
	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>

<table class="tabla" width="896">
	<tr>
		<td class="texto_bold" colspan="11">
		<div align="center" class="texto_bold">Detalle Orden de Compra Tipo de Inversi&oacute;n</div>
		</td>
	</tr>
	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>

<table class="tabla" width="896">
	<tr class="texto_grilla">
		<td></td>
		<td class="texto_bold">
		<div align="right">Del</div>
		</td>
		<td>
		<div align="center" class="texto">
		<div align="left">${model.FECHA_INI}</div>
		</div>
		</td>
		<td class="texto_bold">
		<div align="right">Al</div>
		</td>
		<td>
		<div align="center" class="texto">${model.FECHA_FIN}
		<div align="left"></div>
		</div>
		</td>
		<td></td>
	</tr>

	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>


<table class="tabla" width="896">
	<tr class="texto_grilla">
		<td class="texto_bold">Usuario:</td>
		<td colspan="2" class="texto_bold">${model.NOM_USUARIO}</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>

	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>


<table width="896" border="1" bordercolor="#000000" class="tabla">
	<tr bgcolor="#CCCCCC" class="cabecera_grilla" style="height: 30px;">
		<td><span class="Estilo5">Nro. Orden </span></td>
		<td><span class="Estilo5">Proveedor</span></td>
		<td>Tip. Pago </td>		
		<td><span class="Estilo5">Comprador</span></td>
		<td><span class="Estilo5">CENCOS</span></td>
		<td><span class="Estilo5">Nombre CENCOS&nbsp;</span></td>
		<td><span class="Estilo5">Fec. Aprobaci&oacute;n </span></td>
		<td>Descripci&oacute;n Bien </td>
		<td>Cant.</td>
		<td>P.U.</td>
		<td>Moneda</td>
		
	</tr>

<%
	if ( request.getAttribute("LST_RESULTADO")!= null ) 
	{
	
		List<ReportesLogistica> consulta = (List<ReportesLogistica>)request.getAttribute("LST_RESULTADO");					
		//ReportesLogistica objFuturo =null;			
		String codcencos= "";
		for(ReportesLogistica rpt : consulta){
			codcencos = " " + rpt.getCodCeco();
					
%>
	
		<tr class="texto_grilla">		
			<td style="text-align:left"><%=rpt.getNroOrden()%></td>
			<td style="text-align:left"><%=rpt.getDscProveedor()%></td>
			<td style="text-align:left"><%=rpt.getDscTipoOrden()%></td>
			<td class="detalleNro" style="text-align:left"><%=rpt.getNomSolicitante()%></td>
			<td class="" style="text-align:center"><%=codcencos%></td>
			<td class="detalleNro" style="text-align:left"><%=rpt.getDscCeco()%></td>
			<td class="" style="text-align:center"><%=rpt.getFecAtencion()%></td>		
			<td class="detalleNro" style="text-align:left"><%=rpt.getDscBien()%></td>
			<td class="detalleNro" style="text-align:right"><%=rpt.getCantidad()%></td>		
			<td class="detalleNro" style="text-align:right"><%=rpt.getPrecioUnitario()%></td>
			<td class="detalleNro" style="text-align:left"><%=rpt.getDscMoneda()%></td>			
		</tr>
										
	<%}/*FOR*/
	%>

<%	
  }/*IF*/
%>
</table>

</body>
</html>