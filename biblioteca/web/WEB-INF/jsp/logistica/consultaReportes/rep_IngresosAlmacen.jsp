<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">
var fecha=new Date();
	
	var miFecha=""+fecha.getFullYear()+fecha.getMonth()+fecha.getDate()+fecha.getHours()+fecha.getMinutes()+fecha.getSeconds();
	var miMes=fecha.getMonth();
	var miAnio=fecha.getFullYear();
	var Mes=parseInt(miMes)+1;
		function onLoad(){
		fc_JuegoBandeja();
		document.getElementById("codMeses").value=Mes;
		document.getElementById("codAnios").value=document.getElementById("txhPeriodoActual").value
		}
		
		function fc_Familia(){
		}
		
		function fc_Etapa(param){
		
		switch(param){
			case '1':
			
			   	document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienConsumible").value ; 
			    if(document.getElementById("txhCodTipoReporte").value!=document.getElementById("txhCodReporte2").value)
			      {	document.getElementById("txhOperacion").value = "FAMILIA";
	   			 	document.getElementById("frmMain").submit();
	   			  }
	   			
			    break;
			case '2':
			    
			    document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienActivo").value ; 
			    if(document.getElementById("txhCodTipoReporte").value!=document.getElementById("txhCodReporte2").value)
			      {	document.getElementById("txhOperacion").value = "FAMILIA";
	   			 	document.getElementById("frmMain").submit();
	   			  }
	   			
			   	break;
			   	
			}
		}
		
		function fc_TipoGastos(){
		}
		
		function fc_JuegoBandeja(){
		   if(document.getElementById("txhCodTipoReporte").value==document.getElementById("txhCodReporte1").value)
		   {
		     Familia.style.display = 'inline-block';
		     Familia1.style.display = 'inline-block';
		     reporte1.style.display = 'inline-block';
		     periodo2.style.display = 'inline-block';
		   }
		   else{ if(document.getElementById("txhCodTipoReporte").value==document.getElementById("txhCodReporte2").value)
		   		{
		   			Reporte_2.style.display = 'inline-block';
		   			periodo2.style.display = 'inline-block';
		   		}
		   		else{
		   		 if(document.getElementById("txhCodTipoReporte").value==document.getElementById("txhCodReporte3").value)
		   		 {  Reporte_3.style.display = 'inline-block';
		   			Familia.style.display = 'inline-block';
		   			Familia1.style.display = 'inline-block';
		   			periodo2.style.display = 'inline-block';
		 		 }
		 		 else{
		 		  if(document.getElementById("txhCodTipoReporte").value==document.getElementById("txhCodReporte4").value)
		   			{
		   				Familia.style.display = 'inline-block';
		   				Familia1.style.display = 'inline-block';
		   				tblMaestro.style.display = 'inline-block';
		  			}
		  			else{
		  			  if(document.getElementById("txhCodTipoReporte").value==document.getElementById("txhCodReporte6").value)
		   			  { Familia.style.display = 'inline-block';
		   				  Familia1.style.display = 'inline-block';
		   				  Reporte_3.style.display = 'inline-block';
		   				  Reporte_2.style.display = 'inline-block';
		   				  periodo2.style.display = 'inline-block';
		 			  }else{
			 			  if(document.getElementById("txhCodTipoReporte").value==document.getElementById("txhCodReporte11").value){
				 			  //...
			 				periodo2.style.display = 'inline-block';
			 				tipoBien1.style.display = 'none';
			 				tipoBien2.style.display = 'none';
			 			  }
		 			  }
		  			}
		 		}
		   	  }
		   }
		}
		
		function fc_Sede(){
		 if(document.getElementById("txhCodTipoReporte").value!=document.getElementById("txhCodReporte3").value
		    && document.getElementById("txhCodTipoReporte").value!=document.getElementById("txhCodReporte4").value)
		  { document.getElementById("txhOperacion").value = "SEDE";
	   	    document.getElementById("frmMain").submit();
	   	  }
		}
		
		function fc_Generar(){
		url="";
		opcion=document.getElementById("txhCodTipoReporte").value;
		
		switch(opcion){
			case document.getElementById("txhCodReporte1").value:
				
				var ban="";
				ban=fc_Val_IngresoAlmacen();
				if(ban=="0")
				{url="?operacion=" + document.getElementById("txhCodTipoReporte").value +
						"&txhCodSede=" + document.getElementById("codSede").value +
						"&txhCodTipo=" + document.getElementById("txhConsteRadio").value +
						"&txhCodFamilia=" + document.getElementById("codFamilia").value +
						"&txhFecIni=" + document.getElementById("txtFecInicio").value +
						"&txhFecFin=" + document.getElementById("txtFecFinal").value +
						"&txhRadio=" + document.getElementById("txhValRadioRep").value +
						"&txhNomSede=" + frmMain.codSede.options[frmMain.codSede.selectedIndex].text +
						"&txhDscUsuario=" + document.getElementById("txhDscUsuario").value;
				}
				else{  if(ban=="2") alert(mstrFechaInicio);
				       else{ if(ban=="3") alert(mstrFechaFin);
				             else alert(mstrFecha);
				           }
				}
				break;
				
			case document.getElementById("txhCodReporte2").value:
				var ban="";
				ban=fc_Val_IngresoAlmacen();
				if(ban=="0")
				{ url="?operacion=" + document.getElementById("txhCodTipoReporte").value +
						"&txhCodSede=" + document.getElementById("codSede").value +
						"&txhCodTipo=" + document.getElementById("txhConsteRadio").value +
						"&txhCodCentroCosto=" + document.getElementById("codTipoCentroCosto").value +
						"&txhDscUsuario=" + document.getElementById("txhDscUsuario").value +
						"&txhCodUsuario=" + document.getElementById("txhCodUsuario").value +
						"&txhCodTipoGasto=" + document.getElementById("codTipoGastos").value +
						"&txhFecIni=" + document.getElementById("txtFecInicio").value +
						"&txhFecFin=" + document.getElementById("txtFecFinal").value +
						"&txhNomSede=" + frmMain.codSede.options[frmMain.codSede.selectedIndex].text ;
				}
				else{  if(ban=="2") alert(mstrFechaInicio);
				       else{ if(ban=="3") alert(mstrFechaFin);
				             else alert(mstrFecha);
				           }
				}
				break;
			
			case document.getElementById("txhCodReporte3").value:
				var ban="";
				ban=fc_Val_IngresoAlmacen();
				if(ban=="0")	
				{
				url="?operacion=" + document.getElementById("txhCodTipoReporte").value +
						"&txhCodSede=" + document.getElementById("codSede").value +
						"&txhCodTipo=" + document.getElementById("txhConsteRadio").value +
						"&txhCodBien=" + document.getElementById("codBien").value +
						"&txhCodFamilia=" + document.getElementById("codFamilia").value +
						"&txhDscUsuario=" + document.getElementById("txhDscUsuario").value +
						"&txhCodUsuario=" + document.getElementById("txhCodUsuario").value +
						"&txhFecIni=" + document.getElementById("txtFecInicio").value +
						"&txhFecFin=" + document.getElementById("txtFecFinal").value +
						"&txhNomSede=" + frmMain.codSede.options[frmMain.codSede.selectedIndex].text ;
				}
				else{  if(ban=="2") alert(mstrFechaInicio);
				       else{ if(ban=="3") alert(mstrFechaFin);
				             else alert(mstrFecha);
				           }
				}
				break;
			//ALQD,22/09/09.LISTAR SALDO > 0
			case document.getElementById("txhCodReporte4").value:
				 var ban="";
				 var indConSaldo="";
				 ban=fc_Val_IngresoAlmacen();
				 if(document.getElementById("chkConSaldo").checked){
				 	indConSaldo="1";
				 }else
				 {
				 	indConSaldo="";
				 }
				 if(ban=="0")
				 {
						url="?operacion=" + document.getElementById("txhCodTipoReporte").value +
						"&txhCodSede=" + document.getElementById("codSede").value +
						"&txhCodTipo=" + document.getElementById("txhConsteRadio").value +
						"&txhCodFamilia=" + document.getElementById("codFamilia").value +
						"&txhDscUsuario=" + document.getElementById("txhDscUsuario").value +
						"&txhCodUsuario=" + document.getElementById("txhCodUsuario").value +
						"&txhCodPeriodo=" + document.getElementById("codAnios").value+"/"+document.getElementById("codMeses").value+
						"&txhNomSede=" + frmMain.codSede.options[frmMain.codSede.selectedIndex].text +
						"&txhConSaldo="+ indConSaldo;
				 }
				else{  if(ban=="2") alert(mstrFechaInicio);
				       else{ if(ban=="3") alert(mstrFechaFin);
				             else alert(mstrFecha);
				           }
				}
				break;
				
			case document.getElementById("txhCodReporte6").value:
				var ban="";
				ban=fc_Val_IngresoAlmacen();
				if(ban=="0")
				{
				url="?tCodRep=" + document.getElementById("txhCodTipoReporte").value +
				"&tCodSede=" + document.getElementById("codSede").value +
				"&tCodTipoBien=" + document.getElementById("txhConsteRadio").value +
				"&tCodFamilia=" + document.getElementById("codFamilia").value +
				"&tCodBien=" + document.getElementById("codBien").value +
				"&tCodCeco=" + document.getElementById("codTipoCentroCosto").value +
				"&tCodTipoGasto=" + document.getElementById("codTipoGastos").value +
				"&tCodUsu=" + document.getElementById("txhCodUsuario").value +
				"&tFecIni=" + document.getElementById("txtFecInicio").value +
				"&tFecFin=" + document.getElementById("txtFecFinal").value +
				"&txhDscUsuario=" + document.getElementById("txhDscUsuario").value;
				
				}
				else{  if(ban=="2") alert(mstrFechaInicio);
				       else{ if(ban=="3") alert(mstrFechaFin);
				             else alert(mstrFecha);
				           }
				}
				break;

			case document.getElementById("txhCodReporte11").value:
				var ban="";
				ban=fc_Val_IngresoAlmacen();
				if(ban=="0")
				{
					url="?operacion=" + document.getElementById("txhCodTipoReporte").value +
					"&txhCodSede=" + document.getElementById("codSede").value +												
					"&txhDscUsuario=" + document.getElementById("txhDscUsuario").value +
					"&txhCodUsuario=" + document.getElementById("txhCodUsuario").value +
					"&txhFecIni=" + document.getElementById("txtFecInicio").value +
					"&txhFecFin=" + document.getElementById("txtFecFinal").value +
					"&txhNomSede=" + frmMain.codSede.options[frmMain.codSede.selectedIndex].text ;
				
				}
				else{  if(ban=="2") alert(mstrFechaInicio);
				       else{ if(ban=="3") alert(mstrFechaFin);
				             else alert(mstrFecha);
				           }
				}
				break;
			
		}
	   if(url!="")
		{if(document.getElementById("txhCodTipoReporte").value!=document.getElementById("txhCodReporte6").value)
		  window.open("${ctx}/logistica/reporteLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");		  
		else
		 window.open("${ctx}/logistica/reportesLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");		
	   }
	}
	
	function fc_Val_IngresoAlmacen(){
	 var rpta="1";
	  if(document.getElementById("txtFecInicio").value!="" && document.getElementById("txtFecFinal").value!="")
	   {  rpta=fc_Validar_Fechas(document.getElementById("txtFecInicio").value, document.getElementById("txtFecFinal").value);
	   }
	   else{ if(document.getElementById("txtFecInicio").value=="")
	          rpta="2";
	         else rpta="3";
	     }
	   return rpta;
	}
	
	function fc_Validar_Fechas(srtFechaInicio, srtFechaFin)
	{
	 var num=0;
	 var srtFecha="0";
	 if( srtFechaInicio!="" && srtFechaFin!="")
	{
		    num=fc_ValidaFechaIniFechaFin(srtFechaFin, srtFechaInicio);
		     //es 1 si el parametro 1 es mayor al parametro 2
		     //es 0 si el parametro 1 es menor o igual al parametro 2
		     if(num==1) srtFecha="0";
		     else srtFecha="1";
	 }
	 else{ if(srtFechaInicio=="")
	          srtFecha="2";
	       else srtFecha="3";
	     } 
    return srtFecha;
	}
	
	function fc_EtapaRep(param){
	   
	   switch(param){
			case '1':
			  
			   	document.getElementById("txhValRadioRep").value=document.getElementById("txhConsteReporteAlmacen").value ; 
			 	break;
			 	
			case '2':
			    
			    document.getElementById("txhValRadioRep").value=document.getElementById("txhConsteReporteAlmacenDev").value ;
			  	break;
			}
	}
	//ALQD,22/09/09.AGREGANDO UN CONTROL MAS (CON SALDO)
	function fc_Limpiar(){
		document.getElementById("codFamilia").value="";
		document.getElementById("codBien").value="";
		document.getElementById("codTipoCentroCosto").value="";
		document.getElementById("codTipoGastos").value="";
		document.getElementById("codSede").value=document.getElementById("txhConsteSede").value;
		document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienConsumible").value;
		document.getElementById("txhValRadioRep").value=document.getElementById("txhConsteReporteAlmacen").value;
		document.getElementById("chkConSaldo").value="";
		document.getElementById("txhOperacion").value = "LIMPIAR";
		document.getElementById("frmMain").submit();
	}
	
	function fc_TipoSugerencia(){
	}
</script>
</head>
<body>
<form:form name="frmMain" commandName="control" action="${ctx}/logistica/repIngresosAlmacen.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="dscUsuario" id="txhDscUsuario"/>
<form:hidden path="consteRadio" id="txhConsteRadio"/>
<form:hidden path="consteBienConsumible" id="txhConsteBienConsumible"/>
<form:hidden path="consteBienActivo" id="txhConsteBienActivo"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="nroReporte" id="txhNroReporte"/>
<form:hidden path="tituloReporte" id="txhTituloReporte"/>
<form:hidden path="codTipoReporte" id="txhCodTipoReporte"/>
<form:hidden path="codReporte1" id="txhCodReporte1"/>
<form:hidden path="codReporte2" id="txhCodReporte2"/>
<form:hidden path="codReporte3" id="txhCodReporte3"/>
<form:hidden path="codReporte4" id="txhCodReporte4"/>
<form:hidden path="codReporte6" id="txhCodReporte6"/>
<form:hidden path="codReporte11" id="txhCodReporte11"/>
<form:hidden path="valRadioRep" id="txhValRadioRep"/>
<form:hidden path="consteReporteAlmacen" id="txhConsteReporteAlmacen"/>
<form:hidden path="consteReporteAlmacenDev" id="txhConsteReporteAlmacenDev"/>
<form:hidden path="consteSede" id="txhConsteSede"/>
<form:hidden path="periodoActual" id="txhPeriodoActual"/>

<table cellpadding="1" cellspacing="0"  ID="Table1" style="width:98%; margin-top:10px;" 
	border="0" bordercolor="Red" background="${ctx}/images/Logistica/back.jpg" class="tabla">
	<tr>
		<td width="10%">&nbsp;&nbsp;
				<c:if test="${control.codTipoReporte!=control.codReporte11}">Sede :</c:if></td>
	    <td width="25%">
			<c:if test="${control.codTipoReporte==control.codReporte11}">Sede:&nbsp;&nbsp;&nbsp;</c:if>
	    	<form:select path="codSede" id="codSede" cssClass="combo_o"
				cssStyle="width:100px" onchange="javascript:fc_Sede();">
				<c:if test="${control.listaSedes!=null}">
					<form:options itemValue="codSede" itemLabel="dscSede"
						items="${control.listaSedes}" />
				</c:if>
			</form:select>
		</td>
	    <td align="left" colspan="2" id="reporte1" style="display: none;">
			<table id="radio1" name="radio1" bordercolor="blue" border="0" width="100%">
				<tr>
					<td width="4%"><input Type="radio" checked name="rdoRep"
						<c:if test="${control.valRadioRep==control.consteReporteAlmacen}"><c:out value=" checked=checked " /></c:if>
						id="gbradio3" onclick="javascript:fc_EtapaRep('1');">&nbsp;Bien/Servicio&nbsp;
						<input Type="radio" name="rdoRep"
						<c:if test="${control.valRadioRep==control.consteReporteAlmacenDev}"><c:out value=" checked=checked " /></c:if>
						id="gbradio4" onclick="javascript:fc_EtapaRep('2');">&nbsp;Devoluciones&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
    <tr>
    	<td ><span style="display: inline;" id="tipoBien1">&nbsp;&nbsp;Tipo Bien :</span></td>
	    <td align="left" style="display: inline;" id="tipoBien2">			
			<table id="radio1" name="radio1" bordercolor="blue" border="0" width="100%">
				<tr>
					<td width="4%"><input Type="radio" checked name="rdoEtapa"
						<c:if test="${control.consteRadio==control.consteBienConsumible}"><c:out value=" checked=checked " /></c:if>
						id="gbradio1" onclick="javascript:fc_Etapa('1');">&nbsp;Consumible&nbsp;
						<input Type="radio" name="rdoEtapa"
						<c:if test="${control.consteRadio==control.consteBienActivo}"><c:out value=" checked=checked " /></c:if>
						id="gbradio2" onclick="javascript:fc_Etapa('2');">&nbsp;Activo&nbsp;
					</td>
				</tr>
			</table>
		</td>
		<td colspan="1" style="display: none;" id="periodo2">&nbsp;&nbsp;Per�odo Inicial:&nbsp;&nbsp;&nbsp;&nbsp;
			<form:input path="fecInicio" id="txtFecInicio" cssClass="cajatexto_o"
				onkeypress="javascript:fc_Slash('frmMain','txtFecInicio','/');"
				onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio');"
				cssStyle="width:70px" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
						align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>&nbsp;&nbsp;Periodo Final:&nbsp;
			<form:input path="fecFinal" id="txtFecFinal" cssClass="cajatexto_o"
				onkeypress="javascript:fc_Slash('frmMain','txtFecFinal','/');"
				onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal');"
				cssStyle="width:70px" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
						align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>
		</td>
		<td colspan="1" id="tblMaestro" style="display: none;" id="periodo1">&nbsp;&nbsp;Periodo :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<form:select path="codMeses" id="codMeses" cssClass="combo_o" cssStyle="width:180px" onchange="javascript:fc_TipoSugerencia();">
				<c:if test="${control.codListaMeses!=null}">
				<form:options itemValue="id" itemLabel="name" items="${control.codListaMeses}" />
				</c:if>
			</form:select>&nbsp;&nbsp;&nbsp;
			<form:select path="codAnios" id="codAnios" cssClass="combo_o" cssStyle="width:80px"  onchange="javascript:fc_TipoSugerencia();">
				<c:if test="${control.codListaAnios!=null}">
				<form:options itemValue="id" itemLabel="name" items="${control.codListaAnios}" />
				</c:if>
			</form:select>&nbsp;&nbsp;&nbsp;
			<input type="checkbox" id="chkConSaldo"/>&nbsp;<label id="lblConSaldo">Saldo &#62; 0</label>
		  </td>
    </tr>
	<tr>
		<td colspan="1" style="display: none;" id="Familia" width="10%">&nbsp;&nbsp;Familia :</td>
	    <td style="display: none;" id="Familia1" width="25%">
	    	<form:select path="codFamilia" id="codFamilia" cssClass="cajatexto"
				cssStyle="width:180px" onchange="javascript:fc_Familia();">
				<form:option  value="">--Todos--</form:option>
				<c:if test="${control.listaCodFamilia!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
						items="${control.listaCodFamilia}" />
				</c:if>
			</form:select>
		</td>
	    <td style="display: none;" id="Reporte_3">&nbsp;&nbsp;Codigo del Bien :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    	<form:input path="codBien" id="codBien" cssClass="cajatexto"
				onkeypress="fc_PermiteNumeros();" onblur="fc_ValidaNumeroOnBlur(this.id);"
				cssStyle="width:65px" maxlength="10"/>
		</td>
	</tr>
	<tr style="display: none;" id="Reporte_2">
		<td colspan="2">&nbsp;&nbsp;Centro Costo :&nbsp;&nbsp;
	    	<form:select path="codTipoCentroCosto" id="codTipoCentroCosto" cssClass="cajatexto"
				cssStyle="width:180px" onchange="javascript:fc_TipoGastos();">
				<form:option  value="">--Seleccione--</form:option>
				<c:if test="${control.listaCodTipoCentroCosto!=null}">
					<form:options itemValue="codTecsup" itemLabel="descripcion"
						items="${control.listaCodTipoCentroCosto}" />
				</c:if>
			</form:select>
		</td>
		<td colspan="1">&nbsp;&nbsp;Tipo Gastos : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    	<form:select path="codTipoGastos" id="codTipoGastos" cssClass="cajatexto" 
				cssStyle="width:180px" onchange="javascript:fc_TipoGastos();">
				<form:option  value="">--Seleccione--</form:option>
				<c:if test="${control.listaCodTipoGastos!=null}">
				<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
					items="${control.listaCodTipoGastos}" />
				</c:if>
			</form:select>
		</td>
	    <td><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
				<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle"
					 style="cursor: pointer; 60px"
					 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
		</td>
	</tr>
</table>

<table align="center" style="margin-top: 10px">
	<tr>
		<td>
			<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar','','${ctx}/images/botones/generar2.jpg',1)">
				<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar"></a>
		</td>
	</tr>
</table>

</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});	
</script>
</body>
</html>