<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.DetalleOrden'%>
<%@page import="com.tecsup.SGA.modelo.CondicionCotizacion"%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<%@page import="java.text.DecimalFormat"%>
<html>

<style >
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.79in .40in .79in .40in;
	mso-header-margin:0cm;
	mso-footer-margin:0cm;}
-->
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:"0\.000";
}

.totalNro {
    /*ALQD,12/11/08. A�adiendo un estilo mas.*/
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:"0\.00";
}
</style>

<body >
<table>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td><img src="/SGA/images/logoTecsup.jpg" width="100" height="27"></td>
	<td colspan="2" align="center" style="font-size:12pt; vertical-align: middle">&nbsp;&nbsp;<u><b>${model.tituloOrden}</b></u></td>
	<td colspan="2" rowspan="2" align="center" style="font-size:12pt; vertical-align: middle">

		<c:if test="${model.codEstadoOC=='0003'}">
			<img src="${ctx}/images/sellooc.jpg" width="100px" height="100px">
		</c:if>

	</td>
	<td align="center" style="font-size:12pt; vertical-align: middle">&nbsp;</td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td></td>
	<td></td><td></td><td></td><td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td></td>
	<td></td>
</tr>
<tr>
	<td align="left" style="font-size:8pt">Nro. Orden </td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.nroOrd}</td>
	<td></td>
	<td></td>
    <td></td>
	<td align="left" style="font-size:8pt">Cotizaci�n</td>
	<td colspan="2" align="left" style="font-size:8pt"><b>:</b> ${model.numCotizacion}</td>
	<td align="right" valign="top">&nbsp;</td>
	<td align="right" valign="top">&nbsp;</td>
</tr>
<tr><td align="left" style="font-size:8pt">Fec. Emisi�n </td>
<td align="left" style="font-size:8pt"><b>:</b> ${model.fEmi}</td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
  <td align="left" style="font-size:8pt">Estado</td>
  <td align="left" style="font-size:8pt"><b>:</b> ${model.desEstadoOC}</td>
  <td align="left" style="font-size:8pt" colspan="4">&nbsp;</td>
  <td></td>
	<td></td>
	<td></td>
	<td></td>	
</tr>
<tr>
	<td align="left" style="font-size:8pt">Proveedor </td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.nroDoc}</td>
	<td align="left" style="font-size:8pt" colspan="4"><b></b> ${model.prov}</td>
<td></td>
<td></td>
<td></td>
<td></td>
  </tr>
<tr><td align="left" style="font-size:8pt">Atencion A </td>
	<td align="left" style="font-size:8pt" colspan="3"><b>:</b> ${model.atenc}</td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
	<td align="left" style="font-size:8pt">Moneda </td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.mond}</td>
	<td align="left" style="font-size:8pt">IGV </td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.igv}</td>
	<td align="left" style="font-size:8pt">Monto </td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.mont}</td>
	<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr><td align="left" style="font-size:8pt">Comprador </td>
	<td align="left" colspan="3" style="font-size:8pt"><b>:</b> ${model.comprador}</td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr><td align="left" style="font-size:8pt">Cen.Costos </td>
	<td align="left" colspan="7" style="font-size:8pt"><b>:</b> ${model.cadCenCosto}</td>
    <td>.</td>

    <td>
		.
	<c:choose>
	  <c:when test="${model.codSede=='L'}"><img src="/SGA/images/Logistica/mem_lima.jpg" ></c:when>
	  <c:when test="${model.codSede=='A'}"><img src="/SGA/images/Logistica/mem_arequipa.jpg" ></c:when>
	  <c:when test="${model.codSede=='T'}"><img src="/SGA/images/Logistica/mem_trujillo.jpg" ></c:when>
	  <c:otherwise>&nbsp;</c:otherwise>
	</c:choose>		

	</td>
	
</tr>
</table>
<br>
<table align="center">
	<tr>
		<td>
			<table border="1" align="center">
			<tr rowspan="2">
				<td class="cabecera_grilla" style="width:20px" nowrap="nowrap">C�digo</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" colspan="3">Producto</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Unidad</td>
				<td class="cabecera_grilla" style="width:40px" nowrap="nowrap" >Cantidad</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Precio</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >SubTotal</td>
				
			  </tr>
			<%
				List alumno = (List)request.getSession().getAttribute("consulta");
				//double l = Long.valueOf("0").longValue();
				long l = Long.valueOf("0").longValue();
				if(alumno != null){
					for(int j = 0; j < alumno.size(); j++){
						DetalleOrden rep = (DetalleOrden)alumno.get(j);
						l = l + (long)(Double.valueOf(rep.getSubtotal()).doubleValue() * 1000);						
						
				%>
				<%if (rep.getDescripcion() != null){ 
					//System.out.println("alumno: "+alumno.size());
				%>
				<tr class="texto_grilla">
					<td width="30%" style="text-align:right"><%=rep.getCodBien()%></td>
					<td width="30%" style="text-align:left" colspan="3"><%=rep.getDescripcion()%></td>
					<td width="30%" style="text-align:center"><%=rep.getDscUnidad()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getCantidad()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getPrecio()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getSubtotal()%></td>
					
				</tr>
				<%}
					}
				}request.removeAttribute("consulta"); 
				%>	
				
				<tr class="cabecera_grilla">
					<td width="30%" style="text-align:left" colspan="4">&nbsp;</td>
					<td width="30%" style="text-align:center">&nbsp;</td>
					<td width="30%" style="text-align:right">&nbsp;</td>
					<td width="30%" style="text-align:right">&nbsp;Total:</td>
					<td width="30%" class="totalNro" style="text-align:right">&nbsp;<%=String.valueOf(Double.valueOf(l).doubleValue()/1000)%> </td>
					
				</tr>
				
			</table>
		</td>
	</tr>
</table>
<br>

<%-- 
<table align="center">
<tr>

<td></td>
	<td style="width:2pt" colspan="2">Detalle Importaci�n</td>
	<td></td>
	<td></td>
	<td></td>
</tr>

	<tr>
		<td></td>
		<td>
			<table border="1" align="center">
			<tr rowspan="2">
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >N�mero</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fecha</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Descripcion</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Monto</td>
				</tr>
			<%
				List documentos = (List)request.getSession().getAttribute("listar");
			System.out.println("tama�o: "+documentos.size());
				if(documentos != null){
					for(int j = 0; j < documentos.size(); j++){
						DocumentoAsociado rep = (DocumentoAsociado)documentos.get(j);
						System.out.println("getDpdo_id : "+rep.getDpdo_id());
				%>
				<%if (rep.getDpdo_id() != null){ 
					System.out.println("listar: "+documentos.size());
				%>
				<tr class="texto_grilla">
					<td width="30%" style="text-align:center"><%=rep.getNumero()%></td>
					<td width="30%" style="text-align:left"><%=rep.getFecha()%></td>
					<td width="30%" style="text-align:left"><%=rep.getDescripcion()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getMonto()%></td>
				</tr>
				<%}
					}
				}request.removeAttribute("listar"); 
				%>	
			</table>
		</td>
		<td></td>
	</tr>
</table>
--%>

<br>
<br>
<table>
	<tr>
		<td style="font-size:8pt"><strong><u>Detalles Comerciales</u></strong></td>
	</tr>	
	<tr>
		<td height="4"></td>
	</tr>	
	<tr>						
		<td>
			<table border="1" width="600px">
			<tr>
				<td class="cabecera_grilla" width="50px">Tipo<br>Condici�n</td>
				<td class="cabecera_grilla" width="550px" colspan="6" >Descripci�n Final</td>
			</tr>			
			<%
			List alumno1 = (List)request.getSession().getAttribute("condComercial");
			if(alumno1 != null){
				for(int j = 0; j < alumno1.size(); j++){
					DetalleOrden detConCom = (DetalleOrden)alumno1.get(j);
			%>
			<%if (detConCom.getDscCondicion() != null){%>
			<tr class="texto_grilla">
				<td valign="top" style="text-align:left"><%=detConCom.getDscCondicion()%></td>							
				<td colspan="6" valign="top" style="text-align:left"><%=detConCom.getDescripcion()%></td>
			</tr>
			
			<%}
				}
			}request.getSession().removeAttribute("condComercial"); 
			%>	
		  </table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>
