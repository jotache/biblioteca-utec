<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.AsignarResponsable'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr>
	<td></td>
	<td colspan="2"><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="5" align="center" class="texto_bold"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="right" colspan="2" class="texto_bold">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="2"></td>
	<td colspan="5" align="center">Reporte de Carga de Trabajo	</td>
	<td align="right" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold" colspan="2">TECSUP-${model.NOM_SEDE}</td>
	</tr>
<tr class="texto">
	<td></td>
	<td><b>Usuario :</b></td>
	<td colspan="5" align="left" class="texto_bold">${model.usuario}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td class="texto_bold" colspan="6">Nro. Requerimiento: ${model.Nro_Req}</td>
	<td class="texto_bold" colspan="3">Definir Otro Responsable:  ${model.Flag}</td>
	<td></td>
</tr>
<%  
	if(request.getAttribute("flagBandeja")!=null){
		if(request.getAttribute("flagBandeja").equals("1")){
%>
<tr class="texto_bold">
	<td></td>
	<td class="texto_bold" colspan="4">Centro de Costo: ${model.Centro_Costo}</td>
	<td class="texto_bold" colspan="4">Nombres: ${model.Nombres}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td class="texto_bold" colspan="4">Apellido Paterno: ${model.Ap_Paterno} </td>
	<td class="texto_bold" colspan="4">Apellido Materno: ${model.Ap_Materno} </td>
	<td></td>
</tr>

<%   }
		}

%>


<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
		  <%  
			if(request.getAttribute("flagBandeja")!=null){
				if(request.getAttribute("flagBandeja").equals("1")){
					
					%>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" colspan="3" width="10%">Nombres</td>
				<td class="cabecera_grilla" colspan="3" width="10%">Centro De Costos</td>
				<td class="cabecera_grilla" colspan="3" width="10%">Cargo</td>
											
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consultaReporteCarga")!=null){
				List consulta = (List) request.getSession().getAttribute("consultaReporteCarga"); 
					for(int i=0;i<consulta.size();i++){
					
						AsignarResponsable mad  = (AsignarResponsable) consulta.get(i);%>
					<tr class="texto_grilla">
						<td style="text-align:left" colspan="3"><%=mad.getNombreUsu()==null?"":mad.getNombreUsu()%></td>
						<td style="text-align:left" colspan="3"><%=mad.getValor1()==null?"":mad.getValor1()%></td>
						<td style="text-align:left" colspan="3"><%=mad.getValor2()==null?"":mad.getValor2()%></td>
											
					</tr>
					<%}
				}
				request.removeAttribute("consultaReporteCarga"); 
			 %>
			</table>
				<%}
			else{  
				if(request.getAttribute("flagBandeja").equals("0")){
					
				%>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" colspan="3" width="30%">Responsable</td>
				<td class="cabecera_grilla" colspan="3" width="50%">Total Trab. Asignado</td>
				<td class="cabecera_grilla" colspan="3" width="20%">Prom. Total Esfuerzo</td>
											
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consultaReporteCarga")!=null){
				List consulta = (List) request.getSession().getAttribute("consultaReporteCarga"); 
					for(int i=0;i<consulta.size();i++){
					
						AsignarResponsable frog  = (AsignarResponsable) consulta.get(i);%>
					<tr class="texto_grilla">
						<td style="text-align:left" colspan="3"><%=frog.getNombreUsu()==null?"":frog.getNombreUsu()%></td>
						<td style="text-align:right" colspan="3"><%=frog.getValor1()==null?"":frog.getValor1()%></td>
						<td style="text-align:left" colspan="3"><%=frog.getValor2()==null?"":frog.getValor2()%></td>
											
					</tr>
					<%}
				}
				request.removeAttribute("consultaReporteCarga"); 
			 %>
			</table>
				<%}
					
				}
			}
				
			%>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>