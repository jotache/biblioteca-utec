<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.modelo.ReportesLogistica'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.79in .40in .79in .40in;
	mso-header-margin:0cm;
	mso-footer-margin:0cm;}
-->
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
	mso-number-format:"\@";
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
.detalleNro {
               FONT-SIZE: 10px;
               /*COLOR: #636563;*/
               COLOR: #000000;
               FONT-FAMILY: Verdana;
               HEIGHT: 18px;
               CURSOR: hand;
               mso-style-parent:style0;
               mso-number-format:Standard;
       }
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td colspan="2"><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="11" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="right" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td colspan="2"></td>
	<td colspan="11" align="center">Reporte de Acumulado de Salidas por Art�culo	</td>
	<td align="right" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td class="texto_bold" colspan="2">TECSUP-${model.NOM_SEDE}</td>
	<td colspan="11" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
	<td><b>Usuario :</b></td>
	<td colspan="3" align="left">${model.usuario}</td>
	<td></td>
</tr>
<tr class="texto">
	<td><b>Del :</b></td>
	<td colspan="2" align="left">${model.periodoInicio}</td>
	<td></td>
</tr>
<tr class="texto">
	<td><b>Al :</b></td>
	<td colspan="2" align="left">${model.periodoFin}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td colspan="13" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="5%">Tipo Bien</td>
				<td class="cabecera_grilla" width="5%">Fam.-Subfamilia</td>
				<td class="cabecera_grilla" width="5%">Descripci�n Familia</td>
				<td class="cabecera_grilla" width="5%">C�digo del bien</td>
				<td class="cabecera_grilla" width="5%">Descripci�n del bien</td>
				<td class="cabecera_grilla" width="5%">C. Costo</td>
				<td class="cabecera_grilla" width="5%">Nombre del C. Costo</td>
				
				<td class="cabecera_grilla" width="10%">Responsable C.C.</td>
				<td class="cabecera_grilla" width="5%">Tipo Gasto</td>
				<td class="cabecera_grilla" width="5%">Nombre T. Gasto</td>
				<td class="cabecera_grilla" width="5%">Moneda</td>
				<td class="cabecera_grilla" width="5%">Total Cargado</td>
				<td class="cabecera_grilla" width="5%">Unid.Medida</td>
				<td class="cabecera_grilla" width="5%">Cant. Consumida</td>
				<td class="cabecera_grilla" width="10%">Precio Promedio</td>
				
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
				List consulta = (List) request.getSession().getAttribute("consulta");
				DecimalFormat df = new DecimalFormat("0.00");
				DecimalFormat df1 = new DecimalFormat("0.00000");
					for(int i=0;i<consulta.size();i++){
					
						ReportesLogistica obj  = (ReportesLogistica) consulta.get(i);%>
					<tr class="texto_grilla">
						<td style="text-align:left"><%=obj.getDscTipoBien()==null?"":obj.getDscTipoBien()%></td>
						<td style="text-align:left"><%=obj.getCodFamilia()==null?"":obj.getCodFamilia()%></td>
						<td style="text-align:left"><%=obj.getDscFamilia()==null?"":obj.getDscFamilia()%></td>
						<td style="text-align:center"><%=obj.getCodBien()==null?"":obj.getCodBien()%></td>
						<td style="text-align:left"><%=obj.getDscBien()==null?"":obj.getDscBien()%></td>
						<td style="text-align:center"><%=obj.getCodCeco()==null?"":obj.getCodCeco()%></td>
						<td style="text-align:left"><%=obj.getDscCeco()==null?"":obj.getDscCeco()%></td>
						
						<td style="text-align:left"><%=obj.getResponsableCeco()==null?"":obj.getResponsableCeco()%></td>
						<td style="text-align:center"><%=obj.getCodTipoGasto()==null?"":obj.getCodTipoGasto()%></td>
						<td style="text-align:left"><%=obj.getDscTipoGasto()==null?"":obj.getDscTipoGasto()%></td>
						<td style="text-align:center"><%=obj.getDscMoneda()==null?"":obj.getDscMoneda()%></td>
						<td class="detalleNro" style="text-align:right"><%=df.format(Float.valueOf(obj.getTotalCargado()==""?"0.00":obj.getTotalCargado()))%></td>
						<td style="text-align:left"><%=obj.getDscUnidad()==null?"":obj.getDscUnidad()%></td>
						
						<td class="detalleNro" style="text-align:right"><%=df.format(Float.valueOf(obj.getCantConsumida()==""?"0.00":obj.getCantConsumida()))%></td>
						<td style="text-align:right"><%=obj.getPrecioPromedio()==""?"0.00000":obj.getPrecioPromedio()%></td>
						
					</tr>
					<%}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
	</tr>
</table>

</body>
</html>
