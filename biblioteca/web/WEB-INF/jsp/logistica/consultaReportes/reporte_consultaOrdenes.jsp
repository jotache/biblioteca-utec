<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.CotizacionProveedor'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td> 
	<td colspan="2"><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="5" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="right" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="2"></td>
	<td colspan="5" align="center">Reporte de Consulta de Ordenes	</td>
	<td align="right" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold" colspan="2">TECSUP-${model.NOM_SEDE}</td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Usuario :</b></td>
	<td colspan="2" align="left">${model.usuario}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td class="texto_bold" colspan="4">Nro. Orden: ${model.Nro_Orden}</td>
	<td class="texto_bold" colspan="3">Fecha Emisi�n: ${model.FechaIni}</td>
	<td class="texto_bold" colspan="1">Al &nbsp;&nbsp;${model.FechaFin}</td>
	<td class="texto_bold" colspan="2">Estado: &nbsp;${model.Tipo_Estado}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td class="texto_bold" colspan="4">Proveedor: ${model.NroProveedor} ${model.NomProveedor}</td>
	<td class="texto_bold" colspan="5">Comprador: ${model.NroComprador} ${model.NomComprador}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td class="texto_bold" colspan="4">Tipo Orden: &nbsp;&nbsp;${model.Tipo_Orden}</td>
	<td class="texto_bold" colspan="4"></td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="10%">Nro. Orden</td>
				<td class="cabecera_grilla" width="10%">Fec. Emisi�n</td>
				<td class="cabecera_grilla" width="10%">Proveedor</td>
				<td class="cabecera_grilla" width="10%">Moneda</td>
				<td class="cabecera_grilla" width="10%">Monto</td>
				<td class="cabecera_grilla" width="10%">Comprador</td>
				<td class="cabecera_grilla" width="10%">Tipo Orden</td>
				
				<td class="cabecera_grilla" width="10%">Estado</td>
				<td class="cabecera_grilla" width="10%">Detalle Estado</td>
											
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consultaReporteOrdenes")!=null){
				List consulta = (List) request.getSession().getAttribute("consultaReporteOrdenes"); 
					for(int i=0;i<consulta.size();i++){
					
						CotizacionProveedor obj  = (CotizacionProveedor) consulta.get(i);%>
					<tr class="texto_grilla">
						<td style="text-align:center"><%=obj.getDscNroOrden()==null?"":obj.getDscNroOrden()%></td>
						<td style="text-align:center"><%=obj.getFechaEmision()==null?"":obj.getFechaEmision()%></td>
						<td style="text-align:left"><%=obj.getDscProveedor()==null?"":obj.getDscProveedor()%></td>
						<td style="text-align:center"><%=obj.getDscMoneda()==null?"":obj.getDscMoneda()%></td>
						<td style="text-align:right"><%=obj.getDscMonto()==null?"":obj.getDscMonto()%></td>
						<td style="text-align:left"><%=obj.getDescripcion()==null?"":obj.getDescripcion()%></td>
						<td style="text-align:left"><%=obj.getDscTipoOrden()==null?"":obj.getDscTipoOrden()%></td>
						
						<td style="text-align:left"><%=obj.getDscEstado()==null?"":obj.getDscEstado()%></td>
						<td style="text-align:left"><%=obj.getDetalleCondicion()==null?"":obj.getDetalleCondicion()%></td>
											
					</tr>
					<%}
				}
				request.removeAttribute("consultaReporteOrdenes"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>