<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.CotizacionDetalle'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page import="com.tecsup.SGA.modelo.CondicionCotizacion"%>
<html>

<style>
<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>
<!-- ALQD,23/04/09. MODIFICANDO EL FORMATO A EXPORTAR -->
<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr>
	<td colspan="1"><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="2" class="texto_bold" align="center"><u>Solicitud de Cotización</u></td>
	<td align="right" class="texto_bold" style="font-size:8pt">Fecha</td>
	<td align="left" class="texto" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr>
	<td colspan="1"></td>
	<td></td><td></td>
	<td align="right" class="texto_bold" style="font-size:8pt">Hora</td>
	<td align="left" class="texto" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td class="texto_bold" colspan="1">TECSUP-${model.NOM_SEDE}</td>
	<td colspan="2" align="left"></td>
</tr>
<tr>
<td></td>
</tr>
<tr class="texto">
	<td><b>Nro. Cotizacion :</b></td>
	<td colspan="1" align="left">${model.NroCo}</td>
	<td></td>
</tr>
<tr class="texto">
	<td><b>Tipo Pago :</b></td>
	<td align="left">${model.tPago}</td>
	</tr>
<tr class="texto">
	<td><b>Periodo Inicio :</b></td>
	<td  align="left">${model.Vc}  ${model.Hi}</td>
	</tr>
<tr class="texto">
<td><b>Periodo Fin :</b></td>
	<td  align="left">${model.Vc1}  ${model.Hi1}</td>
</tr>

<!--ALQD,23/04/09. AÑADIENDO DATOS DEL COMPRADOR, GRUPO Y TIPO DE SERVICIO -->
<tr class="texto_bold">
	<td align="left">Comprador :</td>
	<td colspan="2">${model.dscUsuarioSolicitante}</td>
</tr>
<tr>
	<td class="texto_bold" align="left">Servicio :</td>
	<td colspan="2">${model.gruServicio}</td>
</tr>
<tr>
	<td class="texto_bold" align="left">Tipo :</td>
	<td colspan="2">${model.tipServicio}</td>
</tr>
</table>
<br>
<!-- ALQD,24/04/09. AGREGANDO LOS COMENTARIOS INGRESADOS POR LOS USUARIOS -->
<table>
	<tr>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="10%">Nro. Item</td>
				<td class="cabecera_grilla" colspan="4" width="90%">Nombre del Servicio</td>
			</tr>
			
			<%
	List alumno = (List)request.getSession().getAttribute("consulta");
	if(alumno != null){
		for(int j = 0; j < alumno.size(); j++){
			CotizacionDetalle asist = (CotizacionDetalle)alumno.get(j);
	%>
	<%if (asist.getCodBien() != null){%>
	<tr class="texto_grilla">
		<td width="20%" style="text-align:center"><%=++j%></td>
		<td width="80%" colspan="4" style="text-align:left"><%=asist.getCantidad()%></td>
	</tr>
		<%if (asist.getDescripcionCot() != null){%>
			<tr class="texto_grilla">
				<td></td>
				<td width="100%" colspan="4" style="text-align:left">
					<%=asist.getDescripcionCot()%>
				</td>
			</tr>
		<%} %>
	<%}
		}
	}request.getSession().removeAttribute("consulta");
	%>	
			</table>
		</td>
	</tr>
</table>
<br><br>
<!-- ALQD,23/04/09. MOSTRANDO LAS CONDICIONES COMERCIALES -->
<table>
	<tr>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="25%">Tipo Condición</td>
				<td class="cabecera_grilla" colspan="2" width="40%">Descripción Inicial</td>
				<td class="cabecera_grilla" colspan="2" width="35%">Descripción Final</td>
			</tr>
			
			<%
	List alumno1 = (List)request.getSession().getAttribute("condComercial");
	if(alumno1 != null){
		for(int j = 0; j < alumno1.size(); j++){
			CondicionCotizacion condicion = (CondicionCotizacion)alumno1.get(j);
	%>
	<%if (condicion.getCodCondicion() != null){%>
	<tr class="texto_grilla">
		<td width="50%" valign="top" style="text-align:left"><%=condicion.getDesTipoCondicion()%></td>
		<td width="50%" colspan="2" valign="top" style="text-align:left"><%=condicion.getDetalleInicial()%></td>
		<td width="50%" colspan="2" valign="top" style="text-align:left"><%=condicion.getDetalleFinal()%></td>
	</tr>
	
	<%}
		}
	}request.getSession().removeAttribute("condComercial");
	%>
			</table>
		</td>
	</tr>
</table>

</body>
</html>
