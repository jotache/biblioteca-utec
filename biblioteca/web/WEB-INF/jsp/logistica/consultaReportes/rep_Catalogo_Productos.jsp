<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.CatalogoProducto'%>
<%@page import='java.util.StringTokenizer'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" colspan="4"><u><b>REPORTE CATALOGO DE PRODUCTOS</b></u></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td><td></td><td></td><td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
</tr>
</table>
<br>
<table >
<tr>
	<td class="texto_bold">Sede</td>
	<td align="left" class="texto">: ${model.dscSede} </td>
</tr>
<tr>
	<td class="texto_bold">Familia</td>
	<td align="left" class="texto">: ${model.dscFamilia} </td>
	<td class="texto_bold">SubFamilia</td>
	<td align="left" class="texto">: ${model.dscSubFamilia} </td>
</tr>
<tr>
	<td class="texto_bold">Cod. Producto</td>
	<td align="left" class="texto">: ${model.codProducto} </td>
	<td class="texto_bold">Producto</td>
	<td align="left" class="texto">: ${model.dscProduc} </td>
</tr>
<tr>
	<td class="texto_bold">Condicion</td>
	<td align="left" class="texto">: ${model.dscCondicion} </td>
	<td class="texto_bold">Nro. Serie</td>
	<td align="left" class="texto">: ${model.nroSerie} </td>
</tr>
</table><br><br>
<table align="center">
	<tr>
		<td>
			<table border="1" align="center">
			<tr rowspan="2">
				<td class="cabecera_grilla" style="width:90px" nowrap="nowrap" >Familia</td>
				<td class="cabecera_grilla" style="width:90px" nowrap="nowrap" >Sub Familia</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >C�digo </td>				
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Producto</td>
				<td class="cabecera_grilla" style="width:120px" nowrap="nowrap">unidad</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Stock<br>Actual</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Stock<br>Disponible</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Precio<br>Unitario</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Condici�n</td>																			
				</tr>
			<%
				List alumno = (List)request.getSession().getAttribute("consulta");
				if(alumno != null){
					for(int j = 0; j < alumno.size(); j++){
						CatalogoProducto rep = (CatalogoProducto)alumno.get(j);
				%>
				<%if (rep.getCodProducto() != null){%>
				<tr class="texto_grilla">
					<td width="33%" style="text-align:center"><%=rep.getFamilia()%></td>
					<td width="33%" style="text-align:center"><%=rep.getSubFamilia()%></td>
					<td width="30%" style="text-align:center"><%=rep.getCodProducto()%></td>
					<td width="30%" style="text-align:left"><%=rep.getNomProducto()%></td>
					<td width="30%" style="text-align:left"><%=rep.getNomUnidad()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getStockActual()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getStockDisponible()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getPrecio()%></td>
					<td width="30%" style="text-align:left"><%=rep.getCondicion()%></td>
				</tr>
				<%}
					}
				}request.removeAttribute("consulta");
				%>	
			</table>
		</td>
	</tr>
</table>
</body>
</html>