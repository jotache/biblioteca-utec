<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.modelo.InterfazContableDetalle'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<!-- ALQD,02/07/09.CUANDO COLOCO EL FORMATO .tipoCambioNro EL NUMERO ES MOSTRADO CON COMA DECIMAL �? -->
<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
	
	mso-number-format:"\@";
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
.detalleNro {
               FONT-SIZE: 10px;
               /*COLOR: #636563;*/
               COLOR: #000000;
               FONT-FAMILY: Verdana;
               HEIGHT: 18px;
               CURSOR: hand;
               mso-style-parent:style0;
               mso-number-format:Standard;
       }
.tipoCambioNro {
               FONT-SIZE: 10px;
               /*COLOR: #636563;*/
               COLOR: #000000;
               FONT-FAMILY: Verdana;
               HEIGHT: 18px;
               CURSOR: hand;
               mso-style-parent:style0;
               mso-number-format:"0\.000";
       }

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >

<table>
	<tr>		
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="5%">DSUBDIA</td>
				<td class="cabecera_grilla" width="5%">DCOMPRO</td>
				<td class="cabecera_grilla" width="5%">DSECUE</td>
				<td class="cabecera_grilla" width="5%">DFECCOM</td>
				<td class="cabecera_grilla" width="5%">DCUENTA</td>
				<td class="cabecera_grilla" width="5%">DCODANE</td>
				<td class="cabecera_grilla" width="5%">DCENCOS</td>
				
				<td class="cabecera_grilla" width="5%">DCODMON</td>
				<td class="cabecera_grilla" width="5%">DDH</td>
				<td class="cabecera_grilla" width="5%">DIMPORT</td>
				<td class="cabecera_grilla" width="5%">DTIPDOC</td>
				<td class="cabecera_grilla" width="5%">DNUMDOC</td>
				
				<td class="cabecera_grilla" width="5%">DFECDOC</td>
				<td class="cabecera_grilla" width="5%">DFECVEN</td>
				<td class="cabecera_grilla" width="5%">DAREA</td>
				<td class="cabecera_grilla" width="5%">DFLAG</td>
				<td class="cabecera_grilla" width="5%">DDATE</td>
				<td class="cabecera_grilla" width="5%">DXGLOSA</td>
				<td class="cabecera_grilla" width="5%">DUSIMPOR</td>
				
				<td class="cabecera_grilla" width="5%">DMNIMPOR</td>
				<td class="cabecera_grilla" width="5%">DCODARC</td>
				
				<td class="cabecera_grilla" width="5%">DFECCOM2</td>
				<td class="cabecera_grilla" width="5%">DFECDOC2</td>
				<td class="cabecera_grilla" width="5%">DFECVEN2</td>
				<td class="cabecera_grilla" width="5%">DCODANE2</td>
				<td class="cabecera_grilla" width="5%">DVANEXO</td>
				<td class="cabecera_grilla" width="5%">DVANEXO2</td>
				<td class="cabecera_grilla" width="5%">DTIPCAM</td>
				<td class="cabecera_grilla" width="5%">DCANTID</td>
					
			</tr>
			
			<%  
				if(request.getSession().getAttribute("lista_cabecera")!=null){
				System.out.println("Reporte xD");
				List consulta = (List) request.getSession().getAttribute("lista_cabecera"); 
				DecimalFormat df = new DecimalFormat("0.00");
				for(int i=0;i<consulta.size();i++){
					InterfazContableDetalle obj  = (InterfazContableDetalle) consulta.get(i);%>
					<tr class="texto_grilla">
					    
						<td style="text-align:right"><%=obj.getDsubdia()%></td>
						<td style="text-align:right"><%=obj.getDcompro()%></td>
						<td style="text-align:right"><%=obj.getDsecue()%></td>
						<td style="text-align:center"><%=obj.getDfeccom()%></td>
						<td style="text-align:right"><%=obj.getDcuenta()%></td>
						<td style="text-align:left"><%=obj.getDcodane()%></td>
						<td style="text-align:center"><%=obj.getDcencos()%></td>
						
						<td style="text-align:center"><%=obj.getDcodmon()%></td>
						<td style="text-align:center"><%=obj.getDdh()%></td>
						<td class="detalleNro" style="text-align:right"><%=obj.getDimport()==""?"0.00":obj.getDimport()%></td>
						<td style="text-align:left"><%=obj.getDtipdoc()%></td>
						<td style="text-align:left"><%=obj.getDnumdoc()%></td>
						<td style="text-align:left"><%=obj.getDfecdoc()%></td>
						<td style="text-align:center"><%=obj.getDfecven()%></td>
						
						<td style="text-align:left"><%=obj.getDarea()%></td>
						<td style="text-align:center"><%=obj.getDflag()%></td>
						<td style="text-align:center"><%=obj.getDdate()%></td>
						<td style="text-align:right"><%=obj.getDxglosa()%></td>
						<td class="detalleNro" style="text-align:right"><%=obj.getDusimpor()==""?"0.00":obj.getDusimpor()%></td>
						<td class="detalleNro" style="text-align:right"><%=obj.getDmnimpor()==""?"0.00":obj.getDmnimpor()%></td>
						<td style="text-align:center"><%=obj.getDcodarc()%></td>
						<td style="text-align:center"><%=obj.getDfeccom2()%></td>
						<td style="text-align:center"><%=obj.getDfecdoc2()%></td>
						<td style="text-align:left"><%=obj.getDfecven2()%></td>
						<td style="text-align:center"><%=obj.getDcodane2()%></td>
						<td style="text-align:left"><%=obj.getDvanexo()%></td>
						<td style="text-align:center"><%=obj.getDvanexo2()%></td>
						<td class="detalleNro" style="text-align:right"><%=obj.getDtipcam()==""?"0.000":obj.getDtipcam()%></td>
						<td style="text-align:center"><%=obj.getDcantid()%></td>
						
					</tr>
					<%}
				}
				request.removeAttribute("lista_cabecera"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>