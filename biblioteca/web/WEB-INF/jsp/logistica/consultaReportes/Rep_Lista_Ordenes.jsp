<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.CotizacionProveedor'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" colspan="5"><u><b>GUIAS DE SALIDA</b></u></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td colspan="2" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td><td></td><td></td><td></td><td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
	<table>	
	<tr>
	<td></td>
	<td class="texto_bold">Nro. Orden</td>
	<td align="left" class="texto">: ${model.nroOrden} </td>
	<td></td>
	<td class="texto_bold">Fecha Emisi�n</td>
	<td align="left" class="texto">: ${model.periodoIni} </td>
	<td align="left" class="texto">: ${model.periodoFin} </td>
	<td class="texto_bold">Estado</td>
	<td align="left" class="texto">: ${model.estado} </td>
	</tr>
	<tr >
	<td></td>
	<td class="texto_bold">Proveedor</td>
	<td align="left" class="texto">: ${model.nroRucProv} </td>
	<td align="left" class="texto">: ${model.nombreProv} </td>
	<td class="texto_bold">Fecha Comprador</td>
	<td align="left" class="texto">: ${model.nroRucComp} </td>
	<td align="left" class="texto">: ${model.nombreComp} </td>
	<td class="texto_bold">tipo Orden</td>
	<td align="left" class="texto">: ${model.tipoOrden} </td>	
	</tr>
	<table align="center">
	<tr>
		<td></td>
		
	<table border="1" align="center">
	<tr rowspan="2">
				<td class="cabecera_grilla" style="width:180px">Nro. Orden.</td>
				<td class="cabecera_grilla" style="width:180px">Fec. Emisi�n</td>
				<td class="cabecera_grilla" style="width:180px">Proveedor</td>							
				<td class="cabecera_grilla" style="width:180px">Moneda</td>
				<td class="cabecera_grilla" style="width:180px">Monto</td>
				<td class="cabecera_grilla" style="width:180px">Comprador</td>		
				<td class="cabecera_grilla" style="width:180px">Tipo Orden</td>							
				<td class="cabecera_grilla" style="width:180px">Estado</td>
				<td class="cabecera_grilla" style="width:180px">Detalle Estado</td>	
	</tr>
	<%
	List alumno = (List)request.getSession().getAttribute("consulta");
	if(alumno != null){
		for(int j = 0; j < alumno.size(); j++){
			CotizacionProveedor asist = (CotizacionProveedor)alumno.get(j);
	%>
	<%if (asist.getCodOrden() != null){%>
	<tr class="texto_grilla">
		<td width="50%"  style="text-align:right"><%=asist.getDscNroOrden()%></td>
		<td width="50%"  style="text-align:right"><%=asist.getFechaEmision()%></td>
		<td width="50%"  style="text-align:left"><%=asist.getDscProveedor()%></td>
		<td width="50%"  style="text-align:center"><%=asist.getDscMoneda()%></td>
		<td width="50%" class="detalleNro" style="text-align:right"><%=asist.getDscMonto()%></td>
		<td width="50%"  style="text-align:left"><%=asist.getDescripcion()%></td>
		<td width="50%"  style="text-align:left"><%=asist.getDscTipoOrden()%></td>
		<td width="50%"  style="text-align:left"><%=asist.getDscEstado()%></td>
		<td width="50%"  style="text-align:left"><%=asist.getDetalleCondicion()%></td>
	</tr>
	
	<%}
		}
	}request.getSession().removeAttribute("consulta"); 
	%>	
	</table></tr></table>
</body>
</html>