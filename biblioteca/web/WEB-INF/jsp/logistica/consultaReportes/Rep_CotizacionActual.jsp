<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.CotizacionDetalle'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page import="com.tecsup.SGA.modelo.CondicionCotizacion"%>
<html>

<style>
<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
	VERTICAL-ALIGN: top;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	VERTICAL-ALIGN: middle;
	BORDER: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	BORDER: 1px solid black;
	VERTICAL-ALIGN: top;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
	VERTICAL-ALIGN: top;
}
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr>
	<td colspan="1"><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="2" class="texto_bold" align="center"><u>Solicitud de Cotizaci�n</u></td>
	<td></td>
	<td align="right" class="texto_bold" style="font-size:8pt">Fecha</td>
	<td align="left" class="texto" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
	<td></td>
</tr>
<tr>
	<td colspan="1"></td>
	<td></td><td></td><td></td>
	<td align="right" class="texto_bold" style="font-size:8pt">Hora</td>
	<td align="left" class="texto" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td class="texto_bold" colspan="2">TECSUP-${model.NOM_SEDE}</td>
	<td colspan="2" align="left"></td>
	<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr class="texto">
	<td><b>Nro. Cotizacion :</b></td>
	<td colspan="2" align="left">${model.NroCo}</td>
	<td></td>
</tr>
<tr class="texto">
	<td><b>Tipo Pago :</b></td>
	<td align="left">${model.tPago}</td>
	</tr>
<tr class="texto">
	<td><b>Periodo Inicio :</b></td>
	<td  align="left">${model.Vc}  ${model.Hi}</td>
	</tr>
<tr class="texto">
<td><b>Periodo Fin :</b></td>
	<td  align="left">${model.Vc1}  ${model.Hi1}</td>
</tr>
<!--ALQD, A�ADIENDO DATOS DEL COMPRADOR -->
<tr class="texto_bold">
	<td align="left">Comprador :</td>
	<td colspan="2">${model.dscUsuarioSolicitante}</td>
</tr>
</table>
<br>
<!-- ALQD,23/04/09. ELIMINANDO UNA COLUMNA DERECHA -->
<table>
	<tr>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="25%">C�digo</td>
				<td class="cabecera_grilla" colspan="2" width="50%">Producto</td>
				<td class="cabecera_grilla" width="25%">Uni. Med.</td>
				<td class="cabecera_grilla" width="25%">Cant.</td>
				<td class="cabecera_grilla" width="25%">Nro. Prov.</td>
			</tr>
			
			<%
	List alumno = (List)request.getSession().getAttribute("consulta");
	if(alumno != null){
		for(int j = 0; j < alumno.size(); j++){
			CotizacionDetalle asist = (CotizacionDetalle)alumno.get(j);
	%>
	<%if (asist.getCodBien() != null){%>
	<tr class="texto_grilla">
		<td width="50%"  style="text-align:center"><%=asist.getCodBien()%></td>
		<td width="50%" colspan="2" style="text-align:left"><%=asist.getProducto()%></td>
		<td width="50%"  style="text-align:left"><%=asist.getUnidadMedida()%></td>
		<td width="50%" class="detalleNro"  style="text-align:right"><%=asist.getCantidad()%></td>
		<td width="50%" class="detalleNro" style="text-align:right"><%=asist.getNroProveedores()%></td>
	</tr>
	
	<%}
		}
	}//request.getSession().removeAttribute("consulta");
	%>
			</table>
		</td>
	</tr>
</table>
<!-- ALQD,24/04/09. A�ADIENDO LOS COMENTARIOS INGRESADOS -->
<br>
<table>
	<tr>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="25%">C�digo</td>
				<td class="cabecera_grilla" colspan="4" width="50%">Comentario</td>
			</tr>
			
			<%
	//List alumno = (List)request.getSession().getAttribute("consulta");
	if(alumno != null){
		for(int j = 0; j < alumno.size(); j++){
			CotizacionDetalle asist = (CotizacionDetalle)alumno.get(j);
	%>
	<%if (asist.getDescripcionCot() != null && asist.getDescripcionCot() != ""){%>
		<tr class="texto_grilla">
			<td width="20%"  style="text-align:center"><%=asist.getCodBien()%></td>
			<td width="80%" colspan="4" style="text-align:left"><%=asist.getDescripcionCot()%></td>
		</tr>
	
	<%}
		}
	}request.getSession().removeAttribute("consulta");
	%>
			</table>
		</td>
	</tr>
</table>
<br><br>
<!-- ALQD,21/09/08. MOSTRANDO LAS CONDICIONES COMERCIALES -->
<table>
	<tr>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="25%">Tipo Condici�n</td>
				<td class="cabecera_grilla" colspan="2" width="40%">Descripci�n Inicial</td>
				<td class="cabecera_grilla" colspan="3" width="35%">Descripci�n Final</td>
			</tr>
			
			<%
	List alumno1 = (List)request.getSession().getAttribute("condComercial");
	if(alumno1 != null){
		for(int j = 0; j < alumno1.size(); j++){
			CondicionCotizacion condicion = (CondicionCotizacion)alumno1.get(j);
	%>
	<%if (condicion.getCodCondicion() != null){%>
	<tr class="texto_grilla">
		<td width="50%" valign="top" style="text-align:left"><%=condicion.getDesTipoCondicion()%></td>
		<td width="50%" colspan="2" valign="top" style="text-align:left"><%=condicion.getDetalleInicial()%></td>
		<td width="50%" colspan="3" valign="top" style="text-align:left"><%=condicion.getDetalleFinal()%></td>
	</tr>
	
	<%}
		}
	}request.getSession().removeAttribute("condComercial"); 
	%>	
			</table>
		</td>
	</tr>
</table>

</body>
</html>
