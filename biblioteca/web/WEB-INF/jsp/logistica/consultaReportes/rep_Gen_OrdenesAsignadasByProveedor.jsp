<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.modelo.ReportesLogistica'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
	mso-number-format:"\@";
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
.detalleNro {
               FONT-SIZE: 10px;
               /*COLOR: #636563;*/
               COLOR: #000000;
               FONT-FAMILY: Verdana;
               HEIGHT: 18px;
               CURSOR: hand;
               mso-style-parent:style0;
               mso-number-format:Standard;
       }
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td> 
	<td colspan="2"><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="13" align="center"><u>SGA - SISTEMA LOGISTICA</u></td>
	<td align="right" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="2"></td>
	<td colspan="13" align="center">Ordenes Asignadas por Proveedor	</td>
	<td align="right" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td class="texto_bold" colspan="2">TECSUP-${model.NOM_SEDE}</td>
	<td colspan="6" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Usuario :</b></td>
	<td colspan="3" align="left">${model.usuario}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Del :</b></td>
	<td colspan="2" align="left">${model.periodoInicio}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Al :</b></td>
	<td colspan="2" align="left">${model.periodoFin}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="15" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="5%">Tipo de Orden</td>
				<td class="cabecera_grilla" width="5%">Tipo de bien</td>
				<td class="cabecera_grilla" width="5%">N�m. Orden</td>
				<td class="cabecera_grilla" width="5%">Fec. Aprobaci�n</td>
				<td class="cabecera_grilla" width="5%">Fec. Cierren</td>
				<td class="cabecera_grilla" width="5%">RUC Proveedor</td>
				<td class="cabecera_grilla" width="5%">Local o Extranjera</td>
				
				<td class="cabecera_grilla" width="10%">Raz�n Social</td>
				<td class="cabecera_grilla" width="5%">Descripci�n del bien</td>
				<td class="cabecera_grilla" width="5%">Unid. Medida</td>
				<td class="cabecera_grilla" width="5%">Cantidad</td>
				<td class="cabecera_grilla" width="5%">Moneda</td>
				<td class="cabecera_grilla" width="5%">Costo Unitario</td>
				<td class="cabecera_grilla" width="5%">Importe del bien o servicio</td>
				<td class="cabecera_grilla" width="10%">Estado de la O/C</td>
				<td class="cabecera_grilla" width="5%">Descripci�n Final de la condici�n de compra</td>
				<td class="cabecera_grilla" width="10%">Tipo de Pago</td>
								
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
				List consulta = (List) request.getSession().getAttribute("consulta"); 
				DecimalFormat df = new DecimalFormat("0.00");
				DecimalFormat df1 = new DecimalFormat("0.00000");
					for(int i=0;i<consulta.size();i++){
					
						ReportesLogistica obj  = (ReportesLogistica) consulta.get(i);%>
					<tr class="texto_grilla">
						<td style="text-align:left"><%=obj.getDscTipoOrden()==null?"":obj.getDscTipoOrden()%></td>
						<td style="text-align:left"><%=obj.getDscTipoBienServ()==null?"":obj.getDscTipoBienServ()%></td>
						<td style="text-align:center"><%=obj.getNroOrden()==null?"":obj.getNroOrden()%></td>
						<td style="text-align:right"><%=obj.getFechaAprobaReq()==null?"":obj.getFechaAprobaReq()%></td>
						<td style="text-align:right"><%=obj.getFechaCierre()==null?"":obj.getFechaCierre()%></td>
						<td style="text-align:right"><%=obj.getRucProveedor()==null?"":obj.getRucProveedor()%></td>
						<td style="text-align:left"><%=obj.getIndiceLocal()==null?"":obj.getIndiceLocal()%></td>
						<td style="text-align:left"><%=obj.getDscRazonSocial()==null?"":obj.getDscRazonSocial()%></td>
						<td style="text-align:left"><%=obj.getDscBienServicio()==null?"":obj.getDscBienServicio()%></td>
						<td style="text-align:left"><%=obj.getDscUnidad()==null?"":obj.getDscUnidad()%></td>
						<td style="text-align:right"><%=df.format(Float.valueOf(obj.getCantidad()==""?"0.00":obj.getCantidad()))%></td>
						<td style="text-align:left"><%=obj.getDscMoneda()==null?"":obj.getDscMoneda()%></td>
						<td style="text-align:right"><%=obj.getCostoUnitario()==""?"0.00000":obj.getCostoUnitario()%></td>
						<td style="text-align:right"><%=df.format(Float.valueOf(obj.getImporteBienServ()==""?"0.00":obj.getImporteBienServ()))%></td>
						<td style="text-align:left"><%=obj.getDscEstadoOC()==null?"":obj.getDscEstadoOC()%></td>
						<td style="text-align:left"><%=obj.getDscCondicionFinal1()==null?"":obj.getDscCondicionFinal1()%></td>
						<td style="text-align:left"><%=obj.getDscTipoPago()==null?"":obj.getDscTipoPago()%></td>
						
					</tr>
					<%}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>