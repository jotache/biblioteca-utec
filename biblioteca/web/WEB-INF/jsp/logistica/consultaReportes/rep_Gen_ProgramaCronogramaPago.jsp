<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.modelo.ReportesLogistica'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
.detalleNro {
               FONT-SIZE: 10px;
               /*COLOR: #636563;*/
               COLOR: #000000;
               FONT-FAMILY: Verdana;
               HEIGHT: 18px;
               CURSOR: hand;
               mso-style-parent:style0;
               mso-number-format:Standard;
       }
</style>
<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td> 
	<td colspan="2"><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="10" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="right" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="2"></td>
	<td colspan="10" align="center">Reporte de Programa (Cronograma) de Pagos	</td>
	<td align="right" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td class="texto_bold" colspan="2">TECSUP-${model.NOM_SEDE}</td>
	<td colspan="2" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td colspan="1"><b>Usuario :</b> </td>
	<td colspan="3" align="left">${model.usuario}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td colspan="1"><b>Del :</b></td>
	<td colspan="2" align="left">${model.periodoInicio}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td colspan="1"><b>Al :</b></td>
	<td colspan="2" align="left">${model.periodoFin}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="12" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="5%">Tipo Per�odo</td>
				<td class="cabecera_grilla" width="5%">Cencos</td>
				<td class="cabecera_grilla" width="5%">Tipo de<br/>Orden</td>
				<td class="cabecera_grilla" width="5%">N�m. Orden</td>
				<td class="cabecera_grilla" width="5%">Fec.<br/>Aprobaci�n</td>
				<td class="cabecera_grilla" width="5%">Tipo de Pago</td>
				<td class="cabecera_grilla" width="5%">Estado Orden</td>
				<td class="cabecera_grilla" width="5%">N�m.<br/>Comprobante</td>
				<td class="cabecera_grilla" width="5%">Estado<br/>Comprobante</td>
				<td class="cabecera_grilla" width="10%">RUC Proveedor</td>
				<td class="cabecera_grilla" width="5%">Raz�n Social del Proveedor</td>
				<td class="cabecera_grilla" width="5%">Fec.<br/>Emisi�n</td>
				<td class="cabecera_grilla" width="5%">Fec.<br/>Recepci�n</td>
				<td class="cabecera_grilla" width="5%">Fec.<br/>Vencimiento</td>
				<td class="cabecera_grilla" width="5%">Moneda</td>
				<td class="cabecera_grilla" width="5%">Importe</td>
			</tr>
			<%if(request.getSession().getAttribute("consulta")!=null){
				List consulta = (List) request.getSession().getAttribute("consulta"); 
				DecimalFormat df = new DecimalFormat("0.00");
					for(int i=0;i<consulta.size();i++){
						ReportesLogistica obj  = (ReportesLogistica) consulta.get(i);%>
					<tr class="texto_grilla">
						<td style="text-align:left"><%=obj.getDscTipoPeriodo()==null?"":obj.getDscTipoPeriodo()%></td>
						<td style="text-align:left"><%=obj.getNomSolicitante()==null?"":obj.getNomSolicitante()%></td>
						<td style="text-align:left"><%=obj.getDscTipoOrden()==null?"":obj.getDscTipoOrden()%></td>
						<td style="text-align:center"><%=obj.getNroOrden()==null?"":obj.getNroOrden()%></td>
						<td style="text-align:right"><%=obj.getFechaAprobaReq()==null?"":obj.getFechaAprobaReq()%></td>
						<td style="text-align:left"><%=obj.getDscTipoPago()==null?"":obj.getDscTipoPago()%></td>
						<td style="text-align:left"><%=obj.getDscEstadoOrden()==null?"":obj.getDscEstadoOrden()%></td>
						<td style="text-align:center"><%=obj.getNroComprobante()==null?"":obj.getNroComprobante()%></td>
						<td style="text-align:center"><%=obj.getDscEstDocPago()==null?"":obj.getDscEstDocPago()%></td>
						<td style="text-align:right"><%=obj.getRucProveedor()==null?"":obj.getRucProveedor()%></td>
						<td style="text-align:left"><%=obj.getDscProveedor()==null?"":obj.getDscProveedor()%></td>
						<td style="text-align:right"><%=obj.getFechaEmision()==null?"":obj.getFechaEmision()%></td>
						<td style="text-align:right"><%=obj.getFechaRecepcion()==null?"":obj.getFechaRecepcion()%></td>
						<td style="text-align:right"><%=obj.getFechaVencimiento()==null?"":obj.getFechaVencimiento()%></td>
						<td style="text-align:center"><%=obj.getDscMoneda()==null?"":obj.getDscMoneda()%></td>
						<td style="text-align:right"><%=df.format(Float.valueOf(obj.getDscImporte()==""?"0.00":obj.getDscImporte()))%></td>
					</tr>
					<%}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>
</body>
</html>