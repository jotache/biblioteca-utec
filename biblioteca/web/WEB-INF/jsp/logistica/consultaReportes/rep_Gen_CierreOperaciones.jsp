<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.modelo.ReportesLogistica'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
.detalleNro {
               FONT-SIZE: 10px;
               /*COLOR: #636563;*/
               COLOR: #000000;
               FONT-FAMILY: Verdana;
               HEIGHT: 18px;
               CURSOR: hand;
               mso-style-parent:style0;
               mso-number-format:Standard;
       }
</style>

<body topmargin="0" leftmargin="8" rightmargin="0" >
<table>




<tr class="texto_bold">
	<td></td> 
	<td colspan="2"><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="6" align="center"><u>SGA - SISTEMA CEDITEC.</u></td>
	<td align="right" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="2"></td>
	<td colspan="6" align="center">Reporte de Cierre de Operaciones	</td>
	<td align="right" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td class="texto_bold" colspan="2">TECSUP-${model.NOM_SEDE}</td>
	<td colspan="6" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Usuario :</b></td>
	<td colspan="4" align="left">${model.usuario}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Del :</b></td>
	<td colspan="4" align="left">${model.periodoInicio}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Al :</b></td>
	<td colspan="4" align="left">${model.periodoFin}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<col style="bgcolor:red;width:400px;" />
			<col style="width:400px;" />
			<col style="width:400px;" />
			<col style="width:400px;" />
			<col style="width:400px;" />
			<col style="width:400px;" />
			<col style="width:400px;" />
			<col style="width:400px;" />
			<col style="width:400px;" />
			<tr>
				<td class="cabecera_grilla" width="5%">Tipo Requerimiento...</td>
				<td class="cabecera_grilla" width="5%">Tipo Bien</td>
				<td class="cabecera_grilla" width="5%">C.Costo</td>
				<td class="cabecera_grilla" width="5%">Descripción</td>
				<td class="cabecera_grilla" width="5%">Responsable</td>
				<td class="cabecera_grilla" width="5%">Tipo de Gasto</td>
				<td class="cabecera_grilla" width="5%">Moneda</td>
				
				<td class="cabecera_grilla" width="10%">Total Cargado</td>
				<td class="cabecera_grilla" width="5%">Cant.Req.Aten</td>
				<td class="cabecera_grilla" width="5%">Cant.Req. X Atender</td>
				
				<%if(request.getAttribute("reqSservicio")!=null ){%>
					<td class="cabecera_grilla" width="5%">Cant.Aten.Interna</td>
					<td class="cabecera_grilla" width="5%">Tipo de cambio</td>
					<td class="cabecera_grilla" width="5%">Importe en Soles</td>				
				<%}%>			
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
				System.out.println("Reporte xD");
				List consulta = (List) request.getSession().getAttribute("consulta"); 
				DecimalFormat df = new DecimalFormat("0.00");
				for(int i=0;i<consulta.size();i++){
					
						ReportesLogistica obj  = (ReportesLogistica) consulta.get(i);%>
					<tr class="texto_grilla">
						<td style="text-align:left"><%=obj.getDscTipoReq()==null?"":obj.getDscTipoReq()%></td>
						<td style="text-align:left"><%=obj.getDscTipoBienServ()==null?"":obj.getDscTipoBienServ()%></td>
						<td style="text-align:center"><%=obj.getCodCeco()==null?"":obj.getCodCeco()%></td>
						<td style="text-align:left"><%=obj.getDscCeco()==null?"":obj.getDscCeco()%></td>
						<td style="text-align:left"><%=obj.getResponsableCeco()==null?"":obj.getResponsableCeco()%></td>
						<td style="text-align:left"><%=obj.getDscTipoGasto()==null?"":obj.getDscTipoGasto()%></td>
						<td style="text-align:center"><%=obj.getDscMoneda()==null?"":obj.getDscMoneda()%></td>
						
						<td style="text-align:right"><%=df.format(Float.valueOf(obj.getTotalCargado()==""?"0.00":obj.getTotalCargado()))%></td>
						<td style="text-align:right"><%=df.format(Float.valueOf(obj.getCantReqAtendidos()==""?"0.00":obj.getCantReqAtendidos()))%></td>
						<td style="text-align:right"><%=df.format(Float.valueOf(obj.getCantReqPorCerrar()==""?"0.00":obj.getCantReqPorCerrar()))%></td>
						<%if(request.getAttribute("reqSservicio")!=null ){%>
							<td style="text-align:right"><%=df.format(Float.valueOf(obj.getCantAtenInt()==""?"0.00":obj.getCantAtenInt()))%></td>
							<td style="text-align:right"><%=df.format(Float.valueOf(obj.getTipoCambio()==""?"0.00":obj.getTipoCambio()))%></td>
							<td style="text-align:right"><%=df.format(Float.valueOf(obj.getImporteSoles()==""?"0.00":obj.getImporteSoles()))%></td>					
						<%}%>
																	
					</tr>
					<%}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>