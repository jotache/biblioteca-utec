<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">
				
		function onLoad(){
		}
		
		function fc_Generar(){
		 var ban="";
		 ban=fc_Val_IngresoAlmacen();
		 if(ban=="0")	
		 { 
		   url="?tCodRep=" + document.getElementById("txhCodTipoReporte").value +
				"&tCodSede=" + document.getElementById("codSede").value +
				"&tCodTipoBien=" + document.getElementById("txhConsteRadio").value +  
				"&tCodTipoOrden=" + document.getElementById("codTipoOrden").value +
				"&tProveedor=" + document.getElementById("proveedor").value + 
				"&tCodUsu=" + document.getElementById("txhCodUsuario").value + 
				"&tFecIni=" + document.getElementById("txtFecInicio").value +
				"&tFecFin=" + document.getElementById("txtFecFinal").value +
				"&tCodTipoEstado=" + document.getElementById("codEstado").value +  
				"&tCodTipoPago=" + document.getElementById("codTipoPago").value + 
				"&tValidado=" + document.getElementById("txhConsteRadio").value ;
				
		 if(url!="");
		 /*alert(url.length);
		 alert(url);*/
		 window.open("${ctx}/logistica/reportesLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");
		}
				else{  if(ban=="2") alert(mstrFechaInicio);
				       else{ if(ban=="3") alert(mstrFechaFin); 
				             else alert(mstrFecha);
				           }
				}
		}
		
	function fc_Val_IngresoAlmacen(){
	 var rpta="1";
	  if(document.getElementById("txtFecInicio").value!="" && document.getElementById("txtFecFinal").value!="")
	   {  rpta=fc_Validar_Fechas(document.getElementById("txtFecInicio").value, document.getElementById("txtFecFinal").value);
	      //alert("Rpta: "+rpta);
	   } 
	   else{ if(document.getElementById("txtFecInicio").value=="")
	          rpta="2";
	         else rpta="3";
	     }
	   return rpta; 
	}
	
	function fc_Validar_Fechas(srtFechaInicio, srtFechaFin)
	{
	 var num=0;
	 var srtFecha="0";
	 if( srtFechaInicio!="" && srtFechaFin!="")
	{   
		    num=fc_ValidaFechaIniFechaFin(srtFechaFin, srtFechaInicio);
		     //alert("Funcion: "+num);
		     //es 1 si el parametro 1 es mayor al parametro 2
		     //es 0 si el parametro 1 es menor o igual al parametro 2
		     if(num==1) srtFecha="0";
		     else srtFecha="1";
	 }
	 else{ if(srtFechaInicio=="")
	          srtFecha="2";
	       else srtFecha="3";
	     } 
    return srtFecha;
	}
	
	function fc_IndiceGrupo(param){
	 
	 switch(param){
			case '1':		  	
			   	document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteValidado").value ; 
			    break;
			    
			case '2':
			    
			    document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteVencido").value ; 
			    break;	
			   	
			}
	}
	
	function fc_Sede(){}
	
	function fc_Limpiar(){
	document.getElementById("proveedor").value="";
	document.getElementById("codEstado").value="-1"; 
	document.getElementById("codTipoPago").value="-1"; 
	document.getElementById("codTipoOrden").value="-1"; 
	document.getElementById("codSede").value=document.getElementById("txhConsteSede").value;
	//document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteValidado").value; 
	
	}
	
	function fc_TipoPago(){}
	
	function fc_Estado(){}
</script>
</head>
<body>
<form:form name="frmMain" commandName="control" action="${ctx}/logistica/repProgramaPagos.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="dscUsuario" id="txhDscUsuario"/>
<form:hidden path="codTipoReporte" id="txhCodTipoReporte"/>
<form:hidden path="consteRadio" id="txhConsteRadio"/>
<form:hidden path="consteValidado" id="txhConsteValidado"/>
<form:hidden path="consteVencido" id="txhConsteVencido"/>
<form:hidden path="consteSede" id="txhConsteSede"/>

<table cellpadding="1" cellspacing="0"  ID="Table1" style="width:98%; margin-top:10px;" 
		  border="0" bordercolor="Red" background="${ctx}/images/Logistica/back.jpg" class="tabla">
	
	<tr>
	       <td>&nbsp;&nbsp;Sede :</td>
	       <td>
	       		<form:select path="codSede" id="codSede" cssClass="combo_o" 
					cssStyle="width:100px" onchange="javascript:fc_Sede();">
					<c:if test="${control.listaSedes!=null}">
					<form:options itemValue="codSede" itemLabel="dscSede" 
						items="${control.listaSedes}" />
					</c:if>
				</form:select></td>
	</tr>	  
	<tr>
		<td colspan="2" width="50%">&nbsp;&nbsp;Per�odo :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<form:input path="fecInicio" id="txtFecInicio" cssClass="cajatexto_o"
								onkeypress="javascript:fc_Slash('frmMain','txtFecInicio','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
								align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>&nbsp;&nbsp;
				<form:input path="fecFinal" id="txtFecFinal" cssClass="cajatexto_o"
								onkeypress="javascript:fc_Slash('frmMain','txtFecFinal','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
								align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>
		</td>
		<td colspan="1">
			<table id="radio2" name="radio2" bordercolor="blue" border="0" width="100%">
					<tr>
						<td width="4%">
							<input type="radio" id="gbradio1" name="rdoBandeja" align="left" 
							<c:if test="${control.consteRadio==control.consteValidado}"><c:out value=" checked=checked " /></c:if> 
							onclick="javascript:fc_IndiceGrupo('1');">
							Validado
							&nbsp;&nbsp;&nbsp;
							<input type="radio" id="gbradio2" name="rdoBandeja" 
							<c:if test="${control.consteRadio==control.consteVencido}"><c:out value=" checked=checked " /></c:if> 
							onclick="javascript:fc_IndiceGrupo('2');">
				 			Vencimiento
				 		</td>
				 	</tr>
			</table>
		</td>
		
	</tr>	
	
	<tr>
		<td align="left">&nbsp;&nbsp;Tipo Pago :</td>
		<td>
			<form:select path="codTipoPago" id="codTipoPago" cssClass="cajatexto" 
				cssStyle="width:180px" onchange="javascript:fc_TipoPago();">
				<form:option  value="-1">--Todos--</form:option>
				<c:if test="${control.listaCodTipoPago!=null}">
				<form:options itemValue="codSecuencial" itemLabel="descripcion" 
				items="${control.listaCodTipoPago}" />
				</c:if>
			</form:select>
		</td>
		<td colspan="2">&nbsp;&nbsp;Tipo Estado:&nbsp;&nbsp;&nbsp;&nbsp;
		   
			<form:select path="codEstado" id="codEstado" cssClass="cajatexto" cssStyle="width:180px" 
			     onchange="javascript:fc_Estado();">
				<form:option  value="-1">--Todos--</form:option>
				<c:if test="${control.listaCodEstado!=null}">
				<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
				items="${control.listaCodEstado}"/>
				</c:if>
			</form:select>
		</td>
	</tr>  
	
	<tr>
		<td class="">&nbsp;&nbsp;Tipo Orden:</td>
		<td><form:select path="codTipoOrden" id="codTipoOrden" cssClass="cajatexto" cssStyle="width:100px">
			<form:option value="-1">-- Todos --</form:option>
			<c:if test="${control.listaCodTipoOrden!=null}">
			<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
			items="${control.listaCodTipoOrden}"/>
			</c:if>
			</form:select>
		</td>
		
		
	</tr>	  
	<tr> <td colspan="2" id="Proveedor_7">&nbsp;&nbsp;Proveedor :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 	<form:input path="proveedor" id="proveedor" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();" 
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Proveedor');" 
					cssClass="cajatexto" size="40"/>
		</td>
		<td align="right">
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
							<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
								 style="cursor: pointer; 60px" 
								 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>	  
</table>

<table align="center" style="margin-top: 10px">
		<tr>
			<td>
			<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar','','${ctx}/images/botones/generar2.jpg',1)">
				<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar"></a>
			</td>
		</tr>
</table>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
</body>
</html>