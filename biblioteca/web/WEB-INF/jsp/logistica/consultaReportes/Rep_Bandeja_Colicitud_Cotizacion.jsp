<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Cotizacion'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" colspan="4"><u><b>SOLICITUD COTIZACIONES</b></u></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td colspan="2" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td><td></td>
<td></td><td></td><td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
	<table>	
	<tr>
	<td></td>
	<td class="texto_bold">Nro. Cotización</td>
	<td align="left" class="texto">: ${model.nroCotizacion} </td>
	<td class="texto_bold">Fecha Cotización</td>
	<td align="left" class="texto">: ${model.fechaIni} </td>
	<td align="left" class="texto">: ${model.fechaFin} </td>
	<td class="texto_bold">Tipo Pago</td>
	<td align="left" class="texto">: ${model.DesTipoPago} </td>
	</tr>
	<tr>
	<td></td>
	<td class="texto_bold">Tipo Req.</td>
	<td align="left" class="texto">: ${model.txhtipo} </td>
	<%  
	if(request.getAttribute("TipoReq")!=null){
		if(request.getAttribute("TipoReq").equals("0001")){
	%>
	<td class="texto_bold">Tipo Bien</td>
	<td align="left" class="texto">: ${model.txhTipoBien} </td>	
	<td></td>
	<td></td>
	<%   }
		else if(request.getAttribute("TipoReq").equals("0002")){
		
	%>	
	<td class="texto_bold">Grupo Servicio </td>
	<td align="left" class="texto" colspan="2">: ${model.txhGrupoServ} </td>	
	<td class="texto_bold">Tipo Servicio</td>
	<td align="left" class="texto">: ${model.txhTipoBien} </td>
	
	<%  
			 }
		}
	%>
	<td></td>
	
	</tr>
	<tr>
	<td></td>
	<td class="texto_bold">Proveedor</td>
	<td align="left" class="texto">: ${model.txtProv1} </td>
	<td align="left" class="texto" colspan="3">: ${model.txtProv2} </td>
	<td class="texto_bold">Estado</td>
	<td align="left" class="texto">: ${model.estado} </td>
	</tr>
	<table align="center">
	<tr>
		<td></td>
		<td>
		
			<table border="1" align="center">
			<tr rowspan="2">
				<td class="cabecera_grilla" style="width:180px" >Nro. Cotización</td>
				<td class="cabecera_grilla" style="width:180px" >Fecha Cotización</td>
				<td class="cabecera_grilla" style="width:180px" >Fecha Inicio<br>Vigencia</td>
				<td class="cabecera_grilla" style="width:180px" >Fecha Fin<br>Vigencia</td>
				<td class="cabecera_grilla" style="width:180px" >Tipo de Pago</td>
				<td class="cabecera_grilla" style="width:180px" >Estado</td>
				<td class="cabecera_grilla" style="width:180px" >Nro. Orden </td>
			</tr>
			<%
			List alumno = (List)request.getSession().getAttribute("consulta");
			if(alumno != null){
				for(int j = 0; j < alumno.size(); j++){
					Cotizacion asist = (Cotizacion)alumno.get(j);
			%>
			<%if (asist.getNroCotizacion() != null){%>
			<tr class="texto_grilla">
				<td width="50%" style="text-align:center"><%=asist.getNroCotizacion()%></td>
				<td width="50%" style="text-align:right"><%=asist.getFechaEmision()%></td>
				<td width="50%" style="text-align:right"><%=asist.getFechaInicio()%></td>
				<td width="50%" style="text-align:right"><%=asist.getFechaFin()%></td>
				<td width="50%" style="text-align:left"><%=asist.getDscTipoPago()%></td>
				<td width="50%" style="text-align:left"><%=asist.getDscEstado()%></td>
				<td width="50%" style="text-align:left"><%=asist.getNroOrdenCompra()%></td>
			</tr>
	
	<%}
		}
	}request.getSession().removeAttribute("consulta"); 
	%>	
	</table>
	</td>
	</tr></table></table>
</body>
</html>