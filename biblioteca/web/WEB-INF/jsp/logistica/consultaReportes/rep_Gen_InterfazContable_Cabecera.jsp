<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.modelo.InterfazContableCabecera'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;	
	
	mso-number-format:"\@";
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
.detalleNro {
               FONT-SIZE: 10px;
               /*COLOR: #636563;*/
               COLOR: #000000;
               FONT-FAMILY: Verdana;
               HEIGHT: 18px;
               CURSOR: hand;
               mso-style-parent:style0;
               mso-number-format:Standard;
       }
.tipoCambioNro {
               FONT-SIZE: 10px;
               /*COLOR: #636563;*/
               COLOR: #000000;
               FONT-FAMILY: Verdana;
               HEIGHT: 18px;
               CURSOR: hand;
               mso-style-parent:style0;
               mso-number-format:"0\.000";
       }
       
.xl24{
	mso-style-parent:style0;
	mso-number-format:"\@";
}
       
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >

<table>
	<tr>		
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="5%">CSUBDIA</td>
				<td class="cabecera_grilla" width="5%">CCOMPRO</td>
				<td class="cabecera_grilla" width="5%">CFECCOM</td>
				<td class="cabecera_grilla" width="5%">CCODMON</td>
				<td class="cabecera_grilla" width="5%">CSITUA</td>
				<td class="cabecera_grilla" width="5%">CTIPCAM</td>
				<td class="cabecera_grilla" width="5%">CGLOSA</td>
				
				<td class="cabecera_grilla" width="5%">CTOTAL</td>
				<td class="cabecera_grilla" width="5%">CTIPO</td>
				<td class="cabecera_grilla" width="5%">CFLAG</td>
				<td class="cabecera_grilla" width="5%">CDATE</td>
				<td class="cabecera_grilla" width="5%">CHORA</td>
				
				<td class="cabecera_grilla" width="5%">CUSER</td>
				<td class="cabecera_grilla" width="5%">CFECCAM</td>
				<td class="cabecera_grilla" width="5%">CORIG</td>
				<td class="cabecera_grilla" width="5%">CFORM</td>
				<td class="cabecera_grilla" width="5%">CTIPCOM</td>
				<td class="cabecera_grilla" width="5%">CEXTOR</td>
				<td class="cabecera_grilla" width="5%">CFECCOM2</td>
				
				<td class="cabecera_grilla" width="5%">CFECCAM2</td>
				<td class="cabecera_grilla" width="5%">COPCION</td>
					
			</tr>
			
			<%  
				if(request.getSession().getAttribute("lista_cabecera")!=null){
				System.out.println("Reporte xD");
				List consulta = (List) request.getSession().getAttribute("lista_cabecera"); 
				DecimalFormat df = new DecimalFormat("0.00");
				for(int i=0;i<consulta.size();i++){
					System.out.println("I: "+i);
					InterfazContableCabecera obj  = (InterfazContableCabecera) consulta.get(i);%>
					<tr class="texto_grilla">
					    
						<td style="text-align:right"><%=obj.getCsubdia()%></td>
						<td style="text-align:right"><%=obj.getCcompro()%></td>
						<td style="text-align:center"><%=obj.getCfeccom()%></td>
						<td style="text-align:center"><%=obj.getCcodmon()%></td>
						<td style="text-align:center"><%=obj.getCsitua()%></td>
						<td style="text-align:right"><%=obj.getCtipcam()==""?"0.000":obj.getCtipcam()%></td>
						<td style="text-align:left"><%=obj.getCglosa()%></td>
						
						<td style="text-align:right"><%=obj.getCtotal()%></td>
						<td style="text-align:center"><%=obj.getCtipo()%></td>
						<td style="text-align:center"><%=obj.getCflag()%></td>
						<td style="text-align:center"><%=obj.getCdate()%></td>
						<td style="text-align:center"><%=obj.getChora()%></td>
						<td style="text-align:left"><%=obj.getCuser()%></td>
						<td style="text-align:center"><%=obj.getCfeccam()%></td>
						
						<td style="text-align:left"><%=obj.getCorig()%></td>
						<td style="text-align:left"><%=obj.getCform()%></td>
						<td style="text-align:center"><%=obj.getCtipcom()%></td>
						<td style="text-align:left"><%=obj.getCextor()%></td>
						<td style="text-align:center"><%=obj.getCfeccom2()%></td>
						<td style="text-align:center"><%=obj.getCfeccam2()%></td>
						<td style="text-align:center"><%=obj.getCopcion()%></td>
											
					</tr>
					<%}
				}
				request.removeAttribute("lista_cabecera"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>