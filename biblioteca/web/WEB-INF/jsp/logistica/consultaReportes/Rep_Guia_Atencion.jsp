<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.GuiaDetalle'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" colspan="3"><u><b>GUIAS DE ATENCION</b></u></td>
	<td></td><td></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td colspan="2" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td><td></td><td></td><td></td><td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
	<table>
	<tr>
	<td></td>
	<td class="texto_bold">Nro. Guia</td>
	<td align="left" class="texto">: ${model.nroGuia} </td>
	<td></td>
	<td class="texto_bold">Fecha Guia</td>
	<td align="left" class="texto">: ${model.fechaGuia} </td>
	<td></td>
	<td class="texto_bold">Estado</td>
	<td align="left" class="texto">: ${model.estado} </td>
	</tr>
	<tr>
	<td></td>
	<td class="texto_bold">Nro. Factura</td>
	<td align="left" class="texto">: ${model.nroFac} </td>
	<td></td>
	<td class="texto_bold">Fecha Emisi�n</td>
	<td align="left" class="texto">: ${model.fechaEmi} </td>
	<td></td>
	<td class="texto_bold">Nro. O/S</td>
	<td align="left" class="texto">: ${model.nroOS} </td>
	</tr>
	<tr>
	<td></td>
	<td class="texto_bold">Observacionnes</td>
	<td align="left" class="texto">: ${model.Obs} </td>
	</tr>
</table>
</body>
</html>