<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.DetalleOrden'%>
<%@page import="com.tecsup.SGA.modelo.CondicionCotizacion"%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<html>

<style >
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.79in .40in .79in .40in;
	mso-header-margin:0cm;
	mso-footer-margin:0cm;}
-->
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:"0\.000";
}

.totalNro {
    /*ALQD,12/11/08. A�adiendo un estilo mas.*/
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:"0\.00";
}
</style>

<body >
<table>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="2" align="center" style="font-size:14pt; vertical-align: middle"><u><b>PURCHASE ORDER</b></u></td>
	<td colspan="2" rowspan="2" align="center" style="font-size:12pt; vertical-align: middle">

		<c:if test="${model.codEstadoOC=='0003'}">
			<img src="${ctx}/images/sellooc.jpg" width="100px" height="100px">
		</c:if>

	</td>
	<td align="center" style="font-size:12pt; vertical-align: middle">&nbsp;</td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td><td></td><td></td><td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr>
	<td></td>
	<td></td>
</tr>
<tr>
	<td align="left" style="font-size:8pt">Orden No:</td>
	<td align="left" style="font-size:8pt">${model.nroOrd}</td>
	<td></td>
	<td></td>
</tr>
<tr><td align="left" style="font-size:8pt">Date:</td>
<td align="left" style="font-size:8pt">${model.fEmi}</td>
</tr>
<tr>
	<td align="left" style="font-size:8pt">Reference:</td>
	<td colspan="2" align="left" style="font-size:8pt">${model.numCotizacion}</td>
	<td></td>
</tr>
<tr>
</tr>
<tr>
	<td align="left" style="font-size:8pt">Supplier:</td>
	<td align="left" style="font-size:8pt" colspan="4"><b>${model.prov}</b></td>
  </tr>
<tr><td align="left" style="font-size:8pt">Address:</td>
	<td align="left" colspan="7" style="font-size:8pt"></td>
</tr>
<tr><td align="left" style="font-size:8pt">Contact:</td>
	<td align="left" style="font-size:8pt" colspan="3">${model.atenc}</td>
</tr>
<tr><td align="left" style="font-size:8pt">Telephone:</td>
	<td align="left" colspan="3" style="font-size:8pt">${model.telefono}</td>
</tr>
<tr><td align="left" style="font-size:8pt">Fax:</td>
	<td align="left" colspan="3" style="font-size:8pt">${model.cadCenCosto}</td>
</tr>
<tr><td align="left" style="font-size:8pt">e-mail:</td>
	<td align="left" colspan="3" style="font-size:8pt">${model.emailProv}</td>
</tr>
<tr>
</tr>
<tr><td align="left" style="font-size:8pt">Invoice / Ship to:</td>
	<td align="left" colspan="3" style="font-size:8pt">Tecsup 1</td>
</tr>
<tr><td align="left" style="font-size:8pt">Address:</td>
	<td align="left" colspan="7" style="font-size:8pt">Av. Cascanueces 2221, Santa Anita, Lima, Per�</td>
</tr>
<tr><td align="left" style="font-size:8pt">Contact:</td>
	<td align="left" colspan="3" style="font-size:8pt">${model.comprador}</td>
</tr>
<tr><td align="left" style="font-size:8pt">Telephone:</td>
	<td align="left" colspan="3" style="font-size:8pt">51-1-354-0617</td>
</tr>
<tr><td align="left" style="font-size:8pt">Fax:</td>
	<td align="left" colspan="3" style="font-size:8pt">51-1-354-2256</td>
</tr>
<tr><td align="left" style="font-size:8pt">e-mail:</td>
	<td align="left" colspan="3" style="font-size:8pt">${model.emailComprador}</td>
</tr>
<tr>
	<td align="left" style="font-size:8pt">Via:</td>
	<td align="left" colspan="2" style="font-size:8pt"></td>
</tr>
<tr>
	<td align="left" style="font-size:8pt">Terms:</td>
	<td align="left" colspan="2" style="font-size:8pt"></td>
</tr>
<tr>
	<td align="left" style="font-size:8pt">Date of delivery:</td>
	<td align="left" colspan="2" style="font-size:8pt"></td>
</tr>
<tr>
	<td align="left" style="font-size:8pt">Instructions:</td>
	<td align="left" colspan="2" style="font-size:8pt"></td>
</tr>
</table>
<br>
<table align="center">
	<tr>
		<td>
			<table border="1" align="center">
			<tr rowspan="2">
				<td class="cabecera_grilla" style="width:20px" nowrap="nowrap">ITEM</td>
				<td class="cabecera_grilla" style="width:20px" nowrap="nowrap">CODE</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" colspan="3">DESCRIPCTION</td>
				<td class="cabecera_grilla" style="width:40px" nowrap="nowrap" >QTY.</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >UNI.PRICE<br>${model.mond}</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >TOTAL<br>${model.mond}</td>
			</tr>
			<%
				List alumno = (List)request.getSession().getAttribute("consulta");
				//double l = Long.valueOf("0").longValue();
				long l = Long.valueOf("0").longValue();
				if(alumno != null){
					for(int j = 0; j < alumno.size(); j++){
						DetalleOrden rep = (DetalleOrden)alumno.get(j);
						l = l + (long)(Double.valueOf(rep.getSubtotal()).doubleValue() * 1000);						
						
				%>
				<%if (rep.getDescripcion() != null){ 
					//System.out.println("alumno: "+alumno.size());
				%>
				<tr class="texto_grilla">
					<td width="30%" style="text-align:center"><%=j+1%></td>
					<td width="30%" style="text-align:right"><%=rep.getCodBien()%></td>
					<td width="30%" style="text-align:left" colspan="3"><%=rep.getDescripcion()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getCantidad()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getPrecio()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getSubtotal()%></td>
				</tr>
				<%}
					}
				}request.removeAttribute("consulta"); 
				%>	
				
				<tr class="cabecera_grilla">
					<td width="30%" style="text-align:left" colspan="4">&nbsp;</td>
					<td width="30%" style="text-align:center">&nbsp;</td>
					<td width="30%" style="text-align:right">&nbsp;</td>
					<td width="30%" style="text-align:right">&nbsp;Total:</td>
					<td width="30%" class="totalNro" style="text-align:right">&nbsp;<%=String.valueOf(Double.valueOf(l).doubleValue()/1000)%> </td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
<br>

<br>
<br>
<table>
	<tr>
		<td style="font-size:8pt"><strong><u>Detalles Comerciales</u></strong></td>
	</tr>	
	<tr>
		<td height="4"></td>
	</tr>	
	<tr>						
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" >Tipo<br>Condici�n</td>
				<td class="cabecera_grilla" colspan="6" >Descripci�n Final</td>
			</tr>			
			<%
			List alumno1 = (List)request.getSession().getAttribute("condComercial");
			if(alumno1 != null){
				for(int j = 0; j < alumno1.size(); j++){
					DetalleOrden detConCom = (DetalleOrden)alumno1.get(j);
			%>
			<%if (detConCom.getDscCondicion() != null){%>
			<tr class="texto_grilla">
				<td width="35%" valign="top" style="text-align:left"><%=detConCom.getDscCondicion()%></td>							
				<td width="65%" colspan="6" valign="top" style="text-align:left"><%=detConCom.getDescripcion()%></td>
			</tr>
			
			<%}
				}
			}request.getSession().removeAttribute("condComercial"); 
			%>	
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>
