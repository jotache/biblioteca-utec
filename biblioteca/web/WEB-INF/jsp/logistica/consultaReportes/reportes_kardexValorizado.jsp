<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.ReportesLogistica'%>
<%@page import='java.math.BigDecimal'%>
<%@page contentType="application/vnd.ms-excel"%>
<%--@ page language="java" errorPage="/error.jsp"--%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator"
	prefix="html"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 
<html>
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.79in .40in .79in .40in;
	mso-header-margin:0cm;
	mso-footer-margin:0cm;}
-->
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
}

.texto_grilla_c {
	FONT-SIZE: 10px; */
	COLOR: #636563; */
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

	.detalleNro {
		FONT-SIZE: 10px;
		/*COLOR: #636563;*/
		COLOR: #000000;
		FONT-FAMILY: Verdana;
		HEIGHT: 18px;
		CURSOR: hand;
		mso-style-parent:style0;
		mso-number-format:Standard;
	}
	
	.detalleNro2 {
		FONT-SIZE: 10px;
		/*COLOR: #636563;*/
		COLOR: #000000;
		FONT-FAMILY: Verdana;
		HEIGHT: 18px;
		CURSOR: hand;
		mso-style-parent:style0;
		mso-number-format:"0\.000";
	}	
	
</style>
<body>

<table class="tabla" width="896">
	<tr>
		<td class="texto_bold" colspan="2">&nbsp;</td>
		<td></td>
		<td></td>
		<td></td><td></td><td></td><td></td><td></td><td></td>
		<td class="texto_bold">${model.FECHA_ACTUAL}</td>
	</tr>

	<tr>
		<td class="texto_bold" colspan="4">TECSUP-${model.NOM_SEDE}</td>

		<td>&nbsp;</td><td></td><td></td><td></td><td></td><td></td>
		<td class="texto_bold">${model.HORA_ACTUAL}</td>
	</tr>
	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>

<table class="tabla" width="896">
	<tr>
		<td class="texto_bold" colspan="11">
		<div align="center" class="texto_bold">Listado de Kardex Valorizado</div>
		</td>
	</tr>
	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>

<table class="tabla" width="896">
	<tr class="texto_grilla">
		<td></td>
		<td class="texto_bold">
		<div align="right">Del</div>
		</td>
		<td>
		<div align="center" class="texto">
		<div align="left">${model.FECHA_INI}</div>
		</div>
		</td>
		<td class="texto_bold">
		<div align="right">Al</div>
		</td>
		<td>
		<div align="center" class="texto">${model.FECHA_FIN}
		<div align="left"></div>
		</div>
		</td>
		<td></td>
	</tr>

	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>


<table class="tabla" width="896">
	<tr class="texto_grilla">
		<td class="texto_bold">Usuario:</td>
		<td colspan="2" class="texto_bold">${model.NOM_USUARIO}</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>

	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>


<table width="896" border="1" bordercolor="#000000" class="tabla">
	<tr bgcolor="#CCCCCC" class="cabecera_grilla" style="height: 30px;">
		<td colspan="2"><span class="Estilo5">Cod. Familia</span></td>
		<td colspan="3"><span class="Estilo5">Familia</span></td>		
		<td colspan="6"><span class="Estilo5">&nbsp;</span></td>
	</tr>
	<tr bgcolor="#CCCCCC" class="cabecera_grilla" style="height: 30px;">
		<td colspan="2"><span class="Estilo5">Cod. Bien</span></td>
		<td colspan="3"><span class="Estilo5">Bien</span></td>		
		<td colspan="1"><span class="Estilo5">Uni. Medida</span></td>
		<td colspan="5"><span class="Estilo5">&nbsp;</span></td>
	</tr>
	<tr bgcolor="#CCCCCC" class="cabecera_grilla" style="height: 30px;">
		<td><span class="Estilo5">Documento</span></td>
		<td><span class="Estilo5">Fec. Documento</span></td>
		<td><span class="Estilo5">&nbsp;</span></td>
		<td><span class="Estilo5">Cant. Ingreso</span></td>
		<td><span class="Estilo5">Cant. Salida</span></td>
		<td><span class="Estilo5">&nbsp;</span></td>
		<td><span class="Estilo5">Saldo</span></td>
		<td><span class="Estilo5">Costo Unitario</span></td>
		<td><span class="Estilo5">Imp Ingreso</span></td>
		<td><span class="Estilo5">Imp Salida</span></td>
		<td><span class="Estilo5">Imp Saldo</span></td>
		<td><span class="Estilo5">Nom. Solicitante</span></td>
	</tr>

<%
	if ( request.getAttribute("LST_RESULTADO")!= null ) 
	{
	
		List consulta = (List) request.getAttribute("LST_RESULTADO");
		
		String guia = "";
		String bienVariable = "";
		String familiaVariable = "";
		ReportesLogistica objFuturo =null;
		String bienFuturo = "";
		String familiaFutura = "";
		BigDecimal sumSaldo = new BigDecimal("0");
		BigDecimal sumCostoUnitario = new BigDecimal("0");
		BigDecimal totImpIngreso = new BigDecimal("0");
		BigDecimal totImpSalida = new BigDecimal("0");
		
		for (int i = 0; i < consulta.size(); i++) 
		{		
			ReportesLogistica obj = (ReportesLogistica) consulta.get(i);			
			bienVariable = obj.getCodBien();
			familiaVariable = obj.getCodFamilia();
%>
	
		<!--  -->
		<%
		sumSaldo = sumSaldo.add(  new BigDecimal( obj.getSaldo() ) );
		sumCostoUnitario = sumCostoUnitario.add(  new BigDecimal( obj.getCostoUnitario()) );
		
		if(i==0){%>
				<tr class="texto_grilla">
					<td colspan="2" align="center" ><%=obj.getCodFamilia()%></td>
					<td colspan="2" align="left" ><%=obj.getDscFamilia()%></td>
				</tr>
				<tr class="texto_grilla">
					<td colspan="2" align="center" ><%=obj.getCodBien()%></td>
					<td colspan="2" align="left" ><%=obj.getDscBien()%></td>
					<td colspan="1" align="left" >&nbsp;</td>
					<td colspan="1" align="center" ><%=obj.getDscUnidad()%></td>
										
					<td class="detalleNro2" style="text-align:right"  ><%=obj.getSaldoInicial()%></td>
					<td  class="detalleNro" style="text-align:right"  ><%=obj.getCostoUnitarioInicial()%></td>
					<td  align="right" >&nbsp;</td>
					<td  align="right" >&nbsp;</td>
					<td  class="detalleNro" style="text-align:right"  ><%=obj.getImporteSaldoInicial()%></td>
									 	
				</tr>			
		<%}%>
		
		
		<tr class="texto_grilla">		
			<td style="text-align:left"><%=obj.getNroDocRelacionado()%></td>
			<td style="text-align:right"><%=obj.getFechaDocumento()%></td>
			<td>&nbsp;</td>
			<td class="detalleNro2" style="text-align:right"><%=obj.getCantidadIngreso()%></td>
			<td class="detalleNro2" style="text-align:right"><%=obj.getCantidadSalida()%></td>
			<td>&nbsp;</td>
			<td class="detalleNro2" style="text-align:right"><%=obj.getSaldo()%></td>
			<td class="detalleNro" style="text-align:right"><%=obj.getCostoUnitario()%></td>		
			<td class="detalleNro" style="text-align:right"><%=obj.getImporteIngreso()%></td>
			<td class="detalleNro" style="text-align:right"><%=obj.getImporteSalida()%></td>		
			<td class="detalleNro" style="text-align:right"><%=obj.getImporteSaldo()%></td>
			<td class="detalleNro" style="text-align:right"><%=obj.getNomSolicitante() %></td>						
		</tr>
				
		<%
		
		totImpIngreso = totImpIngreso.add( BigDecimal.valueOf( Double.valueOf(  obj.getImporteIngreso()==null?"0.0":(obj.getImporteIngreso().trim().equals("")?"0.0":obj.getImporteIngreso().trim())   ).doubleValue()) );
		totImpSalida = totImpSalida.add( BigDecimal.valueOf( Double.valueOf(  obj.getImporteSalida()==null?"0.0":(obj.getImporteSalida().trim().equals("")?"0.0":obj.getImporteSalida().trim())   ).doubleValue()) );
		
		if( i+1<consulta.size() )
		{	objFuturo = (ReportesLogistica) consulta.get(i+1);
			bienFuturo = objFuturo.getCodBien();
			familiaFutura = objFuturo.getCodFamilia();
			
			if( !familiaVariable.equalsIgnoreCase(familiaFutura) ){%>
				<tr class="texto_grilla">
					<td colspan="2" align="left" >TOTAL CODIGO:</td>
					<td colspan="4">&nbsp;</td>
					<td class="detalleNro2" style="text-align:right" colspan="1"><%=obj.getSaldoFinal()%></td>
					<td class="detalleNro" style="text-align:right" colspan="1"><%=obj.getCostoUnitarioFinal()%></td>
					<td class="detalleNro" style="text-align:right" colspan="3"><%=obj.getImporteSaldoFinal()%></td>
										
				</tr>	
				<tr class="texto_grilla">
					<td colspan="2" align="center"> <%=objFuturo.getCodFamilia()%></td>
					<td colspan="2" align="left"><%=objFuturo.getDscFamilia()%></td>
				</tr>
				<tr class="texto_grilla">
					<td colspan="2" align="center" ><%=objFuturo.getCodBien()%></td>
					<td colspan="2" align="left" ><%=objFuturo.getDscBien()%></td>
					<td colspan="1" align="left" >&nbsp;</td>
					<td colspan="1" align="center" ><%=objFuturo.getDscUnidad()%></td>
					
					<td   class="detalleNro2" style="text-align:right"  ><%=objFuturo.getSaldoInicial()%></td>
					<td   class="detalleNro" style="text-align:right"  ><%=objFuturo.getCostoUnitarioInicial()%></td>
					<td  align="rigth" >&nbsp;</td>
					<td  align="rigth" >&nbsp;</td>
					<td   class="detalleNro" style="text-align:right"  ><%=objFuturo.getImporteSaldoInicial()%></td>
					
										
				</tr>					
			<%	sumSaldo = new BigDecimal("0");
			sumCostoUnitario = new BigDecimal("0");		
			}else if(!bienVariable.equalsIgnoreCase(bienFuturo))
			{%>
				<tr class="texto_grilla">
					<td colspan="2" align="left" >TOTAL CODIGO:</td>
					<td colspan="4">&nbsp;</td>
					
					<td class="detalleNro2" style="text-align:right" colspan="1"><%=obj.getSaldoFinal()%></td>
					<td class="detalleNro" style="text-align:right" colspan="1"><%=obj.getCostoUnitarioFinal()%></td>
					<td class="detalleNro" style="text-align:right" colspan="3"><%=obj.getImporteSaldoFinal()%></td>					
				</tr>		
				<tr class="texto_grilla">
					<td colspan="2" align="center" ><%=objFuturo.getCodBien()%></td>
					<td colspan="2" align="left" ><%=objFuturo.getDscBien()%></td>
					<td colspan="1" align="left" >&nbsp;</td>
					<td colspan="1" align="center" ><%=obj.getDscUnidad()%></td>
					
					<td  class="detalleNro2" style="text-align:right"  ><%=objFuturo.getSaldoInicial()%></td>
					<td  class="detalleNro" style="text-align:right"  ><%=objFuturo.getCostoUnitarioInicial()%></td>
					<td  align="rigth" >&nbsp;</td>
					<td  align="rigth" >&nbsp;</td>
					<td  class="detalleNro" style="text-align:right"  ><%=objFuturo.getImporteSaldoInicial()%></td>					
				</tr>					
			<%
			sumSaldo = new BigDecimal("0");
			sumCostoUnitario = new BigDecimal("0");
			}
		}else if( i+1 == consulta.size()){%>
			<tr class="texto_grilla">
				<td colspan="2" align="left" >TOTAL CODIGO:</td>
				<td colspan="4">&nbsp;</td>
					<td class="detalleNro2" style="text-align:right" colspan="1"><%=obj.getSaldoFinal()%></td>
					<td class="detalleNro" style="text-align:right" colspan="1"><%=obj.getCostoUnitarioFinal()%></td>
					<td class="detalleNro" style="text-align:right" colspan="3"><%=obj.getImporteSaldoFinal()%></td>									
			</tr>
		<%
		sumSaldo = new BigDecimal("0");
		sumCostoUnitario = new BigDecimal("0");
		}
		%>
				
	<%}/*FOR*/
	%>

	<tr class="texto_grilla">
		<td colspan="2" align="left" >TOTAL KARDEX:</td>
		<td colspan="6">&nbsp;</td>
		<!--td class="detalleNro" style="text-align:right" colspan="1">&nbsp;</td-->
		<!--td class="detalleNro" style="text-align:right" colspan="1">&nbsp;</td-->
		<td class="detalleNro" style="text-align:right" ><strong><%=totImpIngreso.toString() %></strong></td>									
		<td class="detalleNro" style="text-align:right" ><strong><%=totImpSalida.toString() %></strong></td>
		<td class="detalleNro" style="text-align:right" colspan="2">&nbsp;</td>
	</tr>
		
<%	
  }/*IF*/
%>
</table>

</body>
</html>