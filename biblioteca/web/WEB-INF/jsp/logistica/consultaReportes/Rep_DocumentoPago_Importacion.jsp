<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.DocumentoAsociado'%>
<%@page import='com.tecsup.SGA.modelo.DetalleOrden'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.79in .40in .79in .40in;
	mso-header-margin:0cm;
	mso-footer-margin:0cm;
	mso-page-orientation:landscape;}
-->
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: White;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:"0\.000";
}

.totalNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:"0\.00";
}

</style>

<body >
<table>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td align="center" style="font-size:12pt; vertical-align: middle" colspan="3"><u><b>Liquidaci&oacute;n de Importaci&oacute;n</b></u></td>
		<td align="right" style="font-size:8pt">Fecha</td>
		<td colspan="1" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td><td></td><td></td>
		<td align="right" style="font-size:8pt">Hora</td>
		<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
	</tr>
</table>
<table>
	<tr>
	  <td></td>
	  <td class="texto_bold">O/C Importaci&oacute;n</td>
	  <td align="left" class="texto">: ${model.nroOrd}</td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Fecha Emisi&oacute;n</td>
	  <td align="left" class="texto">: ${model.fEmi}</td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Estado</td>
	  <td align="left" class="texto">: ${model.desEstadoOC}</td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Compra Tipo</td>
	  <td align="left" class="texto">: </td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Monto</td>
	  <td align="left" class="totalNro">: ${model.totsum}</td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Moneda</td>
	  <td align="left" class="texto">: ${model.mond}</td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Proveedor</td>
	  <td colspan="4" align="left" class="texto">: ${model.prov}</td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Pa&iacute;s Origen</td>
	  <td align="left" class="texto">: ${model.pais}</td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Representante Local</td>
	  <td align="left" class="texto">: ${model.atenc}</td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Importador</td>
	  <td colspan="2" align="left" class="texto">: Tecsup - ${model.lugarTrabajo}</td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Origen de la Importaci&oacute;n</td>
	  <td align="left" class="texto">: Orden de Compra</td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Forma de Env&iacute;o</td>
	  <td align="left" class="texto">: </td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Forma de Pago</td>
	  <td align="left" class="texto">: </td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Fecha de Pago</td>
	  <td align="left" class="texto">: </td>
	</tr>
	<tr>
	  <td></td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">No. de Gu&iacute;a Aerea o B/L</td>
	  <td align="left" class="texto">: </td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">No. de Manifiesto</td>
	  <td align="left" class="texto">: </td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">No. de DUA</td>
	  <td align="left" class="texto">: </td>
	</tr>
	<tr>
	  <td></td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Fecha de Llegada a Puerto</td>
	  <td align="left" class="texto">: </td>
	  </tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Fecha de Ingreso a Tecsup</td>
	  <td align="left" class="texto">: </td>
	  </tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Dpto. de Destino</td>
	  <td align="left" class="texto">: ${model.cadCenCosto}</td>
	</tr>
	<tr>
	  <td></td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Descripción del Material</td>
	</tr>
	<tr>
		<td colspan="8">
		<table border="1" align="center">
			<tr rowspan="2">
				<td class="cabecera_grilla" style="width:160px" colspan="1">C&oacute;digo</td>
				<td class="cabecera_grilla" style="width:160px" colspan="2">Producto</td>
				<td class="cabecera_grilla" style="width:120px" >Precio<br>Unitario</td>
				<td class="cabecera_grilla" style="width:120px" >Cantidad</td>
				<td class="cabecera_grilla" style="width:100px" >Unidad<br>Medida</td>
				<td class="cabecera_grilla" style="width:120px" >Total<br>Facturado</td>
				<td class="cabecera_grilla" style="width:120px" >Prorrateo<br>S/.</td>
			</tr>
			<%
			List alumno = (List)request.getSession().getAttribute("consulta");
			if(alumno != null){
				for(int j = 0; j < alumno.size(); j++){
					DetalleOrden asist = (DetalleOrden)alumno.get(j);
			%>
			<tr class="texto_grilla">
				<td class="texto" style="text-align:right"><%=asist.getCodBien()%></td>
				<td style="text-align:left" colspan="2"><%=asist.getDescripcion()%></td>
				<td class="detalleNro" style="text-align:right"><%=asist.getPrecio()%></td>
				<td class="detalleNro" style="text-align:right"><%=asist.getCantidad()%></td>
				<td class="texto" style="text-align:left"><%=asist.getDscUnidad()%></td>
				<td class="totalNro" style="text-align:right"><%=asist.getSubtotal()%></td>
				<td class="detalleNro" style="text-align:right"><%=asist.getProrrateo()%></td>
			</tr>
			<%
				}
			}
			request.getSession().removeAttribute("consulta");
			alumno=null;
			%>
			<tr class="texto_grilla">
				<td style="text-align:right">&nbsp;</td>
				<td style="text-align:left" colspan="2">&nbsp;</td>
				<td style="text-align:right">&nbsp;</td>
				<td style="text-align:center">&nbsp;</td>
				<td style="text-align:right">&nbsp;</td>
				<td style="text-align:right">&nbsp;</td>				
			</tr>
			<tr class="texto_grilla">
				<td style="text-align:right">&nbsp;</td>
				<td style="text-align:left" colspan="2">&nbsp;</td>
				<td style="text-align:right">&nbsp;</td>
				<td style="text-align:center">&nbsp;</td>
				<td class="texto_bold" style="text-align:right">Total</td>
				<td class="totalNro" style="text-align:right">${model.totsum}</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
	  <td></td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Fecha de Preparaci&oacute;n</td>
	  <td align="left" class="texto">: </td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">T/Cambio</td>
	  <td align="left" class="texto">: </td>
	</tr>
	<tr>
	  <td></td>
	</tr>
</table>
<table align="center">
	<tr>
		<td colspan="9">
		<table border="1" align="center">
			<tr rowspan="1">
				<td class="cabecera_grilla" style="width:320px" colspan="3">Concepto</td>
				<td class="cabecera_grilla" style="width:120px" >Factura</td>
				<td class="cabecera_grilla" style="width:120px" >Fecha</td>
				<td class="cabecera_grilla" style="width:100px" >Moneda</td>
				<td class="cabecera_grilla" style="width:120px" >T/C</td>
				<td class="cabecera_grilla" style="width:120px" >Monto</td>
				<td class="cabecera_grilla" style="width:120px" >Monto S/.</td>
			</tr>
			<%
			alumno = (List)request.getSession().getAttribute("listar");
			double cantidadent = 0;
			if(alumno != null){
				for(int j = 0; j < alumno.size(); j++){
					DocumentoAsociado asist = (DocumentoAsociado)alumno.get(j);
					if (asist.getMonSoles()!=null)
						cantidadent = cantidadent+Double.valueOf(asist.getMonSoles()).doubleValue();
					else
						cantidadent = 0;
			%>
			<tr class="texto_grilla">
				<td class="texto" style="text-align:left" colspan="3"><%=asist.getDescripcion()%></td>
				<td class="texto" style="text-align:right"><%=asist.getNumero()%></td>
				<td class="texto" style="text-align:right"><%=asist.getFecha()%></td>
				<td class="texto" style="text-align:left"><%=asist.getDesTipMoneda()%></td>
				<td class="detalleNro" style="text-align:right"><%=asist.getImpTipCamSoles()%></td>
				<td class="totalNro" style="text-align:right"><%=asist.getMonto()%></td>
				<td class="detalleNro" style="text-align:right"><%=asist.getMonSoles()%></td>
			</tr>
			<%
				}
			}
			request.getSession().removeAttribute("consulta");
			%>
			<tr class="texto_grilla">
				<td style="text-align:left" colspan="3">&nbsp;</td>
				<td style="text-align:right">&nbsp;</td>
				<td style="text-align:right">&nbsp;</td>
				<td style="text-align:center">&nbsp;</td>
				<td style="text-align:right">&nbsp;</td>
				<td style="text-align:right">&nbsp;</td>
				<td style="text-align:right">&nbsp;</td>				
			</tr>
			<tr class="texto_grilla">
				<td style="text-align:left" colspan="3">&nbsp;</td>
				<td style="text-align:right">&nbsp;</td>
				<td style="text-align:right">&nbsp;</td>
				<td style="text-align:center">&nbsp;</td>
				<td style="text-align:right">&nbsp;</td>
				<td class="texto_bold" style="text-align:right">Total</td>
				<td class="totalNro" style="text-align:right"><%=cantidadent%></td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<table>
	<tr><td height="7px"></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr><td height="7px"></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>
</body>
</html>