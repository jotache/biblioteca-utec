<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.InterfazContableResultado'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>
<body >
<table>
<tr>
	<td align="center" style="font-size:12pt; vertical-align: middle" colspan="10"><u><b>MOVIMIENTO CONTABLE DE ALMACEN - SEDE  ${model.NOM_SEDE}</b></u></td>
</tr>
<tr>
	<td align="center" style="font-size:12pt; vertical-align: middle"  colspan="10"><u><b>${model.dMes} - ${model.dAnio}</b></u></td>
</tr>
<tr>
</tr>
</table>
	<table align="center">
	<tr>
	<td>
	<table border="1" align="center">
	<tr rowspan="2">
				<td class="cabecera_grilla" style="width:180px">CD</td>
				<td class="cabecera_grilla" style="width:180px">TD</td>
				<td class="cabecera_grilla" style="width:180px">NUMERO</td>							
				<td class="cabecera_grilla" style="width:180px">ITEM</td>
				<td class="cabecera_grilla" style="width:180px">FECHA</td>
				<td class="cabecera_grilla" style="width:180px">DESCRIPCION</td>		
				<td class="cabecera_grilla" style="width:180px">CODIGO <br> CUENTA</td>							
				<td class="cabecera_grilla" style="width:180px">CONTABLE<br>C. GASTO</td>
				<td class="cabecera_grilla" style="width:180px">DEBE</td>
				<td class="cabecera_grilla" style="width:180px">HABER</td>	
	</tr>
	<%
	List alumno = (List)request.getSession().getAttribute("consulta");
	if(alumno != null){
		for(int j = 0; j < alumno.size(); j++){
			InterfazContableResultado asist = (InterfazContableResultado)alumno.get(j);
	%>
	<%if (asist.getCd() != null){%>
	<tr class="texto_grilla">
		<td width="50%"  style="text-align:center"><%=asist.getCd()%></td>
		<td width="50%"  style="text-align:center"><%=asist.getTd()%></td>
		<td width="50%"  style="text-align:right"><%=asist.getNumero()%></td>
		<td width="50%"  style="text-align:center"><%=j+1%></td>
		<td width="50%"  style="text-align:right"><%=asist.getFecha()%></td>
		<td width="50%"  style="text-align:left"><%=asist.getDescripcion()%></td>
		<td width="50%"  style="text-align:center"><%=asist.getCodigoCuenta()%></td>
		<td width="50%"  style="text-align:center"><%=asist.getContableCeco()%></td>
		<td width="50%" class="detalleNro" style="text-align:right"><%=asist.getDebe()%></td>
		<td width="50%" class="detalleNro" style="text-align:right"><%=asist.getHaber()%></td>
	</tr>
	<%}
		}
	}request.getSession().removeAttribute("consulta"); 
	%>
	</table>
	</td>
	</tr>
	</table>
	<table align="right">
	<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
	<td align="right" class="detalleNro" style="font-size:8pt">${model.totDebe}</td>
	<td align="right" class="detalleNro" style="font-size:8pt">${model.totHaber}</td>
	</tr>	
	</table>
</body>
</html>