<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.DetalleDocPago'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.79in .40in .79in .40in;
	mso-header-margin:0cm;
	mso-footer-margin:0cm;}
-->
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: White;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:"0\.000";
}

.totalNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:"0\.00";
}

</style>

<body >
<table>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" colspan="3"><u><b>DOCUMENTO DE PAGO</b></u></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td colspan="1" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td><td></td><td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<!-- ALQD,13/11/08. ELIMINANDO UNA LINEA TR-->
</table>
	<table>	
<!-- ALQD,13/11/08. A�ADIENDO UNA LINEA TR Y NUEVOS CAMPOS-->
	<tr>
	  <td></td>
	  <td class="texto_bold">Nro. Interno</td>
	  <td align="left" class="texto">: ${model.sNroInterno}</td>
	  <td align="right" class="texto">Fecha Cierre </td>
	  <td align="left" class="texto">: ${model.sFecCierre}</td>
	  <td align="right" class="texto">Estado</td>
	  <td align="left" class="texto">: ${model.sEstIngreso}</td>
	</tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Nro. Factura</td>
	  <td align="left" class="texto">: ${model.sNroFactura}</td>
	  <td align="right" class="texto">Fecha Emisi&oacute;n: </td>
	  <td align="left" class="texto">: ${model.sFechaEmi1}</td>
	  <td align="right" class="texto">Fecha Vcto</td>
	  <td align="left" class="texto">: ${model.sFechaVcto}</td>
<!--	  <td align="left" class="texto">&nbsp;</td>
	  <td align="left" class="texto">&nbsp;</td> -->
	  </tr>
	<tr>
	  <td></td>
	  <td class="texto_bold">Nro. Gu&iacute;a </td>
	  <td align="left" class="texto">: ${model.sNroGuia}</td>
	  <td align="right" class="texto">Fecha Emisi&oacute;n </td>
	  <td align="left" class="texto">: ${model.sFechaEmi2}</td>
	  <td align="right" class="texto">Moneda</td>
	  <td align="left" class="texto">: ${model.sMoneda}</td>
<!--	  <td align="left" class="texto">&nbsp;</td>
	  <td align="left" class="texto">&nbsp;</td> -->
	  </tr>
	<tr>
	<td></td>
	<td class="texto_bold">Nro. Orden </td>
	<td align="left" class="texto">: ${model.sNroOrden}</td>
	<td align="right" class="texto">Fecha Emisi&oacute;n</td>
	<td align="left" class="texto">: ${model.sFechaEmi3}</td>
	<td align="right" class="texto">Monto</td>
	<td align="left" class="totalNro">: ${model.sMonto}</td>
<!--	<td align="left" class="texto">&nbsp;</td>
	<td align="left" class="texto">&nbsp;</td> -->
	</tr>
	<tr>
	<td></td>
	<td class="texto_bold">Sede</td>
	<td colspan="2" align="left" class="texto">: ${model.lugarTrabajo}</td>
	<td align="left" class="texto">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td class="texto_bold">Proveedor</td>
		<td colspan="4" align="left" class="texto">: ${model.sNomProveedor}</td>
		<td></td>
<!--		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>-->
	</tr>
	<table align="center">
	<tr>		
		<td>		
	   <table border="1" align="center">
			<tr rowspan="2">			
				<td class="cabecera_grilla" style="width:160px" colspan="1">C&oacute;digo</td>		
				<td class="cabecera_grilla" style="width:160px" colspan="2">Producto/Servicio</td>		
				<!--td class="cabecera_grilla" style="width:120px" >Cant.<br>Solicitada</td-->
				<td class="cabecera_grilla" style="width:120px" >Precio<br>Unitario</td>
				<td class="cabecera_grilla" style="width:120px" >Cant.<br>Facturada</td>
				<td class="cabecera_grilla" style="width:100px" >Unidad</td>
				<td class="cabecera_grilla" style="width:120px" >SubTotal<br>Facturado</td>
				<!--td class="cabecera_grilla" style="width:200px" >Observaciones</td-->
			</tr>
	<%
	List alumno = (List)request.getSession().getAttribute("consulta");
	if(alumno != null){
		for(int j = 0; j < alumno.size(); j++){
			DetalleDocPago asist = (DetalleDocPago)alumno.get(j);
			
			/*double cantidadent = 0;
			if (asist.getCantidad()!=null)
				cantidadent = Double.valueOf(asist.getCantidad()).doubleValue();
			else
				cantidadent = 0;*/
	%>
	
	<tr class="texto_grilla">
		<td class="texto" style="text-align:right"><%=asist.getCodItem()%></td>	
		<td style="text-align:left" colspan="2"><%=asist.getNomItem()%></td>	
		<%--						
		<td class="detalleNro" style="text-align:right"><%=asist.getCantidad()%></td>
		--%>
		<td class="detalleNro" style="text-align:right"><%=asist.getPreUni()%></td>
		<td class="detalleNro" style="text-align:right"><%=asist.getCantFacturada()%></td>
		<td class="texto" style="text-align:left"><%=asist.getUnidadMedida()%></td>	
		<td class="totalNro" style="text-align:right"><%=asist.getSubTotal()%></td>
		<%--
		<td class="detalleNro" style="text-align:right"><%=asist.getObservacion()%></td>
		--%>			
	</tr>
	
	<%
		}//if
	}
	request.getSession().removeAttribute("consulta"); 
	%>	

	<tr class="texto_grilla">
		<td style="text-align:right">&nbsp;</td>
		<td style="text-align:left" colspan="2">&nbsp;</td>							
		<td style="text-align:right">&nbsp;</td>
		<td style="text-align:center">&nbsp;</td>
		<td style="text-align:right">&nbsp;</td>	
		<td style="text-align:right">&nbsp;</td>				
	</tr>	
	<tr class="texto_grilla">
		<td style="text-align:right">&nbsp;</td>
		<td style="text-align:left" colspan="2">&nbsp;</td>							
		<td style="text-align:right">&nbsp;</td>
		<td style="text-align:center">&nbsp;</td>
		<td class="texto_bold" style="text-align:right">Sub - Total</td>	
		<td class="totalNro" style="text-align:right">${model.sSubTotal}</td>				
	</tr>
	<tr class="texto_grilla">
		<td style="text-align:right">&nbsp;</td>
		<td style="text-align:left" colspan="2">&nbsp;</td>							
		<td style="text-align:right">&nbsp;</td>
		<td style="text-align:center">&nbsp;</td>
		<td class="texto_bold" style="text-align:right">sIGV</td>	
		<td class="totalNro" style="text-align:right">${model.sIGV}</td>		
	</tr>
	<tr class="texto_grilla">
		<td style="text-align:right">&nbsp;</td>
		<td style="text-align:left" colspan="2">&nbsp;</td>							
		<td style="text-align:right">&nbsp;</td>
		<td style="text-align:center">&nbsp;</td>
		<td class="texto_bold" style="text-align:right">Total</td>	
		<td class="totalNro" style="text-align:right">${model.sTotal}</td>		
	</tr>
	
	</table>

<table>
<%--
<tr>
	<td></td>
	<td></td>
	<td></td><td></td>
	<td align="right" style="font-size:8pt">Total S/. :</td>
	<td align="right" class="texto_grilla" class="detalleNro" style="font-size:8pt">${model.total}</td><td></td>
	</tr>
		--%>
	
	<tr><td height="7px"></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>	
	<tr><td height="7px"></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>		
	</table></td>
	</tr></table></table>
</body>
</html>