<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.ReportesLogistica'%>
<%@page import='java.math.BigDecimal'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator"
	prefix="html"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.79in .40in .79in .40in;
	mso-header-margin:0cm;
	mso-footer-margin:0cm;}
-->
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
}

.texto_grilla_c {
	FONT-SIZE: 10px; */
	COLOR: #636563; */
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body>
<table class="tabla" width="896">
	<tr>
		<td><img src="${ctx}/images/logoTecsup.jpg"></td>
		<td class="texto_bold" colspan="2">&nbsp;</td>
		<td></td>
		<td></td><td></td><td></td><td></td><td></td><td></td>
		<td></td><td></td><td></td><td></td>
		<td class="texto_bold">${model.FECHA_ACTUAL}</td>
	</tr>

	<tr>
		<td colspan="13">&nbsp;</td>
		<td class="texto_bold" colspan="2" align="right">Hora :${model.HORA_ACTUAL}</td>
	</tr>
	<tr>
	    <td class="texto_bold" colspan="4">TECSUP-${model.NOM_SEDE}</td>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>

<table class="tabla" width="896">
	<tr>
		<td class="texto_bold" colspan="15">
		<div align="center" class="texto_bold">Reporte de Ingresos a Almacen</div>
		</td>
	</tr>
	<tr>
		<td class="texto_bold" colspan="3">&nbsp;</td>
	</tr>
</table>

<table class="tabla" width="896">
	<tr class="texto_grilla">
		<td></td>
		<td class="texto_bold">
		<div align="right">Del</div>
		</td>
		<td>
		<div align="center" class="texto">
		<div align="left">${model.FECHA_INI}</div>
		</div>
		</td>
		<td class="texto_bold">
		<div align="right">Al</div>
		</td>
		<td>
		<div align="center" class="texto">${model.FECHA_FIN}
		<div align="left"></div>
		</div>
		</td>
		<td></td>
	</tr>

	<tr>
		<td class="texto_bold" colspan="3">&nbsp;</td>
	</tr>
</table>


<table class="tabla" width="896">
	<tr class="texto_grilla">
		<td class="texto_bold">Usuario:</td>
		<td colspan="2" class="texto_bold">${model.NOM_USUARIO}</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>

	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>


<table width="896" border="1" bordercolor="#000000" class="tabla">
	<tr bgcolor="#CCCCCC" class="cabecera_grilla" style="height: 30px;">
		<td><span class="Estilo5">Factura</span></td>
		<td><span class="Estilo5">Fecha Ing.</span></td>
		<td><span class="Estilo5">O / C</span></td>
		<td><span class="Estilo5">Fec. Aprob. O/C</span></td>
		<td><span class="Estilo5">Proveedor</span></td>
		<td><span class="Estilo5">Tipo Bien</span></td>
		<td><span class="Estilo5">Fam-Subfamilia</span></td>
		<td><span class="Estilo5">Codigo Bien</span></td>
		<td><span class="Estilo5">Descripcion</span></td>
		<td><span class="Estilo5">Uni. Medida</span></td>
		<td><span class="Estilo5">Cantidad</span></td>
		<td><span class="Estilo5">P. Unitario</span></td>
		<td><span class="Estilo5">Importe</span></td>
		<td><span class="Estilo5">Cant. X Entregar</span></td>
		<td><span class="Estilo5">Est. O / C</span></td>
	</tr>

<%
	if ( request.getAttribute("LST_RESULTADO")!= null ) 
	{
	
		List consulta = (List) request.getAttribute("LST_RESULTADO");
		
		BigDecimal sumImporte = new BigDecimal("0");
		BigDecimal sumImporteTotal = new BigDecimal("0");
		String guia = "";
		String guiaVariable = "";
		ReportesLogistica objFuturo =null;
		String nroGuiaFutura = "";
		
		for (int i = 0; i < consulta.size(); i++) 
		{
			ReportesLogistica obj = (ReportesLogistica) consulta.get(i);
			guiaVariable = obj.getNroGuia();
%>
	<tr class="texto_grilla">
		<td  style="text-align:center"><%=obj.getNroGuia()%></td>
		<td  style="text-align:right"><%=obj.getFechaCierreGuia()%></td>
		<td  style="text-align:center"><%=obj.getNroOrden()%></td>
		<td  style="text-align:right"><%=obj.getFechaOrden()%></td>
		<td  style="text-align:left"><%=obj.getDscProveedor()%></td>
		<td  style="text-align:left"><%=obj.getDscTipoBien()%></td>
		<td  style="text-align:center"><%=obj.getDscFamiliaBien()%></td>
		<td  style="text-align:center"><%=obj.getCodBien()%></td>
		<td  style="text-align:left"><%=obj.getDscBien()%></td>
		<td  style="text-align:left"><%=obj.getDscUnidad()%></td>
		<td class="detalleNro" style="text-align:right"><%=obj.getCantidad()%></td>
		<td class="detalleNro" style="text-align:right"><%=obj.getCostoUnitario()%></td>
		<td class="detalleNro" style="text-align:right"><%=obj.getImporte()%></td>
		<td class="detalleNro" style="text-align:right"><%=obj.getCantPorEntregar()%></td>
		<td class="detalleNro" style="text-align:left"><%=obj.getDscEstadoOrden()%></td>
	</tr>
		
		<%sumImporte = sumImporte.add(  new BigDecimal(obj.getImporte())  );
		
		if( i+1<consulta.size() )
		{	objFuturo = (ReportesLogistica) consulta.get(i+1);
			nroGuiaFutura = objFuturo.getNroGuia();
			if(!guiaVariable.equalsIgnoreCase(nroGuiaFutura))
			{/**PARA LOS QUIEBRES**/%>
				<tr class="texto_grilla">
					<td colspan="10">&nbsp;</td>
					<td class="Estilo5" style="text-align:left" colspan="2">TOTAL INGRESO S/</td>
					<td class="detalleNro" style="text-align:right"><%=sumImporte.toString()%></td>
				</tr>
			<%
			sumImporteTotal = sumImporteTotal.add(sumImporte);
			sumImporte = new BigDecimal("0");
			}
		}else if( (consulta.size()-(i+1)) == 0  ){
			/**PARA EL ULTIMO REGISTRO**/
			sumImporteTotal = sumImporteTotal.add(sumImporte);
		%>
			<tr class="texto_grilla">
			 <td colspan="10">&nbsp;</td>
			 <td class="Estilo5" style="text-align:left" colspan="2">TOTAL INGRESO S/</td>
			 <td class="detalleNro" style="text-align:right"><%=sumImporte.toString()%></td>
			</tr>
			<!-- TOTAL GENERAL -->
			<tr class="texto_grilla"><td colspan="15">&nbsp;</td></tr>
			<tr class="texto_grilla">
			 <td colspan="10">&nbsp;</td>
			 <td class="Estilo5" style="text-align:left" colspan="2">TOTAL GENERAL S/</td>
			 <td class="detalleNro" style="text-align:right"><%=sumImporteTotal.toString()%></td>
			</tr>
		<%
			
		}
	}/*FOR*/
  }/*IF*/
%>
</table>

</body>
</html>