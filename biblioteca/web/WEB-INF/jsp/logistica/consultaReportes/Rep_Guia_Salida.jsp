<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.GuiaDetalle'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.79in .40in .79in .40in;
	mso-header-margin:0cm;
	mso-footer-margin:0cm;}
-->
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: White;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:"0\.000";
}

.totalNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body >
<table>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" colspan="3"><u><b>GUIAS DE SALIDA</b></u></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td colspan="1" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td><td></td><td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
	<!-- ALQD,13/11/08. A�ADIENDO MAS DATOS -->
	<table>	
	<tr>
	<td></td>
	<td class="texto_bold">Nro. Gu&iacute;a </td>
	<td align="left" class="texto">: ${model.nroGuia}</td>
	<td align="right" class="texto">Fecha Gu&iacute;a </td>
	<td align="left" class="texto">: ${model.fechaGuia}</td>
	<td align="right" class="texto">Estado</td>
	<td align="left" class="texto">: ${model.estado}</td>
	<td align="left" class="texto">&nbsp;</td>
	<td align="left" class="texto">&nbsp;</td>
	</tr>
	<tr>
	<td></td>
	  <td class="texto_bold">Solicitante</td>
	  <td colspan="3" align="left" class="texto">: ${model.solicitante}</td>
	  
	  <td align="right" class="texto">fec. Cierre</td>
	  <td align="left" class="texto">: ${model.fecCierre}</td>
	  
	  <td align="left" class="texto">&nbsp;</td>
	  </tr>
	<tr>
	<td></td>
	<td class="texto_bold">Lugar de trabajo:</td>
	<td colspan="5" align="left" class="texto">: ${model.lugarTrabajo}</td>
	<td align="left" class="texto">&nbsp;</td>
	</tr>
	
	<tr>
	<td></td>
		<td class="texto_bold">Nro. de Requerimiento</td>
		<td align="left" class="texto">: ${model.nroRequerimiento}</td>
		<td colspan="2" class="texto_bold">Fec. Aprobaci&oacute;n</td>
		<td align="left" class="texto">: ${model.fecAprRequerimiento}</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>

	<tr>
	<td></td>
		<td class="texto_bold">Centro de Costo</td>
		<td colspan="4" align="left" class="texto">: ${model.nomCenCosto}</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>

	<table align="center">
	<tr>		
		<td>		
	   <table border="1" align="center">
			<tr rowspan="2">			
				<td class="cabecera_grilla" style="width:30px">C&oacute;digo</td>
				<td class="cabecera_grilla" style="width:180px" colspan="2">Producto</td>		
				<td class="cabecera_grilla" style="width:120px" >Precio<br>Unitario</td>
				<td class="cabecera_grilla" style="width:120px" >Cantidad<br>Entregada</td>
				<td class="cabecera_grilla" style="width:100px" >Unidad</td>
				<td class="cabecera_grilla" style="width:150px" >SubTotal</td>
			</tr>
	<%
	List alumno = (List)request.getSession().getAttribute("consulta");
	if(alumno != null){
		for(int j = 0; j < alumno.size(); j++){
			GuiaDetalle asist = (GuiaDetalle)alumno.get(j);

			/*Long cantSol = Long.valueOf("0");
			Long precioUnit = Long.valueOf("0");
			Long cant = Long.valueOf("0");
			Long total = Long.valueOf("0");
			
			if (asist.getCantidadSolicitada()!=null)
				cantSol = Long.valueOf(asist.getCantidadSolicitada());				 				
			if (asist.getPrecioUnitario()!=null)			
				precioUnit = Long.valueOf(asist.getPrecioUnitario());
			if (asist.getCantidad()!=null)
				cantSol = Long.valueOf(asist.getCantidad());				 				
			if (asist.getTot()!=null)			
				precioUnit = Long.valueOf(asist.getPrecioUnitario());*/
			double cantidadent = 0;
			if (asist.getCantidad()!=null)
				cantidadent = Double.valueOf(asist.getCantidad()).doubleValue();
			else
				cantidadent = 0;
	%>
	<%if (asist.getDscBien() != null && cantidadent>0){%>
	<tr class="texto_grilla">
		<td class="texto" style="text-align:right"><%=asist.getCodBien()%></td>
		<td style="text-align:left" colspan="2"><%=asist.getDscBien()%></td>							
		<td class="detalleNro" style="text-align:right"><%=asist.getPrecioUnitario()%></td>
		<td class="detalleNro" style="text-align:right"><%=asist.getCantidad()%></td>
		<td class="detalleNro" style="text-align:center"><%=asist.getUnidadMedida()%></td>
		<td class="detalleNro" style="text-align:right"><%=asist.getTot()%></td>
	</tr>
	
	<%}
		}
	}request.getSession().removeAttribute("consulta"); 
	%>	
	</table>

<table>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td><td></td>
	<td align="right" style="font-size:8pt">Total S/. :</td>
	<td align="right" class="texto_grilla" class="detalleNro" style="font-size:8pt">${model.total}</td><td></td>
	</tr>
	
	<tr><td height="7px"></td>
	<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>	
	<tr><td height="7px"></td>
	<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>		
		<tr>
	<td></td>
		<td height="7px" colspan="2" align="center">________________________________</td>
		<td></td>		
		<td></td>
		<td colspan="2" align="center">______________________________</td>		
	</tr>
	<tr>
	<td></td>
		<td height="7px" colspan="2" class="texto_bold" align="center">Firma Entregado</td>
		<td></td>		
		<td></td>
		<td colspan="2" class="texto_bold" align="center">Firma Recibido</td>		
	</tr>	
	
	</table></td>
	</tr></table></table>
</body>
</html>