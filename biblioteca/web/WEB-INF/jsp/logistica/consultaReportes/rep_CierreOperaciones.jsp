<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">
				
		function onLoad(){
		fc_JuegoBandeja();
		}
		
		function fc_Etapa(param){
		
		switch(param){
			case '1':	
			  	
			   	document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienConsumible").value ; 
			  	   			
			    break;
			case '2':
			    
			    document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienActivo").value ; 
			   
			   	break;	
			   	
			}
		}
		
		function fc_IndiceGrupo(param){
		
		document.getElementById("txhValorIndGrupo").value=param;
		document.getElementById("txhOperacion").value = "SERVICIO_RADIO";
		document.getElementById("frmMain").submit();
		}
		
		function fc_TipoReq(){
		if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteBien").value)
		{  radio1.style.display = 'inline-block';
		   radio2.style.display = 'none';
		   //Tipo_Servicio.style.display = 'none';
		   
		}
		else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
				{ radio2.style.display = 'inline-block';
			  	  radio1.style.display = 'none';
			 	  //Tipo_Servicio.style.display = 'inline-block';
		   	 	  
				}
		 }
	  
		}
		
		function fc_Sede(){
		
			document.getElementById("txhOperacion").value = "SEDE";
			document.getElementById("frmMain").submit();
		  
		}
		
		function fc_JuegoBandeja(){
		
		if(document.getElementById("txhCodTipoReporte").value==document.getElementById("txhCodReporte5").value)
		{  CentroCosto_1.style.display = 'inline-block';
		   Tipo_Gastos_1.style.display = 'inline-block';
		   //Tipo_Servicio.style.display = 'inline-block';
		   fc_TipoReq();
		}
		else{ if(document.getElementById("txhCodTipoReporte").value==document.getElementById("txhCodReporte7").value)
			  {  EstadoRequerimiento_7.style.display = 'inline-block';
				 Solicitante_7.style.display = 'inline-block';
				 //Tipo_Servicio.style.display = 'inline-block';
				 fc_TipoReq();
			  }	
			  else{	if(document.getElementById("txhCodTipoReporte").value==document.getElementById("txhCodReporte9").value)
		 			 { Proveedor_7.style.display = 'inline-block';
		 			   fc_TipoReq();
		 			   
		 			 }
			  }	
		}
		     
		}
		
		function fc_Generar(){ 
		url="";
		opcion=document.getElementById("txhCodTipoReporte").value;
		
		switch(opcion){
			case document.getElementById("txhCodReporte5").value:
				var ban="";
				ban=fc_Val_IngresoAlmacen();
				var radio="";
				 var grupoServ="";
				 if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
				 { radio="";
				   grupoServ=fc_Trim(document.getElementById("codGrupoServicio").value);
				 }
				 else{ radio=fc_Trim(document.getElementById("txhConsteRadio").value);
				       grupoServ="";
				 }
				//alert("bandera: "+ban);
				if(ban=="0")	
				{
				        url="?tCodRep=" + fc_Trim(document.getElementById("txhCodTipoReporte").value) +
						"&tCodSede=" + fc_Trim(document.getElementById("codSede").value) +
						"&tCodTipoReq=" + fc_Trim(document.getElementById("codTipoRequerimiento").value) + 
						"&tCodTipoBien=" + radio +  
						"&tCodTipoServicio=" + grupoServ + 
						"&tCodCeco=" + fc_Trim(document.getElementById("codCentroCosto").value) + 
						"&tCodTipoGasto=" + fc_Trim(document.getElementById("codTipoGastos").value) + 
						"&tFecIni=" + fc_Trim(document.getElementById("txtFecInicio").value) +
						"&tFecFin=" + fc_Trim(document.getElementById("txtFecFinal").value) +
						"&tCodUsu=" + fc_Trim(document.getElementById("txhCodUsuario").value) ; 
					//alert(url);		
				}
				else{  if(ban=="2") alert(mstrFechaInicio);
				       else{ if(ban=="3") alert(mstrFechaFin); 
				             else alert(mstrFecha);
				           }
				} 
				break;
			case document.getElementById("txhCodReporte7").value:				
				 var ban="";
				 ban=fc_Val_IngresoAlmacen();
				 var radio="";
				 var grupoServ="";
				 
				 if(ban=="0")	
				 { var al="0";
				   if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
				   {    al="1";
				   		radio="";
				   		grupoServ=fc_Trim(document.getElementById("codGrupoServicio").value);
				   }
				 	else{ radio=fc_Trim(document.getElementById("txhConsteRadio").value);
				       	  grupoServ="";
				 	}     
				       
				   if(al=="1")
				   { if(document.getElementById("codGrupoServicio").value=="-1")
					     { alert(mstrSelTipoServicio);
					       return false;
					     } 
				   }   
					 url="?tCodRep=" + fc_Trim(document.getElementById("txhCodTipoReporte").value) +
						"&tCodSede=" + fc_Trim(document.getElementById("codSede").value) +
						"&tCodTipoReq=" + fc_Trim(document.getElementById("codTipoRequerimiento").value) +
						"&tCodTipoBien=" + radio +  
						"&tCodTipoServicio=" + grupoServ + 
						"&tCodUsu=" + fc_Trim(document.getElementById("txhCodUsuario").value) + 
						"&tCodEstadoReq=" + fc_Trim(document.getElementById("codEstadoReq").value) +
						"&tFecIni=" + fc_Trim(document.getElementById("txtFecInicio").value) +
						"&tFecFin=" + fc_Trim(document.getElementById("txtFecFinal").value) +
						"&tUSol=" + fc_Trim(document.getElementById("usuSolicitante").value) ;
					//alert(url);	
				   
				}
				else{  if(ban=="2") alert(mstrFechaInicio);
				       else{ if(ban=="3") alert(mstrFechaFin); 
				             else alert(mstrFecha);
				           }
				} 
				break;
			
			case document.getElementById("txhCodReporte9").value:				
				 var ban="";
				 ban=fc_Val_IngresoAlmacen();
				 var radio="";
				 var grupoServ="";
				 if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
				 { radio="";
				   grupoServ=fc_Trim(document.getElementById("codGrupoServicio").value);
				 }
				 else{ radio=fc_Trim(document.getElementById("txhConsteRadio").value);
				       grupoServ="";
				 }
				 
				 if(ban=="0")	
				 {
					url="?tCodRep=" + fc_Trim(document.getElementById("txhCodTipoReporte").value) +
					"&tCodSede=" + fc_Trim(document.getElementById("codSede").value) +
					"&tCodTipoReq=" + fc_Trim(document.getElementById("codTipoRequerimiento").value) + 
					"&tCodTipoBien=" +  radio +  
					"&tCodTipoServicio=" + grupoServ +
					"&tProveedor=" + fc_Trim(document.getElementById("proveedor").value) + 
					//"&txhDscUsu=" + fc_Trim(document.getElementById("txhDscUsuario").value) + 
					"&tCodUsu=" + fc_Trim(document.getElementById("txhCodUsuario").value) + 
					"&tFecIni=" + fc_Trim(document.getElementById("txtFecInicio").value) +
					"&tFecFin=" + fc_Trim(document.getElementById("txtFecFinal").value) ;
									 
				}
				else{  if(ban=="2") alert(mstrFechaInicio);
				       else{ if(ban=="3") alert(mstrFechaFin); 
				             else alert(mstrFecha);
				           }
				}  
				break;
			
		}
		/*alert(url.length);
		alert(url);*/
		if(url!="")
		 window.open("${ctx}/logistica/reportesLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");
	}
	
	function fc_Val_IngresoAlmacen(){
	 var rpta="1";
	  if(document.getElementById("txtFecInicio").value!="" && document.getElementById("txtFecFinal").value!="")
	   {  rpta=fc_Validar_Fechas(document.getElementById("txtFecInicio").value, document.getElementById("txtFecFinal").value);
	      //alert("Rpta: "+rpta);
	   }
	   else{ if(document.getElementById("txtFecInicio").value=="")
	          rpta="2";
	         else rpta="3";
	     }
	   //alert("Rpta: "+rpta);
	   return rpta; 
	}
	
	function fc_Validar_Fechas(srtFechaInicio, srtFechaFin)
	{
	 var num=0;
	 var srtFecha="0";
	 //alert(srtFechaInicio+">><<"+srtFechaFin);
	 if( srtFechaInicio!="" && srtFechaFin!="")
	{   
		    num=fc_ValidaFechaIniFechaFin(srtFechaFin, srtFechaInicio);
		     //alert("Funcion: "+num);
		     //es 1 si el parametro 1 es mayor al parametro 2
		     //es 0 si el parametro 1 es menor o igual al parametro 2
		     if(num==1) srtFecha="0";
		     else srtFecha="1";
	 }
	 else{ if(srtFechaInicio=="")
	          srtFecha="2";
	       else srtFecha="3";
	     }
	//alert("ValFecha: "+srtFecha);      
    return srtFecha;
	}
	
	function fc_GrupoServicio(){
	/*document.getElementById("txhOperacion").value = "GRUPOSERVICIO";
	document.getElementById("frmMain").submit();*/
	}
	
	function fc_GrupoGastos(){}
	
	function fc_Limpiar(){
	
	document.getElementById("proveedor").value="";
	document.getElementById("usuSolicitante").value="";
	document.getElementById("codEstadoReq").value="-1"; 
	//document.getElementById("codBien").value=""; 
	document.getElementById("codTipoRequerimiento").value="-1";
	document.getElementById("codCentroCosto").value="-1"; 
	document.getElementById("codGrupoServicio").value="-1"; 
	document.getElementById("codTipoGastos").value="-1"; 
	document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienConsumible").value;
	document.getElementById("txhValorIndGrupo").value="0"; 
	
	document.getElementById("codSede").value=document.getElementById("txhConsteSede").value;
	document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienConsumible").value;
	document.getElementById("txhOperacion").value = "LIMPIAR";
	document.getElementById("frmMain").submit();
	}
	
	function fc_TipoServicio(){
	
	}
	
	function fc_ValidarServicio(){
	 if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
	  {  if(document.getElementById("codGrupoServicio").value!="-1")
	        return 1;
	      else{ alert(mstrSelGrupoServicio);
	            return 0;
	          }
	  }
	 else return 1;
	}
	
	/*<td width="4%">
						<input type="radio" id="gbradio1" name="rdoBandeja" align="left" 
						<c:if test="${control.valorIndGrupo=='0'}"><c:out value=" checked=checked " /></c:if> 
						onclick="javascript:fc_IndiceGrupo('0');">
						Grupo Generales / Servicios
						&nbsp;&nbsp;&nbsp;
						<input type="radio" id="gbradio2" name="rdoBandeja" 
						<c:if test="${control.valorIndGrupo=='1'}"><c:out value=" checked=checked " /></c:if> 
						onclick="javascript:fc_IndiceGrupo('1');">
				 		Grupo Mantenimiento
				 	</td>
	*/
</script>
</head>
<body>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/logistica/repCierraOperacion.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="dscUsuario" id="txhDscUsuario"/>
<form:hidden path="consteRadio" id="txhConsteRadio"/>
<form:hidden path="consteBien" id="txhConsteBien"/>
<form:hidden path="consteServicio" id="txhConsteServicio"/>
<form:hidden path="consteBienConsumible" id="txhConsteBienConsumible"/>
<form:hidden path="consteBienActivo" id="txhConsteBienActivo"/>
<form:hidden path="consteGrupoGenerales" id="txhConsteGrupoGenerales"/>
<form:hidden path="consteGrupoServicios" id="txhConsteGrupoServicios"/>
<form:hidden path="consteGrupoMantenimiento" id="txhConsteGrupoMantenimiento"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="valorIndGrupo" id="txhValorIndGrupo"/>
<form:hidden path="nroReporte" id="txhNroReporte"/>
<form:hidden path="tituloReporte" id="txhTituloReporte"/>
<form:hidden path="codTipoReporte" id="txhCodTipoReporte"/>
<form:hidden path="codReporte5" id="txhCodReporte5" />
<form:hidden path="codReporte7" id="txhCodReporte7" />
<form:hidden path="codReporte9" id="txhCodReporte9" />
<form:hidden path="consteSede" id="txhConsteSede" />


<table cellpadding="1" cellspacing="0"  ID="Table1" style="width:98%; margin-top:7px;" 
		  border="0" bordercolor="Red" background="${ctx}/images/Logistica/back.jpg" class="tabla">
	 <tr>
	       <td width="11%">&nbsp;&nbsp;Sede :</td>
	       <td width="25%">
	       		<form:select path="codSede" id="codSede" cssClass="combo_o" 
					cssStyle="width:100px" onchange="javascript:fc_Sede();">
					<c:if test="${control.listaSedes!=null}">
					<form:options itemValue="codSede" itemLabel="dscSede" 
						items="${control.listaSedes}" />
					</c:if>
				</form:select></td>
			
	</tr>
	<tr>
				<td width="10%">&nbsp;&nbsp;Tipo Req. :</td>
				<td width="24%" align="left">
							<form:select path="codTipoRequerimiento" id="codTipoRequerimiento" cssClass="combo_o" 
								cssStyle="width:100px" onchange="javascript:fc_TipoReq();">
								
								<c:if test="${control.listCodTipoRequerimiento!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
									items="${control.listCodTipoRequerimiento}" />
								</c:if>
							</form:select>
				</td>
				<td width="50%">
					<table id="radio1" name="radio1" style="display: none;" bordercolor="blue" border="0" width="100%">
					 <tr>
						<td align="left" width="4%"><input Type="radio" checked name="rdoEtapa"
						    <c:if test="${control.consteRadio==control.consteBienConsumible}"><c:out value=" checked=checked " /></c:if>
							id="gbradio1" onclick="javascript:fc_Etapa('1');">&nbsp;Consumible&nbsp;
						<input Type="radio" name="rdoEtapa"
							<c:if test="${control.consteRadio==control.consteBienActivo}"><c:out value=" checked=checked " /></c:if>
							id="gbradio2" onclick="javascript:fc_Etapa('2');">&nbsp;Activo&nbsp;
						</td>   	
					  
					</tr>
					</table>
					<table id="radio2" name="radio2" style="display: none;" bordercolor="blue" border="0" width="100%">
					<tr>
					<td colspan="4">Grupo Servicio:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<form:select path="codGrupoServicio" id="codGrupoServicio" cssClass="cajatexto" 
							cssStyle="width:180px" onchange="javascript:fc_GrupoServicio();">
							<form:option  value="-1">--Todos--</form:option>
							<c:if test="${control.listaCodGrupoServicio!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaCodGrupoServicio}" />
							</c:if>
						</form:select>
					</td>   	
					  
				</tr>
			  </table>
				</td>
			  	
			</tr>
	      
	       <tr>
	       <td width="25%" colspan="2" style="display: none;" id="CentroCosto_1">&nbsp;&nbsp;Centro Costos:&nbsp;&nbsp;&nbsp;&nbsp;
					<form:select path="codCentroCosto" id="codCentroCosto" cssClass="cajatexto" 
								cssStyle="width:180px">
								<form:option  value="-1">--Todos--</form:option>
								<c:if test="${control.listaCodCentroCosto!=null}">
								<form:options itemValue="codTecsup" itemLabel="descripcion" 
									items="${control.listaCodCentroCosto}" />
								</c:if>
					</form:select>
		   </td>
		   <td idth="25%" colspan="2" style="display: none;" id="EstadoRequerimiento_7">&nbsp;&nbsp;Estado :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		   			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		   			<form:select path="codEstadoReq" id="codEstadoReq" cssClass="cajatexto" 
								cssStyle="width:180px">
								<form:option  value="-1">--Todos--</form:option>
								<c:if test="${control.listaCodEstadoReq!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
									items="${control.listaCodEstadoReq}" />
								</c:if>
					</form:select>
		   </td>
		   <td colspan="2" style="display: none;" id="Proveedor_7">&nbsp;&nbsp;Proveedor :&nbsp;&nbsp;
		   		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 	<form:input path="proveedor" id="proveedor" maxlength="50"
			 		onkeypress="fc_ValidaNombreAutorOnkeyPress();" 
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Proveedor');" 
					cssClass="cajatexto" size="40"/>
		</td>
			    
	       <td colspan="1" width="25%">&nbsp;&nbsp;Per�odo :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<form:input path="fecInicio" id="txtFecInicio" cssClass="cajatexto_o"
								onkeypress="javascript:fc_Slash('frmMain','txtFecInicio','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
								align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>&nbsp;&nbsp;
				<form:input path="fecFinal" id="txtFecFinal" cssClass="cajatexto_o"
								onkeypress="javascript:fc_Slash('frmMain','txtFecFinal','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
								align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>
			</td>
			<td><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
							<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
								 style="cursor: pointer; 60px" 
								 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
			</td>	
			
	       </tr>
	       <tr>
	       		
			 <td colspan="2" style="display: none;" id="Tipo_Servicio">&nbsp;&nbsp;Tipo Servicio :&nbsp;
			 		&nbsp;&nbsp;&nbsp;&nbsp;
					<form:select path="codTipoServicio" id="codTipoServicio" cssClass="cajatexto" 
								cssStyle="width:180px" onchange="javascript:fc_TipoServicio();">
								<form:option  value="-1">--Seleccione--</form:option>
								<c:if test="${control.listaCodTipoServicio!=null}">
								<form:options itemValue="codSecuencial" itemLabel="descripcion" 
								items="${control.listaCodTipoServicio}" />
								</c:if>
							</form:select>
			 </td>
			 <td colspan="2" style="display: none;" id="Tipo_Gastos_1">&nbsp;&nbsp;Tipo Gastos :&nbsp;
			 		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;<form:select path="codTipoGastos" id="codTipoGastos" cssClass="cajatexto" 
								cssStyle="width:180px" onchange="javascript:fc_GrupoGastos();">
								<form:option  value="-1">--Todos--</form:option>
								<c:if test="${control.listaCodTipoGastos!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
									items="${control.listaCodTipoGastos}" />
								</c:if>
							</form:select>
			 </td>
			 <td colspan="2" style="display: none;" id="Solicitante_7">Usuario Solicitante
			 	<form:input path="usuSolicitante" id="usuSolicitante" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();" 
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Usuario Solicitante');" 
					cssClass="cajatexto" size="40"/>
			 </td>
		   </tr>
		   
		   
		</table>
		
<table align="center" style="margin-top: 10px">
		<tr>
			<td>
			<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar','','${ctx}/images/botones/generar2.jpg',1)">
				<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar"></a>
			</td>
		</tr>
</table>

</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
</body>
</html>