<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.DetOrdenPendienteEntrega'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style >
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.79in .40in .79in .40in;
	mso-header-margin:0cm;
	mso-footer-margin:0cm;}
-->
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:"0\.000";
}
.TotalNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Fixed;
}
</style>

<body >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" colspan="6"><u><b>ORDEN COMPRA - PENDIENTE POR ENTREGAR</b></u></td>
	<td></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td><td></td><td></td><td></td><td></td><td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
</table>
<br>
<table align="center">
	<tr>
		<td>
			<table border="1" align="center">
			<tr rowspan="2">
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Orden Num.</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" colspan="3">Proveedor</td>
				<td class="cabecera_grilla" style="width:40px" nowrap="nowrap" >Tipo Pago</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fec. Aprob.</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" colspan="3">Comprador</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Moneda</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Sub. Total</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >IGV</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Total</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" colspan="3">Producto</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Cant. Solicit.</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Cant. Pendien.</td>
				</tr>
			<%
				List alumno = (List)request.getSession().getAttribute("consulta");
			
				if(alumno != null){
					for(int j = 0; j < alumno.size(); j++){
						DetOrdenPendienteEntrega rep = (DetOrdenPendienteEntrega)alumno.get(j);
				%>
				<%if (rep.getNumOrdCompra() != null){ 
					System.out.println("alumno: "+alumno.size());
				%>
				<tr class="texto_grilla">
					<td width="30%" style="text-align:left" ><%=rep.getNumOrdCompra()%></td>
					<td width="30%" style="text-align:left" colspan="3"><%=rep.getNomProveedor()%></td>
					<td width="30%" style="text-align:left"><%=rep.getDesTipoPago()%></td>
					<td width="30%" style="text-align:left"><%=rep.getFecAprobacion()%></td>
					<td width="30%" style="text-align:left" colspan="3"><%=rep.getNomComprador()%></td>
					<td width="30%" style="text-align:left"><%=rep.getMoneda()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getSubTotal()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getTotIGV()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getTotalOC()%></td>
					<td width="30%" style="text-align:left" colspan="3"><%=rep.getNomProducto()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getCanTotal()%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getCanSaldo()%></td>
				</tr>
				<%}
					}
				}request.removeAttribute("consulta"); 
				%>	
			</table>
		</td>
	</tr>
</table>
<br>

</body>
</html>
















