<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">
				
		function onLoad(){
			fc_JuegoBandeja();
		}
		
		function fc_Etapa(param){	
			document.getElementById("txhConsteRadio").value=param;			
		}
		
		function fc_IndiceGrupo(param){		
			document.getElementById("txhValorIndGrupo").value=param;
			document.getElementById("txhOperacion").value = "SERVICIO_RADIO";
			document.getElementById("frmMain").submit();
		}
		
		function fc_TipoReq(){
			if("1"==document.getElementById("txhConsteBien").value){
			  radio1.style.display = 'inline-block';		   		  
			}else if("1"==document.getElementById("txhConsteServicio").value){				 
				  	  radio1.style.display = 'none';			 	  		   	 	  
			}		 	 
		}
		
		function fc_Sede(){		
			//document.getElementById("txhOperacion").value = "SEDE";
			//document.getElementById("frmMain").submit();		  
		}
		
		function fc_JuegoBandeja(){
		
		if(document.getElementById("txhCodTipoReporte").value==document.getElementById("txhCodReporte5").value)
		{  
		   
		   //Tipo_Servicio.style.display = 'inline-block';
		   fc_TipoReq();
		}
		else{ if(document.getElementById("txhCodTipoReporte").value==document.getElementById("txhCodReporte7").value)
			  {  
				 
				 //Tipo_Servicio.style.display = 'inline-block';
				 fc_TipoReq();
			  }	
			  else{	if(document.getElementById("txhCodTipoReporte").value==document.getElementById("txhCodReporte9").value)
		 			 { 
		 			   fc_TipoReq();
		 			   
		 			 }
			  }	
		}
		     
		}
		
		function fc_Generar(){ 
		url="";
		opcion=document.getElementById("txhCodTipoReporte").value;
		
		switch(opcion){
			case document.getElementById("txhCodReporte29").value:
				var ban="";
				var radio="";
				ban=fc_Val_IngresoAlmacen();				
				radio=fc_Trim(document.getElementById("txhConsteRadio").value);				
				if(ban=="0"){				
				  	url="?tCodRep=" + fc_Trim(document.getElementById("txhCodTipoReporte").value) +
						"&tCodSede=" + fc_Trim(document.getElementById("codSede").value) +
						"&tNomSede=" + document.getElementById("codSede").options[document.getElementById("codSede").selectedIndex].text +					
						"&tCodTipoOrden=" + radio + //inversion Si No  
						"&tFecIni=" + fc_Trim(document.getElementById("txtFecInicio").value) +
						"&tFecFin=" + fc_Trim(document.getElementById("txtFecFinal").value) +
						"&tCodUsu=" + fc_Trim(document.getElementById("txhCodUsuario").value) +
						"&tUSol=" + fc_Trim(document.getElementById("usuSolicitante").value) ; 
					//alert(url);		
				}
				else{  if(ban=="2") alert(mstrFechaInicio);
				       else{ if(ban=="3") alert(mstrFechaFin); 
				             else alert(mstrFecha);
				           }
				}
				break;
		}
				
		if(url!="")
		 window.open("${ctx}/logistica/reportesLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");
	}
	
	function fc_Val_IngresoAlmacen(){
	 var rpta="1";
	  if(document.getElementById("txtFecInicio").value!="" && document.getElementById("txtFecFinal").value!=""){
	     rpta=fc_Validar_Fechas(document.getElementById("txtFecInicio").value, document.getElementById("txtFecFinal").value);	    
	  }else if(document.getElementById("txtFecInicio").value==""){
	     rpta="2";
	  }else 
		 rpta="3";
	     	   
	   return rpta; 
	}
	
	function fc_Validar_Fechas(srtFechaInicio, srtFechaFin)
	{
	 var num=0;
	 var srtFecha="0";
	 //alert(srtFechaInicio+">><<"+srtFechaFin);
	 if( srtFechaInicio!="" && srtFechaFin!="")
	{   
		    num=fc_ValidaFechaIniFechaFin(srtFechaFin, srtFechaInicio);
		     //alert("Funcion: "+num);
		     //es 1 si el parametro 1 es mayor al parametro 2
		     //es 0 si el parametro 1 es menor o igual al parametro 2
		     if(num==1) srtFecha="0";
		     else srtFecha="1";
	 }
	 else{ if(srtFechaInicio=="")
	          srtFecha="2";
	       else srtFecha="3";
	     }
	//alert("ValFecha: "+srtFecha);      
    return srtFecha;
	}
	
	function fc_GrupoServicio(){
	/*document.getElementById("txhOperacion").value = "GRUPOSERVICIO";
	document.getElementById("frmMain").submit();*/
	}
	
	function fc_GrupoGastos(){}
	
	function fc_Limpiar(){
	
	
	document.getElementById("usuSolicitante").value="";
	 
	//document.getElementById("codBien").value=""; 		 	 
	document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienConsumible").value;
	document.getElementById("txhValorIndGrupo").value="0"; 	
	document.getElementById("codSede").value=document.getElementById("txhConsteSede").value;
	document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienConsumible").value;
	document.getElementById("txhOperacion").value = "LIMPIAR";
	document.getElementById("frmMain").submit();
	}
	
	function fc_TipoServicio(){
	
	}
	
	function fc_ValidarServicio(){
	 if("1"==document.getElementById("txhConsteServicio").value)
	  {  if(document.getElementById("codGrupoServicio").value!="-1")
	        return 1;
	      else{ alert(mstrSelGrupoServicio);
	            return 0;
	          }
	  }
	 else return 1;
	}
		
</script>
</head>
<body>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/logistica/repFechasOperacion.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="dscUsuario" id="txhDscUsuario"/>
<form:hidden path="consteRadio" id="txhConsteRadio"/>
<form:hidden path="consteBien" id="txhConsteBien"/>
<form:hidden path="consteServicio" id="txhConsteServicio"/>
<form:hidden path="consteBienConsumible" id="txhConsteBienConsumible"/>
<form:hidden path="consteBienActivo" id="txhConsteBienActivo"/>
<form:hidden path="consteGrupoGenerales" id="txhConsteGrupoGenerales"/>
<form:hidden path="consteGrupoServicios" id="txhConsteGrupoServicios"/>
<form:hidden path="consteGrupoMantenimiento" id="txhConsteGrupoMantenimiento"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="valorIndGrupo" id="txhValorIndGrupo"/>
<form:hidden path="nroReporte" id="txhNroReporte"/>
<form:hidden path="tituloReporte" id="txhTituloReporte"/>
<form:hidden path="codTipoReporte" id="txhCodTipoReporte"/>
<form:hidden path="codReporte5" id="txhCodReporte5" />
<form:hidden path="codReporte7" id="txhCodReporte7" />
<form:hidden path="codReporte9" id="txhCodReporte9" />
<form:hidden path="codReporte29" id="txhCodReporte29" />
<form:hidden path="consteSede" id="txhConsteSede" />


<table cellpadding="1" cellspacing="0"  ID="Table1" style="width:98%; margin-top:7px;" 
		  border="0" bordercolor="Red" background="${ctx}/images/Logistica/back.jpg" class="tabla">
	 <tr>
	       <td width="20%">&nbsp;Sede :</td>
	       <td width="40%">
	       		<form:select path="codSede" id="codSede" cssClass="combo_o" 
					cssStyle="width:100px" onchange="javascript:fc_Sede();">
					<c:if test="${control.listaSedes!=null}">
					<form:options itemValue="codSede" itemLabel="dscSede" 
						items="${control.listaSedes}" />
					</c:if>
				</form:select></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
	</tr>
	<tr>
				<td >&nbsp;Tipo Inversi�n?</td>
				<td>
				<table id="radio1" name="radio1" bordercolor="blue" border="0" width="100%">
					 <tr>
						<td align="left" width="4%">
							<input Type="radio" checked name="rdoEtapa" id="gbradio1" onclick="javascript:fc_Etapa('1');">&nbsp;S�&nbsp;
						<input Type="radio" name="rdoEtapa" id="gbradio2" onclick="javascript:fc_Etapa('0');">&nbsp;No&nbsp;
						</td>   	
					  
					</tr>
					</table>							
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>	 
				<td>&nbsp;</td>			  	
			</tr>
	      
	       <tr>
	       <td >&nbsp;Periodo:
		   </td>
		   		   
		   <td>
			   <form:input path="fecInicio" id="txtFecInicio" cssClass="cajatexto_o"
									onkeypress="javascript:fc_Slash('frmMain','txtFecInicio','/');"
									onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio');"  
									cssStyle="width:70px" maxlength="10"/>&nbsp;
								<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
								<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
									align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>&nbsp;&nbsp;
					<form:input path="fecFinal" id="txtFecFinal" cssClass="cajatexto_o"
									onkeypress="javascript:fc_Slash('frmMain','txtFecFinal','/');"
									onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal');"  
									cssStyle="width:70px" maxlength="10"/>&nbsp;
								<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
								<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
									align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>
		   </td>
		   			    
	       <td>
				&nbsp;
			</td>
			<td>&nbsp;</td>	
			<td>&nbsp;</td>
	       </tr>
	       <tr>
	       		
			 <td>&nbsp;Nombre Comprador:</td>
			 <td><form:input path="usuSolicitante" id="usuSolicitante" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();" 
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Usuario Solicitante');" 
					cssClass="cajatexto" size="40"/></td>
			 <td>&nbsp;</td>
			 <td>&nbsp;</td>
			 <td>&nbsp;</td>
		</tr>
		   
		   
		</table>
		
<table align="center" style="margin-top: 10px">
		<tr>
			<td>
			<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar','','${ctx}/images/botones/generar2.jpg',1)">
				<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar"></a>
			</td>
		</tr>
</table>

</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
</body>
</html>