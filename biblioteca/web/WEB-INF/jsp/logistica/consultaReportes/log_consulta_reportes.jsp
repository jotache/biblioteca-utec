<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad(){
	fc_JuegoReportes();
	
	}
	
	function fc_Regresar()
	{
		document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhCodSede").value;
	}
	
	function fc_JuegoReportes(){
	var codReporte=document.getElementById("codTipoReporte").value;
	// codReporte==document.getElementById("txhCodReporte12").value
	if(codReporte!="-1")
	{	
	 
		if(codReporte==document.getElementById("txhCodReporte5").value || codReporte==document.getElementById("txhCodReporte7").value ||
		   codReporte==document.getElementById("txhCodReporte9").value ){
		    Tabla_iframe1.style.display='inline-block';
		     Tabla_iframe2.style.display='none';
		     Tabla_iframe3.style.display='none';
		     Tabla_iframe4.style.display='none';
		     //0005:Cierre de Oper., 0007:Req. Valor., 0009: Sol. de Cotizaci�n
		     document.getElementById("iFrame1").src = "${ctx}/logistica/repCierraOperacion.html?"+
					"txhCodTipoReporte=" + codReporte + "&txhCodUsuario=" +
							document.getElementById("txhCodUsuario").value + "&txhCodSede=" +
							document.getElementById("txhCodSede").value + "&txhDscUsuario=" +
							document.getElementById("txhDscUsuario").value;
			
		}else if(codReporte==document.getElementById("txhCodReporte1").value || codReporte==document.getElementById("txhCodReporte2").value ||
			       codReporte==document.getElementById("txhCodReporte3").value || codReporte==document.getElementById("txhCodReporte4").value || 
			       codReporte==document.getElementById("txhCodReporte6").value || codReporte==document.getElementById("txhCodReporte11").value) {
			       Tabla_iframe2.style.display='inline-block';
			        Tabla_iframe1.style.display='none';
			        Tabla_iframe3.style.display='none';
		            Tabla_iframe4.style.display='none';
		            //0001: Ing. Almac�n, 0002: Sal. Almac�n, 0003: Kardex Valor., 0004: Maestro Saldo
		            //0006: Acum. de Salida por Art�culo
			      	document.getElementById("iFrame2").src = 
						"${ctx}/logistica/repIngresosAlmacen.html?"
						+"txhCodTipoReporte=" + codReporte + "&txhCodUsuario=" +
							document.getElementById("txhCodUsuario").value + "&txhCodSede=" +
							document.getElementById("txhCodSede").value + "&txhDscUsuario=" +
							document.getElementById("txhDscUsuario").value;
					
	    }else if(codReporte==document.getElementById("txhCodReporte8").value){
			             Tabla_iframe3.style.display='inline-block';
			              Tabla_iframe1.style.display='none';
			        	  Tabla_iframe2.style.display='none';
		            	  Tabla_iframe4.style.display='none';
		            	  //0008: Ord. Asignadas por Proveedor
			              document.getElementById("iFrame3").src = 
							"${ctx}/logistica/repOrdenesAsigandasByProveedor.html?"+
							"txhCodTipoReporte=" + codReporte + "&txhCodUsuario=" +
							document.getElementById("txhCodUsuario").value + "&txhCodSede=" +
							document.getElementById("txhCodSede").value + "&txhDscUsuario=" +
							document.getElementById("txhDscUsuario").value;
		}else if(codReporte==document.getElementById("txhCodReporte10").value){
			             		Tabla_iframe4.style.display='inline-block';
			             		Tabla_iframe1.style.display='none';
			        			Tabla_iframe3.style.display='none';
		           				Tabla_iframe2.style.display='none';
		           				//0010: Cronograma de Pago
			             		document.getElementById("iFrame4").src = 
								"${ctx}/logistica/repProgramaPagos.html?"+
								"txhCodTipoReporte=" + codReporte + "&txhCodUsuario=" +
								document.getElementById("txhCodUsuario").value + "&txhCodSede=" +
								document.getElementById("txhCodSede").value + "&txhDscUsuario=" + 
								document.getElementById("txhDscUsuario").value;
		}else if(codReporte==document.getElementById("txhCodReporte29").value){
			 Tabla_iframe1.style.display='inline-block';
		     Tabla_iframe2.style.display='none';
		     Tabla_iframe3.style.display='none';
		     Tabla_iframe4.style.display='none';
		     //0005:Cierre de Oper., 0007:Req. Valor., 0009: Sol. de Cotizaci�n
		     document.getElementById("iFrame1").src = "${ctx}/logistica/repFechasOperacion.html?"+
					"txhCodTipoReporte=" + codReporte + "&txhCodUsuario=" +
							document.getElementById("txhCodUsuario").value + "&txhCodSede=" +
							document.getElementById("txhCodSede").value + "&txhDscUsuario=" +
							document.getElementById("txhDscUsuario").value;
		}							 
	}
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/consulta_reportes.html">
<form:hidden path="codUsuario" id="txhCodUsuario" />
<form:hidden path="dscUsuario" id="txhDscUsuario" />
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codSede" id="txhCodSede"/>

<form:hidden path="codReporte1" id="txhCodReporte1" />
<form:hidden path="codReporte2" id="txhCodReporte2" />
<form:hidden path="codReporte3" id="txhCodReporte3" />
<form:hidden path="codReporte4" id="txhCodReporte4" />
<form:hidden path="codReporte5" id="txhCodReporte5" />
<form:hidden path="codReporte6" id="txhCodReporte6" />
<form:hidden path="codReporte7" id="txhCodReporte7" />
<form:hidden path="codReporte8" id="txhCodReporte8" />
<form:hidden path="codReporte9" id="txhCodReporte9" />
<form:hidden path="codReporte10" id="txhCodReporte10" />
<form:hidden path="codReporte11" id="txhCodReporte11" />
<form:hidden path="codReporte29" id="txhCodReporte29" />

<table cellpadding="0" cellspacing="0" border="0" width="97%">
	<tr>
		<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
		<td align=right>&nbsp;<img	src="${ctx}/images/iconos/Regresar.gif"
			onclick="javascript:fc_Regresar();" style="cursor: hand" alt="Regresar"></td>
	</tr>
</table>
	<!--Page Title -->
<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:11px; margin-top:10px">
	<tr>
		<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="770px" class="opc_combo"><font style="">Consultas y Reportes</font></td>
		<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0"  ID="Table1" style="width:96%; margin-top: 5px; margin-left: 10px" 
	border="0" bordercolor="Red" background="${ctx}/images/Logistica/back.jpg" class="tabla">
	<tr>
		<td nowrap="nowrap">&nbsp;Reportes :&nbsp;&nbsp;&nbsp;
			<form:select path="codTipoReporte" id="codTipoReporte" cssClass="cajatexto_o" 
				cssStyle="width:350px;" onchange="javascript:fc_JuegoReportes();">
			<form:option value="-1">--Seleccione--</form:option>
			<c:if test="${control.listaReportes!=null}">
				<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
					items="${control.listaReportes}" />
			</c:if>
			</form:select>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" id="Tabla_iframe1" class="tabla" border="0" bordercolor="blue" 
		style="display: none;margin-left:11px; width:98%">
		<tr><td><iframe id="iFrame1" name="iFrame1" frameborder="0" height="290px" width="100%"></iframe>
		</td></tr>
</table>
<table cellpadding="0" cellspacing="0" id="Tabla_iframe2" class="tabla" border="0" bordercolor="blue" 
		style="display: none;margin-left:11px; width:98%">
		<tr><td><iframe id="iFrame2" name="iFrame2" frameborder="0" height="290px" width="100%"></iframe>
		</td></tr>
</table>
<table cellpadding="0" cellspacing="0" id="Tabla_iframe3" class="tabla" border="0" bordercolor="blue" 
		style="display: none;margin-left:11px; width:98%">
		<tr><td><iframe id="iFrame3" name="iFrame3" frameborder="0" height="290px" width="100%"></iframe>
		</td></tr>
</table>
<table cellpadding="0" cellspacing="0" id="Tabla_iframe4" class="tabla" border="0" bordercolor="blue" 
		style="display: none;margin-left:11px; width:98%">
		<tr><td><iframe id="iFrame4" name="iFrame4" frameborder="0" height="290px" width="100%"></iframe>
		</td></tr>
</table>
</form:form>
</body>
