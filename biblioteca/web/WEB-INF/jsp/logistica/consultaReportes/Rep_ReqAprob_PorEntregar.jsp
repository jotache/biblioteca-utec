<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.modelo.ReportesLogistica'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.79in .40in .79in .40in;
	mso-header-margin:0cm;
	mso-footer-margin:0cm;}
-->
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
.detalleNro {
               FONT-SIZE: 10px;
               /*COLOR: #636563;*/
               COLOR: #000000;
               FONT-FAMILY: Verdana;
               HEIGHT: 18px;
               CURSOR: hand;
               mso-style-parent:style0;
               mso-number-format:"0\.000";
       }
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td colspan="2"><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="4" align="center"><u>SGA - SISTEMA LOGISTICA</u></td>
	<td align="right" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td colspan="2"></td>
	<td colspan="6" align="center">Reporte de Requerimientos Aprobados Pendiente de Entrega</td>
	<td align="right" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td class="texto_bold" colspan="2">TECSUP-${model.NOM_SEDE}</td>
	<td colspan="4" align="left"></td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td colspan="1"><b>Usuario :</b></td>
	<td colspan="3" align="left">${model.usuario}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td colspan="2" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="5%">Sede</td>
				<td class="cabecera_grilla" width="5%">Tipo de Requerimiento</td>
				<td class="cabecera_grilla" width="5%">Nro. Req.</td>
				<td class="cabecera_grilla" width="5%">Fecha Aprob.</td>
				<td class="cabecera_grilla" width="5%">Aprobador</td>
				<td class="cabecera_grilla" width="5%">C. Costo</td>
				<td class="cabecera_grilla" width="5%">C�d. Bien</td>
				<td class="cabecera_grilla" width="10%">Descripci�n del Bien</td>
				<td class="cabecera_grilla" width="5%">Uni. Medida</td>
				<td class="cabecera_grilla" width="5%">Cant. Aprob.</td>
				<td class="cabecera_grilla" width="5%">Moneda</td>
				<td class="cabecera_grilla" width="5%">Prec. Unit.</td>
				<td class="cabecera_grilla" width="5%">Tipo Gasto</td>
				<td class="cabecera_grilla" width="5%">Cant. Pend.</td>
				<td class="cabecera_grilla" width="5%">Cant. Devuelta</td>
				<td class="cabecera_grilla" width="5%">Num. Cotiz.</td>
				<td class="cabecera_grilla" width="5%">Comprador</td>
				<td class="cabecera_grilla" width="5%">Num. OC</td>
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
				List consulta = (List) request.getSession().getAttribute("consulta"); 
				DecimalFormat df = new DecimalFormat("0.000");	
				for(int i=0;i<consulta.size();i++){
					
						ReportesLogistica obj  = (ReportesLogistica) consulta.get(i);
						
						%>
					<tr class="texto_grilla">
						<td style="text-align:left"><%=obj.getCodSede()==null?"":obj.getCodSede()%></td>
						<td style="text-align:left"><%=obj.getDscTipoReq()==null?"":obj.getDscTipoReq()%></td>
						<td style="text-align:center"><%=obj.getNroRequerimiento()==null?"":obj.getNroRequerimiento()%></td>
						<td style="text-align:right"><%=obj.getFechaAprobaReq()==null?"":obj.getFechaAprobaReq()%></td>
						<td style="text-align:left"><%=obj.getResponsableCeco()==null?"":obj.getResponsableCeco()%></td>
						<td style="text-align:center"><%=obj.getCodCeco()==null?"":obj.getCodCeco()%></td>
						<td style="text-align:center"><%=obj.getCodBien()==null?"":obj.getCodBien()%></td>
						<td style="text-align:left"><%=obj.getDscBien()==null?"":obj.getDscBien()%></td>
						<td style="text-align:left"><%=obj.getDscUnidad()==null?"":obj.getDscUnidad()%></td>
						<td style="text-align:right"><%=df.format(Float.valueOf(obj.getCantRequerimiento()==""?"0.000":obj.getCantRequerimiento()))%></td>
						<td style="text-align:center"><%=obj.getDscMoneda()==null?"":obj.getDscMoneda()%></td>
						<td style="text-align:right"><%=df.format(Float.valueOf(obj.getPrecioUnitario()==""?"0.000":obj.getPrecioUnitario()))%></td>
						<td style="text-align:left"><%=obj.getDscTipoGasto()==null?"":obj.getDscTipoGasto()%></td>
						<td style="text-align:right"><%=df.format(Float.valueOf(obj.getCantPorEntregar()==""?"0.000":obj.getCantPorEntregar()))%></td>
						<td style="text-align:right"><%=df.format(Float.valueOf(obj.getCantReqDevuelto()==""?"0.000":obj.getCantReqDevuelto()))%></td>
						<td style="text-align:left"><%=obj.getNroCotizacion()==null?"":obj.getNroCotizacion()%></td>
						<td style="text-align:left"><%=obj.getComprador()==null?"":obj.getComprador()%></td>
						<td style="text-align:left"><%=obj.getNroOrden()==null?"":obj.getNroOrden()%></td>
					</tr>
					<%}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
	</tr>
</table>

</body>
</html>
