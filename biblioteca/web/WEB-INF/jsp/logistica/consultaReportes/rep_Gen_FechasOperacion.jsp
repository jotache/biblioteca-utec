<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.tecsup.SGA.common.Fecha"%>
<%@page import="com.tecsup.SGA.modelo.SolRequerimiento"%>
<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.CatalogoProducto'%>
<%@page import='java.util.StringTokenizer'%>
<%@page contentType="application/vnd.ms-excel"%>
<%--@ page language="java" errorPage="/error.jsp" --%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" colspan="4"><u><b>REPORTE DE FLUJO DE PROCESOS DE LOGISTICA</b></u></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td><td></td><td></td><td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td><td></td><td></td><td></td>
	<td align="right" style="font-size:8pt">Usuario</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.usuario}</td>
</tr>
<tr >
	<td></td>
	<td></td>
</tr>
</table>
<br>
<table >
<tr>
	<td class="texto_bold">Sede</td>
	<td align="left" class="texto">: ${model.NOM_SEDE} </td>
</tr>
<tr>
	<td class="texto_bold">Fec. Inicio</td>
	<td align="left" class="texto">: ${model.periodoIni} </td>
	<td class="texto_bold">Fec. Fin</td>
	<td align="left" class="texto">: ${model.periodoFin} </td>
</tr>
<tr>
	<td class="texto_bold">Inversi�n</td>
	<td align="left" class="texto">:
		<c:if test="${model.codTipoOrden=='1'}">S�</c:if> 
		<c:if test="${model.codTipoOrden=='0'}">No</c:if>
	</td>
	<td class="texto_bold"></td>
	<td align="left" class="texto"></td>
</tr>
<tr>
	<td class="texto_bold">Nombre Comprador</td>
	<td align="left" class="texto">: ${model.dscUsuSol} </td>
	<td class="texto_bold">&nbsp;</td>
	<td align="left" class="texto">&nbsp;</td>
</tr>

</table><br><br>
<table align="center">
	<tr>
		<td>
			<table border="1" align="center">
			<tr rowspan="2">
				<td class="cabecera_grilla" style="width:90px" nowrap="nowrap" >Usuario</td>
				<td class="cabecera_grilla" style="width:90px" nowrap="nowrap" >Item</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Cant. </td>				
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >UM</td>
				<td class="cabecera_grilla" style="width:120px" nowrap="nowrap">Familia</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fec. Env�o</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fec. Aprobaci�n</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fec. Estimada</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Comprador</td>				
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fec. Asignado</td>																			
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Pago</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fec. Gen. O.C.</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Jefe Log.</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Dir. Admin.</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Dir. Cencos.</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Nom Dir</td>				
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Dir. G.</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fec. Recepci�n</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fec. Entrega.</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Despachada</td>
				</tr>
			<%
				List alumno = (List)request.getSession().getAttribute("consulta");							
				Fecha ofec = new Fecha();
				
				if(alumno != null){
					for(int j = 0; j < alumno.size(); j++){
						SolRequerimiento rep = (SolRequerimiento)alumno.get(j);
				%>
				
				<tr class="texto_grilla">
					<td style="text-align:center"><%=rep.getUsuAsignado()==null?"":rep.getUsuAsignado()%></td>														
					<td style="text-align:center"><%=rep.getNroItem()%></td>
					<td style="text-align:center"><%=rep.getCantidad()==null?"":rep.getCantidad()%></td>
					<td style="text-align:left"><%=rep.getUnidMed()==null?"":rep.getUnidMed()%></td>
					<td style="text-align:left"><%=rep.getFamilia()==null?"":rep.getFamilia()%></td>
					<td style="text-align:left"><%=rep.getFecEnvio()==null?"":rep.getFecEnvio()%></td>
					<td style="text-align:left"><%=rep.getFecAprobacion()==null?"":rep.getFecAprobacion()%></td>
					<td style="text-align:left"><%=rep.getFecEstimada()==null?"":rep.getFecEstimada()%></td>
					<td style="text-align:left"><%=rep.getComprador()==null?"":rep.getComprador()%></td>
					<td style="text-align:left"><%=rep.getFecAsignado()==null?"":rep.getFecAsignado()%></td>					
					<td style="text-align:left"><%=rep.getTipoPago()==null?"":rep.getTipoPago()%></td>
					<td style="text-align:left"><%=rep.getFecGenOC()==null?"":rep.getFecGenOC()%></td>
					<td style="text-align:left"><%=rep.getFecJefeLog()==null?"":rep.getFecJefeLog()%></td>
					<td style="text-align:left"><%=rep.getFecDirAdmin()==null?"":rep.getFecDirAdmin()%></td>
					<td style="text-align:left"><%=rep.getFecDirCencos()==null?"":rep.getFecDirCencos()%></td>
					<td style="text-align:left"><%=rep.getNombreServicio()==null?"":rep.getNombreServicio()%></td>
					<td style="text-align:left"><%=rep.getFecDirGeneral()==null?"":rep.getFecDirGeneral()%></td>
					<td style="text-align:left"><%=rep.getFecRecepcion()==null?"":rep.getFecRecepcion()%></td>
					<td style="text-align:left"><%=rep.getFecEnvio()==null?"":rep.getFecEnvio()%></td>
					<td style="text-align:left"><%=rep.getCantApoyo()==null?"":rep.getCantApoyo()%></td>					
				</tr>
				<%
					}
				}request.removeAttribute("consulta");
				%>	
			</table>
		</td>
	</tr>
</table>
</body>
</html>