<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='java.util.StringTokenizer'%> 
<%@page import='com.tecsup.SGA.modelo.ReportesLogistica'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
	mso-number-format:"\@";
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
.detalleNro {
               FONT-SIZE: 10px;
               /*COLOR: #636563;*/
               COLOR: #000000;
               FONT-FAMILY: Verdana;
               HEIGHT: 18px;
               CURSOR: hand;
               mso-style-parent:style0;
               mso-number-format:Standard;
       }
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
    <td></td> 
	<td colspan="2"><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="17" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="right" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="2"></td>
	<td colspan="17" align="center">Reporte de Solicitudes de Cotizaci�n </td>
	<td align="right" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td class="texto_bold" colspan="2">TECSUP-${model.NOM_SEDE}</td>
	<td colspan="17" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td colspan="1"><b>Del :</b> </td>
	<td colspan="2" align="left">${model.periodoInicio}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td colspan="1"><b>Al :</b> </td>
	<td colspan="2" align="left">${model.periodoFin}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="14" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="5%">Tipo de cotizaci�n</td>
				<td class="cabecera_grilla" width="5%">Tipo Bien</td>
				<td class="cabecera_grilla" width="5%">Tipo de pago</td>
				<td class="cabecera_grilla" width="5%">N�m. Cotizaci�n</td>
				<td class="cabecera_grilla" width="5%">Fec. Env�o</td>
				<td class="cabecera_grilla" width="5%">Inicio de vigencia</td>
				<td class="cabecera_grilla" width="5%">Fin de vigencia</td>
				
				<td class="cabecera_grilla" width="10%">Estado Cotizaci�n</td>
				<td class="cabecera_grilla" width="5%">C�d. Bien</td>
				<td class="cabecera_grilla" width="5%">Descripci�n Bien</td>
				<td class="cabecera_grilla" width="5%">Uni. Medida</td>
				<td class="cabecera_grilla" width="5%">Cantidad</td>
				<td class="cabecera_grilla" width="5%">Moneda</td>
				<td class="cabecera_grilla" width="5%">Cot.<br>Ganadora</td>
				<td class="cabecera_grilla" width="10%">Prov. Ganador</td>
				<td class="cabecera_grilla" width="5%">Cotiz. 1</td>
				<td class="cabecera_grilla" width="5%">Proveedor 1</td>
				<td class="cabecera_grilla" width="5%">Cotiz. 2</td>
				<td class="cabecera_grilla" width="5%">Proveedor 2</td>
				<td class="cabecera_grilla" width="5%">Cotiz. 3</td>
				<td class="cabecera_grilla" width="5%">Proveedor 3</td>
												
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
				List consulta = (List) request.getSession().getAttribute("consulta"); 
				DecimalFormat df = new DecimalFormat("0.00");
					for(int i=0;i<consulta.size();i++){
					
						ReportesLogistica obj  = (ReportesLogistica) consulta.get(i);%>
					<tr class="texto_grilla">
						<td style="text-align:left"><%=obj.getDscTipoCotizacion()==null?"":obj.getDscTipoCotizacion()%></td>
						<td style="text-align:left"><%=obj.getDscTipoBienServ()==null?"":obj.getDscTipoBienServ()%></td>
						<td style="text-align:left"><%=obj.getDscTipoPago()==null?"":obj.getDscTipoPago()%></td>
						<td style="text-align:center"><%=obj.getNroCotizacion()==null?"":obj.getNroCotizacion()%></td>
						<td style="text-align:right"><%=obj.getFecEnvioCotizacion()==null?"":obj.getFecEnvioCotizacion()%></td>
						<td style="text-align:right"><%=obj.getFecInicioVigencia()==null?"":obj.getFecInicioVigencia()%></td>
						<td style="text-align:right"><%=obj.getFecFinVigencia()==null?"":obj.getFecFinVigencia()%></td>
						<td style="text-align:left"><%=obj.getDscEstadoCotizacion()==null?"":obj.getDscEstadoCotizacion()%></td>
						<td style="text-align:center"><%=obj.getCodBien()==null?"":obj.getCodBien()%></td>
						<td style="text-align:left"><%=obj.getDscBienServicio()==null?"":obj.getDscBienServicio()%></td>
						<td style="text-align:left"><%=obj.getDscUnidad()==null?"":obj.getDscUnidad()%></td>
						<td style="text-align:right"><%=df.format(Float.valueOf(obj.getCantidad()==""?"0.00":obj.getCantidad()))%></td>
						<td style="text-align:center"><%=obj.getDscMoneda()==null?"":obj.getDscMoneda()%></td>
						<td style="text-align:right"><%=df.format(Float.valueOf(obj.getCotiGanador()==""?"0.00":obj.getCotiGanador()))%></td>
						<td style="text-align:left"><%=obj.getDscProveedorGanador()==null?"":obj.getDscProveedorGanador()%></td>
						<%
					int nrocol = 6;
					String muestra = "";
					String data = "";
					String posicion="left";
					StringTokenizer st = new StringTokenizer(obj.getListadoCotizaciones()==null?"":obj.getListadoCotizaciones(),"|");
					int nroNotas=st.countTokens();
					
					while ( st.hasMoreTokens())
    					{ int a=0;
    					  posicion="left";
						data= st.nextToken();
						if(data=="-1"){
							muestra="";
						}
						else{
							muestra=data;
							try{muestra=muestra.replace(",",".");
								muestra=df.format(Float.valueOf(muestra));
								a=1;
								posicion="right";
							}catch(Exception e){
								a=0;
							}
						}
						//System.out.println("posicion: "+posicion+">><<data: "+data+">><<muestra: "+muestra);
							
    				%>
						<td style="text-align:<%=posicion%>"><%=muestra%></td>
				<%}%>
					<%for(int j=0;j<nrocol - nroNotas;j++){%>
					<td width="30%" style="text-align:left"></td>
					<%}%>
				
					</tr>
					<%}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>