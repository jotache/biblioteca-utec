<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>

	function onLoad()
	{
		document.getElementById("txhSecuencial").value = "";
		document.getElementById("txhCtaBle").value = "";
		document.getElementById("txhTipB").value = "";
		document.getElementById("txhFamilia").value = "";
		document.getElementById("txhCodPadre").value = "";
		document.getElementById("txhCodigo").value = "";
		document.getElementById("txhOperacion").value = "";
	
		objMsg = document.getElementById("txhMsg");
	
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			alert(mstrElimino);
			
		}
		else if ( objMsg.value == "ERROR" )
		{
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			alert(mstrProbEliminar);
		}
		else if ( objMsg.value == "DOBLE" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrNoEli);
		}
	}
	function Fc_Agregar()
	{
		strtipo = "";
		Fc_Popup("${ctx}/logistica/log_Agregar_Familia.html?txhSecuencial=" + "" + "&txhCtaBle=" + "" +
		 "&txhTipB=" + "" + "&txhFamilia=" + "" + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhCodPadre=" + document.getElementById("codTipo").value + "&txhTipo=" + strtipo + "&txhCodigo=" + ""
		,450,190);
	}
	function Fc_Modificar()
	{
		if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
			strtipo = "MODIFICAR";
			Fc_Popup("${ctx}/logistica/log_Agregar_Familia.html?txhSecuencial=" + document.getElementById("txhSecuencial").value + "&txhCtaBle=" + document.getElementById("txhCtaBle").value +
			"&txhTipB=" + document.getElementById("txhTipB").value + "&txhFamilia=" + document.getElementById("txhFamilia").value + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhCodPadre=" + document.getElementById("txhCodPadre").value + "&txhTipo=" + strtipo + "&txhCodigo=" + document.getElementById("txhCodigo").value
			,450,190);
		}
		else
		{
			alert(mstrSeleccion);
		}
	}
	function fc_muestra(){
		document.getElementById("txhOperacion").value = "DETALLE";
		document.getElementById("frmMain").submit();
		document.getElementById("txhOperacion").value = "";
	}
	function fc_seleccionarRegistro(strCodsec,strDes,strdscPadre,strValor1,strcodPadre,strcodigo){
		document.getElementById("txhSecuencial").value = strCodsec;
		document.getElementById("txhCtaBle").value = strValor1;
		document.getElementById("txhTipB").value = strdscPadre;
		document.getElementById("txhFamilia").value = strDes;
		document.getElementById("txhCodPadre").value = strcodPadre;
		document.getElementById("txhCodigo").value = strcodigo;
	}
	
	function Fc_Eliminar(){
		if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhOperacion").value = "ELIMINAR";
				document.getElementById("frmMain").submit();
				document.getElementById("txhOperacion").value = "";
			}
			else{
				return false;
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
	
</script>
</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Mantenimiento_Familia.html">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="codigo" id="txhCodigo"/>
	<form:hidden path="familia" id="txhFamilia"/>
	<form:hidden path="tipB" id="txhTipB"/>
	<form:hidden path="ctaBle" id="txhCtaBle"/>
	<form:hidden path="secuencial" id="txhSecuencial"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codPadre" id="txhCodPadre"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
			<tr>
				<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="700px" height="4px"></td>
			</tr>
			<tr>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
				<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Consulta de Familia</font></td>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
			</tr>
			<tr>
				<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="555px"></td>
			</tr>
		</table>
		<table cellpadding="1" cellspacing="0" ID="Table1" style="width:99%;margin-top:10px" border="0" bordercolor="Red" 
		class="tabla" background="${ctx}/images/Logistica/back.jpg" height="50px">
			<tr height="5px">
			</tr>
			<tr>
				<td width="10%">&nbsp;Tipo Bien :</td>
				<td align="left">
					<form:select path="codTipo" id="codTipo" cssClass="cajatexto"
					cssStyle="width:280px" onchange="javascript:fc_muestra();">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.tipoBien!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.tipoBien}" />
					</c:if>
					</form:select></td>
			</tr>
			<tr height="5px">
				<td></td>
			</tr>
			</table>
			<br>
		<table style="width:99%" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray'>
			<tr><td>
				<div style="overflow: auto; height: 351px;width:100%">
						<display:table name="sessionScope.listFamilia" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
							requestURI="" style="border: 1px solid #048BBA;width:97.5%">
							<display:column property="rbtSelFamilia" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
							<display:column property="codigo" title="C�digo" headerClass="grilla" class="tablagrilla" style="align:left;width:150px"/>
							<display:column property="descripcion" title="Familia" headerClass="grilla" class="tablagrilla" style="align:left;width:180px"/>
							<display:column property="dscPadre" title="Tipo Bien" headerClass="grilla" class="tablagrilla" style="align:left;width:300px"/>
							<display:column property="valor1" title="Cta. Cble." headerClass="grilla" class="tablagrilla" style="align:left;width:220px"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
						</display:table>
						<% request.getSession().removeAttribute("listFamilia"); %>
					</div>
				<td valign=top>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src="${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar();" alt="Agregar" style="cursor:hand" id="icoAgregar"></a><br>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoModificar','','${ctx}/images/iconos/modificar2.jpg',1)">
					<img src="${ctx}/images/iconos/modificar1.jpg" onclick="javascript:Fc_Modificar();" alt="Modificar" style="cursor:hand;" id="icoModificar"></a><br>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)">
					<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_Eliminar();" alt="Eliminar" style="cursor:hand;" id="icoEliminar"></a>
				</td>
		</table>
		<br>
	</form:form>	
