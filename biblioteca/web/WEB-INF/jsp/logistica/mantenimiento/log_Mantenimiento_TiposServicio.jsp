<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>	
	var valor1="";
		function onLoad()
		{
		
		document.getElementById("txhSecuencial").value = "";
		document.getElementById("txhDesc").value = "";
		document.getElementById("txhCodPadre").value = "";
		document.getElementById("txhDscPadre").value = ""; 
		
		objMsg = document.getElementById("txhMsg");
	
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			document.getElementById("txhOperacion").value = "";
			alert(mstrElimino);
		}		
		else if ( objMsg.value == "ERROR" )
			{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			document.getElementById("txhOperacion").value = "";
			alert(mstrProbEliminar);			
			}
		else if ( objMsg.value == "DOBLE" )
			{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			document.getElementById("txhOperacion").value = "";
			alert(mstrNoEli);			
			}	
		}
		function fc_muestra(){
			document.getElementById("txhOperacion").value = "DETALLE";
			document.getElementById("frmMain").submit();	
		}
		
		function Fc_Modificar(){
		  if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
		strTipo = "MODIFICAR";
		Fc_Popup("${ctx}/logistica/log_Agregar_TipoServicio.html?txhSecuencial=" + document.getElementById("txhSecuencial").value + "&txhDesc=" + document.getElementById("txhDesc").value + "&txhTipo=" + strTipo + "&txhCodPadre=" + document.getElementById("txhCodPadre").value + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhValor1=" + valor1
		,450,170);
			}
			 else
		 {
			alert(mstrSeleccion);
		 }
		}
		function Fc_Agregar(){
		strTipo = "";
		
		Fc_Popup("${ctx}/logistica/log_Agregar_TipoServicio.html?txhSecuencial=" + "" + "&txhDesc=" + "" + "&txhTipo=" + strTipo + "&txhCodPadre=" + "" + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhValor1=" + ""  
		,450,170);
			
		}
		function fc_seleccionarRegistro(strCod,strDes,strCodPadre,strDesPadre,strValor1){
		document.getElementById("txhSecuencial").value = strCod;
		document.getElementById("txhDesc").value = strDes;
		document.getElementById("txhCodPadre").value = strCodPadre;
		document.getElementById("txhDscPadre").value = strDesPadre;   
		valor1=strValor1;
		}
	function Fc_Eliminar(){
		if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhOperacion").value = "ELIMINAR";
				document.getElementById("frmMain").submit();		
			}
			else{
				return false;
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
	</script>
	</head>
	<form:form action="${ctx}/logistica/log_Mantenimiento_TiposServicio.html" commandName="control" id="frmMain">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="secuencial" id="txhSecuencial"/>
		<form:hidden path="desc" id="txhDesc"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="codPadre" id="txhCodPadre"/>
		<form:hidden path="dscPadre" id="txhDscPadre"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
			<tr>
				<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="700px" height="4px"></td>
			</tr>
			<tr>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
				<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Consulta de Tipo de Servicio</font></td>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
			</tr>
			<tr>
				<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="555px"></td>
			</tr>
		</table>
		<table cellpadding="1" cellspacing="0"  ID="Table1" style="width:94%;margin-top:10px" border="0"
		bordercolor="Red" class="tabla" background="${ctx}/images/Logistica/back.jpg">
			<tr height="5px">
				<td></td>
			</tr>
			<tr>	
				<td width="5%">&nbsp;Grupo Servicio:</td>
				<td width="30%">
				<form:select path="codServicio" id="codServicio" cssClass="cajatexto"
					cssStyle="width:280px" onchange="javascript:fc_muestra();">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listServicios!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listServicios}" />
					</c:if>
					</form:select>
					</td>
			<tr height="5px">
				<td></td>
			</tr>
			</table>
			<br>
			
		<table style="width:95%" cellSpacing="0" cellPadding="0" border="0" bordercolor="red"> 
			<tr><td width="95%">
				<div style="overflow: auto; height: 367px;width:100%">									
						<display:table name="sessionScope.listGrupoFamilia" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
							requestURI="" style="border: 1px solid #048BBA;width:97.5%">
							<display:column property="rbtSelGrupoFamilia" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
							<display:column property="dscPadre" title="Grupo Servicio" headerClass="grilla" class="tablagrilla" style="align:left;width:25%"/>
							<display:column property="descripcion" title="Tipo de Servicio" headerClass="grilla" class="tablagrilla" style="align:left;width:50%"/>
							<display:column property="valor1" title="Cta. Cble." headerClass="grilla" class="tablagrilla" style="align:left;width:30%"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
						</display:table>
						<% request.getSession().removeAttribute("listGrupoFamilia"); %>
					</div>
				</td>
				<td valign=top>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src="${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar();" alt="Agregar" style="cursor:hand" id="icoAgregar"></a><br>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoModificar','','${ctx}/images/iconos/modificar2.jpg',1)">
					<img src="${ctx}/images/iconos/modificar1.jpg" onclick="javascript:Fc_Modificar();" alt="Modificar" style="cursor:hand;" id="icoModificar"></a><br>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)">
					<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_Eliminar();" alt="Eliminar" style="cursor:hand;" id="icoEliminar"></a>
				</td>
			</tr>
		</table>	
		</form:form>
