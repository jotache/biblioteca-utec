<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<script language=javascript>
		function onLoad()
		{
		document.getElementById("txhCodUnico").value = "";
		document.getElementById("txhCodGen").value = "";
		document.getElementById("txhDesSubFam").value = "";
		document.getElementById("txhDesFam").value = "";
		document.getElementById("txhOperacion").value ="";
		
		objMsg = document.getElementById("txhMsg");
	
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			alert(mstrElimino);
			
		}
		else if ( objMsg.value == "ERROR" )
			{
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			alert(mstrProbEliminar);
			}
		else if ( objMsg.value == "DOBLE" )
			{
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			alert(mstrNoEli);
			}
		}
		function Fc_Agregar(){
		
		strTipo = "";
		Fc_Popup("${ctx}/logistica/log_Agregar_SubFamilia.html?txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhTipo=" + strTipo + "&txtCodBien=" + document.getElementById("codTipo").value
		         + "&txhCodUnico=" + "" + "&txhCodGen=" + "" + "&txhDesSubFam=" + ""
		         + "&txhDesFam=" + ""
		,450,165);
		}
		function Fc_Modificar()
		{
		if(fc_Trim(document.getElementById("txhCodUnico").value) !== ""){
		strTipo = "MODIFICAR";
		Fc_Popup("${ctx}/logistica/log_Agregar_SubFamilia.html?txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhTipo=" + strTipo + "&txtCodBien=" + document.getElementById("codTipo").value
		         + "&txhCodUnico=" + document.getElementById("txhCodUnico").value + "&txhCodGen=" + document.getElementById("txhCodGen").value + "&txhDesSubFam=" + document.getElementById("txhDesSubFam").value 
		         + "&txhDesFam=" + document.getElementById("txhDesFam").value
		,450,165);
			}
			 else
		 {
			alert(mstrSeleccion);
		 }
		}
		function Fc_Limpia(){
		
		document.getElementById("codTipo").value = "";
		document.getElementById("codFam").value = "";
		}
		function fc_seleccionarRegistro(strcod,strCodGen,strDesSubFam,strFam){
		document.getElementById("txhCodUnico").value = strcod;
		document.getElementById("txhCodGen").value = strCodGen;
		document.getElementById("txhDesSubFam").value = strDesSubFam;
		document.getElementById("txhDesFam").value = strFam;
		}
		function Fc_Buscar(){
		document.getElementById("txhOperacion").value = "BUSCAR";
		document.getElementById("frmMain").submit();
		}
	
		function Fc_Eliminar(){
		if(fc_Trim(document.getElementById("txhCodUnico").value) !== ""){
		if( confirm(mstrEliminar) ){
		document.getElementById("txhOperacion").value = "ELIMINAR";
		document.getElementById("frmMain").submit();
		}
			else{
			return false;
			}
	}
	else{
		alert(mstrSeleccion);
	}
		}
		
		function Fc_Muestra(){
			document.getElementById("txhOperacion").value = "MUESTRA";
		document.getElementById("frmMain").submit();
		}
	</script>
		<form:form action="${ctx}/logistica/log_Mantenimiento_SubFamilia.html" commandName="control" id="frmMain">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="codUnico" id="txhCodUnico"/>
		<form:hidden path="codGen" id="txhCodGen"/>
		<form:hidden path="desSubFam" id="txhDesSubFam"/>
		<form:hidden path="desFam" id="txhDesFam"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<form:hidden path="msg" id="txhMsg"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
			<tr>
				<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="700px" height="4px"></td>
			</tr>
			<tr>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
				<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Consulta de SubFamilia</font></td>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
			</tr>
			<tr>
				<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="555px"></td>
			</tr>
		</table>
		<table cellpadding="1" cellspacing="0"  ID="Table1" style="width:99%;margin-top:10px" border="0"
			bordercolor="Red" class="tabla" background="${ctx}/images/Logistica/back.jpg" height="50px" width="994px">
			<tr height="5px">
				<td></td>
			</tr>
			<tr>
				<td width="13%">&nbsp;Tipo Bien:</td>
				<td width="30%"><form:select path="codTipo" id="codTipo" cssClass="cajatexto"
					cssStyle="width:265px" onchange="javascript:Fc_Muestra();">
					<form:option value="">--todos--</form:option>
					<c:if test="${control.listTipoBien!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listTipoBien}" />
					</c:if>
					</form:select></td>
				<td width="10%">&nbsp;Familia:</td>
				<td width="30%"><form:select path="codFam" id="codFam" cssClass="cajatexto"
					cssStyle="width:265px" >
					<form:option value="">--todos--</form:option>
					<c:if test="${control.listFamilia!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listFamilia}" />
					</c:if>
					</form:select></td>
				<td align="right">
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:Fc_Buscar();" alt="Buscar" style="cursor:hand;"id="icoBuscar"></a>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" onclick="javascript:Fc_Limpia();" alt="Limpiar" style="cursor:hand;"id="icoLimpiar"></a>
				</td>
			<tr height="5px">
				<td></td>
			</tr>
		</table>
		<br>
		<table style="width:99%" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray'> 
			<tr>
				<td colspan="4">
					<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" width="100%">
				  		<tr>
							<td>
							<div style="overflow: auto; height: 349px;width:100%">
								<display:table name="sessionScope.listSubFamilia" cellpadding="0" cellspacing="1"
								decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
								requestURI="" style="border: 1px solid #048BBA;width:99%">
								<display:column property="rbtSelSubFamilia" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
								<display:column property="codGenerado" title="C�digo" headerClass="grilla" class="tablagrilla" style="align:center;width:10%"/>
								<display:column property="desSubFam" title="SubFamilia" headerClass="grilla" class="tablagrilla" style="align:left;width:45%"/>
								<display:column property="desFam" title="Familia" headerClass="grilla" class="tablagrilla" style="align:left;width:40%"/>
								
								<display:setProperty name="basic.empty.showtable" value="true"  />
								<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='4' align='center'>No se encontraron registros</td></tr>"  />
								<display:setProperty name="paging.banner.placement" value="bottom"/>
								<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
								<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
								<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
								</display:table>
								<% request.getSession().removeAttribute("listSubFamilia"); %>
							</div>
							</td>
							<td valign=top>&nbsp;
								<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
								<img src="${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar();" alt="Agregar" style="cursor:hand" id="icoAgregar"></a><br>&nbsp;
								<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoModificar','','${ctx}/images/iconos/modificar2.jpg',1)">
								<img src="${ctx}/images/iconos/modificar1.jpg" onclick="javascript:Fc_Modificar();" alt="Modificar" style="cursor:hand;" id="icoModificar"></a><br>&nbsp;
								<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)">
								<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_Eliminar();" alt="Eliminar" style="cursor:hand;" id="icoEliminar"></a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br>
	</form:form>
</html>
