<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<script language=javascript>
	var objFilaSeleccionada = null;
	var pos = null;
	var codigo = "";
	function onLoad()
	{
		document.getElementById("txhSecuencial").value = "";
		document.getElementById("txhDesc").value = "";
		document.getElementById("txhOperacion").value = "";
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			alert(mstrElimino);
		}
		else if ( objMsg.value == "ERROR" )
			{
			objMsg.value = "";
			alert(mstrProbEliminar);
			}
		else if ( objMsg.value == "DOBLE" )
		{
			objMsg.value = "";
			alert(mstrNoEli);
		}
		objMen = document.getElementById("txhMen");
		if ( objMen.value == "OK" )
		{
			objMen.value = "";
			document.getElementById("txhMen").value = "";
			alert(mstrSeGraboConExito);
		}
		else if( objMen.value == "ERROR" )
		{
			objMen.value = "";
			document.getElementById("txhMen").value = "";
			alert(mstrProblemaGrabar);
		}
	}
	function Fc_Agregar()
	{
		strTipo = "";
		Fc_Popup("${ctx}/logistica/log_Agregar_Eva_Prov.html?txhSecuencial=" + "" + "&txhDesc=" + "" + "&txhTipo=" + strTipo+ "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value
		,450,120);
	}
	function Fc_modificar()
	{
		if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
			strTipo = "MODIFICAR";
			Fc_Popup("${ctx}/logistica/log_Agregar_Eva_Prov.html?txhSecuencial=" + document.getElementById("txhSecuencial").value + "&txhDesc=" + document.getElementById("txhDesc").value + "&txhTipo=" + strTipo+ "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value 
			,450,120);
		}
		else
		{
			alert(mstrSeleccion);
		}
	}
	
	function Fc_Eliminar(){
		if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhOperacion").value = "ELIMINAR";
				document.getElementById("frmMain").submit();
			}
			else{
				return false;
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
	
	function fc_seleccionarRegistro(strCod,strDesc,a,i,objCheckBox){
		document.getElementById("txhSecuencial").value = strCod;
		document.getElementById("txhDesc").value = strDesc;
		document.getElementById("txhPos").value = i;
		
		pos = document.getElementById("txhIndice" + i).value;
	}
 	function Fc_Subir(){
		var tabla = document.getElementById('listaCalificacion').getElementsByTagName("TBODY")[0];
		if ( pos != '' )
		{
			if ( parseFloat(pos) > 0 ){
				posNueva = parseInt(pos) - 1;
				tabla.moveRow(posNueva,pos);
				pos = posNueva;
				fc_Enumera();
			}
		}
 	}
	
	function Fc_Bajar(){
	 	tot = document.getElementById("txhTotal").value;
		var tabla = document.getElementById('listaCalificacion').getElementsByTagName("TBODY")[0];
		if ( pos != -1 )
		{
			posNueva = parseInt(pos) + 1;
			if ( parseFloat(posNueva) < tot ){	
				tabla.moveRow(posNueva,pos);
				pos = posNueva;
				fc_Enumera();
			}
		}
	}
	
	function fc_Enumera()
 	{
		var objTabla = document.getElementById('listaCalificacion').getElementsByTagName("TBODY")[0];
		var cantFila = objTabla.rows.length;
		if ( cantFila > 0 )
		{
			for (var i = 0; i < cantFila;i++)
			{
				objFil = objTabla.rows[i];
				objTd = objFil.cells[0];
				objChild = objTd.firstChild;
				objChild.value = i;
			}
		}
 	}
	 function fc_Actualiza(){
	 	objCadenas = "";
	 	objIndicador = "";
	 	con = 0;
	 	tot = document.getElementById("txhTotal").value;
	 	for(var i =0;i<tot;i++){
	 		con = con+1;
	 		objCadenas =objCadenas +  document.getElementById("text"+i).value + "|";
	 		objIndicador = objIndicador + document.getElementById("txhIndice"+i).value + "|";
	 	}
		document.getElementById("txhCadDetalle").value = objCadenas;
	 	document.getElementById("txhCadIndicador").value = objIndicador;
	 	if (confirm(mstrSeguroGrabar)){
	 		document.getElementById("txhOperacion").value = "ACTUALIZAMOS";
			document.getElementById("frmMain").submit();
	 	}
	 }
	 	
	</script>
	</head>
	<form:form action="${ctx}/logistica/log_Mantenimiento_Eva_Proveedores.html" commandName="control" id="frmMain">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="secuencial" id="txhSecuencial"/>
		<form:hidden path="desc" id="txhDesc"/>
		<form:hidden path="msg" id="txhMsg"/> 
		<form:hidden path="pos" id="txhPos"/>
		<form:hidden path="men" id="txhMen"/>
		<form:hidden path="cadDetalle" id="txhCadDetalle"/>
		<form:hidden path="cadIndicador" id="txhCadIndicador"/>
		<form:hidden path="total" id="txhTotal"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
			<tr>
				<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="700px" height="4px"></td>
			</tr>
			<tr>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
				<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Consulta de Criterios de Evaluación</font></td>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
			</tr>
			<tr>
				<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="555px"></td>
			</tr>
		</table>
		<table height="2px"><tr><td></td></tr></table>
		<table style="width:97%" cellSpacing="0" cellPadding="2" border="0" bordercolor="red">
			<tr><td>
				<div style="overflow: auto; height: 420px;width:100%">
					<display:table id="listaCalificacion" name="sessionScope.listCalificacion" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
						requestURI="" style="border: 1px solid #048BBA;width:98%">
						<display:column property="rbtSelCalif" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
						<display:column property="descripcion" title="Calificación" headerClass="grilla" class="tablagrilla" style="align:left;width:100px"/>
					
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registos</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					</display:table>
					<% request.getSession().removeAttribute("listaCalificacion"); %>
				</div>
			</td>
			<td valign=top>&nbsp;
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
				<img src="${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar();" alt="Agregar" style="cursor:hand" id="icoAgregar"></a><br>&nbsp;
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoModificar','','${ctx}/images/iconos/modificar2.jpg',1)">
				<img src="${ctx}/images/iconos/modificar1.jpg" onclick="javascript:Fc_modificar();" alt="Modificar" style="cursor:hand;" id="icoModificar"></a><br>&nbsp;
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)">
				<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_Eliminar();" alt="Eliminar" style="cursor:hand;" id="icoEliminar"></a>&nbsp;
				<br><br><br><br><br>&nbsp;
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoSubir','','${ctx}/images/iconos/subir2.jpg',1)">
				<img src="${ctx}/images/iconos/subir1.jpg" onclick="javascript:Fc_Subir();" alt="Subir" style="cursor:hand;" id="icoSubir"></a><br>&nbsp;
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoBajar','','${ctx}/images/iconos/bajar2.jpg',1)">
				<img src="${ctx}/images/iconos/bajar1.jpg" onclick="javascript:Fc_Bajar();" alt="Bajar" style="cursor:hand;" id="icoBajar"></a><br>&nbsp;
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoActualiza','','${ctx}/images/iconos/grabar2.jpg',1)">
				<img src="${ctx}/images/iconos/grabar1.jpg" onclick="javascript:fc_Actualiza();" alt="Modificar" style="cursor:hand;" id="icoActualiza"></a><br>&nbsp;
			</td>
		</tr>
	</table>
	<br>
	</form:form>