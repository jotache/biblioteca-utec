<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>

	function fc_ValidaTextoEspecialEncuestas() {
       if((window.event.keyCode != 209) && (window.event.keyCode != 241) && (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && (window.event.keyCode != 218) && (window.event.keyCode != 252)){
         	var ch_Caracter = String.fromCharCode(window.event.keyCode);
         	
 			var intEncontrado = " 0123456789abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ.!#%&/()=?�+-<>{}[]@:;*'�������������,".indexOf(ch_Caracter);
 			if (intEncontrado == -1)
 				window.event.keyCode = 0;
 			else
 				window.event.keyCode = ch_Caracter.charCodeAt();
 		}
	 }
	 
	 function fc_ValidaTextoEspecialEncuestasOnblur(strNameObj){
	 	var Obj = document.all[strNameObj];
	 	var strCadena = Obj.value;
	 	if(strCadena == "")
	 		return true;
	
	 	var valido = " 0123456789abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ.!#%&/()=?�+-<>{}[]@:;*'�������������,";
	 	strCadena = strCadena;
	 	for (i = 0 ; i <= strCadena.length - 1; i++)
	 	{
	 		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1) {
	 			valido = strCadena.substring(i,i + 1);
	 			alert ('El nombre del producto contiene caracteres no permitidos.' );
	 			Obj.value = "";
	 			Obj.focus();
	 			return false;
	 		}
	 	}
	 	return true;
	 }
	 
	function onLoad()
	{
		if (document.getElementById("txtNroDias").value==""){
			document.getElementById("txtNroDias").value="0";
		}
		if(document.getElementById("txhBien").value=="0001"){
			fc_rad1();
			document.getElementById("txhCondicion").value = "0001";
			document.getElementById("rad2").disabled="true";
			document.getElementById("rad1").checked="true";
			}
		else if(document.getElementById("txhBien").value=="0002"){
			fc_rad2()
			document.getElementById("txhCondicion").value = "0002";
			document.getElementById("rad1").disabled="true";
			document.getElementById("rad2").checked="true";
		}
		Fc_Habilitar();
		document.getElementById("codSede").value = document.getElementById("txhCodSedeAux").value;
		if(document.getElementById("txhTipo").value =="MODIFICAR"){
			addImagen.style.display="";
			documentos.style.display="";
			descriptores.style.display="";
		}
		if(document.getElementById("txhCondicion").value == "0001"){
			fc_rad1()
		}
		if(document.getElementById("txhCondicion").value == "0002"){
			fc_rad2()
		}
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrSeGraboConExito);
		}
		else if( objMsg.value == "ERROR" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrProblemaGrabar);
		}
		else if( objMsg.value == "DOBLE" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrRegistroDoble);
		}
		
		objMsg = document.getElementById("txhMens");
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			 document.getElementById("txhMens").value = "";
			alert(mstrElimino);
		}
		else if ( objMsg.value == "ERROR" )
		{
			objMsg.value = "";
			document.getElementById("txhMens").value = "";
			alert(mstrProbEliminar);
		}
		else if ( objMsg.value == "DOBLE" )
		{
			objMsg.value = "";
			document.getElementById("txhMens").value = "";
			alert(mstrNoEli);
		}
		document.getElementById("txhOperacion").value="";
		fc_Ubicacion();
	}
	
	function fc_Regresa(){
		location.href = "${ctx}/logistica/log_Consulta_Catalogo_Productos.html?txhCodAlumno=" + document.getElementById("txhCodAlumno").value;
	}	
	function fc_Grabar(){
		if(document.getElementById("codSede").value!=="" && document.getElementById("codFamilia").value!=="" &&
		document.getElementById("codSubFamilia").value!=="" && document.getElementById("txtNomPro").value!=="" &&
		document.getElementById("codFamilia").value!=="" && document.getElementById("txtDescripcion").value!=="" &&
		document.getElementById("codUnidad").value!=="" && document.getElementById("txhCondicion").value!=="" &&
		document.getElementById("txtNroDias").value!=="" ){
			if(fc_Validar_Ubicacion()==1)
			{	min = document.getElementById("txtMin").value;
				max = document.getElementById("txtMax").value;
				if(parseFloat(min)>parseFloat(max)){
					alert(mstrStock);
					return false;
				}
				if(confirm(mstrSeguroGrabar)){
					document.getElementById("txhOperacion").value="GRABAR";
					document.getElementById("frmMain").submit();
				}
			}
			else return false;
	 		}
		else{
			alert(mstrLleneTodoCampos);
		}
	}
	function Fc_Adjuntar(){
		Fc_Popup("${ctx}/logistica/log_Adjuntar_Imagen.html?txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhCodProducto=" + document.getElementById("txhCodProducto").value
		,450,130);
	}
	function Fc_EliminarFoto(){
		document.getElementById("txhOperacion").value="ELI_FOTO";
		document.getElementById("frmMain").submit();
	}
	function Fc_Agrega(){
		Fc_Popup("${ctx}/logistica/log_Buscar_Descriptor.html?txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhCodProducto=" + document.getElementById("txhCodProducto").value
		,480,400);
	}
	function Fc_Eliminar(){
		if(fc_Trim(document.getElementById("txhcodAdj").value) !== ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhOperacion").value="ELIMINAR";
				document.getElementById("frmMain").submit();
			}
			else{
				return false;
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
	
	function Fc_Adjunta(){
		Fc_Popup("${ctx}/logistica/log_Adjuntar_Documento.html?txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhCodProducto=" + document.getElementById("txhCodProducto").value,450,150);
	}
	function fc_rad1(){
		document.getElementById("txhCondicion").value = "0001";
		activo.style.display="";
		document.getElementById("chkInv").style.display="none";
		lblInv.style.display="none";
	}
	function fc_rad2(){
		document.getElementById("txhCondicion").value = "0002";
		activo.style.display="none";
		document.getElementById("chkInv").style.display="";
		lblInv.style.display="";
	}
	function fc_seleccionaDoc(strCodigo,strCodAdj){
		document.getElementById("txhcodAdj").value = strCodigo;
	}
	function fc_Adjunto(strCod){
		strRuta = "<%=(( request.getAttribute("msgRutaServerDoc")==null)?"": request.getAttribute("msgRutaServerDoc"))%>";
		if ( strRuta != "" )
		{
			if ( fc_Trim(strCod) != "") window.open(strRuta + strCod);
			else alert(mstrNoTieneCV);
		}
	}
	function fc_buscarDescriptor(strCod){
		document.getElementById("txhCodDescriptor").value = strCod; 
	}
	function Fc_EliminaDescriptor(){
		if(fc_Trim(document.getElementById("txhCodDescriptor").value) !== ""){
			if( confirm(mstrEliminar) ){
			document.getElementById("txhOperacion").value="ELIMINARDESCRIPTOR";
			document.getElementById("frmMain").submit();
			}
			else{
				return false;
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
	function Fc_MuestraSubFamilia(){
		document.getElementById("txhOperacion").value="MUESTRASUBFAMILIA";
		document.getElementById("frmMain").submit();
	}
	
	function fc_Ubicacion(){
		if(document.getElementById("codUbicacion").value!="-1")
		{  document.getElementById("txtUbiFila").disabled=false;
		   document.getElementById("txtUbiColum").disabled=false;
		}
		else{  document.getElementById("txtUbiFila").value="";
			document.getElementById("txtUbiColum").value="";
		    document.getElementById("txtUbiFila").disabled=true;
			document.getElementById("txtUbiColum").disabled=true;
		}
	}
	
	function fc_Validar_Ubicacion(){
		if(document.getElementById("codUbicacion").value!="-1")
		{  if(document.getElementById("txtUbiFila").value!="" && document.getElementById("txtUbiColum").value!="")
		       return 1;
		   else{ if(document.getElementById("txtUbiFila").value==""){
		    	 alert(mstrUbiFila);
		    	 document.getElementById("txtUbiFila").focus();
		    	 return 0;
		    	 }
		    	 else{ alert(mstrUbiColum);
		    	 		document.getElementById("txtUbiColum").focus();
		    	       return 0;
		    	 }
		    }
		}
		else return 1;
	}
	
	function Fc_Habilitar(){
		if(document.getElementById("codUbicacion").value == ""){
			document.getElementById("txtUbiFila").value = "";
			document.getElementById("txtUbiColum").value = "";
			document.getElementById("txtUbiFila").disabled = true; 
			document.getElementById("txtUbiColum").disabled = true;
		}	
		if(document.getElementById("codUbicacion").value !== ""){
			document.getElementById("txtUbiFila").disabled = false; 
			document.getElementById("txtUbiColum").disabled = false;
		}			
	}
	
</script>
	</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Agregar_Producto.html">
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="condicion" id="txhCondicion"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="mens" id="txhMens"/>
		<form:hidden path="codAdj" id="txhCodAdj"/>
		<form:hidden path="codProducto" id="txhCodProducto"/>
		<form:hidden path="tipo" id="txhTipo"/>
		<form:hidden path="ruta" id="txhRuta"/>
		<form:hidden path="ru" id="txhRu"/>
		<form:hidden path="bien" id="txhBien"/>
		<form:hidden path="codDescriptor" id="txhCodDescriptor"/>
		<form:hidden path="codSedeAux" id="txhCodSedeAux"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
			<tr>
				<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="695px" height="4px"></td>
			</tr>
			<tr>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
				<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Cat�logo de Productos</font></td>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
			</tr>
			<tr>
				<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="545px"></td>
			</tr>
		</table>
		<table height="2px"><tr><td></td></tr></table>
		<div style="overflow: auto; height: 420px;width:100%">
		<!-- ALQD,24/02/09.SE MUESTRA P.UNITARIO, PERMITIR GRABAR UN PRECIO REFERENCIAL -->
		<!-- ALQD,25/05/09.AGREGANDO COLUMNA DE NO AFECTO A IGV -->
		<table cellpadding="2" cellspacing="2" style="width:97.5%"
		class="tablaflotante" border="0" bordercolor="red">
			<tr>
				<td>&nbsp;Sede :</td>
				<td>&nbsp;<form:select path="codSede" id="codSede" cssClass="cajatexto_o"
					cssStyle="width:190px" disabled="true">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listSede!=null}">
					<form:options itemValue="codPadre" itemLabel="descripcion"
					items="${control.listSede}" />
					</c:if>
					</form:select>
				</td>
				<td id="addImagen" align="left" rowspan="3" colspan="2" style="display:none">
					<c:if test="${control.ru!=''}">
					<img src="<c:out value="${control.ruta}"></c:out>"  width="100px" height="100px">&nbsp;
					</c:if>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoAdjuntar1','','${ctx}/images/iconos/adjuntar2.jpg',1)">
					<img src="${ctx}/images/iconos/adjuntar1.jpg" onclick="javascript:Fc_Adjuntar();" alt="Adjuntar Foto" style="cursor:hand;" id="icoAdjuntar1">
					</a>
					<c:if test="${control.ru!=''}">
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoEliminar3','','${ctx}/images/iconos/kitar2.jpg',1)">
					<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_EliminarFoto();" alt="Eliminar Foto" style="cursor:hand;" id="icoEliminar3">
					</a>
					</c:if>
				</td>
			</tr>
			<tr>
				<td>&nbsp;Familia :</td>
				<td>&nbsp;<form:select path="codFamilia" id="codFamilia" cssClass="cajatexto"
					cssStyle="width:190px" onchange="javascript:Fc_MuestraSubFamilia();">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listFamilia!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listFamilia}" />
					</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td>&nbsp;SubFamilia :</td>
				<td>&nbsp;<form:select path="codSubFamilia" id="codSubFamilia" cssClass="cajatexto"
					cssStyle="width:190px">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listSubFamilia!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listSubFamilia}" />
					</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td>&nbsp;Producto :</td>
				<td colspan="3">&nbsp;<form:input path="nomPro" id="txtNomPro" 
					cssClass="cajatexto_o" size="45" maxlength="250" cssStyle="width:530px"	
					onkeypress="fc_ValidaTextoEspecialEncuestas();" onblur="fc_ValidaTextoEspecialEncuestasOnblur('txtNomPro');"/>
				</td>
			</tr>
			<tr>
				<td valign="top" rowspan="2">&nbsp;Descripci�n :</td>
				<td rowspan="2">&nbsp;
					<form:textarea path="descripcion" id="txtDescripcion" cssClass="cajatexto_o" cssStyle="width:220px;height:50px;"></form:textarea>
				</td>
			</tr>
			<tr>
				<td valign="top">&nbsp;Unidad :</td>
				<td valign="top">&nbsp;<form:select path="codUnidad" id="codUnidad" cssClass="cajatexto_o"
					cssStyle="width:150px">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listunidad!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listunidad}" />
					</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td>&nbsp;Condici�n :</td>
				<td>&nbsp;<input class="radio" type="radio" <%if("0001".equalsIgnoreCase((String)request.getAttribute("condicion"))){ %> checked<%} %> id="rad1" onclick="javascript:fc_rad1();" name="rad1" VALUE="${status.value}">&nbsp;Activo&nbsp;
					<input class="radio" type="radio" <%if("0002".equalsIgnoreCase((String)request.getAttribute("condicion"))){ %> checked<%} %> id="rad2" onclick="javascript:fc_rad2();" name="rad1" VALUE="${status.value}">&nbsp;Consumible
					&nbsp;<form:checkbox id="chkInv" path="chkInv"  value="1"/><span id="lblInv">&nbsp;Inversi�n</span>
				</td>
				<td>&nbsp;Ubicaci�n :</td>
				<td>&nbsp;<form:select path="codUbicacion" id="codUbicacion" cssClass="cajatexto"
					cssStyle="width:150px" onchange="javascript:fc_Ubicacion();">
					<form:option value="-1">--Seleccione--</form:option>
					<c:if test="${control.listUbicacion!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listUbicacion}" />
					</c:if>
					</form:select>
					<form:input path="ubiFila" id="txtUbiFila" onkeypress="fc_ValidaNumerico2();"
					onblur="fc_ValidaNumeroOnBlur('txtNroDias');" disabled="true"
					cssClass="cajatexto" size="10" maxlength="6" cssStyle="width:32px"/>
					<form:input path="ubiColum" id="txtUbiColum" onkeypress="fc_ValidaNumerico2();"
					onblur="fc_ValidaNumeroOnBlur('txtNroDias');" disabled="true"
					cssClass="cajatexto" size="10" maxlength="6" cssStyle="width:32px"/>
				</td>
			</tr>
			<tr>
				<td>&nbsp;Stock M�n/Max :</td>
				<td>&nbsp;<form:input path="min" id="txtMin" onkeypress="fc_ValidaNumeroDecimal();"
								onblur="fc_ValidaDecimalOnBlur(this.id,'10','3');"
					cssClass="cajatexto" maxlength="8" cssStyle="width:35px" />&nbsp;-&nbsp;
					<form:input path="max" id="txtMax" onkeypress="fc_ValidaNumeroDecimal();"
								onblur="fc_ValidaDecimalOnBlur(this.id,'10','3');"
					cssClass="cajatexto" maxlength="8" cssStyle="width:35px"/>
					&nbsp;Stock Mostrar :&nbsp;
					<form:input path="stockMostrar" id="txtStockMostrar" onkeypress="fc_ValidaNumeroDecimal();"
								onblur="fc_ValidaDecimalOnBlur(this.id,'10','3');"
					cssClass="cajatexto" maxlength="8" cssStyle="width:35px" />
				</td>
				<td>&nbsp;Stock Disponible :</td>
				<td>&nbsp;<form:input path="stockDisponible" id="stockDisponible" 
								 readonly="true"
					cssClass="cajatexto_1" maxlength="6" cssStyle="width:45px" />
					&nbsp;
					Stock Actual :
					&nbsp;
					<form:input path="stockActual" id="stockActual" 
								 readonly="true"
					cssClass="cajatexto_1" maxlength="6" cssStyle="width:45px" />
				</td>
			</tr>
			<tr>
				<td>&nbsp;Precio Unitario :</td>
				<td>&nbsp;<form:input path="precio" id="precio" 
								 readonly="false"
					cssClass="cajatexto_o" maxlength="10" cssStyle="width:55px" />
					&nbsp;Precio Ref. :
					&nbsp;<form:input path="preURef" id="preURef"
					onkeypress="fc_ValidaNumeroDecimal();"
					onblur="fc_ValidaDecimalOnBlur(this.id,'10','5');"
					cssClass="cajatexto" maxlength="10" cssStyle="width:75px" />
				</td>
				<td>&nbsp;Nro. D�as Atenci�n :</td>
				<td>&nbsp;<form:input path="nroDias" id="txtNroDias" onkeypress="fc_ValidaNumerico2();"
								onblur="fc_ValidaNumeroOnBlur('txtNroDias');"
					cssClass="cajatexto_o" size="6" maxlength="60" />
				</td>
			</tr>
			<tr id="activo" name="activo" style="display:none">
				<td>&nbsp;Marca :</td>
				<td>&nbsp;<form:input path="marca" id="txtMarca" 
					cssClass="cajatexto" size="7" maxlength="30" />
				</td>
				<td>&nbsp;Modelo :</td>
				<td>&nbsp;<form:input path="modelo" id="txtModelo" 
					cssClass="cajatexto" size="7" maxlength="30" />
				</td>
			</tr>
			<tr>
			<td>&nbsp;Estado :</td>
				<td>
				&nbsp;<form:select path="codEstado" id="codEstado" cssClass="cajatexto"
					cssStyle="width:190px">
					<c:if test="${control.listEstado!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listEstado}" />
					</c:if>
					</form:select>
				</td>
				<td>&nbsp;<form:checkbox id="chkPromo" path="chkPromo"  value="1"/><span id="lblPromo">&nbsp;Promocional</span>
				</td>
				<td>&nbsp;<form:checkbox id="chkNoAfecto" path="chkNoAfecto"  value="1"/><span id="lblNoAfecto">&nbsp;No Afecto al IGV</span>
				</td>
			</tr>
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" class="" border="0" bordercolor="red" width="97.5%">
			<tr>
				<td width="50%">
					<table cellpadding="0" cellspacing="0" bgcolor="#048bba" height="30" border="0" bordercolor="white" width="98%">
						<tr>
							<td class="opc_combo">&nbsp;<img src="${ctx}/images/Logistica/vineta.jpg">&nbsp;&nbsp;&nbsp;&nbsp;Descriptores</td>
						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" class="" style="width:98%;margin-top: 4px;" border="0" bordercolor="red">
						<tr>
							<td>
								<div style="overflow: auto;height:100px;width:100%;">
									<display:table name="sessionScope.listDescriptores" cellpadding="0" cellspacing="1"
										decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
										requestURI="" style="border: 1px solid #048BBA;width:95%">
										<display:column property="rbtSelBuscarDescriptor2" title="Sel." headerClass="grilla" class="tablagrilla" style="width:10%;text-align:center"/>
										<display:column property="dscDescriptor" title="Descriptores" headerClass="grilla" class="tablagrilla" style="align:left;width:90%"/>
										
										<display:setProperty name="basic.empty.showtable" value="true"  />
										<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
										<display:setProperty name="paging.banner.placement" value="bottom"/>
										<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
										<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
										<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
									</display:table>
									<% request.getSession().removeAttribute("listDescriptores"); %>
								</div>
							</td>
							<td id="descriptores" style="display:none" width="30px" valign="top" align="right">
								<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
									<img src="${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agrega();" alt="Agregar Descriptor" style="cursor:hand" id="icoAgregar">
								</a>
								<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)">
									<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_EliminaDescriptor();" alt="Eliminar Descriptor" style="cursor:hand;" id="icoEliminar">
								</a>
							</td>
						</tr>
					</table>
				</td>
				<td width="50%">
					<table cellpadding="0" class="tabla2" cellspacing="0" bgcolor="#048bba" height="30" border="0" bordercolor="white" width="100%">
						<tr>
							<td class="opc_combo">&nbsp;<img src="${ctx}/images/Logistica/vineta.jpg">&nbsp;&nbsp;&nbsp;&nbsp;Documentos Relacionados</td>
						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" style="width:100%;margin-top: 4px;" border="0" bordercolor="red">
						<tr>
							<td>
								<div style="overflow: auto; height:100px;width:100%">
									<display:table name="sessionScope.listdocRela" cellpadding="0" cellspacing="1"
										decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
										requestURI="" style="border: 1px solid #048BBA;width:95%">
										
										<display:column property="rbtSelDocPro" title="Sel." headerClass="grilla" class="tablagrilla" style="width:10%"/>
										<display:column property="dscTipAdj" title="Tipo de Adjunto" headerClass="grilla" class="tablagrilla" style="align:left;width:80%"/>
										<display:column property="adjunto" title="Ver" headerClass="grilla" class="tablagrilla" style="width:10%"/>
										
										<display:setProperty name="basic.empty.showtable" value="true"  />
										<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='3' align='center'>No se encontraron registros</td></tr>"  />
										<display:setProperty name="paging.banner.placement" value="bottom"/>
										<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
										<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
										<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
									</display:table>
									<% request.getSession().removeAttribute("listdocRela"); %>
								</div>
							</td>
							<td id="documentos" style="display:none" width="30px" valign="top" align="right">
								<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoAdjuntar2','','${ctx}/images/iconos/adjuntar2.jpg',1)">
									<img src="${ctx}/images/iconos/adjuntar1.jpg" onclick="javascript:Fc_Adjunta();" alt="Adjuntar Documento" style="cursor:hand;" id="icoAdjuntar2">
								</a>
								<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar1','','${ctx}/images/iconos/kitar2.jpg',1)">
									<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_Eliminar();" alt="Eliminar Documento" style="cursor:hand;" id="icoEliminar1">
								</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table align="center" style="margin-top:5px">
			<tr>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:fc_Grabar();" alt="Grabar" id="btnGrabar"></a>
				</td>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:fc_Regresa();" alt="Cancelar" id="btnCancelar"></a>
				</td>
			</tr>
		</table>
	</div>
</form:form>
