<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<script language=javascript>
	function onLoad()
	{
	
	document.getElementById("txhSecuencial").value = "";
	document.getElementById("txhDesc").value = "";
	document.getElementById("txhOperacion").value = "";
	objMsg = document.getElementById("txhMsg");
	
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			alert(mstrElimino);
			
		}		
		else if ( objMsg.value == "ERROR" )
			{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrProbEliminar);			
			}
			else if ( objMsg.value == "DOBLE" )
			{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrNoEli);			
			}
	}
	function Fc_Agregar(param)
	{
	
	strTipo = "";
	Fc_Popup("${ctx}/logistica/log_Agregar_TipoAdjunto.html?txhSecuencial=" + ""+ "&txhDesc=" + "" + "&txhTipo=" + strTipo + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value
	,450,110);
	}
	function Fc_Modificar(){
	if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
	strTipo = "MODIFICAR";
	Fc_Popup("${ctx}/logistica/log_Agregar_TipoAdjunto.html?txhSecuencial=" + document.getElementById("txhSecuencial").value+ "&txhDesc=" + document.getElementById("txhDesc").value + "&txhTipo=" + strTipo + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value
	,450,110);
		}
		 else
		 {
			alert(mstrSeleccion);
		 }
	}

	function fc_seleccionarRegistro(strCod,strDes){
	document.getElementById("txhSecuencial").value = strCod;
	document.getElementById("txhDesc").value = strDes;
	}
	function Fc_Eliminar(){
	if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
	if( confirm(mstrEliminar) ){
			document.getElementById("txhOperacion").value = "ELIMINAR";
			document.getElementById("frmMain").submit();
		
			}
			else{
			return false;
			}
	}
	else{
		alert(mstrSeleccion);
	}
	}
	</script>
	</head>
	<form:form action="${ctx}/logistica/log_Mantenimiento_TipoAdjuntos.html" commandName="control" id="frmMain">
			<form:hidden path="operacion" id="txhOperacion"/>
			<form:hidden path="secuencial" id="txhSecuencial"/>
			<form:hidden path="desc" id="txhDesc"/>
			<form:hidden path="msg" id="txhMsg"/>
			<form:hidden path="codAlumno" id="txhCodAlumno"/>
			<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
			<tr>
				<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="700px" height="4px"></td>
			</tr>
			<tr>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
				<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Consulta de Tipos de Adjuntos</font></td>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
			</tr>
			<tr>
				<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="555px"></td>
			</tr>
		</table>
	<table style="width:100%;margin-top:10px" cellSpacing="0" cellPadding="1" border="0" bordercolor="red"> 
			<tr><td width="90%">
			<div style="overflow: auto; height: 418px;width:100%">									
					<display:table name="sessionScope.listAdjuntos" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
						requestURI="" style="border: 1px solid #048BBA;width:98%">
						<display:column property="rbtSelAdjuntos" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
						<display:column property="descripcion" title="Tipos De Adjuntos" headerClass="grilla" class="tablagrilla" style="align:left;width:95%"/>
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					</display:table>
					<% request.getSession().removeAttribute("listAdjuntos"); %>
				</div>
				<td valign=top>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src="${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar();" alt="Agregar" style="cursor:hand" id="icoAgregar"></a><br>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoModificar','','${ctx}/images/iconos/modificar2.jpg',1)">
					<img src="${ctx}/images/iconos/modificar1.jpg" onclick="javascript:Fc_Modificar();" alt="Modificar" style="cursor:hand;" id="icoModificar"></a><br>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)">
					<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_Eliminar();" alt="Eliminar" style="cursor:hand;" id="icoEliminar"></a>
				</td>
			</tr>
		</table>
		<br>
	</form:form>
