<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script type="text/javascript">
	function onLoad()
	{
		document.getElementById("txhSecuencial").value = "";
		document.getElementById("txhDescripcion").value = "";
		document.getElementById("txhValor1").value = "";
		document.getElementById("txhValor2").value = "";
		document.getElementById("txhOperacion").value = "";
		
		objMsg = document.getElementById("txhMsg");
	
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrElimino);
			
		}	 	
		else if ( objMsg.value == "ERROR" )
			{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrProbEliminar);
			}
		else if ( objMsg.value == "DOBLE" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrNoEli);			
		}	
	}
	
	function Fc_Agregar(){
		strTipo = "";
		Fc_Popup("${ctx}/logistica/log_Agregar_Unidad_Medida.html?txhSecuencial=" + "" + "&txhDescripcion=" + "" + "&txhValor1=" + "" + "&txhValor2=" + "" + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhTipo=" + strTipo
		,520,165);
	}
	function fc_seleccionarRegistro(strCod,strDes,strVal1,strVal2){
		document.getElementById("txhSecuencial").value = strCod;
		document.getElementById("txhDescripcion").value = strDes;
		document.getElementById("txhValor1").value = strVal1;
		document.getElementById("txhValor2").value = strVal2;
	}
	function Fc_Modificar(){
		if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
			strTipo = "MODIFICAR";
			Fc_Popup("${ctx}/logistica/log_Agregar_Unidad_Medida.html?txhSecuencial=" + document.getElementById("txhSecuencial").value + "&txhDescripcion=" + document.getElementById("txhDescripcion").value + "&txhValor1=" + document.getElementById("txhValor1").value + "&txhValor2=" + document.getElementById("txhValor2").value + "&txhTipo=" + strTipo + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value
			,520,165);
		}
		else
		{
			alert(mstrSeleccion);
		}
	}
	function Fc_Eliminar(){
		if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhOperacion").value = "ELIMINAR";
				document.getElementById("frmMain").submit();
				document.getElementById("txhOperacion").value ="";
			}
			else{
				return false;
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
	</script>
	</head>
	<form:form action="${ctx}/logistica/log_Mantenimiento_Unidad_Medida.html" commandName="control" id="frmMain">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="secuencial" id="txhSecuencial"/>
		<form:hidden path="descripcion" id="txhDescripcion"/>
		<form:hidden path="valor1" id="txhValor1"/>
		<form:hidden path="valor2" id="txhValor2"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
			<tr>
				<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="684px" height="4px"></td>
			</tr>
			<tr>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
				<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Consulta de Unidades de Medida</font></td>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
			</tr>
			<tr>
				<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="535px"></td>
			</tr>
		</table>	
		<table style="width:100%;margin-top:10px" cellSpacing="0" cellPadding="0" border="1" bordercolor="white"> 
			<tr><td>
				<div style="overflow: auto; height: 415px;width:100%">									
					<display:table name="sessionScope.listUnidad" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
						requestURI="" style="border: 1px solid #048BBA;width:99%">
						<display:column property="rbtSelUnidad" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
						<display:column property="descripcion" title="Nombre" headerClass="grilla" class="tablagrilla" style="align:left;width:65%"/>
						<display:column property="valor1" title="Abrev." headerClass="grilla" class="tablagrilla" style="align:left;width:15%"/>
						<display:column property="valor2" title="Nro. Decimales" headerClass="grilla" class="tablagrilla" style="text-align:center;width:15%"/>
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					</display:table>
					<%request.getSession().removeAttribute("listUnidad"); %>
				</div>
				</td>
				<td valign=top>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src="${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar();" alt="Agregar" style="cursor:hand" id="icoAgregar"></a><br>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoModificar','','${ctx}/images/iconos/modificar2.jpg',1)">
					<img src="${ctx}/images/iconos/modificar1.jpg" onclick="javascript:Fc_Modificar();" alt="Modificar" style="cursor:hand;" id="icoModificar"></a><br>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)">
					<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_Eliminar();" alt="Eliminar" style="cursor:hand;" id="icoEliminar"></a>
				</td>
			</tr>
		</table>
		<br>
 	</form:form>
</html>
