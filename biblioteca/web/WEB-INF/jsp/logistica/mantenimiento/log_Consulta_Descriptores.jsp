<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script type="text/javascript">
	
	function onLoad()	
	{
		document.getElementById("txhSecuencial").value = "";
		document.getElementById("txhDesc").value = "";
		document.getElementById("txhCodigo").value = "";
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrElimino);
			
		}		
		else if ( objMsg.value == "ERROR" )
			{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrProbEliminar);			
			}
		else if ( objMsg.value == "DOBLE" )
		{
		objMsg.value = "";
		document.getElementById("txhMsg").value = "";
		alert(mstrNoEli);			
		}
	}

	function Fc_Busca(){
		document.getElementById("txhOperacion").value = "BUSCAR";
		document.getElementById("frmMain").submit();
	}
	function Fc_Agregar(){
		strTipo = "";
		Fc_Popup("${ctx}/logistica/log_Mantenimiento_Descriptor.html?txhSecuencial=" + "" + "&txhDesc=" + "" + "&txhCodigo=" + "" + "&txhTipo=" + strTipo + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value
		,450,180);
	}
	function Fc_Modificar(){
		if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
			strTipo = "MODIFICAR";
			Fc_Popup("${ctx}/logistica/log_Mantenimiento_Descriptor.html?txhSecuencial=" + document.getElementById("txhSecuencial").value + "&txhDesc=" + document.getElementById("txhDesc").value + "&txhCodigo=" + document.getElementById("txhCodigo").value + "&txhTipo=" + strTipo + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value
			,450,180);
		}
		else
		{
			alert(mstrSeleccion);
		}
	}
	function Fc_Limpia(){
		document.getElementById("txhDescripcion").value = "";
	}
	function fc_seleccionarRegistro(strCod,strDes,strCodigo){
		document.getElementById("txhSecuencial").value = strCod;
		document.getElementById("txhDesc").value = strDes;
		document.getElementById("txhCodigo").value = strCodigo;
	}
	function Fc_Eliminar(){
		if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhOperacion").value = "ELIMINAR";
				document.getElementById("frmMain").submit();
			}
			else{
				return false;
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
	</script>
	</head>
	<form:form action="${ctx}/logistica/log_Consulta_Descriptores.html" commandName="control" id="frmMain">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="secuencial" id="txhSecuencial"/>
		<form:hidden path="desc" id="txhDesc"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/> 
		<form:hidden path="codigo" id="txhCodigo"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
			<tr>
				<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="695px" height="4px"></td>
			</tr>
			<tr>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
				<td valign="top" class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Consulta de Descriptores</font></td>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
			</tr>
			<tr>
				<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="545px"></td>
			</tr>
		</table>			
		<table bgcolor="" style="width:93%;margin-top:10px" border="0" cellspacing="0" class="tabla"
		 background="${ctx}/images/Logistica/back.jpg" height="50px" bordercolor="red">
			<tr height="5px">
				<td></td>
			</tr>
			<tr>
				<td class="">&nbsp;Descriptor:</td>
				<td><form:input path="descripcion" id="txhDescripcion"
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaTextoEspOnBlur('txhDescripcion','descriptor');" 
					cssClass="cajatexto" size="40" /></td>
				<td align="right">
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" onclick="javascript:Fc_Limpia();" alt="Limpiar" style="cursor:hand;"id="icoLimpiar"></a>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:Fc_Busca();" alt="Buscar" style="cursor:hand;"id="icoBuscar"></a>&nbsp;&nbsp;
				</td>
			</tr>
			<tr height="5px">
				<td></td>
			</tr>
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" ID="Table1" width="94%">
			<tr>
				<td>
					<div style="overflow: auto; height: 347px;width:100%">									
					<display:table name="sessionScope.listDescriptores" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.LogisticaDecorator" style="border: 1px solid #048BBA;width:97.5%">
						<display:column property="rbtSelDescriptor" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
						<display:column property="codSecuencial" title="C�digo" headerClass="grilla" class="tablagrilla" style="align:left;width:20%"/>
						<display:column property="descripcion" title="Descriptor" headerClass="grilla" class="tablagrilla" style="align:left;width:75%"/>
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>" />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					</display:table>
					<%request.getSession().removeAttribute("listDescriptores"); %>
					</div>
				</td>
				<td valign="top" width="27px">&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src="${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar();" alt="Agregar" style="cursor:hand" id="icoAgregar"></a>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoModificar','','${ctx}/images/iconos/modificar2.jpg',1)">
					<img src="${ctx}/images/iconos/modificar1.jpg" onclick="javascript:Fc_Modificar();" alt="Modificar" style="cursor:hand;" id="icoModificar"></a>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)">
					<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_Eliminar();" alt="Eliminar" style="cursor:hand;" id="icoEliminar"></a>
				</td>
			</tr>
		</table>
	</form:form>
	