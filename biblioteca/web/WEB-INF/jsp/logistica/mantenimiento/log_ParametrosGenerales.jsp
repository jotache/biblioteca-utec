<%@ include file="/taglibs.jsp"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script type="text/javascript">
	
	function onLoad()	
	{
	objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{	
			objMsg.value = "";
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);
		}
	}
	
	function fc_Grabar(){
	
	if(document.getElementById("txtIgv").value==""){
	alert(mstrIgv);
	return false;
	}
	if(document.getElementById("glosa").value==""){
	alert(mstrGlosa);
	return false;
	}
	if(document.getElementById("txtCtaExis").value==""){
	alert(mstrExistenciaExistir);
	return false;
	}
	if(document.getElementById("txtCtaTipoGas").value==""){
	alert(mstrTipoGastos);
	return false;
	}
	if(document.getElementById("txtCtaCentroCosto").value==""){
	alert(mstrCentroCostos);
	return false;
	}
	if(document.getElementById("detalle").value==""){
	alert(mstrGlosaDetalle);
	return false;
	}
	if(document.getElementById("cuser").value==""){
	alert(cuser);
	return false;
	}
	if(confirm(mstrSeguroGrabar)){
	document.getElementById("txGlosaCabecera").value = document.getElementById("glosa").value;
	document.getElementById("txhGlosaDetalle").value = document.getElementById("detalle").value;
	document.getElementById("txhOperacion").value="GRABAR";
		document.getElementById("frmMain").submit();
	}
	}
	
	</script>
	<form:form action="${ctx}/logistica/log_ParametrosGenerales.html" commandName="control" id="frmMain">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<form:hidden path="msg" id="txhMsg"/>  
		<form:hidden path="glosaDetalle" id="txhGlosaDetalle"/> 
		<form:hidden path="glosaCabecera" id="txGlosaCabecera"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
			<tr>
				<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="700px" height="4px"></td>
			</tr>
			<tr>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
				<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Parámetros del Sistema</font></td>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
			</tr>
			<tr>
				<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="555px"></td>
			</tr>
		</table>
	
		<table cellpadding="1" cellspacing="0"  ID="Table1" style="margin-top:10px;width:93.5%" border="0" bordercolor="Red" 
		 background="${ctx}/images/Logistica/back.jpg" class="tabla">		<tr>
				<td width="25%">I.G.V. :</td>
				<td>
				<form:input path="igv" id="txtIgv" cssStyle="text-align:right"
				
					onblur="fc_ValidaObjDecimalOnBlur(this,'2','2');"
					cssClass="cajatexto_o" maxlength="10" size="18" />
				</td>
			</tr>
			<tr>
					<td style="width: 20%;">Periodo Actual</td>
					<td>	<form:select path="codAnios" id="codAnios" cssClass="combo_o" cssStyle="width:80px">
								
									<c:if test="${control.codListaAnios!=null}">
										<form:options itemValue="id" itemLabel="name" 
											items="${control.codListaAnios}" />
									</c:if>
						</form:select>&nbsp;&nbsp;&nbsp;
						<form:select path="codMeses" id="codMeses" cssClass="combo_o" cssStyle="width:180px">
								
									<c:if test="${control.codListaMeses!=null}">
										<form:options itemValue="id" itemLabel="name" 
											items="${control.codListaMeses}" />
									</c:if>
						</form:select>
					</TD>					
			</tr>
			<tr>
				<td >Glosa Cabecera :</td>
				<td>
					<textarea id="glosa" maxlength="255" lang="255" class="cajatexto_o"  style="HEIGHT:55px;width:300px">${control.glosaCabecera}</textarea>
				</td>
			</tr>
			<tr>
				<td >Glosa Detalle :</td>
				<td>
					<textarea id="detalle" maxlength="255" lang="255" class="cajatexto_o"  style="HEIGHT:55px;width:300px">${control.glosaDetalle}</textarea>
				</td>
			</tr>
			<tr>
				<td >Cta. Existencia por Existir :</td>
				<td>
				<form:input path="ctaExis" id="txtCtaExis" cssStyle="text-align:right"
					onkeypress="fc_ValidaNumero();"
					onblur="fc_ValidaNumeroOnBlur('txtCtaExis');"
					cssClass="cajatexto_o" maxlength="10" size="18" />
				</td>
			</tr>
			<tr>
				<td >Cta. Tipo Gasto Ajustes :</td>
				<td>
				<form:input path="ctaTipoGas" id="txtCtaTipoGas" cssStyle="text-align:right"
					onkeypress="fc_ValidaNumero();"
					onblur="fc_ValidaNumeroOnBlur('txtCtaTipoGas');"
					cssClass="cajatexto_o" maxlength="10" size="18" />
				</td>
			</tr>
			<tr>
				<td >Cta. Centro Costo Ajustes :</td>
				<td>
				<form:input path="ctaCentroCosto" id="txtCtaCentroCosto" cssStyle="text-align:right"
					onkeypress="fc_ValidaNumero();"
					onblur="fc_ValidaNumeroOnBlur('txtCtaCentroCosto');"
					cssClass="cajatexto_o" maxlength="10" size="18" />
				</td>
			</tr>
			<tr>
				<td >Cuser :</td>
				<td>
				<form:input path="cuser" id="cuser" cssStyle="text-align:right"
					cssClass="cajatexto_o" maxlength="10" size="10" />
				</td>
			</tr>
		</table>
	
			<table align="center" style="margin-top:5px">
			<tr>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:fc_Grabar();" alt="Grabar" id="btnGrabar"></a>
						</td>
			</tr>
		</table>  
	</form:form>
</html>	