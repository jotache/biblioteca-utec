<%@ include file="/taglibs.jsp"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script type="text/javascript">
	
	function onLoad()	
	{
	objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if ( objMsg.value == "ERROR" )
		{
		alert(mstrProblemaGrabar);
		}
		else if(objMsg.value == "DOBLE"){
		alert(mstrOtraCompetencia);
		}
	}	
	function Fc_Graba(){
	
	document.getElementById("txtAbrev").value =document.getElementById("txtAbrev").value.toUpperCase();
	if ( fc_Trim(document.getElementById("txtAbrev").value) !== "" && fc_Trim(document.getElementById("txtNombre").value) !== "" && fc_Trim(document.getElementById("txtNroDecimales").value) !== "")
			{
			if(document.getElementById("txtNroDecimales").value<=5){
		document.getElementById("txhOperacion").value="GRABAR";
		document.getElementById("frmMain").submit();
			}
			else {
				alert(mstrNumeroMenor);
			}
		}
		else{
		alert(mstrLleneLosCampos);
		}
	}
	</script>
	</head>
	<form:form action="${ctx}/logistica/log_Agregar_Unidad_Medida.html" commandName="control" id="frmMain">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="tipo" id="txhTipo"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<form:hidden path="secuencial" id="txhSecuencial"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:5px;margin-left:10px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="350px" class="opc_combo"><font style="">Mantenimiento de Unidades de Medidas</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
			 </tr>
		 </table>
		<table style="width:95%;margin-top:10px;margin-left:10px" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray'> 
			<tr>
				<td valign='top' colspan="2">
					<table class="tablaflotante"  cellSpacing="1" cellPadding="1" border="0" bordercolor='red' height="70px"> 
						<tr>
							<td width="25%">Abrev :</td>
							<td>
								<form:input path="abrev" id="txtAbrev"
								onkeypress="fc_ValidaTextoNumeroEspecial();"
								onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'abrev.');"
								cssClass="cajatexto_o" size="15" maxlength="4"/>
							</td>
						</tr>
						 <tr>
							<td>Nombre :</td>
							<td>			
								<form:input path="nombre" id="txtNombre" 
								onkeypress="fc_ValidaTextoNumeroEspecial();"
								onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'nombre');"
								cssClass="cajatexto_o" size="60" maxlength="30"/>
							</td>
						</tr>
						<tr>
							<td> Nro. Decimales :</td>
							<td>		
								<form:input path="nroDecimales" id="txtNroDecimales" 
								onkeypress="fc_ValidaNumerico2();"
								onblur="fc_ValidaNumeroOnBlur('txtNroDecimales');"
								cssClass="cajatexto_o" size="3" maxlength="1"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>	
		<table align=center style="margin-top:7px">
			<tr>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Graba();" alt="Grabar" id="btnGrabar"></a>	
				</td>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
					<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="window.close();" alt="Cancelar" id="btnCancelar"></a>
				</td>
			</tr>
		</table>	
</form:form>
