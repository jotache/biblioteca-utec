<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
		var strExtensionGIF = "<%=(String)request.getAttribute("strExtensionGIF")%>";
		var strExtensionJPG = "<%=(String)request.getAttribute("strExtensionJPG")%>";
		function onLoad()
		{
			objMsg = document.getElementById("txhMsg");
			if ( objMsg.value == "OK" )
			{
				alert(mstrGrabar);
				window.opener.document.getElementById("frmMain").submit();
				window.close();
			}
			else if ( objMsg.value == "ERROR" )
			{
				alert(mstrProblemaGrabar);
			}
		}
		
		function fc_Valida(){
			var strExtension;
			var lstrRutaArchivo;
			if(fc_Trim(document.getElementById("txtCV").value) == ""){
				alert(mstrInforme);
				return false;
			}
			else{
				if(fc_Trim(document.getElementById("txtCV").value) != ""){
					lstrRutaArchivo = document.getElementById("txtCV").value;
					strExtension = "";
					strExtension = lstrRutaArchivo.toLowerCase().substring(lstrRutaArchivo.length - 3);
				
					nombre = (lstrRutaArchivo.substring(lstrRutaArchivo.lastIndexOf("\\"))).toLowerCase();
					document.getElementById("txhNomArchivo").value=nombre.substring("1");
			
					if (strExtension != strExtensionGIF && strExtension != strExtensionJPG){
						alert(mstrExtensionFoto);
						return false;
					}
					else{
						document.getElementById("txhExtCv").value = strExtension;
					}
				}
			}
			return true;
		}
		
		function fc_GrabarArchivos(){
			if (fc_Valida()){
				if (confirm(mstrSeguroGrabar)){
					document.getElementById("txhOperacion").value="GRABAR";
					document.getElementById("frmMain").submit();
				}
			}
		}

	</script>
	</head>
	<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/log_Adjuntar_Imagen.html" 
		enctype="multipart/form-data" method="post">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="extCv" id="txhExtCv"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="codPro" id="txhCodPro"/>
		<form:hidden path="nomArchivo" id="txhNomArchivo"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:5px">
			<tr>
				<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
				<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="250px" class="opc_combo"><font style="">Adjuntar Archivo</font></td>
				<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
			</tr>
		</table>
		<table style="width:98%;margin-top:5px" align="center" cellpadding="1" cellspacing="2" class="tablaflotante" >
			<tr>
				<td class="" width="50%">Archivo:</td>
				<td>
					<input type="file" id="txtCV" name="txtCV" class="cajatexto_1" size="50">
				</td>
			</tr>
		</table>
		<table align="center" style="margin-top: 5px">
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img src= "${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_GrabarArchivos();" style="cursor:hand" id="imgAgregar" alt="Grabar Foto">&nbsp;</a>
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminar','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img  src="${ctx}/images/botones/cancelar1.jpg" alt="Cancelar" style="cursor:hand" onclick="window.close();" id="imgEliminar"></a>
				</td>
			</tr>
		</table>
</form:form>
