<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>	
		function onLoad()
		{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{	
			objMsg.value = "";
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);
		}
		}
		
		function fc_Graba(){
		if(document.getElementById("txhCodResponsable").value !== ""){
		if (confirm(mstrSeguroGrabar)){
		document.getElementById("txhOperacion").value="GRABAR";
		document.getElementById("frmMain").submit();
			}
			}
			else
			{
				alert(mstrSeleccione);
			}  
		}
		function fc_Busca(){
		document.getElementById("txhOperacion").value="BUSCA";
		document.getElementById("frmMain").submit();
		}
		function fc_Limpia(){
		document.getElementById("codPer").value = ""; 
		document.getElementById("txtNombre").value = "";
		document.getElementById("txtApPat").value = "";
		document.getElementById("txtApMat").value = "";
		}
		function fc_seleccionarRegistro(strCod){
		document.getElementById("txhCodResponsable").value = strCod;
		}
			</script>
	</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Agregar_Flujo.html">
	<form:hidden path="operacion" id="txhOperacion"/>  
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="codigo" id="txhCodigo"/>
	<form:hidden path="codReq" id="txhCodReq"/>  
	<form:hidden path="msg" id="txhMsg"/>  
	<form:hidden path="codSede" id="txhSede"/>  
	<form:hidden path="codResponsable" id="txhCodResponsable"/>
		
<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left: 7px; margin-top: 6px;">
		<tr>
			<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="695px" height="4px"></td>
		</tr>
		<tr>
			<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
			<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Busqueda de Empleados</font></td>
			<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
		</tr>
		<tr>
			<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="545px"></td>
		</tr>
	</table>
	<table class="tabla" style="margin-top:10px;width:97.5%; margin-left: 7px;" border="0" bordercolor="0" cellpadding="0" cellspacing="0" background="${ctx}/images/Logistica/back.jpg">
		<tr>
			<td width="20%">&nbsp;&nbsp;Tipo Personal :</td>
			<td><form:select path="codPer" id="codPer" cssClass="cajatexto"
				cssStyle="width:240px">
				<form:option value="">--Todos--</form:option>
				<c:if test="${control.listPersonal!=null}">
				<form:options itemValue="codSecuencial" itemLabel="descripcion"
				items="${control.listPersonal}" />
				</c:if>
				</form:select></td>
				<td></td>
				<td align=right>
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
			<img src="${ctx}/images/iconos/limpiar1.jpg" onclick="javascript:fc_Limpia();" alt="Limpiar" style="cursor:hand" id="icoLimpiar"></a>&nbsp;
			<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
			<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:fc_Busca();" alt="Buscar" style="cursor:hand" id="icoBuscar"></a>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;&nbsp;Nombre :&nbsp;</td>
			<td>			
				<form:input path="nombre" id="txtNombre" 
				onkeypress="fc_ValidaTextoEspecial();"
				onblur="fc_ValidaTextoEspOnBlur('txtNombre','nombre');"
				cssClass="cajatexto" size="50" maxlength="100"/>
			</td>
		</tr>
		<tr>
			<td>&nbsp;&nbsp;Apellido Paterno:</td>
			<td>
			<form:input path="apPat" id="txtApPat" 
				onkeypress="fc_ValidaTextoEspecial();"
				onblur="fc_ValidaTextoEspOnBlur('txtApPat','apellido paterno');"
				cssClass="cajatexto" size="30" />
			</td>
			<td>Apellido Materno:</td>
			<td>
			<form:input path="apMat" id="txtApMat" 
				onkeypress="fc_ValidaTextoEspecial();"
				onblur="fc_ValidaTextoEspOnBlur('txtApMat','apellido materno');"
				cssClass="cajatexto" size="30" />
			</td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" width="100%" border="0" bordercolor="" 
	 		style="margin-left: 7px; margin-top: 6px;" class="">
		<tr>
			<td>
				<table id="tabla1" cellpadding="0" cellspacing="0" border="0" bordercolor="white" 
					style="width:100%">
					<tr><td>
					<div style="overflow: auto; height: 180px;width:100%">									
					<display:table name="sessionScope.listEmpleados" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
						requestURI="" style="border: 1px solid #048BBA;width:97.5%">
						<display:column property="rbtSelEmpleados" title="Sel" headerClass="grilla" class="tablagrilla" style="text-align:center;width:5%"/>
						<display:column property="nombre" title="Nombres" headerClass="grilla" class="tablagrilla" style="align:left;width:30%"/>
						<display:column property="tipoPersonal" title="Tipo Personal" headerClass="grilla" class="tablagrilla" style="align:left;width:25%"/>
						<display:column property="departamento" title="Departamento" headerClass="grilla" class="tablagrilla" style="align:left;width:40%"/>
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					</display:table>
					 <% request.getSession().removeAttribute("listEmpleados"); %>
				</div>
					</td></tr>
				</table>	
			</td>
		</tr>
	</table>
	<table align=center style="margin-top:7px">
			<tr>
			<td>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
			<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:fc_Graba();" alt="Grabar" id="btnGrabar"></a>
				</td>
				<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
				<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" alt="cancelar" id="btnCancelar"></a>
			</td>
			</tr>
		</table>	
</form:form>
	