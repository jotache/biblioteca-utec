<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>	
		function onLoad()
		{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{	
			objMsg.value = "";
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);
		}
		else if(objMsg.value == "DOBLE"){
		alert(mstrOtraCompetencia);
		}
			}
	function Fc_Grabar(){
	if ( fc_Trim(document.getElementById("txtCodigo").value) !== "" && fc_Trim(document.getElementById("txtTipGas").value) !== "" && fc_Trim(document.getElementById("txtCtaContable").value) !== "")
		{
	if(document.getElementById("txtCodigo").value !=="" && document.getElementById("txtTipGas").value !==""){
		document.getElementById("txhOperacion").value="GRABAR";
		document.getElementById("frmMain").submit();
		}
		else{
		alert(mstrLleneLosCampos);
			}
		}
		else{
		alert(mstrLleneLosCampos);
			}
	}
	</script>
	</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Agregar_TipoGasto.html">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="tipo" id="txhTipo"/>
	<form:hidden path="secuencial" id="txhSecuencial"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:5px;margin-left:10px">
		<tr>
			<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
			<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="240px" class="opc_combo"><font style="">Tipo de Gasto</font></td>
			<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
		</tr>
	</table>
	<table class="tablaflotante" style="width:95%;margin-top:5px;margin-left:10px" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray'> 
		<tr>
			<td nowrap>C�digo :&nbsp;</td>
			<td>
				<form:input path="codigo" id="txtCodigo" 
				onkeypress="fc_ValidaNumerico2();"
				onblur="fc_ValidaNumeroOnBlur('txtCodigo');"
				cssClass="cajatexto_o" size="15" maxlength="10" />
			</td>
		</tr>
		<tr>
			<td nowrap>Tipo Gasto :&nbsp;</td>
			<td>
				<form:input path="tipGas" id="txtTipGas" 
				cssClass="cajatexto_o" size="50" maxlength="60"/>
			</td>
		</tr>
		<tr>
			<td nowrap>Cta. Contable  :&nbsp;</td>
			<td>
			<form:input path="ctaContable" id="txtCtaContable" cssStyle="text-align:right"
			onkeypress="fc_ValidaNumero();"
			onblur="fc_ValidaNumeroOnBlur('txtCtaContable');"
			cssClass="cajatexto_o" maxlength="9" size="10" />
			</td>
		</tr>
	</table>	
	<table align="center" style="margin-top:7px">
		<tr>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Grabar();" alt="Grabar" id="btnGrabar"></a>
			</td>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img src="${ctx}/images/botones/cancelar1.jpg" style="cursor:hand;" onclick="window.close();" alt="Cancelar" id="btnCancelar"></a>
			</td>
		</tr>
	</table>	
</form:form>
