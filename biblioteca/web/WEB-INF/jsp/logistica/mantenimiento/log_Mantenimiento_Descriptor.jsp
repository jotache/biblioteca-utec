<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>
	function onLoad()
	{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{	
			objMsg.value = "";
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);
		}
		else if(objMsg.value == "DOBLE"){
		alert(mstrOtraCompetencia);
		}
	}
	function Fc_Graba(){
		if ( fc_Trim(document.getElementById("txtDescriptor").value) !== "")
		{
			document.getElementById("txhOperacion").value="GRABAR";
			document.getElementById("frmMain").submit();
		}
		else{
			alert(mstrLleneLosCampos);
		}
	}
	</script>
	</head>
		<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Mantenimiento_Descriptor.html">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="secuencial" id="txhSecuencial"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<form:hidden path="tipo" id="txhTipo"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:5px;margin-left:10px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="330px" class="opc_combo"><font style="">Mantenimiento de Descriptores</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
			 </tr>
		</table>
		<table style="width:95%;margin-top:10px;margin-left:10px" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray'> 
			<tr>
				<td valign='top' colspan="2">
					<table class="tablaflotante"  cellSpacing="1" cellPadding="1" border="0" bordercolor='red' height="70px"> 
						<tr>
							<td nowrap>C�digo :</td>
							<td>
								<form:input path="codigo" id="txtCodigo" 
								onkeypress="fc_ValidaNumerico2();"
								onblur="fc_ValidaNumeroOnBlur('txtCodigo');"
								cssClass="cajatexto_o" size="10" maxlength="10" readonly="true"/>
							</td>
						</tr>
						<tr>
							<td nowrap>Descriptor :</td>
							<td>
								<form:input path="descriptor" id="txtDescriptor" 
								cssClass="cajatexto_o" size="60" maxlength="60"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table align="center" style="margin-top:7px">
			<tr>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img src="${ctx}/images/botones/grabar1.jpg" onclick="javascript:Fc_Graba();" style="cursor:hand;" alt="Grabar" id="btnGrabar"></a>
				</td>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
					<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" id="btnCancelar"></a>
				</td>
			</tr>
		</table>	
	</form:form>
</html>