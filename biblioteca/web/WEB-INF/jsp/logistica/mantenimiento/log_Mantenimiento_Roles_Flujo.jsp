<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>	
		function onLoad()
		{
		Fc_Muestra();
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{	
			objMsg.value = "";
			alert(mstrSeGraboConExito);
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);
		}
		
		}
		
		function fc_Buscar(){
			document.getElementById("txhOperacion").value = "BUSCAR";
			document.getElementById("frmMain").submit();
			}
		
			function Fc_Muestra(){
			
			obj = document.getElementById("codTipo").value;
				if (obj == '0001'){
					tabla1.style.display="";
					tabla2.style.display="none";
					tabla3.style.display="none";
				}
				else if (obj == '0002'){
					tabla1.style.display="none";
					tabla2.style.display="";
					tabla3.style.display="none";
				}
				else if (obj == '0003'){
					tabla1.style.display="none";
					tabla2.style.display="none";
					tabla3.style.display="";
				}
			}
			function Fc_Grabar(){
			obj = document.getElementById("codTipo").value;
			if(obj == '0001'){
				if(!document.getElementById("item1").checked && !document.getElementById("item2").checked && !document.getElementById("item3").checked && !document.getElementById("item4").checked )
				{
					alert(mstrSelCheked2);
				 return false;
				}
				if(!document.getElementById("item41").checked && !document.getElementById("item42").checked && !document.getElementById("item43").checked && !document.getElementById("item44").checked)
				{
				alert(mstrSelCheked8);
				return false;
				}
			}
																														
			else if(obj == '0002'){
				if(!document.getElementById("item14").checked && !document.getElementById("item12").checked && !document.getElementById("item13").checked )
				{
				alert(mstrSelCheked4);
				return false;
				}
				if(!document.getElementById("item111").checked && !document.getElementById("item112").checked && !document.getElementById("item113").checked)
				{
				alert(mstrSelCheked7);
				return false;
				}
				if(!document.getElementById("item114").checked && !document.getElementById("item5").checked && !document.getElementById("item6").checked && !document.getElementById("item7").checked)
				{
				alert(mstrSelCheked2);
				return false;
				}
				if(!document.getElementById("item8").checked && !document.getElementById("item9").checked && !document.getElementById("item10").checked && !document.getElementById("item11").checked){
				alert(mstrSelCheked3);
				return false;
				}
				if(!document.getElementById("item81").checked && !document.getElementById("item82").checked && !document.getElementById("item83").checked && !document.getElementById("item84").checked )
				{
				alert(mstrSelCheked8);
				return false;
				}	
			}
																						
			else if(obj == '0003'){
				if(!document.getElementById("item16").checked && !document.getElementById("item17").checked 
				&& !document.getElementById("item18").checked && !document.getElementById("item19").checked)
			  	{
				alert(mstrSelCheked6);
				return false;
				}
				if(!document.getElementById("item121").checked && !document.getElementById("item122").checked
				&& !document.getElementById("item123").checked)
				{
				alert(mstrSelCheked);
				return false;
				}
				if(!document.getElementById("item128").checked && !document.getElementById("item129").checked
				&& !document.getElementById("item1210").checked && !document.getElementById("item1211").checked)
				{
				alert(mstrSelCheked3);
				return false;
				}
				if(!document.getElementById("item124").checked && !document.getElementById("item125").checked
				&& !document.getElementById("item126").checked && !document.getElementById("item127").checked)
				{
				alert(mstrSelCheked2);
				return false;
				}
				if(!document.getElementById("item20").checked && !document.getElementById("item21").checked
				&& !document.getElementById("item22").checked && !document.getElementById("item23").checked)
				{
				alert(mstrSelCheked4);
				return false;
				}
				if(!document.getElementById("item1212").checked && !document.getElementById("item12113").checked
				&& !document.getElementById("item12114").checked && !document.getElementById("item15").checked)
				{
				alert(mstrSelCheked5);
				return false;
				}
			}
			
			if (confirm(mstrSeguroGrabar)){
			document.getElementById("txhOperacion").value = "GRABAR";
			document.getElementById("frmMain").submit();
			 }
			}
			
		
			</script>
	</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Mantenimiento_Roles_Flujo.html">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="msg" id="txhMsg"/>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
		<tr>
			<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="695px" height="4px"></td>
		</tr>
		<tr>
			<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
			<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Configuraci�n de Participaci�n de Roles en Flujos de Aprobaciones</font></td>
			<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
		</tr>
		<tr>
			<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="545px"></td>
		</tr>
	</table>
		<table cellpadding="2" cellspacing="2" border="0" bordercolor="red"
		background="${ctx}/images/Logistica/back.jpg" class="tabla" style="width:97%;margin-top:10px">
			<tr>
				<td width="15%">Tipo Flujo</td>
				<td>
					<form:select path="codTipo" id="codTipo" cssClass="cajatexto"
					cssStyle="width:280px" onchange="javascript:fc_Buscar();">
					<c:if test="${control.listTipo!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listTipo}" />
					</c:if>
					</form:select>
				</td>
				<td>&nbsp;Sede :</td>
				<td>
				&nbsp;<form:select path="codSede" id="codSede" cssClass="cajatexto"
					cssStyle="width:190px" onchange="javascript:fc_Buscar();">
					<c:if test="${control.listSede!=null}">
					<form:options itemValue="codPadre" itemLabel="descripcion"
					items="${control.listSede}" />
					</c:if>
					</form:select>
					</td>
			</tr>
		</table>
		<br>
		<div id="tabla1" style="overflow: auto; height: 330px;width:100%">
		<table cellpadding="0" cellspacing="0" border="1" bordercolor="#048BBA"  ID="Table1" width="97.5%">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" bordercolor="white" class="tablagrilla" 
					width="100%" ID="Table2">
						<tr>							
							<td class="grilla" style="width:50%;" >Bienes</td>
							<td class="grilla" style="width:50%;">Servicios</td>
						</tr>
						<tr valign="bottom" height="22px">
							<td class="texto_bold">* CONSUMIBLES</td>
							<td class="texto_bold">* GENERALES</td>
						</tr>
						<tr>							
							<td class="texto_bold">&nbsp;&nbsp;- Sin Inversi�n.</td>
							<td>&nbsp;&nbsp;&nbsp;No Requiere</td>
						</tr>
						<tr>
							<td>&nbsp;&nbsp;<input type="checkbox" name="checkbox1"  ID="checkbox1" checked disabled/> Resp. Ce Co. Afectado</td>
						<td class="texto_bold">* MANTENIMIENTO</td>
						</tr>
						<tr>
							<td class="texto_bold">&nbsp;&nbsp;- Con Inversi�n.</td>
							<td>&nbsp;&nbsp;<input type="checkbox" name="checkbox3"  ID="Checkbox3" VALUE="Radio2" checked disabled/>Jefe de Log�stica</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item41" path="item41"  value="1"/>Resp. Ce Co. Afectado</td>
							<td>&nbsp;&nbsp;<input type="checkbox" name="checkbox3"  ID="Checkbox5" VALUE="Radio2" checked disabled/>Resp. Ce Co. Afectado (S�lo si Participa)</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item42" path="item42"  value="1"/>Director Ce Co. Afectado</td>
							<td valign="bottom" class="texto_bold">* OTROS</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item43" path="item43"  value="1"/>Director Administrativo</td>
							<td>&nbsp;&nbsp;<input type="checkbox" name="checkbox2"  ID="Checkbox8" VALUE="Radio2" checked disabled/>Resp. Ce. Co. Afectado</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item44" path="item44"  value="1"/>Director General</td>
						</tr>
						
						<tr valign="bottom" height="22px">
							<td class="texto_bold">* ACTIVOS</td>
							
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item1" path="item1"  value="1"/>Resp. Ce Co. Afectado</td>
						</tr>	
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item2" path="item2"  value="1"/>Director Ce. Co. Afectado</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item3" path="item3"  value="1"/>Director Administrativo</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item4" path="item4"  value="1"/>Director General</td>
						</tr>								
					</table>
				</td>				
			</tr>
		</table>
		</div>
		<!-- ALQD,09/10/08. AGREGANDO DOS ROLES AL FLUJO DE APROBACION. ITEM 85 Y 86 -->
		<!-- ALQD,30/01/09. AGREGANDO UN ROL AL FLUJO DE APROBACION DE OC CONSUMIBLE. ITEM 87 -->
		<div id="tabla2" style="display:none;overflow: auto; height:350px;width:100%">
		<table cellpadding="0" cellspacing="0" border="1" bordercolor="#048BBA"  ID="Table3" width="97.5%">
			<tr>
				<td>
					<table cellpadding="1" cellspacing="0" border="0" bordercolor="white" class="tablagrilla" style="width:100%" ID="Table4">
						<tr>							
							<td class="grilla" style="width:50%;" >Bienes</td>
							<td class="grilla" style="width:50%;">Servicios</td>
						</tr>
						<tr valign="bottom" height="24px">
							<td class="texto_bold">&nbsp;* CONSUMIBLES</td>
							<td class="texto_bold">&nbsp;* GENERALES</td>
						</tr>
						<tr class="texto">		
							<td class="texto_bold">&nbsp;&nbsp;- Sin Inversi�n.</td>			
							<td>&nbsp;&nbsp;<form:checkbox id="item8" path="item8" value="1"/>Comprador</td>
						</tr>
						<tr class="texto">		
							<td>&nbsp;&nbsp;<form:checkbox id="item111" path="item111" value="1"/>Jefe de Log�stica</td>
							<td>&nbsp;&nbsp;<form:checkbox id="item9" path="item9" value="1"/>Director Ce. Co. Afectados</td>
						</tr>
						<tr class="texto">	
							<td>&nbsp;&nbsp;<form:checkbox id="item112" path="item112" value="1"/>Director Administrativo</td>
							<td>&nbsp;&nbsp;<form:checkbox id="item10" path="item10" value="1"/>Director Administrativo</td>
						</tr>
						<tr class="texto">
						<td>&nbsp;&nbsp;<form:checkbox id="item87" path="item87" value="1"/>Director Tipo Gasto Afectado</td>
						<td>&nbsp;&nbsp;<form:checkbox id="item11" path="item11" value="1"/>Director General</td>
						</tr>
						<tr class="texto">	
						<td>&nbsp;&nbsp;<form:checkbox id="item113" path="item113" value="1"/>Director General</td>
						<td>&nbsp;&nbsp;</td>
						</tr>
						<tr class="texto">		
							<td class="texto_bold">&nbsp;&nbsp;- Con Inversi�n.</td>
							<td class="texto_bold">* MANTENIMIENTO</td>
						</tr>
						<tr class="texto">	
							<td>&nbsp;&nbsp;<form:checkbox id="item82" path="item82"  value="1"/>Jefe de Log�stica</td>
							<td>&nbsp;&nbsp;<input type="checkbox" name="checkbox3"  ID="Checkbox11" VALUE="Radio2" checked disabled/>Jefe de Log�stica</td>
						</tr>
						<tr class="texto">	
						<td>&nbsp;&nbsp;<form:checkbox id="item81" path="item81"  value="1"/>Resp. Ce Co. Afectado</td>
						<td>&nbsp;&nbsp;<input type="checkbox" name="checkbox3"  ID="Checkbox13" VALUE="Radio2" checked disabled/>Resps. Ce Co. Afectados(S�lo si participan)</td>
						</tr>
						<tr class="texto">	
						<td>&nbsp;&nbsp;<form:checkbox id="item83" path="item83"  value="1"/>Director Administrativo</td>
						<td valign="bottom" class="texto_bold">* OTROS</td>
						</tr>
						<tr class="texto">	
						<td>&nbsp;&nbsp;<form:checkbox id="item85" path="item85"  value="1"/>Director Ce Co. Afectado</td>
						<td>&nbsp;&nbsp;<form:checkbox id="item12" path="item12" value="1"/>Jefe de Log�stica</td>
						</tr>
						<tr class="texto">	
						<td>&nbsp;&nbsp;<form:checkbox id="item84" path="item84"  value="1"/>Director General</td>
						<td>&nbsp;&nbsp;<form:checkbox id="item13" path="item13" value="1"/>Director Administrativo</td>
						</tr>
						<tr class="texto" valign="bottom">
							<td class="texto_bold">* ACTIVOS</td>
							<td>&nbsp;&nbsp;<form:checkbox id="item14" path="item14" value="1"/>Director General</td>
						</tr>
						<tr class="texto" valign="bottom" height="22px">
							<td>&nbsp;&nbsp;<form:checkbox id="item5" path="item5" value="1"/>Jefe Log�stica</td>							
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item114" path="item114" value="1"/>Resp. Ce. Co. Afectado</td>
						</tr>	
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item6" path="item6" value="1"/>Director Administrativo</td>							
						</tr>
						<tr class="texto" style="cursor:hand">							
						<td>&nbsp;&nbsp;<form:checkbox id="item86" path="item86"  value="1"/>Director Ce Co. Afectado</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item7" path="item7" value="1"/>Director General</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;</td>
						</tr>	
						<tr class="texto" style="cursor:hand">
							<td>&nbsp;</td>
						</tr>								
					</table>
				</td>				
			</tr>
		</table>
		</div>
		<div id="tabla3" style="display:none;overflow: auto; height:350px;width:100%">
		<table cellpadding="0" cellspacing="0" border="1" bordercolor="#048BBA"  ID="Table5" width="97.5%">
			<tr>
				<td>
					<table cellpadding="2" cellspacing="0" border="1" bordercolor="white" class="tablagrilla" width="100%" ID="Table6">
						<tr>							
							<td class="grilla" style="width:50%;" >Bienes</td>
							<td class="grilla" style="width:50%;">Servicios</td>
						</tr>
						<tr valign="bottom" height="24px">
							<td class="texto_bold">* CONSUMIBLES</td>
							<td class="texto_bold">* GENERALES</td>
						</tr>
						<tr class="texto">							
							<td>&nbsp;&nbsp;<form:checkbox id="item121" path="item121" value="1"/>Comprador</td>
							<td>&nbsp;&nbsp;<form:checkbox id="item128" path="item128" value="1"/>Comprador</td>
						</tr>
						<tr class="texto">							
							<td>&nbsp;&nbsp;<form:checkbox id="item122" path="item122" value="1"/>Director Administrativo</td>
							<td>&nbsp;&nbsp;<form:checkbox id="item129" path="item129" value="1"/>Director Ce. Co. Afectados</td>
						</tr>
						<tr class="texto">							
							<td>&nbsp;&nbsp;<form:checkbox id="item123" path="item123" value="1"/>Director General</td>
							<td>&nbsp;&nbsp;<form:checkbox id="item1210" path="item1210" value="1"/>Director Administrativo</td>
						</tr>
						<tr class="texto" valign="bottom">
							<td class="texto_bold">* ACTIVOS</td>
							<td>&nbsp;&nbsp;<form:checkbox id="item1211" path="item1211" value="1"/>Director General</td>
						</tr>
						
						<tr class="texto" valign="bottom" height="22px">
							<td>&nbsp;&nbsp;<form:checkbox id="item124" path="item124" value="1"/>Resps. Ce. Co. Afectados</td>
							<td class="texto_bold">* MANTENIMIENTO</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item125" path="item125" value="1"/>Directores Ce. Co. Afectados</td>
							<td class="texto_bold">&nbsp;&nbsp;- Sin env�o a Resp. Ceco Afectado.</td>
						</tr>	
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item126" path="item126" value="1"/>Director Administrativo</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;<form:checkbox id="item1212" path="item1212" value="1"/>Jefe de Log�stica</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;&nbsp;<form:checkbox id="item127" path="item127" value="1"/>Director General</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;<form:checkbox id="item12113" path="item12113" value="1"/>Directores Ce. Co. Afectados</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td height="24px">&nbsp;</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;<form:checkbox id="item12114" path="item12114" value="1"/>Director Administrativo</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td height="24px">&nbsp;</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;<form:checkbox id="item15" path="item15" value="1"/>Director General</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td height="24px">&nbsp;</td>
							<td class="texto_bold">&nbsp;&nbsp;- Con env�o a Resop. Ceco. Afectado</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td height="24px">&nbsp;</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;<form:checkbox id="item16" path="item16" value="1"/>Resps. Ce. Co. Afectados</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td height="24px">&nbsp;</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;<form:checkbox id="item17" path="item17" value="1"/>Director Ce. Co. Afectados</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td height="24px">&nbsp;</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;<form:checkbox id="item18" path="item18" value="1"/>Director Administrativo</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td height="24px">&nbsp;</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;<form:checkbox id="item19" path="item19" value="1"/>Director General</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td height="24px">&nbsp;</td>
							<td valign="bottom" class="texto_bold">* OTROS</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;</td>
							<td>&nbsp;&nbsp;<form:checkbox id="item20" path="item20" value="1"/>Resps. Ce. Co.Afectados</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;</td>
							<td>&nbsp;&nbsp;<form:checkbox id="item21" path="item21" value="1"/>Directores Ce. Co.Afectados</td>
						</tr>
						<tr class="texto" style="cursor:hand">							
							<td>&nbsp;</td>
							<td>&nbsp;&nbsp;<form:checkbox id="item22" path="item22" value="1"/>Director Administrativo</td>
						</tr>	
						<tr class="texto" style="cursor:hand">
							<td>&nbsp;</td>
							<td>&nbsp;&nbsp;<form:checkbox id="item23" path="item23" value="1"/>Director General</td>
						</tr>								
					</table>
				</td>				
			</tr>
		</table>
		</div>				
		<table border="0" width="100%" style="margin-top:5px">
			<tr>
				<td align="center">		
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img src="${ctx}/images/botones/grabar1.jpg" onclick="javascript:Fc_Grabar();" alt="Grabar" style="cursor:hand" id="icoGrabar"></a><br>&nbsp;		
				</td>
			</tr>
		</table>
	
</form:form>

