<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	var ctaCont = "";	
	function onLoad()
	{
		document.getElementById("txhSecuencial").value = "";
		document.getElementById("txhDesc").value = "";
		document.getElementById("txhOperacion").value = "";
		objMsg = document.getElementById("txhMsg");
	
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrElimino);
			
		}
		else if ( objMsg.value == "ERROR" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrProbEliminar);			
		}
		else if ( objMsg.value == "DOBLE" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrNoEli);			
		}
	}
	function Fc_Agregar(){
		document.getElementById("txhSecuencial").value = "";
		document.getElementById("txhDesc").value = "";
		strTipo = "";
		Fc_Popup("${ctx}/logistica/log_Agregar_TipoGasto.html?txhSecuencial=" + "" + "&txhDesc=" + "" + "&txhTipo=" + strTipo + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhCodigo=" + ""
		,450,145);
	}
	function Fc_Modificar(){
		if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
			strTipo = "MODIFICAR";
			Fc_Popup("${ctx}/logistica/log_Agregar_TipoGasto.html?txhSecuencial=" + document.getElementById("txhSecuencial").value + "&txhDesc=" + document.getElementById("txhDesc").value + "&txhTipo=" + strTipo + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhCodigo=" + document.getElementById("txhCodigo").value + "&txhCtaCont=" + ctaCont
			,450,145);
		}
		else
		{
			alert(mstrSeleccion);
		}
	}
	function fc_seleccionarRegistro(strCod,strDesc,strcodigo,strctaCont){
		document.getElementById("txhSecuencial").value = strCod;
		document.getElementById("txhDesc").value = strDesc;
		document.getElementById("txhCodigo").value = strcodigo;
		ctaCont = strctaCont;
	}
	function Fc_Eliminar(){
		if(fc_Trim(document.getElementById("txhSecuencial").value) !== ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhOperacion").value = "ELIMINAR";
				document.getElementById("frmMain").submit();
			}
			else{
				return false;
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
	</script>
	</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Mantenimiento_Tipo_Gastos.html">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="secuencial" id="txhSecuencial"/>
	<form:hidden path="desc" id="txhDesc"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codigo" id="txhCodigo"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
		<tr>
			<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="695px" height="4px"></td>
		</tr>
		<tr>
			<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
			<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Consulta Tipos de Gasto</font></td>
			<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
		</tr>
		<tr>
			<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="545px"></td>
		</tr>
	</table>
	<table style="width:105%;margin-top:10px" cellSpacing="0" cellPadding="0" border="0" bordercolor="white">
		<tr><td>
			<div style="overflow: auto; height: 418px;width:100%">
				<display:table name="sessionScope.listGastos" cellpadding="0" cellspacing="1"
					decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
					requestURI="" style="border: 1px solid #048BBA;width:98%">
					<display:column property="rbtSelGastos" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
					<display:column property="valor2" title="C�digo" headerClass="grilla" class="tablagrilla" style="align:left;width:20%"/>
					<display:column property="descripcion" title="Tipo Gasto" headerClass="grilla" class="tablagrilla" style="align:left;width:50%"/>
					<display:column property="codPadre" title="Cta. Contable" headerClass="grilla" class="tablagrilla" style="align:right;width:50%"/>
					
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
				</display:table>
				<% request.getSession().removeAttribute("listGastos"); %>
			</div>
		</td>
		<td valign=top>&nbsp;
			<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
			<img src="${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar();" alt="Agregar" style="cursor:hand" id="icoAgregar"></a><br>&nbsp;
			<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoModificar','','${ctx}/images/iconos/modificar2.jpg',1)">
			<img src="${ctx}/images/iconos/modificar1.jpg" onclick="javascript:Fc_Modificar();" alt="Modificar" style="cursor:hand;" id="icoModificar"></a><br>&nbsp;
			<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)">
		    <img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_Eliminar();" alt="Eliminar" style="cursor:hand;" id="icoEliminar"></a>
		</td>
		</tr>
	</table>
</form:form>