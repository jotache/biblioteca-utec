<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>
		function onLoad()
		{
		document.getElementById("texDefecto").value = document.getElementById("txhTextoDefecto").value;
		
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);
		}
		else if(objMsg.value == "DOBLE"){
		alert(mstrOtraCompetencia);
		}
		}
		function Fc_Grabar(){
		
		if(fc_Trim(document.getElementById("texDefecto").value) == ""){
		alert(mstrTextoDefec);
		return false;
		}
		var texto=document.getElementById("texDefecto").value;
		if(texto.length >4000){
		alert(mstrMaximoLetras);
		return false;
		}
		if ( fc_Trim(document.getElementById("txtConComp").value) !== "")
		{
		if(document.getElementById("txtConComp").value !==""){
		document.getElementById("txhTextoDefecto").value = document.getElementById("texDefecto").value;
		document.getElementById("txhOperacion").value="GRABAR";
		document.getElementById("frmMain").submit();
			}
			else{
			alert(mstrValidaCampo+" condicion de compra.");
			}
		 }
		 else{
		 	alert(mstrValidaCampo+"condicion de compra.");
		 }
		}
		function fc_ValidaTextoDefecto() {  
		      //� � � � � � � � � � � � �
	      if((window.event.keyCode != 209) && (window.event.keyCode != 241) && (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && (window.event.keyCode != 218) && (window.event.keyCode != 252)){
		       	var ch_Caracter = String.fromCharCode(window.event.keyCode);
				var intEncontrado = "'".indexOf(ch_Caracter);
				if (intEncontrado == -1)
				{
					window.event.keyCode = ch_Caracter.charCodeAt();
				}
				else
				{
					window.event.keyCode = 0;
				}
			}
		}
		function fc_ValidaTextoDefectoOnBlur(strNameObj,strMensaje){
		//*****************************Onblur********************************************************
			var Obj = document.all[strNameObj];
			var strCadena = new String(strNameObj.value);
			if(strCadena == "")
				return true;
		
			//var valido = "0123456789abcdefghijklmn�opqrstuvwxyz ABCDEFGHIJKLMN�OPQRSTUVWXYZ�����������";
			var valido = "'";
			
			strCadena = strCadena;
			
			for (i = 0 ; i <= strCadena.length - 1; i++)
			{
				if (valido.indexOf (strCadena.substring(i,i+1),0) != -1)
				{
					valido = strCadena.substring(i,i + 1);
					alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' );
					strNameObj.value = "";
					strNameObj.focus();	
					return false;
				}
			}
			return true;
		}
	</script>
	</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Agregar_Cond_Compra.html">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="secuencial" id="txhSecuencial"/>
		<form:hidden path="tipo" id="txhTipo"/>
		<form:hidden path="textoDefecto" id="txhTextoDefecto"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:5px;margin-left:10px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="350px" class="opc_combo"><font style="">Condiciones de Compra</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
			 </tr>
		 </table>
		<table style="width:95%;margin-top:10px;margin-left:10px; height: 150px;" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray'>
			<tr>
				<td valign='top' colspan="2">
					<table class="tablaflotante"  cellSpacing="1" cellPadding="1" border="0" bordercolor='red' height="150px">
						<tr>
							<td nowrap width="30%">Condici�n de Compra :</td>
							<td width="70%">
								<form:input path="conComp" id="txtConComp" 
								cssClass="cajatexto_o" size="61" maxlength="60" />
							</td>
						</tr>
						<tr >
							<td valign="top">Texto Defecto :</td>
							<td style="vertical-align: text-top;">
							<textarea maxlength="4000" lang="4000" id="texDefecto"
								onkeypress="fc_ValidaTextoDefecto();" 
								onblur="fc_ValidaTextoDefectoOnBlur(this,'texto defecto');"
							 class="cajatexto_o"  style="HEIGHT:100px;width:266px" ></textarea>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table height="3px">
			<tr><td></td></tr>
		</table>
		<table align=center>
			<tr>
			<td>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
			<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Grabar();" alt="Grabar" id="btnGrabar"></a>
			</td>
			<td>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
			<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" id="btnCancelar"></a>
			</tr>
		</table>
	</form:form>
