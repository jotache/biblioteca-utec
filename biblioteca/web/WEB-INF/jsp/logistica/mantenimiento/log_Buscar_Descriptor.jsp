<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>
		var tot = "";	
		function onLoad()
		{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{	
			objMsg.value = "";
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);
		}
		else if( objMsg.value == "DOBLE" )
		{
		objMsg.value = "";
		alert(mstrUsado);
		}
			}	
		function Fc_Busca(){
			document.getElementById("txhOperacion").value = "DETALLE";
			document.getElementById("frmMain").submit();
		}	
		function Fc_Agregar(){
		if(fc_Trim(document.getElementById("txhCodDescriptor").value) !== "" ){
			document.getElementById("txhOperacion").value = "GRABAR";
			document.getElementById("frmMain").submit();
			}
			else{
		alert(mstrSeleccion);
	}
		}	
		function fc_seleccionarRegistro(strCod){
		
		strCodSel = document.getElementById("txhCodDescriptor").value;
				flag=false;
		if (strCodSel!='')
		{	
			ArrCodSel = strCodSel.split("|");
			strCodSel = "";
			for (i=0;i<=ArrCodSel.length-2;i++)
			{
				if (ArrCodSel[i] == strCod){ 
				flag = true; 
				}
				else{
					strCodSel = strCodSel + ArrCodSel[i]+'|';
				}
			}
		}
		if (!flag)
		{   	
			strCodSel = strCodSel + strCod + '|';
		}
		document.getElementById("txhCodDescriptor").value = strCodSel;
		
		fc_GetNumeroSeleccionados();
		}
		function fc_GetNumeroSeleccionados()
		{
		strCodSel = document.getElementById("txhCodProducto").value;
		if ( strCodSel != '' )
		{
			ArrCodSel = strCodSel.split("|");
			tot = ArrCodSel.length - 1;
		}
		else{ 
		tot = 0;
	
		}
	}
</script>
</head>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Buscar_Descriptor.html">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codProducto" id="txhCodProducto"/> 
	<form:hidden path="codDescriptor" id="txhCodDescriptor"/>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:3px;">
		<tr>
			<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
			<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="290px" class="opc_combo"><font style="">Agregar Descriptores</font></td>
			<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
		</tr>
	</table>
	<table class="tabla" style="width:98%;margin-top:4px" cellSpacing="2" cellPadding="2" border="0" bordercolor="white" 
		id="Table2" background="${ctx}/images/Logistica/back.jpg" align="center">		
		<tr>
			<td nowrap="nowrap">&nbsp;Descriptor :</td>
			<td>
				<form:input path="codDes" id="txtCodDes" 
				onkeypress="fc_ValidaTextoNumeroEspecial();" 
				onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'descriptor');"
				cssClass="cajatexto_o" size="63" maxlength="60"/>
			</td>
			<td>&nbsp;
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
				<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:Fc_Busca()" alt="Buscar" style="cursor:hand;"id="icoBuscar"></a>&nbsp;
			</td>
		</tr>
	</table>	
	<table style="width:98%;margin-top:3px" class="tabla" cellspacing="0" cellpadding="0" border="0" align="center">		
		<tr>
			<td>
				<div style="overflow: auto; height:280px;width:100%">									
					<display:table name="sessionScope.listDes" cellpadding="0" cellspacing="1"
					decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
					requestURI="" style="border: 1px solid #048BBA;width:96%">
					<display:column property="rbtSelBuscarDescriptor" title="Sel." headerClass="grilla" class="tablagrilla" style="width:8%;text-align:center"/>
					<display:column property="descripcion" title="Descriptores" headerClass="grilla" class="tablagrilla" style="align:left;"/>
					
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					</display:table>
				<% request.getSession().removeAttribute("listDes"); %>
				</div>
			</td>
		</tr>
	</table>
	<table align="center" style="margin-top:1px">
		<tr>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnAceptar','','${ctx}/images/botones/aceptar2.jpg',1)">
				<img src="${ctx}/images/botones/aceptar1.jpg" onclick="javascript:Fc_Agregar()" style='cursor:hand;' alt="Aceptar" id="btnAceptar"></a>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
				<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" id="btnCancelar"></a>
			</td>
		</tr>
	</table>
</form:form>
</html>