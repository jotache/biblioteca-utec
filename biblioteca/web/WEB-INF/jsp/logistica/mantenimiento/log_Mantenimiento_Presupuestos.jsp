<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>	
		
		function onLoad(){
			var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
			fc_msgManager(mensaje);				
		}

		function fc_msgManager(mensaje){	
			if(mensaje!="")
			{
				if(mensaje.length>1)           		
				alert(mensaje);				    
			}
		}
		
		function fc_muestra(){
			document.getElementById("txhOperacion").value = "LISTAR";
			document.getElementById("frmMain").submit();	
		}

		function fc_Grabar(){
			if(confirm('�Seguro de Grabar los Presupuestos?')){
				form = document.forms[0];
				var max = form.txhTamListPresup.value;
				if(max>0){
					var datos = "";
					var flgnp = "";
					var id = ""
					var codCencos="";
					var importe="";
					for(i=0;i<max;i++){
						objCodigo 	= document.getElementById("hidCodCencos"+i);
						objId = document.getElementById("hidId"+i);
						objImporte = document.getElementById("hidId"+i)
						id = objId.value;
						codCencos = objCodigo.value;						
						importe = document.getElementById('txtImporte'+i).value;						
						if(document.getElementById('txtCtrl'+i).checked == true)
							flgnp = "1";
						else
							flgnp = "0";
						
						datos = datos + codCencos + '$' + id + '$' + importe + '$' + flgnp + '|';
					}
				}
				//alert(datos);
				document.getElementById("cadenaDatos").value = datos;
				document.getElementById("txhOperacion").value = "GRABAR";
				document.getElementById("frmMain").submit();
			}

		}
		
		function fc_permiteNumerosValEnter(posObjEvt){
			if(window.event.keyCode ==13 ){
				var nomactual = "txtImporte" + String(posObjEvt - 1);			
				var nom = "txtImporte"+posObjEvt;
							
				if(Number(posObjEvt)<Number(document.getElementById("txhTamListPresup").value))
					document.getElementById(""+nom).focus();			
				return true;
			}	
			fc_PermiteNumerosPunto();
		}
		
		function fc_verificaImporteTxt(sNomObj){
			Obj = document.getElementById(""+sNomObj);	
			fc_ValidaObjDecimalOnBlur(Obj,'13','2');
		}	
		
		function fc_Ctrl(fil){
			//if(document.getElementById("txtCtrl" + fil).checked == true){
				//document.getElementById("txtImporte" + fil).value = "0";
				//document.getElementById("txtImporte" + fil).disabled = true;
			//}else{
				//document.getElementById("txtImporte" + fil).value = "";
				//document.getElementById("txtImporte" + fil).disabled = false;
				//document.getElementById("txtImporte" + fil).focus();			
			//}
		}	
			
	</script>
	</head>
	<form:form action="${ctx}/logistica/log_Mantenimiento_Presupuestos.html" commandName="control" id="frmMain">
		 
		<form:hidden path="operacion" id="txhOperacion"/>
		<%--
		<form:hidden path="secuencial" id="txhSecuencial"/>
		<form:hidden path="desc" id="txhDesc"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="codPadre" id="txhCodPadre"/>
		<form:hidden path="dscPadre" id="txhDscPadre"/>
		 --%>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<form:hidden path="codSede" id="codSede"/>
		<form:hidden path="tamListPresup" id="txhTamListPresup"/>
		<form:hidden path="cadenaDatos" id="cadenaDatos"/>
		
		<table cellpadding="0" cellspacing="0" border="0" >
			<tr>
				<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="700px" height="4px"></td>
			</tr>
			<tr>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
				<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px">
					<font>Mantenimiento de Presupuestos por Centro de Costo&nbsp;
						<c:choose>
							<c:when test="${control.codSede=='L'}">(Lima)</c:when>
							<c:when test="${control.codSede=='T'}">(Trujillo)</c:when>
							<c:when test="${control.codSede=='A'}">(Arequipa)</c:when>
							<c:otherwise>(Desconocido)</c:otherwise>
						</c:choose>	
					</font>
				</td>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
			</tr>
			<tr>
				<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="555px"></td>
			</tr>
		</table>
		<table cellpadding="1" cellspacing="0"  ID="Table1" style="width:94%;margin-top:10px" border="0"
			bordercolor="Red" class="tabla" background="${ctx}/images/Logistica/back.jpg">
			<tr height="5px">
				<td></td>
				<td></td>
			</tr>
			<tr>	
				<td width="5%">&nbsp;A�o:</td>
				<td width="95%">
					<form:select path="codAnioSelec" id="codAnioSelec" cssClass="cajatexto" cssStyle="width:100px" onchange="javascript:fc_muestra();">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaAniosCencos!=null}">
							<form:options itemValue="id" itemLabel="name" items="${control.listaAniosCencos}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr height="5px">
				<td></td>
				<td></td>
			</tr>
		</table>
		<br>
			
		<table style="width:98%" cellSpacing="0" cellPadding="0" border="0" > 
			<tr>
				<td width="98%">				
					<div style="overflow: auto; height: 320px;width:98%">
						<table style="WIDTH: 97%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" 
							cellSpacing="1" cellPadding="0" border="0" id="tbl_bandeja01">
							<tr height="20px">
								<td class="grilla" width="40%" align="center">&nbsp;Centro de Costo</td>
								<td class="grilla" width="10%" align="center">&nbsp;C�digo</td>
								<td class="grilla" width="30%">&nbsp;Responsable</td>
								<td class="grilla" width="20%" align="center">&nbsp;&nbsp;Importe | Control</td>
							</tr>
							<tr>
								<td width="100%" colspan="4">
									<table class="" width="100%" cellSpacing="1" cellPadding="0" align="center" border="0">
										<c:forEach varStatus="loop" var="lista" items="${control.listaPresupuestos}"  >
											<tr class="tablagrilla">
												<td style="text-align: left" width="40%" >

													<input type="hidden" value='<c:out value="${lista.id}" />' id='hidId<c:out value="${loop.index}"/>'>
													<input type="hidden" value='<c:out value="${lista.codCencos}" />' id='hidCodCencos<c:out value="${loop.index}"/>'>

													<c:out value="${lista.descripcion}" />		
												</td>
												<td style="text-align: center" width="10%" >
													<c:out value="${lista.codCencos}" />							
												</td>
												<td style="text-align: left" width="30%" >
													<c:out value="${lista.responsableCencos}" />							
												</td>
												<td style="text-align: center" width="30%" >
													<input id="txtImporte<c:out value="${loop.index}" />" name="txtImporte<c:out value="${loop.index}" />" class="cajatexto" value="<c:out value="${lista.importe}"  />"   
														   style="width: 70px;text-align: center" onkeypress="fc_permiteNumerosValEnter('<c:out value="${loop.index+1}" />')" maxlength="17" onblur="fc_verificaImporteTxt('txtImporte<c:out value="${loop.index}" />')" >
																	
														&nbsp;&nbsp;
													<input type="checkbox" onclick="javascript:fc_Ctrl('<c:out value="${loop.index}" />')" id="txtCtrl<c:out value="${loop.index}" />"
														<c:if test="${lista.flgControl=='1'}"> checked </c:if> 
													/>&nbsp;
												</td>
											</tr>
										</c:forEach>
									</table>
								</td>
							</tr>
						</table>					
					</div>
				</td>				
			</tr>
		</table>
		<table align="center" cellpadding="0" cellspacing="0" border="0" align="center" height="30px" width="98%" style="margin-top:6px">	
			<tr>
				<td align="center">																				
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" style="cursor:pointer;" id="imgregistrar1" onclick="fc_Grabar();"></a>											
				</td>
			</tr>
		</table>
		</form:form>
