<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>
	function onLoad()
	{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").vlaue = "";
			alert(mstrSeGraboConExito);
			window.opener.document.getElementById("frmMain").submit();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		document.getElementById("txhMsg").vlaue = "";
		alert(mstrProblemaGrabar);
		}
	}
	
	function Fc_Limpia(){
		document.getElementById("codBien").value = "";
		document.getElementById("txtCodFamilia").value = "";
		document.getElementById("txtCodSubFamilia").value = "";
		document.getElementById("txtFamilia").value = "";
		document.getElementById("txtSubFamilia").value = "";
	}
	
	function Fc_Busca(){
		document.getElementById("txhOperacion").value="BUSCAR";
		document.getElementById("frmMain").submit();
	}
	function Fc_Graba(){
		if ( fc_Trim(document.getElementById("txhCodFamiliaSubFamilia").value) !== "")
		{
			if (confirm(mstrSeguroGrabar)){
				document.getElementById("txhOperacion").value="GRABAR";
				document.getElementById("frmMain").submit();
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
	function fc_seleccionarRegistro(strCod){
		strCodSel = document.getElementById("txhCodFamiliaSubFamilia").value;
				flag=false;
		if (strCodSel!='')
		{	
			ArrCodSel = strCodSel.split("|");
			strCodSel = "";
			for (i=0;i<=ArrCodSel.length-2;i++)
			{
				if (ArrCodSel[i] == strCod){ 
				flag = true; 
				}
				else{
					strCodSel = strCodSel + ArrCodSel[i]+'|';
				}
			}
		}
		if (!flag)
		{
			strCodSel = strCodSel + strCod + '|';
		}
		document.getElementById("txhCodFamiliaSubFamilia").value = strCodSel;
		
		fc_GetNumeroSeleccionados();
	}
	function fc_GetNumeroSeleccionados()
	{
		strCodSel = document.getElementById("txhCodFamiliaSubFamilia").value;
		if ( strCodSel != '' )
		{
			ArrCodSel = strCodSel.split("|");
			tot = ArrCodSel.length - 1;
		}
		else{
		tot = 0;
	
		}
	}
</script>
	</head>
	<form:form action="${ctx}/logistica/log_Agregar_Familia_SubFamilia.html" commandName="control" id="frmMain">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>  
	<form:hidden path="codProveedor" id="txhCodProveedor"/>
	<form:hidden path="codFamiliaSubFamilia" id="txhCodFamiliaSubFamilia"/>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left: 5px;margin-top: 4px;">
		<tr>
			<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="695px" height="4px"></td>
		</tr>
		<tr>
			<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
			<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Agregar Familia SubFamilia</font></td>
			<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
		</tr>
		<tr>
			<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="545px"></td>
		</tr>
	</table>
	
	<table cellpadding="2" cellspacing="2" class="tablaflotante" style="width:99%;margin-left: 6px;margin-top: 4px;" border="0">
		<tr>
			<td>Tipo Bien :</td>
			<td><form:select path="codBien" id="codBien" cssClass="cajatexto"
				cssStyle="width:180px" >
				<form:option value="">--Seleccione--</form:option>
				<c:if test="${control.listBien!=null}">
				<form:options itemValue="codSecuencial" itemLabel="descripcion"
				items="${control.listBien}" />
				</c:if>
				</form:select>
			</td>
			<td nowrap>Cod. Familia :</td>
			<td><form:input path="codFamilia" id="txtCodFamilia" 
						onkeypress="fc_ValidaNumerico2();"
						onblur="fc_ValidaNumeroOnBlur('txtCodFamilia');"
						cssClass="cajatexto" maxlength="10" size="10" /></td>
			<td nowrap>Cod. SubFamilia :
			<form:input path="codSubFamilia" id="txtCodSubFamilia" 
						onkeypress="fc_ValidaNumerico2();"
						onblur="fc_ValidaNumeroOnBlur('txtCodSubFamilia');"
						cssClass="cajatexto" maxlength="10" size="10" />
			</td>
			<td></td>
		</tr>
		<tr>
			<td>Familia :</td>
			<td>
				<form:input path="familia" id="txtFamilia" 
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaTextoEspOnBlur('txtFamilia','familia');"
					cssClass="cajatexto" maxlength="100" size="50" />
			</td>
			<td>SubFamilia</td>
			<td >
				<form:input path="subFamilia" id="txtSubFamilia" 
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaTextoEspOnBlur('txtSubFamilia','subFamilia');"
					cssClass="cajatexto" maxlength="100" size="50" />
			</td>
			<td align="right">
			<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
			<img src="${ctx}/images/iconos/limpiar1.jpg" onclick="javascript:Fc_Limpia();" alt="Limpiar" style="cursor:hand" id="icoLimpiar"></a>&nbsp;
			<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
			<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:Fc_Busca();" alt="Buscar" style="cursor:hand" id="icoBuscar"></a>&nbsp;
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		style="width:98%;margin-top:6px;margin-left:6px;" >
		<tr>
			<td>
			<div style="overflow: auto; height: 190px;width:100%">							
				<display:table name="sessionScope.ListFamSubFam" cellpadding="0" cellspacing="1"
					decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
					requestURI="" style="border: 1px solid #048BBA;width:98%">
					
					<display:column property="rbtSelSubFamilia" title="Sel" headerClass="grilla" class="tablagrilla" style="text-align:center;width:5%"/>
					<display:column property="desFam" title="Familia" headerClass="grilla" class="tablagrilla" style="align:left;width:100px"/>
					<display:column property="desSubFam" title="SubFamilia" headerClass="grilla" class="tablagrilla" style="align:left;width:300px"/>
					
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
				</display:table>
			</div>
			</td>
		</tr>
	</table>
	<table align="center" style="margin-top: 10px;">
		<tr>
			<td align="center">
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Graba();" alt="Grabar" id="btnGrabar"></a>
				&nbsp;
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" id="btnCancelar"></a>
			</td>
		</tr>
	</table>
</form:form>
