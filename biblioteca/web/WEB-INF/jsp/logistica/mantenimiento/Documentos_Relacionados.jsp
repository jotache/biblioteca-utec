<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

	<script language=javascript>
	function fc_ValidaTextoEspecialEncuestas() {		       
	       if((window.event.keyCode != 209) && (window.event.keyCode != 241) && (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && (window.event.keyCode != 218) && (window.event.keyCode != 252)){            
	         	var ch_Caracter = String.fromCharCode(window.event.keyCode);
	         	
	 			var intEncontrado = " 0123456789abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ.!#%&/()=?�+-<>{}[]@:;*'�������������,".indexOf(ch_Caracter);            
	 			if (intEncontrado == -1)	 			          
	 				window.event.keyCode = 0;          
	 			else
	 				window.event.keyCode = ch_Caracter.charCodeAt();
	 		}
	 }
	 
	 	 
	 function fc_ValidaTextoEspecialEncuestasOnblur(strNameObj){	 
	 	var Obj = document.all[strNameObj];	 		
	 	var strCadena = Obj.value;
	 	if(strCadena == "")
	 		return true;
	
	 	var valido = " 0123456789abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ.!#%&/()=?�+-<>{}[]@:;*'�������������,";	 			
	 	strCadena = strCadena;	 	
	 	for (i = 0 ; i <= strCadena.length - 1; i++)
	 	{	
	 		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1) {	 		
	 			valido = strCadena.substring(i,i + 1);
	 			alert ('El nombre del producto contiene caracteres no permitidos.' );
	 			Obj.value = "";
	 			Obj.focus();	
	 			return false;
	 		} 
	 	}	
	 	return true;
	 }		
		function onLoad()
		{
			objMsg = document.getElementById("txhMsg");
			if ( objMsg.value == "OK" )
			{
				objMsg.value = "";
				alert(mstrSeGraboConExito);
				document.getElementById("txhMsg").value = "";
			}
			else if( objMsg.value == "ERROR" )
			{
			objMsg.value = "";
			alert(mstrProblemaGrabar);
			document.getElementById("txhMsg").value = "";
			}
			objMsgE = document.getElementById("txhMsgE");
		
			if ( objMsgE.value == "OK" )
			{
				objMsgE.value = "";
				 document.getElementById("txhMsgE").value = "";
				alert(mstrElimino);
			}		
			else
			if ( objMsgE.value == "ERROR" ){
				objMsgE.value = "";
				document.getElementById("txhMsgE").value = "";
				alert(mstrProbEliminar);
			}
			document.getElementById("txhOperacion").value="";
			
			fc_DesabilitarBotones();
		}
		function fc_DesabilitarBotones(){
			if(document.getElementById("txhCodEstadoOrden").value == "1"){
				document.getElementById("botonGrabar").disabled = true;
				document.getElementById("botonLimpiar").disabled = true;
				document.getElementById("botonEliminar").disabled = true;
			}
		}
		//ALQD,12/06/09.VALIDANDO LA MONEDA
		function Fc_Grabar(){
		if ( fc_Trim(document.getElementById("txhFechaIni").value) !== ""
			&& fc_Trim(document.getElementById("txtMonto").value) !== ""
			&& fc_Trim(document.getElementById("txtNumero").value) !== ""
			&& fc_Trim(document.getElementById("cboMoneda").value) != "")
			{
			if (confirm(mstrSeguroGrabar)){
				document.getElementById("txhDescripcion").value = document.getElementById("descrip").value;
				document.getElementById("txhOperacion").value="GRABAR";
				document.getElementById("frmMain").submit();
				Fc_Limpia();
			}
		}
			else{
			alert(mstrLleneLosCampos);
			}
		}
		
		function Fc_Eliminar(){
		if(fc_Trim(document.getElementById("txhCodDoc").value) != ""){
		if( confirm(mstrEliminar) ){
			document.getElementById("txhOperacion").value="ELIMINAR";
			document.getElementById("frmMain").submit();
			Fc_Limpia();
			}
			else{
			return false;
			}
		}
		else{
			alert(mstrSeleccion);
		}
		}
		//ALQD,01/07/09.AUMENTADO DOS PARAMETROS MAS
		//ALQD,21/07/09.AUMENTADO TRES PARAMETROS MAS
		function fc_Muestra(strFecha,strcodId,strMonto,strNumero,strDesc
		,strCodMoneda,strImpTC){
		
		document.getElementById("txhFechaIni").value = strFecha;
		document.getElementById("txhCodDoc").value = strcodId;
		document.getElementById("txtMonto").value = strMonto;
		document.getElementById("txtNumero").value = strNumero;
		document.getElementById("descrip").value = strDesc;
		document.getElementById("cboMoneda").value = strCodMoneda;
		document.getElementById("txtImpTC").value = strImpTC;
		
		}
		function fc_SelDocumento(strCod){
		document.getElementById("txhCodDoc").value = strCod;
		}
		//ALQD,01/07/09.LIMPIANDO DOS CONTROLES MAS
		function Fc_Limpia(){
		document.getElementById("txhFechaIni").value = "";
		document.getElementById("txhCodDoc").value = "";
		document.getElementById("txtMonto").value = "";
		document.getElementById("txtNumero").value = "";
		document.getElementById("descrip").value = "";
		document.getElementById("cboMoneda").value = "";
		document.getElementById("txtImpTC").value = "";
		document.getElementById("txhCodProvee").value = "";
		document.getElementById("txhNomProvee").value = "";
		document.getElementById("txhRucProvee").value = "";
		}
</script>
		<form:form id="frmMain" commandName="control" action="${ctx}/logistica/Documentos_Relacionados.html">
		<form:hidden path="codDoc" id="txhCodDoc"/>
		<form:hidden path="codDocPago" id="txhCodDocPago"/>
		<form:hidden path="codUsuario" id="txhCodUsuario"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="msgE" id="txhMsgE"/>
		<form:hidden path="descripcion" id="txhDescripcion"/>
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="codEstadoOrden" id="txhCodEstadoOrden"/>
		<form:hidden path="codProvee" id="txhCodProvee"/>
		<form:hidden path="nomProvee" id="txhNomProvee"/>
		<form:hidden path="rucProvee" id="txhRucProvee"/>
		
		<!-- ALQD,12/06/09.SE A�ADE COMBO DE MONEDA -->
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo">
			 	<font>
			 		Documentos Relacionados
				</font>
			 	</td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
			 </tr>
		 </table>
		 <table cellpadding="1" cellspacing="0" ID="Table1" style="width:98%;margin-top:10px" border="1" bordercolor="Red" 
			class="tabla" background="${ctx}/images/Logistica/back.jpg" height="50px" align="center">
			<tr height="5px">
			</tr>
			<tr>
				<td width="10%">&nbsp;N�mero :</td>
				<td width="20%" align="left">
					<form:input path="numero" id="txtNumero"
					cssClass="cajatexto" size="20" maxlength="20"/></td>
<!-- 						onkeypress="fc_ValidaNroOrden();"
						onblur="fc_ValidaNroOrdenOnblur(this, 'n�mero.');"
 -->					
				<td width="24%">&nbsp;Fecha :&nbsp;&nbsp;
				<form:input path="fecha" id="txhFechaIni"
					onkeypress="fc_ValidaFecha('txhFechaIni');"
					onblur="fc_FechaOnblur('txhFechaIni');" 
					cssClass="cajatexto" maxlength="10" size="10" />
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecIni" alt="Calendario" style='cursor:hand;' align="absmiddle"></a>
 				</td>	
				
				<td width="8%">&nbsp;Monto :</td>
				<td width="27%" align="left" >
					<form:select path="codTipMoneda" id="cboMoneda" cssClass="combo_o" cssStyle="width:80px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listMonedas!=null}">
						<form:options itemValue="codSecuencial" itemLabel="descripcion"
							items="${control.listMonedas}" />
						</c:if>
					</form:select>
					<form:input path="monto" id="txtMonto"
						onkeypress="fc_ValidaNumeroGuion();"
						onblur="fc_ValidaNumeroGuionOnBlur(this.id,'Monto');"
					cssClass="cajatexto" cssStyle="text-align: right;" size="15" maxlength="12" /></td>
				<td>&nbsp;T/C :&nbsp;&nbsp;
					<form:input path="impTipCamSoles" id="txtImpTC"
						onkeypress="fc_PermiteNumerosPunto();"
						onblur="fc_ValidaDecimalOnBlur(this.id,'7','3');"
					cssClass="cajatexto" cssStyle="text-align: right;" size="10" maxlength="10" />
				</td>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/iconos/grabar2.jpg',1)" id="botonGrabar">
						<img src="${ctx}/images/iconos/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Grabar();" alt="Grabar" id="btnGrabar">
					</a>
				</td>
			</tr>
			<tr>
				<td width="10%">&nbsp;Proveedor :</td>
				<td width="10%">&nbsp;RUC :</td>
				<td width="10%" colspan="3">&nbsp;Raz�n social:</td>
				<td width="10%">&nbsp;</td>
			</tr>
			<tr height="5px">
				<td width="10%">&nbsp;Descripci�n :</td>
				<td align="left" colspan="5">
				<textarea maxlength="300" lang="300" id="descrip"
				onkeypress="fc_ValidaTextoEspecialEncuestas();"
				onblur="fc_ValidaTextoEspecialEncuestasOnblur("descrip");"
				 class="texto"  style="HEIGHT:55px;width:600px" ></textarea>
				</td>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)" id="botonLimpiar">
						<img src="${ctx}/images/iconos/limpiar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Limpia();" alt="Limpiar" id="btnLimpiar">
					</a>
				</td>
			</tr>
			</table>
			
			<br>
		 <table cellpadding="0" cellspacing="0" ID="Table1" style="width:98%;margin-top:10px" border="0" bordercolor="Red"
			 height="50px" align="center">
			<tr><td>
				<div style="overflow: auto; height: 285px;width:100%">
						<display:table name="sessionScope.listDocumentosRelacionados" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.LogisticaDecorator"
							requestURI="" style="border: 1px solid #048BBA;width:97.5%">
							<display:column property="rbtSelDocumentoRelacionado" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
							<display:column property="numeroSelecciona" title="N�mero" headerClass="grilla" class="tablagrilla" style="align:left;width:200px;text-align:right"/>
							<display:column property="fechaSelecciona" title="Fecha" headerClass="grilla" class="tablagrilla" style="align:left;width:150px;text-align:right"/>
							<display:column property="monedaSelecciona" title="Moneda" headerClass="grilla" class="tablagrilla" style="align:left;width:150px;text-align:right"/>
							<display:column property="montoSelecciona" title="Monto" headerClass="grilla" class="tablagrilla" style="align:right;width:300px;text-align:right"/>
							<display:column property="tcSelecciona" title="T/C" headerClass="grilla" class="tablagrilla" style="align:right;width:300px;text-align:right"/>
							<display:column property="descripcionSelecciona" title="Descripci�n" headerClass="grilla" class="tablagrilla" style="align:left;width:600px;text-align:left"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>" />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
						</display:table>
						<% request.getSession().removeAttribute("listDocumentosRelacionados"); %>
					</div>
				<td valign=top>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)" id="botonEliminar">
						<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_Eliminar();" alt="Eliminar" style="cursor:hand;" id="icoEliminar"></a>
				</td>
		</table>
			
		<table align=center>
			<tr>
				<td>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
			<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" id="btnCancelar"></a>
			</tr>
		</table>
		 
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txhFechaIni",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	</script>