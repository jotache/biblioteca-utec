<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>	
		function onLoad()
		{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{	
			objMsg.value = "";
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);
		}
		else if(objMsg.value == "DOBLE"){
		alert(mstrOtraCompetencia);
		}
		}
		function Fc_Graba(){
		if ( fc_Trim(document.getElementById("txtUbicacion").value) !== "")
			{
		document.getElementById("txhOperacion").value="GRABAR";
		document.getElementById("frmMain").submit();
			}
			else{
			alert(mstrValidaCampo+"ubicaci�n f�sica.");
			}
		}
		
	</script>
	</head>
	<form:form action="${ctx}/logistica/log_Agregar_Ubicacion_Fisica.html" commandName="control" id="frmMain">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<form:hidden path="tipo" id="txhTipo"/>
		<form:hidden path="codSede" id="txhCodSede"/>
		<form:hidden path="secuencial" id="txhSecuencial"/>
		 
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:5px;margin-left:10px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="320px" class="opc_combo"><font style="">Mantenimiento de Ubicaciones F�sicas</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
			 </tr>
		 </table>
		<table class="tablaflotante" style="width:95%;margin-top:10px;margin-left:10px" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray'> 
			<tr>
				<td valign='top' colspan="2">
					<table class=""  cellSpacing="1" cellPadding="1" border="0" bordercolor='red' height="45px"> 
						<tr>
							<td nowrap>Ubicaci�n F�sica :&nbsp;			
								<form:input path="ubicacion" id="txtUbicacion" 
								onkeypress="fc_ValidaTextoNumeroEspecial();"
								onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'ubicaci�n f�sica');"
								cssClass="cajatexto_o" size="60" maxlength="60"/>
							</td>
						</tr>					
					</table>
				</td>
			</tr>
		</table>	
		<table align=center style="margin-top:7px">
			<tr>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Graba();" alt="Grabar" id="btnGrabar"></a>
				</td>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
					<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="window.close();" alt="cancelar" id="btnCancelar"></a>
				</td>
			</tr>
		</table>	
	</form:form>

