<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>
	function onLoad()
	{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			alert(mstrSeGraboConExito);
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);
		}
		else if(objMsg.value == "DOBLE"){
		alert(mstrOtraCompetencia);
		}
	}
	function Fc_Grabar(){
		if ( fc_Trim(document.getElementById("txtFamilia").value) !== "" && fc_Trim(document.getElementById("codTipo").value) !== "" && fc_Trim(document.getElementById("txtCta").value) !== "")
		{
			document.getElementById("txhOperacion").value="GRABAR";
			document.getElementById("frmMain").submit();
		}
		else{
			alert(mstrLleneLosCampos);
		}
	}
		
	</script>
	</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Agregar_Familia.html">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="tipo" id="txhTipo"/>
		<form:hidden path="secuencial" id="txhSecuencial"/>
		<form:hidden path="codigoUni" id="txhCodigoUni"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:5px;margin-left:10px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="370px" class="opc_combo"><font style="">Mantenimiento de Familias</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
			 </tr>
		 </table>
		<table class="tablaflotante" style="width:96%;margin-top:10px;margin-left:10px" cellSpacing="0" cellPadding="1" border="0" bordercolor='white'> 
			<tr>
				<td valign='top' colspan="2">
					<table class="" background="${ctx}/images/logistica/back.jpg" height="40px" cellSpacing="1" cellPadding="1" border="0" bordercolor='red' height="60px"> 
						<tr>
							<td nowrap>C�digo :</td>
							<td>
								<form:input path="codigo" id="txtCodigo" 
							 	readonly="true"
								cssClass="cajatexto_o" size="15" />
							</td>
						</tr>
						<tr>
							<td nowrap>Familia :</td>
							<td>
								<form:input path="familia" id="txtFamilia" 
								onkeypress="fc_ValidaTextoNumeroEspecial();" 
								onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'familia');"
								cssClass="cajatexto_o" size="63" maxlength="60"/>
							</td>
						</tr>
						 <tr>
							<td nowrap>Tipo Bien :</td>
							<td>
								<form:select path="codTipo" id="codTipo" cssClass="cajatexto_o"
								cssStyle="width:280px">
								<form:option value="">--Seleccione--</form:option>
								<c:if test="${control.tipoBien!=null}">
								<form:options itemValue="codSecuencial" itemLabel="descripcion"
								items="${control.tipoBien}" />
								</c:if>
								</form:select>
							</td>
						</tr>
						<tr>
							<td nowrap>Cta.Cble :</td>
							<td>
								<form:input path="cta" id="txtCta" 
								onkeypress="fc_ValidaNumerico2();"
								onblur="fc_ValidaNumeroOnBlur('txtCta');"
								cssClass="cajatexto_o" size="15" maxlength="10" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br>
		<table align="center">
			<tr>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
						<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Grabar();" alt="Grabar" id="btnGrabar">
					</a>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
						<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" alt="cancelar" id="btnCancelar">
					</a>
				</td>
			</tr>
		</table>
	</form:form>
</html>
