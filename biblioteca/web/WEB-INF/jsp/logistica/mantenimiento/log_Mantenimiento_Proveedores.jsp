<%@ include file="/taglibs.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script type="text/javascript">
	
	function onLoad()
	{
		document.getElementById("txhCodUnico").value = "";
		document.getElementById("txhOperacion").value= "";
		objMsg = document.getElementById("txhMsg");
	
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			alert(mstrElimino);
		}
		else if ( objMsg.value == "ERROR" )
		{
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			alert(mstrProbEliminar);
		}
		else if ( objMsg.value == "DOBLE" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrNoEli);
		}
	}
	function Fc_Agregar()
	{
	document.getElementById("txhCodUnico").value = "";
		strTipo = "";
		location.href=("${ctx}/logistica/log_Agregar_Proveedores.html?txhCodAlumno=" + document.getElementById("txhCodAlumno").value+ "&txhCodUnico=" + document.getElementById("txhCodUnico").value + "&txhTipo=" + strTipo);
	}
	function Fc_Modificar(){
		if(fc_Trim(document.getElementById("txhCodUnico").value) !== ""){
			strTipo = "MODIFICAR";
			location.href=("${ctx}/logistica/log_Agregar_Proveedores.html?txhCodAlumno=" + document.getElementById("txhCodAlumno").value + "&txhCodUnico=" + document.getElementById("txhCodUnico").value + "&txhTipo=" + strTipo);
		}
		else{
			alert(mstrSeleccion);
		}
	}
	function Fc_Eliminar(){
		if(fc_Trim(document.getElementById("txhCodUnico").value) !== ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhOperacion").value="ELIMINAR";
				document.getElementById("frmMain").submit();
			}
			else{
				return false;
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
	function Fc_Limpia(){
		document.getElementById("txtRuc").value = "";
		document.getElementById("codCalif").value = "-1";
		document.getElementById("txtRazonSocial").value = "";
		document.getElementById("codEstado").value = "0001";
	}
	
	function Fc_Busca(){
		document.getElementById("txhOperacion").value="BUSCAR";
		document.getElementById("frmMain").submit();
	}
	function fc_seleccionarRegistro(strCodUni){
		document.getElementById("txhCodUnico").value = strCodUni;
	}
	</script>
	<form:form action="${ctx}/logistica/log_Mantenimiento_Proveedores.html" commandName="control" id="frmMain">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<form:hidden path="codUnico" id="txhCodUnico"/>
		<form:hidden path="msg" id="txhMsg"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
			<tr>
				<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="700px" height="4px"></td>
			</tr>
			<tr>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
				<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Consulta de Proveedores</font></td>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
			</tr>
			<tr>
				<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="555px"></td>
			</tr>
		</table>
		<table cellpadding="1" cellspacing="0"  ID="Table1" style="width:94%;margin-top:10px" border="0"
			background="${ctx}/images/Logistica/back.jpg" bordercolor="Red" class="tabla">
			<tr height="5px">
				<td></td>
			</tr>
			<tr>
				<td width="15%" colspan="">&nbsp;RUC :</td>
				<td width="40%">
					<form:input path="ruc" id="txtRuc" 
					onkeypress="fc_ValidaNumerico2();"
					onblur="fc_ValidaLongitudRuc('txtRuc');"
					cssClass="cajatexto" maxlength="11" size="11" />
				</td>
				<td width="18%">Tipo Calificaci�n:</td>
				<td width="18%">
				<form:select path="codCalif" id="codCalif" cssClass="cajatexto"
					cssStyle="width:80px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listCalificacion!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listCalificacion}" />
					</c:if>
				</form:select>
				</td>
				<td width="9%">&nbsp;</td>
			</tr>
			<tr>
				<td nowrap>&nbsp;Raz�n Social :</td>
				<td width="40%">
					<form:input path="razonSocial" id="txtRazonSocial" 
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaTextoEspOnBlur('txtRazonSocial','raz�n social');"
					cssClass="cajatexto" size="55" />
				</td>
				<td width="18%">Estado:</td>
				<td width="18%">
				<form:select path="codEstado" id="codEstado" cssClass="cajatexto"
					cssStyle="width:80px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listEstado!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listEstado}" />
					</c:if>
				</form:select>
				</td>
				<td align="right">
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:Fc_Busca();" alt="Buscar" style="cursor:hand" id="icoBuscar"></a>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" onclick="javascript:Fc_Limpia();" alt="Limpiar" style="cursor:hand" id="icoLimpiar"></a>&nbsp;
				</td>
			</tr>
			<tr>
				<td>Tipo Proveedor:</td>
				<td><form:select path="codProv" id="codProv" cssClass="cajatexto"
						cssStyle="width:160px" >
						<form:option value="">--Todos--</form:option>
						<c:if test="${control.listprov!=null}">
						<form:options itemValue="codSecuencial" itemLabel="descripcion"
						items="${control.listprov}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr height="5px">
				<td></td>
			</tr>
			</table>
	
			<table style="width:97%; margin-top: 10px;" cellSpacing="0" cellPadding="0" border="1" bordercolor="white">
				<tr><td>
					<div style="overflow: auto; height: 343px;width:100%">
						<display:table name="sessionScope.listProveedor" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
						requestURI="" style="border: 1px solid #048BBA;width:98%">
						<display:column property="rbtSelProveedores" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
						<display:column property="nroDco" title="RUC" headerClass="grilla" class="tablagrilla" style="align:left;width:15%"/>
						<display:column property="razSoc" title="Raz�n social" headerClass="grilla" class="tablagrilla" style="align:left;width:50%"/>
						<display:column property="descGiro" title="Giro" headerClass="grilla" class="tablagrilla" style="align:left;width:15%"/>
						<display:column property="descCalificacion" title="Tipo Calificaci�n" headerClass="grilla" class="tablagrilla" style="align:left;width:15%"/>
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
						</display:table>
						<% request.getSession().removeAttribute("listProveedor"); %>
					</div>
				<td valign=top>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src="${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar();" alt="Agregar" style="cursor:hand" id="icoAgregar"></a><br>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoModificar','','${ctx}/images/iconos/modificar2.jpg',1)">
					<img src="${ctx}/images/iconos/modificar1.jpg" onclick="javascript:Fc_Modificar();" alt="Modificar" style="cursor:hand;" id="icoModificar"></a><br>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)">
					<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_Eliminar();" alt="Eliminar" style="cursor:hand;" id="icoEliminar"></a>&nbsp;
				</td>
			</tr>
		</table>
	</form:form>
</html>
