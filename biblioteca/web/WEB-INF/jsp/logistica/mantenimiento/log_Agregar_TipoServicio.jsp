<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>	
		function onLoad()
		{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{	
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrSeGraboConExito);
			window.close();
			window.opener.document.getElementById("frmMain").submit();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		document.getElementById("txhMsg").value = "";
		alert(mstrProblemaGrabar);
		}
		else if(objMsg.value == "DOBLE"){
		objMsg.value = "";
		document.getElementById("txhMsg").value = "";
		alert(mstrOtraCompetencia);
		}
		}
		function Fc_Graba(){
		if ( fc_Trim(document.getElementById("txtTipSer").value) !== "" && document.getElementById("codSer").value !=="-1")
			{
		document.getElementById("txhOperacion").value="GRABAR";
		document.getElementById("frmMain").submit();
		}
		else{
			alert(mstrLleneLosCampos);
			}
		}
	</script>
	</head>
	<form:form action="${ctx}/logistica/log_Agregar_TipoServicio.html" commandName="control" id="frmMain">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="secuencial" id="txhSecuencial"/>
		<form:hidden path="tipo" id="txhTipo"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:5px;margin-left:10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="320px" class="opc_combo"><font style="">Mantenimiento Tipo Servicio</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
		 </tr>
	 </table>
	<table class="tablaflotante" style="width:95%;margin-left:10px;margin-top:10px" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray'> 
			<tr>
				<td valign='top' colspan="2">
					<table class="tablaflotante"  cellSpacing="1" cellPadding="1" border="0" bordercolor='red' height="60px"> 
						<tr>
							<td nowrap>Grupo Servicio :</td>			
							<td >
								<form:select path="codSer" id="codSer" cssClass="cajatexto"
									cssStyle="width:280px">
									<form:option value="-1">--Seleccione--</form:option>
									<c:if test="${control.listSer!=null}">
									<form:options itemValue="codSecuencial" itemLabel="descripcion"
									items="${control.listSer}" />
									</c:if>
									</form:select>
							</td>
						<tr>
							<td nowrap>Tipo de Servicio :</td>			
							<td >		
								<form:input path="tipSer" id="txtTipSer" 
								onkeypress="fc_ValidaTextoNumeroEspecialDos();"
								onblur="fc_ValidaNumeroLetrasGuionOnblurDos(this,'tipo de servicio');"
								cssClass="cajatexto_o" size="70" maxlength="60"/>
							</td>
						</tr>
						<tr>
							<td nowrap>Cta.Cble:</td>	<td >				
								<form:input path="cta" id="txtCta" 
								onkeypress="fc_ValidaNumerico2();"
								onblur="fc_ValidaNumeroOnBlur('txtCta');"
								cssClass="cajatexto_o" size="15" maxlength="10" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>	
		<br>
		<table align=center>
			<tr>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Graba();" alt="Grabar" id="btnGrabar"></a>
				</td>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
					<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="window.close();" alt="cancelar" id="btnCancelar"></a>
				</td>
			</tr>
		</table>	
</form:form>
