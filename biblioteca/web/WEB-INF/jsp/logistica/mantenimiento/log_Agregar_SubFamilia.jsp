<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>
		function onLoad()
		{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);
		}
		else if(objMsg.value == "DOBLE"){
		alert(mstrOtraCompetencia);
		}
			}
	    function Fc_Grabar(){
	if ( fc_Trim(document.getElementById("txtSubFamilia").value) !== "" && fc_Trim(document.getElementById("codFam").value) !== "")
			{
		document.getElementById("txhOperacion").value="GRABAR";
		document.getElementById("frmMain").submit();
			}
			else{
			alert(mstrLleneLosCampos);
			}
		}
	</script>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Agregar_SubFamilia.html">
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="tipo" id="txhTipo"/>
	<form:hidden path="secuencial" id="txhSecuencial"/>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:5px;margin-left:10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="370px" class="opc_combo"><font style="">Mantenimiento de SubFamilias</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
		 </tr>
	 </table>
	<table class="" style="width:98%;margin-top:10px;margin-left:10px" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray'> 
		<tr>
			<td valign='top' colspan="2">
				<table class="tablaflotante" background="${ctx}/images/logistica/back.jpg" height="40px" 
				cellSpacing="1" cellPadding="1" border="0" bordercolor='red' style="width:98%">
					<tr>
						<td nowrap>C�digo :</td>
						<td>
							<form:input path="codigo" id="txtCodigo" 
							readonly="true"
							cssClass="cajatexto_o" size="15" />
						</td>
					</tr>
					<tr>
						<td nowrap>SubFamilia :</td>
						<td>
							<form:input path="subfamilia" id="txtSubFamilia" 
								onkeypress="fc_ValidaTextoNumeroEspecial();" 
							onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'subfamilia');"
							cssClass="cajatexto_o" maxlength="60" cssStyle="width: 272px"/>
						</td>
					</tr>
					 <tr>
						<td nowrap>Familia :</td>
						<td>
							<form:select path="codFam" id="codFam" cssClass="cajatexto"
								cssStyle="width:280px">
								<form:option value="-1">--Seleccione--</form:option>
								<c:if test="${control.listFamilia!=null}">
								<form:options itemValue="codSecuencial" itemLabel="descripcion"
								items="${control.listFamilia}" />
								</c:if>
							</form:select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table align=center style="margin-top:7px">
		<tr>
			<td>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
			<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Grabar();" alt="Grabar" id="btnGrabar"></a>
			</td>
			<td>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
			<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" alt="Cancelar" id="btnCancelar"></a>
			</td>
		</tr>
	</table>
</form:form>
</html>
