<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>
	var esta = ""
	function onLoad()
	{
		//Seleccionar por defecto lima
		if (document.getElementById("codSede").value==""){
			document.getElementById("codSede").value="L";
		}
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			alert(mstrElimino);
		}
		else if ( objMsg.value == "ERROR" )
		{
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			alert(mstrProbEliminar);
		}
		else if ( objMsg.value == "DOBLE" )
		{
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			alert(mstrNoEli);
		}
		document.getElementById("txhMsg").value = "";
		document.getElementById("txhOperacion").value = "";
	}
	function Fc_Eliminar(){
		if(fc_Trim(document.getElementById("txhCodProducto").value) !== ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhOperacion").value="ELIMINAR";
				document.getElementById("frmMain").submit();
			}
			else{
				return false;
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
		
	function Fc_Buscar(){
		if(fc_Trim(document.getElementById("codSede").value) != "")
		{
			document.getElementById("txhOperacion").value = "BUSCAR";
			document.getElementById("frmMain").submit();
		}
		else
		{
			alert(mstrSede);
		}
	}
	function Fc_Agregar(){
		document.getElementById("txhCodProducto").value = "";
		strTipo = "";
		if ( document.getElementById("codSede").value != "" )
		{
			location.href = "${ctx}/logistica/log_Agregar_Producto.html" +
						"?txhCodAlumno=" + document.getElementById("txhCodAlumno").value +
						"&txhTipo=" + strTipo +
						"&txhCodProducto=" + document.getElementById("txhCodProducto").value +
						"&txhEstado=" + esta +
						"&txhSede=" + document.getElementById("codSede").value;
		}
		else { alert(mstrSede); }
		
	}
		
	function Fc_Limpiar(){
		document.getElementById("codSede").value = "";
		document.getElementById("codFamilia").value = "";
		document.getElementById("codSubFamilia").value = "";
		document.getElementById("codCondicion").value = "";
		document.getElementById("txtCodPro").value = "";
		document.getElementById("txtProducto").value = "";
		document.getElementById("txtNroSerie").value = "";
	}
	
	function fc_seleccionarRegistro(strCod,strRuta,strCon,strEstado){
		document.getElementById("txhCodProducto").value = strCod;
		document.getElementById("txhRuta").value = strRuta;
		esta = strEstado;
	}
	function Fc_Modificar(){
		strTipo = "MODIFICAR";
		strSede = document.getElementById("codSede").value;
		strFamilia = document.getElementById("codFamilia").value;
		strSubFamilia = document.getElementById("codSubFamilia").value;
		if(fc_Trim(document.getElementById("txhCodProducto").value) !== "")
		{
			if ( document.getElementById("codSede").value != "" )
			{
				location.href = "${ctx}/logistica/log_Agregar_Producto.html" +
							"?txhCodAlumno=" + document.getElementById("txhCodAlumno").value +
							"&txhTipo=" + strTipo +
							"&txhCodProducto=" + document.getElementById("txhCodProducto").value +
							"&txhRuta=" + document.getElementById("txhRuta").value +
							"&txhSede=" + strSede +
							"&txhFamilia=" + strFamilia +
							"&txhEstado=" + esta +
							"&txhSubFamlia=" + strSubFamilia;
			}
			else { alert(mstrSede); }
		}
		else
		{
			alert(mstrSeleccion);
		}
	}
	function Fc_MuestraSubFamilia(){
		document.getElementById("txhOperacion").value="MUESTRASUBFAMILIA";
		document.getElementById("frmMain").submit();
	}
	function Fc_Exporta(){
		u = "0011";
		url="?tCodRep=" + u +
			"&tCPro=" + document.getElementById("txtCodPro").value +
			"&tDPro=" + document.getElementById("txtProducto").value +
			"&tCSA=" + document.getElementById("codSede").value +
			"&tDSe=" + document.getElementById("codSede").options[document.getElementById("codSede").selectedIndex].text +
			"&tCoFa=" + document.getElementById("codFamilia").value +
			"&tDFa=" + frmMain.codFamilia.options[frmMain.codFamilia.selectedIndex].text +
			"&tCSFa=" + document.getElementById("codSubFamilia").value +
			"&tCond=" + document.getElementById("codCondicion").value +
			"&tDCon=" + frmMain.codCondicion.options[frmMain.codCondicion.selectedIndex].text +
			"&tNSe=" + document.getElementById("txtNroSerie").value +
			"&tDSubFa=" + frmMain.codSubFamilia.options[frmMain.codSubFamilia.selectedIndex].text;
	
		window.open("/SGA/logistica/reportesLogistica.html"+url,"Reportes","resizable=yes, menubar=yes");
	}
	function fc_VerImagen(strRuta){
		strRu = "<%=(( request.getAttribute("msgRutaServer")==null)?"": request.getAttribute("msgRutaServer"))%>";
		if ( strRuta != "" )
		{
			if ( fc_Trim(strRuta) != "") window.open(strRu + strRuta);
			else alert(mstrNoTiene2);
		}
	}
	function fc_verKardex(codProd){		
		var url="?operacion=0003" + //document.getElementById("txhCodTipoReporte").value +
			"&txhCodSede=" + document.getElementById("codSede").value +
			"&txhCodTipo=" + //document.getElementById("txhConsteRadio").value +
			"&txhCodBien=" + codProd + //document.getElementById("codBien").value +
			"&txhCodFamilia=" + //document.getElementById("codFamilia").value +
			"&txhDscUsuario=" + document.getElementById("txhDscUsuario").value +
			"&txhCodUsuario=" + document.getElementById("txhCodAlumno").value +
			"&txhFecIni=" + document.getElementById("txtFecInicio").value +
			"&txhFecFin=" + document.getElementById("txtFecFinal").value +
			"&txhNomSede=" + frmMain.codSede.options[frmMain.codSede.selectedIndex].text ;
		
		window.open("${ctx}/logistica/reporteLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");
	}
	</script>
	</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Consulta_Catalogo_Productos.html">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<form:hidden path="codProducto" id="txhCodProducto"/>
		<form:hidden path="ruta" id="txhRuta"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="nomPro" id="txhNomPro"/>
		<form:hidden path="fec1" id="txtFecInicio"/>
		<form:hidden path="fec2" id="txtFecFinal"/>
		<form:hidden path="dscUsuario" id="txhDscUsuario"/>

		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
			<tr>
				<td colspan="3"><img src="${ctx}/images/Logistica/logistica1_r5_c2.jpg" width="700px" height="4px"></td>
			</tr>
			<tr>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c2.jpg"></td>
				<td valign="top"  class="opc_combo" background="${ctx}/images/Logistica/logistica1_r6_c4.jpg" height="33px"><font>Consulta del Catalogo de Productos</font></td>
				<td rowspan="2"><img src="${ctx}/images/Logistica/logistica1_r6_c9.jpg"></td>
			</tr>
			<tr>
				<td><img src="${ctx}/images/Logistica/logistica1_r9_c4.jpg" height="5px" width="555px"></td>
			</tr>
		</table>
		
		<table cellpadding="2" cellspacing="0"  ID="Table1" border="0" bordercolor="Red" class="tabla"
			background="${ctx}/images/Logistica/back.jpg" style="margin-top:10px;width:99%">
			<tr>
				<td>&nbsp;Sede :</td>
				<td>
				&nbsp;<form:select path="codSede" id="codSede" cssClass="cajatexto_o"
					cssStyle="width:190px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listSede!=null}">
					<form:options itemValue="codPadre" itemLabel="descripcion"
					items="${control.listSede}" />
					</c:if>
					</form:select>
				</td>
				<td>&nbsp;Estado :</td>
				<td colspan="2">
				&nbsp;<form:select path="codEstado" id="codEstado" cssClass="cajatexto"
					cssStyle="width:190px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listestado!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listestado}" />
					</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="14%">&nbsp;Familia :</td>
				<td width="28%">
				&nbsp;<form:select path="codFamilia" id="codFamilia" cssClass="cajatexto"
					cssStyle="width:190px" onchange="javascript:Fc_MuestraSubFamilia();">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listFamilia!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listFamilia}" />
					</c:if>
					</form:select>
				</td>
				<td width="13%">&nbsp;Sub Familia :</td>
				<td colspan="2">&nbsp;<form:select path="codSubFamilia" id="codSubFamilia" cssClass="cajatexto"
					cssStyle="width:190px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listSubFamilia!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listSubFamilia}" />
					</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td>&nbsp;Cod. Producto :</td>
				<td>&nbsp;<form:input path="codPro" id="txtCodPro"
					onkeypress="fc_ValidaNumerico2();"
					onblur="fc_ValidaNumeroOnBlur('txtCodPro');"
					cssClass="cajatexto" size="20" maxlength="12" /></td>
				<td>&nbsp;Producto :</td>
				<td colspan="2">&nbsp;<form:input path="producto" id="txtProducto"
					cssClass="cajatexto" size="60" maxlength="50"/></td>
			</tr>
			<tr>
				<td>&nbsp;Condici&oacute;n :</td>
				<td>&nbsp;<form:select path="codCondicion" id="codCondicion" cssClass="cajatexto"
					cssStyle="width:190px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listCondicion!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listCondicion}" />
					</c:if>
					</form:select></td>
				<td>&nbsp;Nro. Serie :</td>
				<td>&nbsp;<form:input path="nroSerie" id="txtNroSerie"
								onkeypress="fc_ValidaTextoNumeroEspecial();"
								onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'nro. serie');"
								cssClass="cajatexto" size="10" maxlength="20"/></td>
				<td align="center">&nbsp;&nbsp;
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
				<img src="${ctx}/images/iconos/limpiar1.jpg" onclick="javascript:Fc_Limpiar();" alt="Limpiar" style="cursor:hand" id="icoLimpiar"></a>&nbsp;
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
				<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:Fc_Buscar();" alt="Buscar" style="cursor:hand" id="icoBuscar"></a>&nbsp;
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoExcel','','${ctx}/images/iconos/exportarex2.jpg',1)">
				<img src="${ctx}/images/iconos/exportarex1.jpg" onclick="javascript:Fc_Exporta();" alt="Excel" style="cursor:hand" id="icoExcel"></a>
				</td>
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" width="99%" style="margin-top: 10px;">
			<tr>
				<td>
	 				<div style="overflow: auto; height: 300px;width:100%">
					<display:table name="sessionScope.listProductos" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.LogisticaDecorator"
						requestURI="" style="border: 1px solid #048BBA;width:98%">
						
						<display:column property="rbtSelCatalogo" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
<%--						<display:column property="codProducto" title="C�digo" headerClass="grilla" class="tablagrilla" style="align:left;width:10%"/> --%>
						<display:column property="lnkCodProducto" title="C�digo" headerClass="grilla" class="tablagrilla" style="align:left;width:10%"/>
						<display:column property="nomProducto" title="Producto" headerClass="grilla" class="tablagrilla" style="align:left;width:25%"/>
						<display:column property="nomUnidad" title="Unidad" headerClass="grilla" class="tablagrilla" style="align:left;width:7%"/>
						<display:column property="stockActual" title="Stock<br>Actual" headerClass="grilla" class="tablagrilla" style="text-align:right;width:10%"/>
						<display:column property="stockDisponible" title="Stock<br>Disponible" headerClass="grilla" class="tablagrilla" style="text-align:right;width:10%"/>
						<display:column property="precio" title="Precio<br>Unitario" headerClass="grilla" class="tablagrilla" style="text-align:right;width:10%"/>
						<display:column property="condicion" title="Condici�n" headerClass="grilla" class="tablagrilla" style="align:left;width:10%"/>
						<display:column property="desEstado" title="Estado" headerClass="grilla" class="tablagrilla" style="align:left;width:10%"/>
						<display:column property="muestraImagen" title="Ver" headerClass="grilla" class="tablagrilla" style="text-align:center;width:5%" />
					
						<display:setProperty name="basic.empty.showtable" value="true" />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>" />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span>Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					</display:table>
					<% request.getSession().removeAttribute("listProductos"); %>
					</div>
				</td>
				<td width="30px" valign="top" align="right">
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
						<img src="${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar();" alt="Agregar" style="cursor:hand" id="icoAgregar">
					</a>
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoModificar','','${ctx}/images/iconos/modificar2.jpg',1)">
						<img src="${ctx}/images/iconos/modificar1.jpg" onclick="javascript:Fc_Modificar();" alt="Modificar" style="cursor:hand;" id="icoModificar">
					</a>
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)">
						<img src="${ctx}/images/iconos/kitar1.jpg" onclick="javascript:Fc_Eliminar();" alt="Eliminar" style="cursor:hand;" id="icoEliminar">
					</a>
				</td>
			</tr>
		</table>
	</form:form>
