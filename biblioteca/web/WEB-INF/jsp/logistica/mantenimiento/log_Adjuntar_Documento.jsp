<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>
		var strExtensionDOC = "<%=(String)request.getAttribute("strExtensionDOC")%>";
		var strExtensionXMS = "<%=(String)request.getAttribute("strExtensionXMS")%>";
		var strExtensionDOCX = "<%=(String)request.getAttribute("strExtensionDOCX")%>";
		var strExtensionXLSX = "<%=(String)request.getAttribute("strExtensionXLSX")%>";
		var strExtensionPDF = "<%=(String)request.getAttribute("strExtensionPDF")%>";
		function onLoad()
		{
		objMsg = document.getElementById("txhMessage");
			if ( objMsg.value == "OK" )
			{
				alert(mstrGrabar);
				window.opener.document.getElementById("frmMain").submit();
				window.close();
			}
			else if ( objMsg.value == "ERROR" )
			{
				alert(mstrProblemaGrabar);
			}
			else if ( objMsg.value == "ERROR_TAMANIO" )
			{
				alert(mstrArchivoMuyGrande);
			}
		}
		
		function fc_Valida(){
			var strExtension;
			var lstrRutaArchivo;
			if(fc_Trim(document.getElementById("txtCV").value) == ""){
				alert(mstrInforme);
					return false;
			}
			else{
				if(fc_Trim(document.getElementById("txtCV").value) != ""){
					lstrRutaArchivo = document.getElementById("txtCV").value;
					strExtension = "";
					
					strExtension = lstrRutaArchivo.toLowerCase().substring(lstrRutaArchivo.length - 3);
				
					nombre = (lstrRutaArchivo.substring(lstrRutaArchivo.lastIndexOf("\\"))).toLowerCase();
					document.getElementById("txhNomArchivo").value=nombre.substring("1");
			
					if (strExtension != strExtensionXMS && strExtension != strExtensionDOC && strExtension != strExtensionDOCX && strExtension != strExtensionXLSX  && strExtension != strExtensionPDF){
						alert(mstrExtencionInf);
						return false;
					}
					else{
						document.getElementById("txhExtCv").value = strExtension;
					}
				}
			}
			return true;
		}
		
		
		
		function fc_GrabarArchivos(){
			if (fc_Valida()){
				if ( fc_Trim(document.getElementById("codtipo").value) !== ""){
					if (confirm(mstrSeguroGrabar)){
						document.getElementById("txhOperacion").value="GRABAR";
						document.getElementById("frmMain").submit();
					}
				}
				else{
					alert("Debe seleccionar un Tipo Adjunto.");
					return;
				}
			}
		}

</script>
</head>

 <form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/log_Adjuntar_Documento.html" 
		enctype="multipart/form-data" method="post">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="message" id="txhMessage"/>
	<form:hidden path="extCv" id="txhExtCv"/>
	<form:hidden path="nomArchivo" id="txhNomArchivo"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="codPro" id="txhCodPro"/>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:3px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="260px" class="opc_combo"><font style="">Adjuntar Documentos</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
		 </tr>
	 </table>
	
	<table class="tablaflotante" background="${ctx}/images/logistica/back.jpg" height="60px" cellSpacing="1" cellPadding="1" border="0" bordercolor='red' style="margin-top: 4px;width: 98%;margin-left: 5px;">
		<tr>
			<td>Tipo Adjunto :</td>
			<td>
				<form:select path="codtipo" id="codtipo" cssClass="cajatexto_o"
					cssStyle="width:200px" >
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listTipo!=null}">
						<form:options itemValue="codSecuencial" itemLabel="descripcion"
						items="${control.listTipo}" />
					</c:if>
					</form:select>
			</td>
		</tr>
		<tr>
			<td width="20%">Archivo:</td>
			<td>
				<input type="file"  name="txtCV"  class="cajatexto_1" size="60">
			</td>
		</tr>
	</table>
	<table align="center" style="margin-top: 10px;">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img src= "${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_GrabarArchivos();" style="cursor:hand" id="imgAgregar" alt="Grabar">
				</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminar','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img  src="${ctx}/images/botones/cancelar1.jpg" alt="Cancelar" style="cursor:hand" onclick="window.close();" id="imgEliminar">
				</a>
			</td>
		</tr>
	</table>
</form:form>


