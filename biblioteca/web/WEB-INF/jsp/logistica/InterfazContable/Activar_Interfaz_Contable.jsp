<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	function onLoad(){
	
		if(document.getElementById("txhMsg").value=="OK")
		 {
		 	//fc_Reporte_Cabecera();
		 	document.getElementById("txhMsg").value=="";
		 }
		 else{ if(document.getElementById("txhMsg").value=="ERROR")
		        alert("Problemas al generar el reporte, consulte con el administrador.");
		 }
		document.getElementById("txhMsg").value = "";
	}

	function fc_TipoSugerencia(){}
	
	function fc_Regresar()
		{
			document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhCodSede").value;
		}
		
	function fc_TipoMovmiento(){
	
	if(document.getElementById("codTipoMovimiento").value!="-1")
	 { document.getElementById("txhOperacion").value = "MOVIMIENTO";
	   document.getElementById("frmMain").submit();
	 }
	 else{ document.getElementById("dscEstado").value="";
	       document.getElementById("txhCodEstado").value="";
	   }
	}
	
	function fc_Cabecera(){
		fc_Reporte_Cabecera();
		if(document.getElementById("codTipoMovimiento").value!="-1"){
		   document.getElementById("txhOperacion").value = "CABECERA";
		   document.getElementById("frmMain").submit();
		 }else 
			 alert("Debe seleccionar un tipo de movimiento.");
	 	
	}
	
	function fc_Detalle(){
	 if(document.getElementById("txhCodEstado").value==document.getElementById("txhConsteGenerada").value)
	  {
	    fc_Reporte_Detalle();
	  }
	  else alert("Primero debe generar la cabecera del informe.");
	}
	
	function fc_Resultado(){
	 if(document.getElementById("txhCodEstado").value==document.getElementById("txhConsteGenerada").value)
	  {
		u = "0022";
		url="?tCodRep=" + u +
			"&txhCodSede=" + document.getElementById("txhCodSede").value +
			"&cMes=" + document.getElementById("codMeses").value +
			"&cAnio=" + document.getElementById("codAnios").value +
			"&tInt=" + document.getElementById("codTipoMovimiento").value +
			"&dAnio=" + frmMain.codAnios.options[frmMain.codAnios.selectedIndex].text +
			"&dMes=" + frmMain.codMeses.options[frmMain.codMeses.selectedIndex].text ;
	
		window.open("/SGA/logistica/reportesLogistica.html"+url,"Reportes","resizable=yes, menubar=yes");

	  }
	  else alert("Primero debe generar la cabecera del informe.");
	}
	
	function fc_Reporte_Cabecera(){
	url="?txhCodSede=" + document.getElementById("txhCodSede").value +
		"&txhCodUsuario=" + document.getElementById("txhCodUsuario").value +
		"&txhCodAnio=" + document.getElementById("codAnios").value +
		"&txhCodTipoMovimiento=" + document.getElementById("codTipoMovimiento").value +
		"&txhCodMes=" + document.getElementById("codMeses").value +
		"&txhOperacion=CABECERA";
		
		window.open("${ctx}/logistica/reportesInterfazContableCabecera.html"+url,"ReportesLogistica1","resizable=yes, menubar=yes");
	
	}
	
	function fc_Reporte_Detalle(){
	url="?txhCodSede=" + document.getElementById("txhCodSede").value +
		"&txhCodUsuario=" + document.getElementById("txhCodUsuario").value +
		"&txhCodAnio=" + document.getElementById("codAnios").value +
		"&txhCodTipoMovimiento=" + document.getElementById("codTipoMovimiento").value +
		"&txhCodMes=" + document.getElementById("codMeses").value +
		"&txhOperacion=DETALLE";
		
		window.open("${ctx}/logistica/reportesInterfazContableCabecera.html"+url,"ReportesLogistica2","resizable=yes, menubar=yes");
	
	}
</script>
</head>

<body>
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/activarInterfazContable.html" method="post" enctype="multipart/form-data" >
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="consteGenerada" id="txhConsteGenerada"/>
<form:hidden path="codEstado" id="txhCodEstado"/>

	<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:0px;margin-top:7px" align="center">
		<tr>
			<td align="left"><img src="/SGA/images/Evaluaciones/izquierda.jpg"></td>
			<td background="/SGA/images/Evaluaciones/centro.jpg" width="525px" class="opc_combo">
				Activar Proceso Interfaz Contable
			</td>
			<td align="right"><img src="/SGA/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	
	<table align="center" class="tabla" style="margin-top:7px; width: 710px" align="center" background="${ctx}/images/Evaluaciones/back.jpg"
		cellspacing="2" cellpadding="2" >
		<tr>
			<TD style="width: 20%;">Periodo Actual</TD>
			<TD style="width: 80%;">:
				<form:select path="codAnios" id="codAnios" cssClass="combo_o" cssStyle="width:80px"
				    onchange="javascript:fc_TipoSugerencia();">
					
					<c:if test="${control.codListaAnios!=null}">
						<form:options itemValue="id" itemLabel="name" 
							items="${control.codListaAnios}" />
					</c:if>
				</form:select>&nbsp;&nbsp;&nbsp;
				<form:select path="codMeses" id="codMeses" cssClass="combo_o" cssStyle="width:180px" 
				    onchange="javascript:fc_TipoSugerencia();">
					
					<c:if test="${control.codListaMeses!=null}">
						<form:options itemValue="id" itemLabel="name" 
							items="${control.codListaMeses}" />
					</c:if>
				</form:select>
			</TD>
		</tr>
		
		<tr>
			<td>Tipo de Movimiento</td>
			<td>:
				<form:select path="codTipoMovimiento" id="codTipoMovimiento" cssClass="combo_o" 
					cssStyle="width:160px" onchange="javascript:fc_TipoMovmiento();">
					<form:option value="-1">-- Seleccione --</form:option>
					<c:if test="${control.listaTipoMovimiento!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
						items="${control.listaTipoMovimiento}" />
					</c:if>
				</form:select>
			</td>
		</tr>
		<tr>
			<td>Estado</td>
			<td>:
				<form:input path="dscEstado" id="dscEstado" cssClass="cajatexto_1"
			       cssStyle="width:90px" maxlength="25" readonly="true"/>
			</td>
		</tr>
	</table>
	
	<table style="width:60%" cellpadding="3" cellspacing="3" border="0" bordercolor="red" align="center">
		<tr>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCabecera','','${ctx}/images/botones/cabecera2.jpg',1)">
					<img src="${ctx}/images/botones/cabecera1.jpg" style="cursor:pointer;" id="imgCabecera" onclick="fc_Cabecera();" 
					alt="Cabecera"></a>
				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgDetalle','','${ctx}/images/botones/detalle2.jpg',1)">
					<img src="${ctx}/images/botones/detalle1.jpg" style="cursor:pointer;" id="imgDetalle" onclick="fc_Detalle();" 
					alt="Detalle"></a>
				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgResultado','','${ctx}/images/botones/resultado2.jpg',1)">
					<img src="${ctx}/images/botones/resultado1.jpg" style="cursor:pointer;" id="imgResultado" onclick="fc_Resultado();" 
					alt="Resultado"></a>
				
			</td>
		</tr>
	</table>
			
</form:form>
</body>
