<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">	
	function onLoad(){		
		fc_Calcular();
		objMsg=document.getElementById("txhMsg");
		if ( objMsg.value == "OKGRABAR" ){		
			alert(mstrGrabar);
			fc_Exportar();
			//parent.fc_Actualizar();			
		}
		else{		
			if ( objMsg.value == "ERRORGRABAR" )
				alert(mstrProblemaGrabar);
			else{   if ( objMsg.value == "ERROR_CANTIDAD_PENDIENTE" )
					alert(mstrDetaExceCantPendientes);
			}
		}		
		document.getElementById("txhMsg").value="";				
	}	
	function fc_Cancelar(){		
		
		codGuia=document.getElementById("txhCodGuia").value;
		parent.fc_Cancelar();		
	}
	function fc_AsignarSubTotal(seleccion){		
		cantidad=parseFloat(document.getElementById("txtCantidadEntregada"+seleccion).value);
		precio=parseFloat(document.getElementById("txtPrecioUnitario"+seleccion).value);
		subTotal=cantidad*precio;
		document.getElementById('txtSubTotal'+seleccion).value=redondea(subTotal,2);
	}
	function fc_AsignarTotal(){
		
		numeroHijoTabla = document.getElementById("txhTamListaGuiaDetalle").value;
		
		subTotal=0;
		for(var i=0; i<numeroHijoTabla;i++){
			subTotal=parseFloat(subTotal)+parseFloat(document.getElementById("txtSubTotal"+i).value);
		}
		document.getElementById("txtTotal").value=redondea(subTotal,2);
	}
	//redondea el sVal con nDec decimales
	function redondea(sVal, nDec){ 
	    var n = parseFloat(sVal); 
	    var s = "0.00"; 
	    if (!isNaN(n)){ 
			n = Math.round(n * Math.pow(10, nDec)) / Math.pow(10, nDec); 
			s = String(n); 
			s += (s.indexOf(".") == -1? ".": "") + String(Math.pow(10, nDec)).substr(1); 
			s = s.substr(0, s.indexOf(".") + nDec + 1); 
	    } 
	    return s; 
	}
	function fc_Calcular(){
	
		numeroHijoTabla=document.getElementById("txhTamListaGuiaDetalle").value;		
		
		for(var i=0; i<numeroHijoTabla;i++){
			fc_AsignarSubTotal(i);
		}
		fc_AsignarTotal();
	}
	function fc_Grabar(){		
		if(fc_ValidarCantidadVacio()==true){
			alert(mstrIngCantidadEntregada);
			return false;
		}
		fechaGuia=document.getElementById("txtFechaGuia").value;
		if(fechaGuia==""){
			alert(mstrIngFechaGuia);
			return false;
		}
		if(document.getElementById("txhTamListaGuiaDetalle").value > 0){
			fc_DatosBandeja();
			if(confirm(mstrSeguroGrabar)){
				document.getElementById("txhOperacion").value="GRABAR";
				document.getElementById("frmMain").submit();
			}
		}
		else{
			alert(mstrNoExisteReg);
		}
	}
	
	function fc_DatosBandeja(){		
		
		numeroHijoTabla=document.getElementById("txhTamListaGuiaDetalle").value;
		
		var cadenaCodDetReq="";
		var cadenaCantSol="";
		var cadenaPrecios="";
		var cadenaCantEnt="";
		
		for(var i=0; i<numeroHijoTabla;i++){
			if(i==0){
				cadenaCodDetReq=document.getElementById("txhCodDetReq"+i).value+"|";				
				cadenaCantSol=document.getElementById("txtCantidadSolicitada"+i).value+"|";
				cadenaPrecios=document.getElementById("txtPrecioUnitario"+i).value+"|";
				cadenaCantEnt=document.getElementById("txtCantidadEntregada"+i).value+"|";												
			}
			else{
				cadenaCodDetReq=cadenaCodDetReq+document.getElementById("txhCodDetReq"+i).value+"|";				
				cadenaCantSol=cadenaCantSol+document.getElementById("txtCantidadSolicitada"+i).value+"|";
				cadenaPrecios=cadenaPrecios+document.getElementById("txtPrecioUnitario"+i).value+"|";
				cadenaCantEnt=cadenaCantEnt+document.getElementById("txtCantidadEntregada"+i).value+"|";				
			}
		}		
		document.getElementById("txhCadenaCodDetReq").value=cadenaCodDetReq;
		document.getElementById("txhCadenaCantSol").value=cadenaCantSol;
		document.getElementById("txhCadenaPrecios").value=cadenaPrecios;
		document.getElementById("txhCadenaCantEnt").value=cadenaCantEnt;
		
	}
	function fc_ValidarCantidad(seleccion){
	
		cantidadSolicitada=document.getElementById("txtCantidadDisponible"+seleccion).value;
		cantidadEntregada=document.getElementById("txtCantidadEntregada"+seleccion).value;
		
		if(parseFloat(cantidadEntregada) > parseFloat(cantidadSolicitada)){
			alert("La cantidad entregada debe ser \nmenor o igual a la cantidad disponible.");
			document.getElementById("txtCantidadEntregada"+seleccion).focus();
			document.getElementById("txtCantidadEntregada"+seleccion).value="";
			return;
		}
	}
	function fc_ValidarCantidadVacio(){
	
		numeroHijoTabla = document.getElementById("txhTamListaGuiaDetalle").value; 
		
		for(var i=0; i<numeroHijoTabla;i++){
			cantidad=document.getElementById("txtCantidadEntregada"+i).value;
			if(cantidad=="" || fc_Trim(cantidad)==""){
				document.getElementById("txtCantidadEntregada"+i).focus();
				return true;
			}
		}
		return false;
	}
	function fc_MostrarDetalleGuia(seleccion){		
		document.getElementById("txhCodGuiaDetalle").value=document.getElementById("txhCodGuiaDetalle"+seleccion).value;
		document.getElementById("txhCodBien").value=document.getElementById("txhCodBien"+seleccion).value;
		document.getElementById("txhCantidad").value=document.getElementById("txhCantidad"+seleccion).value;
		
		ArrCodSel = document.getElementById("txhCantidad").value.split(".");
		var num=ArrCodSel[0];
		
		prmCodGuia=document.getElementById("txhCodGuia").value;
		prmCodGuiaDetalle=document.getElementById("txhCodGuiaDetalle").value;
		prmCantidadEntregada=num;
		prmCodBien=document.getElementById("txhCodBien").value;
		prmCodSede=document.getElementById("txhCodSede").value;
		prmCodUsuario=document.getElementById("txhCodUsuario").value;
	
		document.getElementById("iFrameDetalleProducto").style.display = "inline-block";
		document.getElementById("iFrameDetalleProducto").src="${ctx}/logistica/registrarResponsable.html?prmCodGuia="+prmCodGuia+
				"&prmCodGuiaDetalle="+prmCodGuiaDetalle+
				"&prmCantidadEntregada="+prmCantidadEntregada+
				"&prmCodBien="+prmCodBien+
				"&prmCodSede="+prmCodSede+
				"&prmCodUsuario="+prmCodUsuario+
				"&txhCodEstado="+document.getElementById("txhCodEstado").value;
		
	}
	
	function fc_EnviarAprobacionMantenimiento(){
	}
	
	function fc_Cerrar(){
	
	document.getElementById("iFrameDetalleProducto").src="";
	}
	
	function fc_Exportar(){
	u = "0012";
	//ALQD,08/01/2009. REEMPLAZANDO EL CARACTER # PUES CORTA LOS PARAMETROS ENVIADOS
	lugTrabajo = document.getElementById("txhlugarTrabajo").value;
	lugTrabajo = lugTrabajo.replace(/#/,"");
	url="?tCodRep=" + u +
			"&nroGuia=" + document.getElementById("txtNroGuia").value +
			"&fechaGuia=" + document.getElementById("txtFechaGuia").value +
			"&estado=" + document.getElementById("txtEstado").value + 
			"&total=" + document.getElementById("txtTotal").value +
			"&codGuia=" + document.getElementById("txhCodGuia").value+
			"&solicitante=" + document.getElementById("txhSolicitante").value+
			"&lugarTrabajo=" + lugTrabajo+
			"&nroRequerimiento=" + document.getElementById("txhNroRequerimiento").value+
			"&fecAprRequerimiento=" + document.getElementById("txhFecAprRequerimiento").value+
			"&nomCenCosto=" + document.getElementById("txhNomCenCosto").value+
			"&fecCierre=" + document.getElementById("txhFecCierre").value;
		window.open("${ctx}/logistica/reportesLogistica.html"+url,"","resizable=yes, menubar=yes");
	 
	}
	
	</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/AtencionRequerimientoBienesGuiaSalida.html">
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="seleccion" id="txhSeleccion" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />
	<form:hidden path="codRequerimiento" id="txhCodRequerimiento" />
	<form:hidden path="codGuia" id="txhCodGuia" />
	
	<form:hidden path="cadenaCodDetReq" id="txhCadenaCodDetReq" />
	<form:hidden path="cadenaCantSol" id="txhCadenaCantSol" />
	<form:hidden path="cadenaPrecios" id="txhCadenaPrecios" />
	<form:hidden path="cadenaCantEnt" id="txhCadenaCantEnt" />	
	
	<form:hidden path="tamListaGuiaDetalle" id="txhTamListaGuiaDetalle" />
	<!-- para ruben -->	
	<form:hidden path="codGuiaDetalle" id="txhCodGuiaDetalle" />
	<form:hidden path="codBien" id="txhCodBien" />
	<form:hidden path="cantidad" id="txhCantidad" /><!-- cantidad entregada -->
	<form:hidden path="codEstado" id="txhCodEstado" />	
	<!-- /para ruben -->		
	<!--T�tulo de la P�gina  -->
	<form:hidden path="solicitante" id="txhSolicitante"/>
	<form:hidden path="lugarTrabajo" id="txhlugarTrabajo"/>
	<!-- ALQD,13/11/08. A�ADIENDO MAS DATOS -->
	<form:hidden path="nroRequerimiento" id="txhNroRequerimiento"/>
	<form:hidden path="fecAprRequerimiento" id="txhFecAprRequerimiento"/>
	<form:hidden path="nomCenCosto" id="txhNomCenCosto"/>
	<form:hidden path="fecCierre" id="txhFecCierre"/>
	<div style="overflow: auto; height: 350px;width:100%">
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px;width: 95%">
		 <tr>
		 	<td align="left" width="22px"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="730px" class="opc_combo">
		 		<font style="">		 			
				 	Gu�a Salida				 			 			
		 		</font>
		 	</td>
		 	<td><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	
	<!-- cabecera detalle guia -->
	<table class="tabla" style="width:94%;margin-top:6px;margin-left:9px;height: 50px;" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="0" border="0" bordercolor="red">
		<tr>
			<td width="8%">&nbsp;&nbsp;&nbsp;Nro. Gu�a:</td>
			<td width="22%">				
				<form:input path="nroGuia" id="txtNroGuia"
					cssClass="cajatexto_1" cssStyle="width: 80px; text-align: right" readonly="true"/>
			</td>
			<td width="8%">Fecha Gu�a:</td>
			<td width="22%">
				<form:input path="fechaGuia" id="txtFechaGuia" maxlength="10" 
					cssClass="cajatexto_1" cssStyle="width: 80px; text-align: right" readonly="true"
					onkeypress="javascript:fc_Slash('frmMain','txtFechaGuia','/');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFechaGuia');"/>
				<!-- >a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaGuia','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaGuia" alt="Calendario" style='cursor:hand;' disabled="true" align="absmiddle">
				</a-->
			</td>		
			<td width="7%">Estado:</td>
			<td width="33%">
				<form:input path="estado" id="txtEstado"
					cssClass="cajatexto_1" cssStyle="width: 120px" readonly="true"/>
			</td>	
		</tr>
	</table>
	<!-- bandeja detalle guia -->
	<div style="overflow: auto; height: 150px;width: 96%">	
	<table cellpadding="0" cellspacing="1" style="width:98%;margin-top:6px;margin-bottom:5px;margin-left:9px;border: 1px solid #048BBA"
	     id="tablaBandeja" border="0" bordercolor="black">
		<tr>		 
			<td class="grilla" width="30%">Producto</td>
			<td class="grilla" width="8%">Cantidad<br>Solicitada</td>
			<td class="grilla" width="8%">Cantidad<br>Pendiente</td>
			<td class="grilla" width="8%">Cantidad<br>Disponible</td>
			<td class="grilla" width="8%">Precio Unitario</td>
			<td class="grilla" width="10%">Cantidad<br>Entregada</td>
			<td class="grilla" width="5%">Unidad</td>
			<td class="grilla" width="12%">SubTotal</td>
			<td class="grilla" width="9%">Ing. Resp.</td>																			
		</tr>
		<c:forEach varStatus="loop" var="lista" items="${control.listaGuiaDetalle}"  >
		<tr class="texto">						
			<td align="left" class="tablagrilla" style="width: 30%">
				&nbsp;&nbsp;&nbsp;<c:out value="${lista.dscBien}" />
				<input type="hidden" 
					id='txhCodDetReq<c:out value="${loop.index}"/>'
					value='<c:out value="${lista.codReqDetalle}" />'>
				<input type="hidden" 
					id='txhCodGuiaDetalle<c:out value="${loop.index}"/>'
					value='<c:out value="${lista.codGuiaDetalle}" />'>
				<input type="hidden" 
					id='txhCodBien<c:out value="${loop.index}"/>'
					value='<c:out value="${lista.codBien}" />'>
				<input type="hidden" 
					id='txhCantidad<c:out value="${loop.index}"/>'
					value='<c:out value="${lista.cantidad}" />'>
			</td>
			<td align="center" class="tablagrilla" style="width: 8%">
				<input type="text" value='<c:out value="${lista.cantidadSolicitada}" />'
					id='txtCantidadSolicitada<c:out value="${loop.index}"/>' readonly
					class="cajatexto_1" style="text-align: right;width: 60px">
			</td>
			<td align="center" class="tablagrilla" style="width: 8%">
				<input type="text" value='<c:out value="${lista.cantidadPendiente}" />'
					id='txtCantidadPendiente<c:out value="${loop.index}"/>' readonly
					class="cajatexto_1" style="text-align: right;width: 60px">
			</td>			
			<td align="center" class="tablagrilla" style="width: 8%">
				<input type="text" value='<c:out value="${lista.cantidadDisponible}" />'
					readonly id='txtCantidadDisponible<c:out value="${loop.index}"/>'
					class="cajatexto_1" style="text-align: right;width: 60px">
			</td>
			<td align="center" class="tablagrilla" style="width: 8%">
				<input type="text" value='<c:out value="${lista.precioUnitario}" />'
					id='txtPrecioUnitario<c:out value="${loop.index}"/>' readonly
					class="cajatexto_1" style="text-align: right;width: 80px">
			</td>
			<td align="center" class="tablagrilla" style="width: 10%">
				
				<c:if test="${control.codEstado=='0003'}">
					<input type="text" value='<c:out value="${lista.cantidad}" />'					
										id='txtCantidadEntregada<c:out value="${loop.index}"/>' readonly
										class="cajatexto_1" style="text-align: right;width: 60px">
				</c:if>
				<c:if test="${control.codEstado!='0003'}">
					<input type="text" value='<c:out value="${lista.cantidadSeleccionada}" />'					
										id='txtCantidadEntregada<c:out value="${loop.index}"/>' readonly
										class="cajatexto_1" style="text-align: right;width: 60px">
				</c:if>
				<%-- 
				<input type="text" value='<c:out value="${lista.cantidadSeleccionada}" />'					
					id='txtCantidadEntregada<c:out value="${loop.index}"/>' readonly
					class="cajatexto_1" style="text-align: right;width: 60px">
					<!--<c:if test="${control.codEstado != '0001'}">disabled</c:if>
					onkeypress="fc_ValidaNumeroDecimal();" 					
					onblur="fc_ValidarCantidad('<c:out value="${loop.index}"/>');fc_ValidaDecimalOnBlur(this.id,'10','2');fc_AsignarSubTotal('<c:out value="${loop.index}"/>');fc_AsignarTotal('<c:out value="${loop.index}"/>');"-->
					--%>
			</td>

			<!-- jhpr 2008-09-25 -->
			<td align="center" class="tablagrilla" style="width: 5%">
				<input type="text" value='<c:out value="${lista.unidadMedida}" />'
					id='unidad<c:out value="${loop.index}"/>' readonly
					class="cajatexto_1" style="text-align: center;width: 50px">
			</td>

			<td align="center" class="tablagrilla" style="width: 12%">
				<input type="text" class="cajatexto_1" readonly
					id='txtSubTotal<c:out value="${loop.index}"/>' 
					value="" style="text-align:right;width: 90px">					
			</td>
			<td align="center" class="tablagrilla" style="width: 9%">
				<c:if test="${lista.indicador == '1'}">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar<c:out value="${loop.index}"/>','','${ctx}/images/iconos/agregar2.jpg',1)">
						<img src="${ctx}/images/iconos/agregar1.jpg" style="cursor:pointer;" id="imgagregar<c:out value="${loop.index}"/>" onclick="fc_MostrarDetalleGuia('<c:out value="${loop.index}"/>');" alt="Agregar">
					</a>					
				</c:if>
			</td>
		</tr>
		</c:forEach>
		<c:if test='${control.listaGuiaDetalle==null}'>
		<tr class="tablagrilla">
			<td align="center" colspan="6" height="20px">No se encontraron registros
			</td>
		</tr>					 		
		</c:if>
		<c:if test='${control.tamListaGuiaDetalle > 5}'>
		<script type="text/javascript">
			document.getElementById("tablaBandeja").style.width = "95%";
		</script>		 		
		</c:if>		
	</table>
	<table border="0" bordercolor="red" width="98%">
		<tr class="texto" >
			<td align="right">Total :&nbsp;S/.&nbsp;&nbsp;&nbsp;
			<input type="text" id="txtTotal" style="text-align:right; width: 90px" class="cajatexto_1" readonly value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>					
	</table>
	</div>
	<!-- botoneria -->
	<table align="center" border="0" bordercolor="red" style="width:95; margin-top: 6px;">
		<tr>
			<td>
			    <c:if test="${control.codEstado == '0001'}">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar1','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar1" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a>										
			    </c:if>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgExportar','','${ctx}/images/botones/exportar2.jpg',1)">
					<img alt="Exportar" src="${ctx}/images/botones/exportar1.jpg" id="imgExportar" style="cursor:pointer;" onclick="javascript:fc_Exportar();">
				</a>
			</td>
			
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgCancelar" style="cursor:pointer;" onclick="javascript:fc_Cancelar();">
				</a>
			</td>
		</tr>
	</table>
	<!-- iframe detalle producto -->
	<iframe id="iFrameDetalleProducto" name="iFrameDetalleProducto" frameborder="0" width="95%" height="250px" scrolling="no" style="display: none" ></iframe>				
</div>					
</form:form>

</body>