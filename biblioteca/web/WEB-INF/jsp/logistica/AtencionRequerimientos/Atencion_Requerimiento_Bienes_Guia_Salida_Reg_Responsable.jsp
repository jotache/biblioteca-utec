<%@page import="java.util.List"%>
<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

	function onLoad(){ 
		
		var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
		
		if(mensaje!="")
			alert(mensaje);
			
		
		if(document.getElementById("txhTamanio").value > 7){
			document.getElementById("tablaResponsable").style.width = "98%";
		}
		
		if ( document.getElementById("txtSerie0") != null )
		{
			document.getElementById("txtSerie0").focus();
		}
	}
	
	function fc_cerrar(){
	  if(confirm(mstrConfCerrar)){
		fc_LimpiarTodo();
		parent.fc_Cerrar();
	  }
	}
	
	function fc_LimpiarTodo(){
	  var long=parseInt(document.getElementById("cantidadEntregada").value);
	  for(a=0; a<long; a++)
	  {   document.getElementById("txhCodSerie"+a).value="";
		  document.getElementById("txtSerie"+a).value="";
		  document.getElementById("txtCodResp"+a).value="";
		  document.getElementById("txtNomResp"+a).value="";
		  document.getElementById("txtUbicacion"+a).value="";
	  
	  }
	}
	
	function fc_sel(pos,flgBD){	
		
		/*if(fc_Trim(document.getElementById("txhCodSerie"+pos).value)!="")
		{
		
		 if(flgBD=="1")
		 {*/
		 	//alert("txhCodBienSel");
		 	//alert("txhCodSerie"+pos);
		 	
			document.getElementById("txhCodBienSel").value = document.getElementById("txhCodSerie"+pos).value;
			document.getElementById("txhCodBienSelPos").value = pos;
			document.getElementById("txhCodBienSelFlgSel").value = flgBD;
					
			//alert("REG A ELIMINAR>"+document.getElementById("txhCodBienSel").value+">");
		/* }else{
			alert("No se puede eliminar\nEl registro seleccionado no ha sido guardado");
			alert(document.getElementById("rdoSel").checked);
			document.getElementById("rdoSel").checked = false;
			alert(document.getElementById("rdoSel").checked);
			
		 }
		 
		}else{
			alert("No se puede eliminar\nEl registro seleccionado no ha sido guardado");
			alert(document.getElementById("rdoSel").checked);
			document.getElementById("rdoSel").checked = false;
			alert(document.getElementById("rdoSel").checked);
		}*/
	}
	
	function fc_eliminar(){
	
	 if(fc_Trim(document.getElementById("txhCodBienSel").value) !==""){
	 
	 if(fc_Trim(document.getElementById("txhCodBienSelFlgSel").value) == "1" ){
	 
	  if(fc_Trim(document.getElementById("txhCodSerie"+document.getElementById("txhCodBienSelPos").value).value)!=""){
	  
	  	if(confirm(mstrSeguroEliminar1)){
			document.getElementById("operacion").value = "irEliminar";
	 		document.getElementById("frmMain").submit();
	 		} 
	  }else{
	  	alert(mstrNoEliminarDebeGrabar);
	  
	  } 
	 }else{
	 	alert(mstrNoEliminarDebeGrabar);
	 }
	 
	
	 		
	 }else{
		alert(mstrSeleccione);
	 }
	
	}
	
	function fc_guardar(){
	var tam = document.getElementById("txhTamanio").value;
		if( tam!="" && Number(tam)>0 ){
			if(fc_valida(tam)){
					
				if(confirm(mstrSeguroGrabar)){
					fc_preparaCadenas(tam);
				}			
				
			}else{
				alert(mstrLleneTodoCampos);
			}
			
		}else{
			alert(mstrNoSePuedeRealizarLaAccion);
		}
	}
	
	function fc_preparaCadenas(tam){
	
	cadCodBienDet = "";
	cadCodUsuResp = "";
	cadDescUbic = "";
	var cant = 0;
	
	 for(var i=0;i<Number(tam);i++){
		
		cad1 = document.getElementById("txhCodSerie"+i).value;
		cadCodBienDet = cadCodBienDet+cad1+"|";
	
		cad2 = document.getElementById("txtCodResp"+i).value;
		cadCodUsuResp  = cadCodUsuResp+cad2+"|";
		
		cad3 = document.getElementById("txtUbicacion"+i).value;
		cadDescUbic = cadDescUbic +cad3+"|";
		
		cant = cant+1;
	 }
	  
	 document.getElementById("txhCadCodBienDet").value = cadCodBienDet;
	 document.getElementById("txhCadCodUsuResp").value = cadCodUsuResp;
	 document.getElementById("txhCadCodDescUbicacion").value = cadDescUbic;
	 document.getElementById("txhCantidad").value = cant;
	 document.getElementById("operacion").value = "irRegistrar";
	 document.getElementById("frmMain").submit();
	 
	 
	}
	
	function fc_valida(tam){
	
	 var numErr = 0;
	
	 for(var i=0;i<Number(tam);i++){
		
		if(document.getElementById("txhCodSerie"+i).value=="" ||
		document.getElementById("txtSerie"+i).value=="" ||
		document.getElementById("txtCodResp"+i).value=="" ||
		document.getElementById("txtNomResp"+i).value=="" ||
		document.getElementById("txtUbicacion"+i).value==""
		){
			numErr = numErr + 1;
		}
		 
	 }
	 
	 if(numErr>0)
	 	return 0;
	 else
	 	return 1;	
	
	}
	
	
	/*FUNCION HEYDI*/
	function fc_BusActivos(codSede,codBien,pos){
		url = "/SGA/logistica/log_BusquedaActivos.html?txhCodSede="+codSede+"&txhCodProducto="+codBien+"&prmPos="+pos;
		Fc_Popup(url,600,400);
	}
	
	function fc_BusResponsable(pos){
		url = "/SGA/logistica/log_BusquedaEmpleados.html?prmPos="+pos+ "&txhSede="+ document.getElementById("codSede").value;
		Fc_Popup(url,600,400);
	}
	
	function repetido(id,codSerie,pos){
	//id:codigoBien
	//1 :repetido
	//0 :no repetido
	var tam = document.getElementById("txhTamanio").value;
	var val = 0;
	
		for(var i=0;i<Number(tam);i++){	
			if( document.getElementById("txhCodSerie"+i).value == id ){
				val = 1;
			}
	 	}
	 	
		return val;
	}
	
	/*********************************
	*id:Identificador seleccionado
	*codSerie:CodSerie seleccionado
	*pos:posicion a asignar
	************************************/
	
	function fc_setSerie(id,codSerie,pos)
	{	
		if(!repetido(id,codSerie,pos)){		
			document.getElementById("txhCodSerie"+pos).value = id;
			document.getElementById("txtSerie"+pos).value = codSerie;
		}else{
			alert(mstrNoSeleccioneRegRepetido);
		}
	}
	
	/***********************************
	*codResponsable:Identificador seleccionado
	*nomResponsable:nom seleccionado
	*pos:posicion a asignar
	************************************/
	function fc_setResponsable(codResponsable,nomResponsable,pos)
	{	
		document.getElementById("txtCodResp"+pos).value = codResponsable;
		document.getElementById("txtNomResp"+pos).value = nomResponsable;
	}

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain"
	action="${ctx}/logistica/registrarResponsable.html">
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="codGuia" id="codGuia" />
	<form:hidden path="codBien" id="codBien" />
	<form:hidden path="codSede" id="codSede" />
	<!--CANTIDAD QUE VIENE DE LA PAG DE RODR  -->
	<form:hidden path="cantidadEntregada" id="cantidadEntregada" />
	<form:hidden path="codEstado" id="txhCodEstado" />	
	
	<input type="hidden" id="txhCadCodBienDet" name="txhCadCodBienDet" />
	<input type="hidden" id="txhCadCodUsuResp" name="txhCadCodUsuResp" />
	<input type="hidden" id="txhCadCodDescUbicacion" name="txhCadCodDescUbicacion" />
	<input type="hidden" id="txhCantidad" name="txhCantidad" />
	
	<!-- ITEM SELECCIONADO A ELIMINAR-->
	<input type="hidden" id="txhCodBienSel" name="txhCodBienSel" />
	<input type="hidden" id="txhCodBienSelPos" name="txhCodBienSelPos" />
	<input type="hidden" id="txhCodBienSelFlgSel" name="txhCodBienSelFlgSel" />
		
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px;width: 95%">
		 <tr>
		 	<td align="left" width="22px"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="730px" class="opc_combo">
		 		<font style="">		 			
				 	Registrar Ubicaci�n y Responsable del Activo				 			 			
		 		</font>
		 	</td>
		 	<td><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<c:set var="tam" value="0"  />
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="width:99%;margin-top:6px;margin-left:9px;">
		<tr>
			<td width="95%">
				<div style="overflow: auto; height: 210px; width: 98%">
				<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaResponsable">
					<tr>
						<td class="grilla" width="4%" align="center">Sel.</td>
						<td class="grilla" width="4%" align="center">Nro.</td>
						<td class="grilla" width="12%" align="center">Nro. Serie</td>
						<td class="grilla" width="40%" align="center">Responsable</td>
						<td class="grilla" width="40%" align="center">Ubicaci�n</td>
					</tr>							
					
					<!-- **************ITERACION EL FOR EACH************* -->
					
					<c:forEach var="objCurrent" items="${control.lstRespuesta}"
						varStatus="loop">
						
						<c:set var="tam" value="${tam+1}"  />
						
					<tr  class="tablagrilla" >
						<td width="4%" align="center">									
							 <input type="radio" name="rdoSel" id="rdoSel" 
							 onclick="fc_sel('<c:out value="${loop.index}"/>','<c:out value="${objCurrent.flgBD}"/>')" >									
						</td>
						<td width="4%" align="center"><c:out value="${loop.index}"/></td>
						<!-- NRO SERIE -->
						<td align="center">
							<input type="hidden" value="<c:out value="${objCurrent.codBienDet}"/>" 
							id="txhCodSerie<c:out value="${loop.index}"/>" 
							name="txhCodSerie<c:out value="${loop.index}"/>"  />
							
							<input type="text" readonly="readonly" class="cajatexto_o" style="width:50px"
							value="<c:out value="${objCurrent.nroSerie}"/>"
							id="txtSerie<c:out value="${loop.index}"/>" 
							name="txtSerie<c:out value="${loop.index}"/>" >
							<c:if test="${control.codEstado == '0001'}">
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar0<c:out value="${loop.index}"/>','','${ctx}/images/iconos/buscar2.jpg',1)">
								<img src="${ctx}/images/iconos/buscar1.jpg" style="cursor:pointer;" id="imgBuscar0<c:out value="${loop.index}"/>"
								onclick="fc_BusActivos('<c:out value="${control.codSede}"/>','<c:out value="${control.codBien}"/>','<c:out value="${loop.index}"/>')" 
								alt="Buscar">
							</a>
							</c:if>					
							<!-- <img src="${ctx}/images/iconos/buscar_on.gif" 
							onclick="fc_BusActivos('<c:out value="${control.codSede}"/>','<c:out value="${control.codBien}"/>','<c:out value="${loop.index}"/>')" > -->									
						</td>
						
						<!-- RESPONSABLE -->
						<td align="left">
							<input type="text" class="cajatexto_o" style="width:50px; text-align: right;" readonly="readonly"
							 value="<c:out value="${objCurrent.codUsuResp}"/>" 
							id="txtCodResp<c:out value="${loop.index}"/>" 
							name="txtCodResp<c:out value="${loop.index}"/>" >
							
							<input type="text" class="cajatexto_o" style="width:250px" readonly="readonly"
							  value="<c:out value="${objCurrent.nomUsuResp}"/>" 
							id="txtNomResp<c:out value="${loop.index}"/>" 
							name="txtNomResp<c:out value="${loop.index}"/>" >										
							<c:if test="${control.codEstado == '0001'}">
								<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar1<c:out value="${loop.index}"/>','','${ctx}/images/iconos/buscar2.jpg',1)">
									<img src="${ctx}/images/iconos/buscar1.jpg" style="cursor:pointer;" id="imgBuscar1<c:out value="${loop.index}"/>"
										onclick="fc_BusResponsable('<c:out value="${loop.index}"/>')" alt="Buscar">
								</a>
							</c:if>						
						</td>
						
						<!-- UBICACION -->
						<td align="left">
							<input type="text" class="cajatexto_o" maxlength="50" style="width:96%" 
							  value="<c:out value="${objCurrent.ubicacion}"/>" 										
							id="txtUbicacion<c:out value="${loop.index}"/>" 
							name="txtUbicacion<c:out value="${loop.index}"/>" > 	
						</td>
					</tr>
					</c:forEach>
					<!-- **************ITERACION EL FOR EACH************* -->
					<c:if test='${control.tama�oLista=="0"}'>
						<tr class="tablagrilla">
							<td align="center" colspan="5" height="20px">No se encontraron registros
							</td>
						</tr>					 		
					</c:if>
				</table>
			</td>
			<td width="5%" valign="middle" align="right" id="tdBotones">			
				<c:if test="${control.codEstado == '0001'}">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgguardar','','${ctx}/images/iconos/grabar2.jpg',1)">
					<img src="${ctx}/images/iconos/grabar1.jpg" style="cursor:pointer;" id="imgguardar" onclick="fc_guardar();" 
					alt="Guardar"></a>
				</c:if>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCerrar','','${ctx}/images/iconos/cerrar2.jpg',1)">
				<img src="${ctx}/images/iconos/cerrar1.jpg" style="cursor:pointer" id="imgCerrar" onclick="javascript:fc_cerrar();"
				alt="Cerrar"></a>
				<br/>
				<c:if test="${control.codEstado == '0001'}">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitar','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img src="${ctx}/images/iconos/quitar1.jpg" style="cursor:pointer;" id="imgquitar" onclick="fc_eliminar();"
					alt="Eliminar"></a>
				</c:if>
			</td>
			<!-- <td align="center">
				<img src="${ctx}/images/iconos/guardar_on.gif" onclick="fc_guardar()">
				<br>
				<img src="${ctx}/images/iconos/cerrar_on.gif" onclick="fc_cerrar()">
				<br>
				<img src="${ctx}/images/iconos/eliminar_on.gif" onclick="fc_eliminar();">
				
			</td>-->
		</tr>
	</table>
	<!-- INDICA EL TAMANIO DE LOS REGISTROS -->
	<input type="hidden" id="txhTamanio" name="txhTamanio"  value="<c:out value="${tam}"/>" />
	
</form:form>
</body>


