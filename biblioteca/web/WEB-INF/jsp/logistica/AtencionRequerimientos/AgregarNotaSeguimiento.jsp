<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>
		function onLoad()
		{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{	
			objMsg.value = "";
			document.getElementById("txhMsg").vlaue = "";
			alert(mstrSeGraboConExito);
			window.opener.document.getElementById("frmMain").submit();
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		document.getElementById("txhMsg").vlaue = "";
		alert(mstrProblemaGrabar);
		}
		document.getElementById("txhOperacion").value="";
		}
		function Fc_Grabar(){
		document.getElementById("txhOperacion").value="GRABAR";
		document.getElementById("frmMain").submit();
		}
		
</script>
</head>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/AgregarNotaSeguimiento.html">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="tipo" id="txhTipo"/>
	<form:hidden path="codNota" id="txhCodNota"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="codReq" id="txhCodReq"/>
	
	<!-- title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:6px;margin-top:6px">
		<tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="250px" class="opc_combo">
		 		<font style="">Agregar Nota Seguimiento
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:6px;height: 30px;" background="${ctx}/images/Evaluaciones/back.jpg" 
		cellspacing="4" cellpadding="0" border="0" bordercolor="red">
		<tr>	
			<td>
			<form:textarea path="nota" cssStyle="height: 50px;width:99%;" cssClass="cajatexto"/>			
			</td>	
		</tr>		
	</table>
	<table align=center style="margin-top:7px">
		<tr>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Grabar();" alt="Grabar" id="btnGrabar"></a>
			</td>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
				<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" alt="cancelar" id="btnCancelar"></a>
		</tr>
	</table>	
</form:form>
	