<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">	
	function onLoad(){		
		objMsg=document.getElementById("txhMsg");
		if ( objMsg.value == "OKGRABAR" ){		
			alert(mstrGrabar);
			parent.fc_Actualizar();			
		}
		else		
		if ( objMsg.value == "ERRORGRABAR" ){
			alert(mstrProblemaGrabar);
		}
		else if ( objMsg.value == "ERROR_EXCEDE" ){
			alert(mstrDetaExceCantPendientes);
		}
				
		document.getElementById("txhMsg").value="";		
	}	
	function fc_Cancelar(){		
		//parent.document.getElementById("iFrameGuiaAtencion").src="";
		codGuia=document.getElementById("txhCodGuia").value;
		parent.fc_Cancelar(codGuia);		
	}
	function fc_Grabar(){
		fechaGuia=document.getElementById("txtFechaGuia").value;
		if(fechaGuia==""){
			alert(mstrFecEmisionGuiaNoVacia);
			return false;
		}
		if(confirm(mstrSeguroGrabar)){
			document.getElementById("txhOperacion").value="GRABAR";
			document.getElementById("frmMain").submit();
		}
	}
	function fc_Exportar(){
	u = "0018";
	url="?tCodRep=" + u +
			"&codGuia=" + document.getElementById("txhCodGuia").value; 
	
		window.open("${ctx}/logistica/reportesLogistica.html"+url,"","resizable=yes, menubar=yes");
	 
	}
	</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/AtencionRequerimientoServiciosGuiaAtencion.html">
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="seleccion" id="txhSeleccion" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />
	<form:hidden path="codRequerimiento" id="txhCodRequerimiento" />
	<form:hidden path="codGuia" id="txhCodGuia" />	
	<form:hidden path="codDetalle" id="txhCodDetalle" />
		
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px;width: 95%">
		 <tr>
		 	<td align="left" width="22px"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="730px" class="opc_combo">
		 		<font style="">		 			
				 	Gu�a Atenci�n Servicios				 			 			
		 		</font>
		 	</td>
		 	<td><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	
	<!-- cabecera detalle guia -->
	<table class="tabla" style="width:94%;margin-top:6px;margin-left:9px;height: 30px;" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="0" >
		<tr>
			<td width="15%">&nbsp;&nbsp;Nro. Gu�a Atenci�n:</td>
			<td width="15%">				
				<form:input path="nroGuia" id="txtNroGuia"
					cssClass="cajatexto_1" cssStyle="width: 90px; text-align:right;" readonly="true"/>
			</td>
			<td width="10%">Fecha Gu�a:</td>
			<td width="30%">
				<form:input path="fechaGuia" id="txtFechaGuia"
				onkeypress="javascript:fc_Slash('frmMain','txtFechaGuia','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFechaGuia');"
					cssClass="cajatexto_o" cssStyle="width: 60px;" maxlength="10"/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaGuia','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaGuia" alt="Calendario" style='cursor:hand;' align="absmiddle">
				</a>
			</td>		
			<td width="7%">Estado:</td>
			<td width="23%">
				<form:input path="estado" id="txtEstado"
					cssClass="cajatexto_1" cssStyle="width: 160px;" readonly="true"/>
			</td>	
		</tr>
		<tr>
			<td width="15%">&nbsp;&nbsp;Nro. Factura :</td>
			<td width="15%">				
				<form:input path="nroFactura" id="txtNroFactura"
					cssClass="cajatexto_1" cssStyle="width: 90px; text-align:right;" readonly="true"/>
			</td>
			<td width="10%">Fecha Emisi�n:</td>
			<td width="30%">
				<form:input path="fechaFactura" id="txtFechaFactura"
					cssClass="cajatexto_1" cssStyle="width: 60px;" readonly="true"/>
			</td>		
			<td width="7%">Nro. O/S :</td>
			<td width="23%">
				<form:input path="nroOS" id="txtNroOS"
					cssClass="cajatexto_1" cssStyle="width: 140px;; text-align:right;" readonly="true"/>
			</td>	
		</tr>
		<tr>
			<td width="15%">&nbsp;&nbsp;Observaciones:
			</td>
			<td colspan="5">
				<form:textarea path="observacion" id="txtObservacion" cssClass="cajatexto" cssStyle="width:80%;height:50px;"/>
			</td>
		</tr>
	</table>
	<!-- botoneria -->
	<table><tr height="20px"><td></td></tr></table>
	<table align="center">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar1','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar1" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a>
			</td>										
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgExportar','','${ctx}/images/botones/exportar2.jpg',1)">
					<img alt="Exportar" src="${ctx}/images/botones/exportar1.jpg" id="imgExportar" style="cursor:pointer;" onclick="javascript:fc_Exportar();">
				</a>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgCancelar" style="cursor:pointer;" onclick="javascript:fc_Cancelar();"></a>
			</td>
		</tr>
	</table>			
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFechaGuia",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFechaGuia",
		singleClick    :    true
	});		
</script>
</body>