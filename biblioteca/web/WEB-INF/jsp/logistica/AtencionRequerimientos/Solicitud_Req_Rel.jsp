<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){
				
	}
	function fc_VerDocumento(codRequerimiento){
		//window.location.href="${ctx}/logistica/SreqBienes.html"+
		window.open("${ctx}/logistica/SreqBienes.html"+
		"?txhCodRequerimiento="+codRequerimiento+
		"&txhCodSede="+ document.getElementById("txhCodSede").value +
		"&txhCodUsuario="+document.getElementById("txhCodUsuario").value+
		"&txhCodRequerimientoOri="+ document.getElementById("txhCodRequerimiento").value +
		"&txhTipoProcedencia=1","","menubar=0,resizable=yes, menubar=yes,width=900,height=400,left=40,top=50,toolbar=0") ;
	}
</script>
</head>
<body MS_POSITIONING="GridLayout" topmargin="0" leftmargin="10 rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/SolicitudReqRel.html">

	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="seleccion" id="txhSeleccion" />	
	<form:hidden path="codRequerimiento" id="txhCodRequerimiento"/>
	<form:hidden path="codSede" id="txhCodSede"/>
	<form:hidden path="tamListaSolicitudes" id="txhTamListaSolicitudes"/>
	

	<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
		<tr>
			<td>Solicitud de Requerimientos Relacionados</td>	
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" border="0" bordercolor="red">
		<tr>
			<td>
				<div style="overflow: auto; height: 90px">	
				<table cellpadding="0" cellspacing="1" style="width:100%;border: 1px solid #048BBA" id="tablaBandeja">
					<tr>															
						<td class="grilla">Nro. Req.</td>										
						<td class="grilla">Fec. Req.</td>
						<td class="grilla">Tipo Req.</td>
						<td class="grilla">Estado</td>
						<td class="grilla">Ver</td>
					</tr>		
					<c:forEach varStatus="loop" var="lista" items="${control.listaSolicitudes}"  >
					<tr class="texto">
						<td align="left" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.nroRequerimiento}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.fechaEmision}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 30%">
							<c:out value="${lista.dscTipoRequerimiento}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 25%">
							<c:out value="${lista.dscEstado}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 5%">
							<img src="/SGA/images/iconos/buscar1.jpg" style="cursor:pointer" 
								onclick="fc_VerDocumento('${lista.idRequerimiento}');"/>

						</td>
					</tr>						
					</c:forEach>
					<c:if test='${control.listaSolicitudes==null}'>
					<tr class="tablagrilla">
						<td align="center" colspan="5" height="20px">No se encontraron registros
						</td>
					</tr>					 		
					</c:if>
					<c:if test='${control.tamListaSolicitudes > 3}'>
					<script type="text/javascript">
						document.getElementById("tablaBandeja").style.width = "96%";
					</script>		 		
					</c:if>
										
				</table>
				</div>
			</td>	
		</tr>
	</table>	
</form:form>	
</body>
</html>