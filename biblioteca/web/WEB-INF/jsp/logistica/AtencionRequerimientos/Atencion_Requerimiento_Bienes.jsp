<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">	
	function onLoad(){		
		objMsg=document.getElementById("txhMsg");
		
		if ( objMsg.value == "OKAGREGAR" ){
			codGuia=document.getElementById("txhCodGuia").value;
			codRequerimiento=document.getElementById("txhCodRequerimiento").value;
			codUsuario=document.getElementById("txhCodUsuario").value;
			codSede=document.getElementById("txhCodSede").value;
			
			document.getElementById("iFrameGuiaSalida").style.display = "inline-block";
			document.getElementById("iFrameGuiaSalida").src="${ctx}/logistica/AtencionRequerimientoBienesGuiaSalida.html?txhCodGuia="+codGuia+
				"&txhCodRequerimiento="+codRequerimiento+
				"&txhCodSede="+codSede+
				"&txhCodUsuario="+codUsuario;
				
		}
		else
		if ( objMsg.value == "OKQUITAR" ){
			document.getElementById("txhSeleccion").value = "";
		}
		else		
		if ( objMsg.value == "ERRORPENDIENTE" ){
			alert(mstrEstadoPendienteGuia);
		}
		else
		if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
		}
		else
		if ( objMsg.value == "OK_REGISTRAR_CONFORMIDAD" ){
			document.getElementById("txhSeleccion").value = "";
			alert(mstrExitoRegConformidad);
		}
		else
		if ( objMsg.value == "ERROR_RESPONSABLE" ){
			alert(mstrFaltanAsigResponsables);			
		}
		else
		if( objMsg.value == "ERROR_REGISTRAR_CONFORMIDAD" ){
		alert(mstrProblemaGrabar);
		}
		else 
		if ( objMsg.value == "ERROR_CANTIDAD_PENDIENTE" ){
		   alert(mstrDetaExceCantPendientes);
		}
		else 
		if ( objMsg.value == "ERROR_CANT_ENTREGADA" ){
		   alert(mstrStockCantEntregDispo);
		}
		else //CCORDOVA 14/06/08		
		if ( objMsg.value == "ERROR_NO_DET" ){
			alert("No se puede generar una guia para este requerimiento. Los detalles para los cuales se habian realizado reservas ya fueron atendidas.");
		}
		else 		
			if ( objMsg.value == "ERROR_FALTA_GRABAR_GUIA" ){
				alert("Primero grabe la Gu�a para confirmar");
			}

		
		document.getElementById("txhMsg").value="";
		document.getElementById("txhCodEstado").value="";
	}
	function fc_Actualizar(){
		document.getElementById("txhSeleccion").value="";
		document.getElementById("txhOperacion").value="ACTUALIZAR";
		document.getElementById("frmMain").submit();
	}
	function fc_Cancelar(){
		/*document.getElementById("txhOperacion").value = "CANCELAR"; //"QUITAR";
		document.getElementById("frmMain").submit();*/
		document.getElementById("iFrameGuiaSalida").src="";
	}
	function fc_MostrarDetalleGuia(){
		if(confirm(mstrConfAgregarNuevoReg)){
			document.getElementById("txhOperacion").value="AGREGAR";
			document.getElementById("frmMain").submit();
		}		
	}
	function fc_Modificar(){
		seleccion=document.getElementById("txhSeleccion").value;
		if(seleccion==""){
			alert(mstrSeleccion);
			return;
		}
		//if(confirm(mstrConfModificarReg)){		
		codGuia=document.getElementById("txhCodGuia").value;
		codRequerimiento=document.getElementById("txhCodRequerimiento").value;
		codUsuario=document.getElementById("txhCodUsuario").value;
		codSede=document.getElementById("txhCodSede").value;
		
		document.getElementById("iFrameGuiaSalida").style.display = "inline-block";
		document.getElementById("iFrameGuiaSalida").src="${ctx}/logistica/AtencionRequerimientoBienesGuiaSalida.html?txhCodGuia="+codGuia+
			"&txhCodRequerimiento="+codRequerimiento+
			"&txhCodSede="+codSede+
			"&txhCodUsuario="+codUsuario+
			"&txhCodEstado="+document.getElementById("txhCodEstado").value;
	   //}
		
	}


	function fc_Quitar(){
		if (document.getElementById("txhSeleccion").value != ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhSeleccion").value = "";
				document.getElementById("txhOperacion").value = "QUITAR";
				document.getElementById("frmMain").submit();
			}
		}
		else{
			alert(mstrSeleccion);
			return false;
		}
		//document.getElementById("iFrameGuiaSalida").src="";
	}
	function fc_SeleccionarRegistro(codGuia, codEstado){
		document.getElementById("txhSeleccion").value = codGuia;
		document.getElementById("txhCodGuia").value = codGuia;
		document.getElementById("txhCodEstado").value = codEstado;
		
	}
	function fc_RegistrarConformidad(){
		form = document.forms[0];
		if (document.getElementById("txhSeleccion").value != ""){
			if(document.getElementById("txhCodEstado").value=="0001"){
				if( confirm(mstrConfRegistrarConformidad) ){
					form.registrarCon.disabled=true;
					document.getElementById("txhSeleccion").value = "";
					document.getElementById("txhOperacion").value = "REGISTRARCONFORMIDAD";
					document.getElementById("frmMain").submit();
				}else
					form.registrarCon.disabled=false;
			}
			else{
				alert(mstrRegistrarConformidad);
				return;
			}
		}
		else{
			alert(mstrSeleccion);
			return false;
		}
	}
	function fc_Regresar(){
		codSede=document.getElementById("txhCodSede").value;		
		window.location.href="${ctx}/logistica/AtenRequerimientosBandejaBienesServicios.html?txhCodSede="+codSede;		
	}
	</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/AtencionRequerimientoBienes.html">
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="seleccion" id="txhSeleccion" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />
	<form:hidden path="codRequerimiento" id="txhCodRequerimiento" />
	<form:hidden path="codGuia" id="txhCodGuia" />
	<form:hidden path="codEstado" id="txhCodEstado" />		
	
	<form:hidden path="tamListaGuias" id="txhTamListaGuias" />
	<div style="overflow: auto; height: 480px;width:100%">
	<!--T�tulo de la P�gina  -->	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px;width: 95%">
		 <tr>
		 	<td align="left" width="22px"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="750px" class="opc_combo">
		 		<font style="">		 			
				 	Atender Requerimiento de Bienes				 			 			
		 		</font>
		 	</td>
		 	<td><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>	
	<table class="tabla" style="width:95%;margin-top:6px;margin-left:9px;height: 50px;" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="1" cellpadding="0" border="0" bordercolor="blue">
		<tr>
			<td width="15%">&nbsp;&nbsp;Nro. Req:</td>
			<td width="30%">
				<form:input path="nroRequerimiento" id="txtNroRequerimiento"
					cssClass="cajatexto_1" cssStyle="width: 80px; text-align:right;" readonly="true"/>
			</td>
			<td width="15%">U. Solicitante:</td>
			<td width="40%">
				<form:input path="usuSolicitante" id="txtUsuSolicitante"
					cssClass="cajatexto_1" cssStyle="width: 180px" readonly="true"/>
			</td>
			
		</tr>
		<tr>
			<td width="15%">Centro de Costo</td>
			<td colspan="2" width="30%">
				<form:input path="nomCeCoSolicitante" id="nomCeCoSolicitante"
					cssClass="cajatexto_1" cssStyle="width: 360px; text-align:left;" readonly="true"/>
			</td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px;width: 95%">
		 <tr>
		 	<td align="left" width="22px"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="730px" class="opc_combo">
		 		<font style="">		 			
				 	Consulta de Gu�as de Salida Asociadas				 			 			
		 		</font>
		 	</td>
		 	<td><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
		
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="width:95%;margin-top:6px;margin-left:9px;">
		<tr>
			<td>
				<div style="overflow: auto; height: 140px">
				<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandeja">
					<tr>						
						<td class="grilla" width="4%">Sel.</td>
						<td class="grilla" width="56%">Nro. Gu�a Salida</td>
						<td class="grilla" width="10%">Fecha Emisi�n</td>
						<td class="grilla" width="30%">Estado</td>																			
					</tr>					
					<c:forEach varStatus="loop" var="lista" items="${control.listaGuias}"  >
					<tr class="texto">						
						<td align="center" class="tablagrilla" style="width: 4%">
							<input type="radio" name="guia"
								value='<c:out value="${lista.codGuia}" />'
								id='txhCodGuia<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro(this.value,'<c:out value="${lista.codEstado}" />');">

							<input type="hidden" id="txhNroGuia<c:out value="${loop.index}"/>" value="<c:out value="${lista.nroGuia}"/>">
														
							<input type="hidden" id="txhFecGuia<c:out value="${loop.index}"/>" value="<c:out value="${loop.index}"/>">

						</td>
						<td align="left" class="tablagrilla" style="width: 56%">
							<c:out value="${lista.nroGuia}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.fechaEmision}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 30%">
							<c:out value="${lista.estado}" />					
						</td>	
					</tr>
					</c:forEach>					
					<c:if test='${control.listaGuias==null}'>
					<tr class="tablagrilla">
						<td align="center" colspan="4" height="20px">No se encontraron registros
						</td>
					</tr>					 		
					</c:if>
					<c:if test='${control.tamListaGuias > 5}'>
					<script type="text/javascript">
						document.getElementById("tablaBandeja").style.width = "98%";
					</script>		 		
					</c:if>
				</table>
				</div>
			</td>
			<td width="30px" valign="middle" align="right" id="tdBotones">			
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/agregar2.jpg',1)">
				<img src="${ctx}/images/iconos/agregar1.jpg" style="cursor:pointer;" id="imgagregar" onclick="fc_MostrarDetalleGuia();" 
				alt="Agregar"></a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificar','','${ctx}/images/iconos/actualizar2.jpg',1)">
				<img src="${ctx}/images/iconos/actualizar1.jpg" style="cursor:pointer" id="imgModificar" onclick="javascript:fc_Modificar();"
				alt="Modificar"></a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitar','','${ctx}/images/iconos/quitar2.jpg',1)">
				<img src="${ctx}/images/iconos/quitar1.jpg" style="cursor:pointer;" id="imgquitar" onclick="fc_Quitar();"
				alt="Eliminar"></a>
			</td>
		</tr>
	</table>
	<!-- botoneria -->
	
	<table align="center" border="0" bordercolor="green" style="margin-top: 6px;">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('registrarCon','','${ctx}/images/botones/registrarconf2.jpg',1)">
					<img alt="Registrar Conformidad" src="${ctx}/images/botones/registrarconf1.jpg" id="registrarCon" style="cursor:pointer;" onclick="javascript:fc_RegistrarConformidad();">
				</a>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar" style="cursor:pointer;" onclick="javascript:fc_Regresar();">
				</a>
			</td>				
		</tr>
	</table>
	<iframe id="iFrameGuiaSalida" name="iFrameGuiaSalida" frameborder="0" height="300px" width="98%" scrolling="no" style="display: none;">
	</iframe>
	</div>			
</form:form>
</body>