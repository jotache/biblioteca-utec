<%@ include file="/taglibs.jsp"%>
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">	
	function onLoad(){
		/*if(document.getElementById("txhCodFactura").value == ""){
			alert(mstrRegistrarDocPagoRelacionado);
			fc_Cancelar();
		}*/
		objMsg=document.getElementById("txhMsg");
		if ( objMsg.value == "OK_GRABAR" ){		
			alert(mstrGrabar);
			fc_Cancelar()
		}
		else
		if ( objMsg.value == "EXISTE" ){		
			alert(mstrNumeroGuiaExiste);			
		}
		else
		if ( objMsg.value == "ERROR_GRABAR" ){
			alert(mstrProblemaGrabar);
		}		
		document.getElementById("txhMsg").value="";
	}
	function fc_Cancelar(){
		window.close();
	}
	//ALQD,20/04/09. NO VALIDAR NI NROGUIA NI FECGUIA
	function fc_Grabar(){
/*		nroGuia=document.getElementById("txtNroGuia").value;
		if(nroGuia==""){
			alert(mstrIngNumeroGuia);
			return false;
		}
		
		fechaGuia=document.getElementById("txtFechaGuia").value;
		if(fechaGuia==""){
			alert(mstrIngFechaGuia);
			return false;
		}
*/
		if(confirm(mstrSeguroGrabar)){
			document.getElementById("txhOperacion").value="GRABAR";
			document.getElementById("frmMain").submit();
		}
	}
	</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/GuiaAtencionServicios.html">
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="seleccion" id="txhSeleccion" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />
	<form:hidden path="codRequerimiento" id="txhCodRequerimiento" />
	<form:hidden path="codReqDetalle" id="txhCodReqDetalle" />
	<form:hidden path="codGuia" id="txhCodGuia" />
	<form:hidden path="codGuiaDetalle" id="txhCodGuiaDetalle" />
	<form:hidden path="codEstado" id="txhCodEstado" />
	<form:hidden path="codFactura" id="txhCodFactura" />
	<form:hidden path="codOrden" id="txhCodOrden" />
	<form:hidden path="fechaOrden" id="txhFechaOrden" />
	<!-- ALQD,20/04/09. EL NUMERO Y LA FECHA DE LA GUIA DE ATENCION SERA DADA POR LA BD. -->
	<!--T�tulo de la P�gina  -->	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="590px" class="opc_combo">
		 		<font style="">		 			
				 	Gu�a de Atenci�n de Servicios
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>	
	<!-- cabecera detalle guia -->
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px;height: 30px;" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="0" >
		<tr>
			<td width="15%">Nro. Gu�a Atenci�n:</td>
			<td width="15%">				
				<form:input path="nroGuia" id="txtNroGuia"
					cssClass="cajatexto_o" cssStyle="width: 80%" readonly="true"/>
			</td>
			<td width="15%">Fecha Gu�a:</td>
			<td width="25%">
				<form:input path="fechaGuia" id="txtFechaGuia"
					cssClass="cajatexto_o" cssStyle="width: 40%" readonly="true"/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaGuia','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaGuia" alt="Calendario" style='cursor:hand;' align="absmiddle">
				</a>
			</td>		
			<td width="15%">Estado:</td>
			<td width="15%">
				<form:input path="estado" id="txtEstado"
					cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>
			</td>	
		</tr>
		<tr>
			<td width="15%">Nro. Factura :</td>
			<td width="15%">				
				<form:input path="nroFactura" id="txtNroFactura"
					cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>
			</td>
			<td width="15%">Fecha Emisi�n:</td>
			<td width="25%">
				<form:input path="fechaFactura" id="txtFechaFactura"
					cssClass="cajatexto_1" cssStyle="width: 40%" readonly="true"/>
			</td>		
			<td width="15%">Nro. O/S :</td>
			<td width="15%">
				<form:input path="nroOS" id="txtNroOS"
					cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>
			</td>	
		</tr>
		<tr>
			<td width="15%">Observaciones:
			</td>
			<td colspan="5">
				<form:textarea path="dscGuiaDetalle" id="txtDscGuiaDetalle" cssClass="cajatexto" cssStyle="width:80%;height:50px;"/>
			</td>
		</tr>
	</table>
	<!-- botoneria -->
	<table><tr height="20px"><td></td></tr></table>
	<table align="center">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar1','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar1" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a></td>										
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgexportar1','','${ctx}/images/botones/exportar2.jpg',1)">
				<img alt="Exportar" src="${ctx}/images/botones/exportar1.jpg" id="imgexportar1" style="cursor:pointer;" onclick="fc_Exportar();"></a></td>										
				<!-- <input type="button" class="boton" value="Exportar" onclick="fc_Exportar();">&nbsp;</td> -->
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgCancelar" style="cursor:pointer;" onclick="javascript:fc_Cancelar();"></a></td>
		</tr>
	</table>			
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFechaGuia",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFechaGuia",
		singleClick    :    true
	});		
</script>
</body>