<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){		
	}
	function fc_Agregar(){		
	    codUsuario=document.getElementById("txhCodUsuario").value;	
	    codNota="";
	    codRequerimiento=document.getElementById("txhCodRequerimiento").value;
	
		Fc_Popup("${ctx}/logistica/AgregarNotaSeguimiento.html?txhCodUsuario="+codUsuario+
			"&txhCodNota="+codNota+
			"&txhCodRequerimiento="+codRequerimiento,450,160);		
	}
	function fc_Quitar(){
		if (document.getElementById("txhSeleccion").value != ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhSeleccion").value="";
				document.getElementById("txhOperacion").value = "QUITAR";
				document.getElementById("frmMain").submit();
			}			
		}
		else{
			alert(mstrSeleccion);
			return false;
		}	
	}	
	function fc_Modificar(){
		codUsuario=document.getElementById("txhCodUsuario").value;	
	    codNota=document.getElementById("txhCodNota").value;
	    codRequerimiento=document.getElementById("txhCodRequerimiento").value;
	    
		seleccion=document.getElementById("txhSeleccion").value;
		
		if(seleccion==""){
			alert(mstrSeleccion);
			return;
		}
		if( confirm(mstrSeguroModificarNotaSeguimiento) ){		
			Fc_Popup("${ctx}/logistica/AgregarNotaSeguimiento.html?txhCodUsuario="+codUsuario+
				"&txhCodNota="+codNota+
				"&txhCodRequerimiento="+codRequerimiento,450,160);
			}
	}
	function fc_SeleccionarRegistro(codNota, seleccion){
		document.getElementById("txhSeleccion").value = codNota;
		document.getElementById("txhCodNota").value = codNota;
		
	}
</script>
</head>
<body MS_POSITIONING="GridLayout" topmargin="0" leftmargin="10 rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/NotasSeguimiento.html">

	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="seleccion" id="txhSeleccion" />	
	<form:hidden path="codRequerimiento" id="txhCodRequerimiento"/>
	<form:hidden path="codSede" id="txhCodSede"/>
	
	<form:hidden path="codNota" id="txhCodNota"/>
	<form:hidden path="tamListaNotas" id="txhTamListaNotas"/>
	

	<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
		<tr>
			<td>Notas de Seguimiento</td>	
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" border="0" bordercolor="red">
		<tr>
			<td>
				<div style="overflow: auto; height: 90px">	
				<table cellpadding="0" cellspacing="1" style="width:100%;border: 1px solid #048BBA" id="tablaBandeja">
					<tr>
						<td class="grilla" width="5%">Sel.</td>
						<td class="grilla" width="95%">Notas</td>
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaNotas}"  >
					<tr class="texto">		
						<td class="tablagrilla" style="width: 5%;">
							<input type="radio" name="nota"
								value='<c:out value="${lista.codNota}" />'
								id='txhCodNota<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro(this.value,'<c:out value="${loop.index}"/>');">
						</td>
						<td class="tablagrilla" style="width: 95%;">
							<textarea class="cajatexto_1" readonly style="height: 23px;width: 380px;"><c:out value="${lista.dscNota}" /></textarea>
						</td>					
					</tr>
					</c:forEach>
					<c:if test='${control.listaNotas==null}'>
					<tr class="tablagrilla">
						<td align="center" colspan="2" height="20px">No se encontraron registros
						</td>
					</tr>					 		
					</c:if>
					<c:if test='${control.tamListaNotas > 2}'>
					<script type="text/javascript">
						document.getElementById("tablaBandeja").style.width = "97%";
					</script>		 		
					</c:if>			
				</table>
				</div>
			</td>
			<td width="30px" valign="top" align="right">
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:fc_Agregar();" id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar">
				</a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/iconos/actualizar2.jpg',1)">
					<img style="cursor:pointer" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" 
						id="imgModificarPerfil" onclick="javascript:fc_Modificar();">
				</a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" 
						id="imgEliminarPerfil" onclick="javascript:fc_Quitar();">
				</a>
			</td>	
		</tr>
	</table>	
</form:form>	
</body>
</html>