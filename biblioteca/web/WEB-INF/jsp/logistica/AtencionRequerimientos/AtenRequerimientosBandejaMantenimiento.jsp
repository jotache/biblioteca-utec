<%@ include file="/taglibs.jsp"%>
<html>
<style>
.tablagrilla02
{
    /*BACKGROUND-COLOR: #CEE3F6;*/
    BACKGROUND-COLOR: #C8E8F0;
	FONT-SIZE: 8pt;
    /*TEXT-TRANSFORM: capitalize;*/
    WIDTH: 100%;
    FONT-FAMILY: Arial;
    color: #606060;
    
}
</style>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	var nroRequerimiento="";
	var objFilaAnt = null;
	var indiceGeneral = "-1";
	var codEstadoReq = "";
	var estadoAprobado = "<%=(String)request.getAttribute("estadoAprobado")%>";
	var estadoEnAtencion = "<%=(String)request.getAttribute("estadoEnAtencion")%>";
	var estadoAtencionParcial = "<%=(String)request.getAttribute("estadoAtencionParcial")%>";
	var estadoEnProceso = "<%=(String)request.getAttribute("estadoEnProceso")%>";
	estadoAprobado='0003';
	estadoEnAtencion='0005'
	estadoAtencionParcial='0006';
	estadoEnProceso='0008';
	function onLoad(){
		objMsg=document.getElementById("txhMsg");
		if ( objMsg.value == "OK_REGISTRAR_CONFORMIDAD" ){
			alert(mstrConformidadExitoRegistro);
		}
		else
		if ( objMsg.value == "ERROR_GUIA" ){
			alert(mstrErrorResponsable);
		}
		else
		if ( objMsg.value == "ERROR_REGISTRAR_CONFORMIDAD" ){
			alert(mstrProblemaGrabar);
		}
		else
		if ( objMsg.value == "ERROR_CANT_EXCEDE" ){
			alert(mstrDetaExceCantPendientes);
		}
		else
		if ( objMsg.value == "ERROR_CANT_ENTREGADA" ){
			alert(mstrStockCantEntregDispo);
		}
		
		document.getElementById("txhMsg").value="";
	}
	function fc_Limpiar(){
		document.getElementById("txtUsuSolicitante").value="";
		document.getElementById("txtNroRequerimiento").value="";
		document.getElementById("cboServicio").value="";
		document.getElementById("cboCentroCosto").value="";
	}
	function fc_Buscar(){
		document.getElementById("txhOperacion").value="BUSCAR";
		document.getElementById("frmMain").submit();
	}
	function fc_MostrarDetalleRegistro(codRequerimiento, srtIndice, codEstado){
		//asignamos el estado del requerimiento para saber si se puede confirmar
		codEstadoReq = codEstado;
		fc_ResaltarRegistro(srtIndice);
		
		tablaDetalle.style.display="";
		tablaBotones.style.display="";
		
		document.getElementById("txtDescripcion").value = document.getElementById("txh_"+srtIndice).value;
		
		codSede=document.getElementById("txhCodSede").value;
		codUsuario=document.getElementById("txhCodUsuario").value;
		
		document.getElementById("txhCodRequerimiento").value=codRequerimiento;
		//****************************************************************
		document.getElementById("iFrameNotasDeSeguimiento").src="${ctx}/logistica/NotasSeguimiento.html?txhCodRequerimiento="+codRequerimiento+
			"&txhCodUsuario="+codUsuario+
			"&txhCodSede="+codSede;
		//****************************************************************
		document.getElementById("iFrameSolicitudDeRequerimientos").src="${ctx}/logistica/SolicitudReqRel.html?txhCodRequerimiento="+codRequerimiento+
			"&txhCodUsuario="+codUsuario+
			"&txhCodSede="+codSede;
		//****************************************************************		
		codRequerimintoDetalle="";
		document.getElementById("iFrameDocumentosRelacionados").src="${ctx}/logistica/SreqDocumentosDetalle.html?txhCodRequerimiento="+codRequerimiento+
					"&txhCodDetRequerimiento="+codRequerimintoDetalle+"&txhCondicion=0";		
	}
	function fc_ResaltarRegistro(srtIndice){
		num=parseInt(srtIndice)+1;
		srtIndice=num+"";
		srtIndice=fc_Trim(srtIndice);
		trHijo=document.getElementById("tablaBandeja").rows[srtIndice];
		if(indiceGeneral!=srtIndice){
		if(indiceGeneral!="-1")
			trHijoAnt=document.getElementById("tablaBandeja").rows[indiceGeneral];
		for(var j=0;j<9;j++){
			trHijo.cells[j].className="tablagrilla02";
			if(indiceGeneral!="-1")
				trHijoAnt.cells[j].className="tablagrilla";
		}
		indiceGeneral=srtIndice;
		}
	}
	function fc_Regresar(){
		document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhCodSede").value;
	}
	function fc_AgregarSolicitud(){
		codUsuario=document.getElementById("txhCodUsuario").value;
		codRequerimiento="";
		codSede=document.getElementById("txhCodSede").value;
		codRequerimientoOri=document.getElementById("txhCodRequerimiento").value;
		
		if(codEstadoReq != ""){
			//'0003', '0005', '0006', '0008'
			if(codEstadoReq == estadoAprobado || codEstadoReq == estadoEnAtencion || codEstadoReq == estadoAtencionParcial || codEstadoReq == estadoEnProceso)
			{
				window.location.href="${ctx}/logistica/SreqBienes.html"+
						"?txhCodRequerimiento="+codRequerimiento+
						"&txhCodSede="+codSede+
						"&txhCodUsuario="+codUsuario+
						"&txhCodRequerimientoOri="+codRequerimientoOri +
						"&txhTipoProcedencia=1" ;
			}
			else{
				alert("Ya se registr� la conformidad para la \nSolicitud de Requerimiento.");
			}
		}
	}
	function fc_AsignarCargaTrabajo(){
		codUsuario=document.getElementById("txhCodUsuario").value;
		codRequerimiento=document.getElementById("txhCodRequerimiento").value;
		codSede=document.getElementById("txhCodSede").value;
		
		if(codEstadoReq != ""){
			//'0003', '0005', '0006', '0008'
			if(codEstadoReq == estadoAprobado || codEstadoReq == estadoEnAtencion || codEstadoReq == estadoAtencionParcial || codEstadoReq == estadoEnProceso)
			{
				Fc_Popup("${ctx}/logistica/log_ConsultarAsignarTrabajo.html?txhCodAlumno="+codUsuario+
					"&txhCodReq="+codRequerimiento+
					"&txhCodSede="+codSede,750,320);
			}
			else{
				alert("Ya se registr� la conformidad para la \nSolicitud de Requerimiento.");
			}
		}
		
	}
	function fc_GuiaAtencion(){
		codUsuario=document.getElementById("txhCodUsuario").value;
		codRequerimiento=document.getElementById("txhCodRequerimiento").value;		
		
		Fc_Popup("${ctx}/logistica/GuiaAtencionServicios.html?txhCodUsuario="+codUsuario+
			"&txhCodRequerimiento="+codRequerimiento,800,250);
	}
	function fc_RegistrarConformidad(){
		if(codEstadoReq != ""){
			//'0003', '0005', '0006', '0008'
			if(codEstadoReq == estadoAprobado || codEstadoReq == estadoEnAtencion || codEstadoReq == estadoAtencionParcial){
				if( confirm(mstrConfRegistrarConformidad) ){		
					document.getElementById("txhOperacion").value = "REGISTRAR_CONFORMIDAD";
					document.getElementById("frmMain").submit();
				}
			}
			else if ( codEstadoReq == estadoEnProceso ){
				alert("La rolicitud de requerimiento se encuentra en proceso, no puede ser confirmada.");
			}
			else{
				alert("Ya se registr� la conformidad para la \nsolicitud de requerimiento.");
			}
		}		
	}
	
	function fc_AsignarDescripcionActivo(){
	}
	function fc_Estado(){}
	
// Descripci�n TextArea onblur="fc_AsignarDescripcionActivo();"
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/AtenRequerimientosBandejaMantenimiento.html" enctype="multipart/form-data" method="post">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codSede" id="txhCodSede"/>
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="codRequerimiento" id="txhCodRequerimiento"/>	
	
	<form:hidden path="codPerfil" id="txhCodPerfil"/>
	<form:hidden path="codOpciones" id="txhCodOpciones"/>
	
	<form:hidden path="tamListaRequerimientos" id="txhTamListaRequerimientos"/>	

	<table cellpadding="0" cellspacing="0" border="0" width="98%">
			<tr>
				<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
			</tr>
	</table>
	<div style="overflow: auto; height: 495px;width:100%">
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:10px" width="97%">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo">
		 		<font style="">		 			
				 	Consulta de Pendientes de Atenci�n de Mantenimiento				 			 			
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px;height: 30px;" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="0" border="0" bordercolor="red">
		<tr>
			<td width="10%">&nbsp;&nbsp;&nbsp;Nro. Req. :</td>
			<td width="25%">
				<form:input path="nroRequerimiento" id="txtNroRequerimiento" cssClass="cajatexto"
                    onkeypress="fc_ValidaNumerosGuion();"
			    	onblur="fc_ValidaNumeroGuionOnblur(this, 'Nro. Req.');"
					cssStyle="width:90px" maxlength="15" />
			</td>
			<td width="10%">&nbsp;&nbsp;Tipo Servicio :</td>
			<td width="25%">
				<form:select path="codServicio" id="cboServicio" cssClass="cajatexto" cssStyle="width:230px">
					<form:option value="">-- Todos --</form:option>
						<c:if test="${control.listaServicio!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaServicio}" />
						</c:if>
				</form:select>
			</td>
			<td align="left" width="25%">
			&nbsp;&nbsp;Estado :&nbsp;
			<form:select path="codEstado" id="cboEstado" cssClass="cajatexto" 
				cssStyle="width:180px" onchange="javascript:fc_Estado();">
				<form:option  value="">--Todos--</form:option>
				<c:if test="${control.listaEstado!=null}">
				<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
					items="${control.listaEstado}" />
				</c:if>
			</form:select>	
			</td>
		</tr>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;Centro Costo:</td>
			<td>
				<form:select path="codCentroCosto" id="cboCentroCosto" cssClass="cajatexto" cssStyle="width:260px">
					<form:option value="">-- Todos --</form:option>
						<c:if test="${control.listaCentroCosto!=null}">
						<form:options itemValue="codTecsup" itemLabel="descripcion" 
							items="${control.listaCentroCosto}" />
						</c:if>
				</form:select>
			</td>	
			<td>&nbsp;&nbsp;U. Solicitante :</td>
			<td>
				<form:input path="usuSolicitante" id="txtUsuSolicitante" maxlength="30"
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaSoloLetrasFinal1(this,'U. Solicitante');" 
					cssClass="cajatexto" cssStyle="width: 80%" />
			</td>
			<td align="right" width="25%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_Limpiar();" ></a>&nbsp;					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>&nbsp;
			</td>
		</tr>
	</table>
	<!-- ALQD,21/04/09. A�ADIENDO DOS COLUMNAS MAS -->
	<!-- bandeja detalle guia -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="width:97%;margin-top:6px;margin-left:9px;">
		<tr>
			<td>
				<div style="overflow: auto; height: 180px">
				<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandeja">				
					<tr>		 
						<td class="grilla" width="7%">Nro. Req.</td>
						<td class="grilla" width="10%">Centro Costo</td>
						<td class="grilla" width="10%">U. Solicitante</td>
						<td class="grilla" width="12%">Tipo Servicio</td>
						<td class="grilla" width="20%">Nombre Servicio</td>
						<td class="grilla" width="7%">Fec. Entrega Deseada</td>
						<td class="grilla" width="5%">Esfuerzo</td>
						<td class="grilla" width="7%">Cant. Doc. Rel.</td>
						<td class="grilla" width="6%">Estado</td>
						<td class="grilla" width="7%">U.Responsable</td>
						<td class="grilla" width="7%">Cant. Apoyo</td>
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaRequerimientos}"  >
					<tr class="texto" id="trHijo${loop.index}" style="cursor: pointer;"
						ondblclick="fc_MostrarDetalleRegistro('${lista.idRequerimiento}','${loop.index}','${lista.codEstado}');">						
						<td align="center" class="tablagrilla" style="width: 10%" id="trHijo${loop.index}">
							<c:out value="${lista.nroRequerimiento}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.dscCecoSolicitante}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.usuSolicitante}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.dscTipoServicio}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 22%">
							<c:out value="${lista.nombreServicio}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.fechaEntrega}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.dscEsfuerzo}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.nroDocRel}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.dscEstado}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.usuResponsable}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.cantApoyo}" />
						</td>
					</tr>
					<input type="hidden" id="txh_<c:out value="${loop.index}" />" value="<c:out value="${lista.dscServicio}" />"/>
					</c:forEach>
					<c:if test='${control.tamListaRequerimientos=="0"}'>
						<tr class="tablagrilla">
							<td align="center" colspan="9">No se encontraron registros
							</td>
						</tr>
					</c:if>
					<c:if test='${control.tamListaRequerimientos > 5}'>
						<script type="text/javascript">
							document.getElementById("tablaBandeja").style.width = "98%";
						</script>
					</c:if>
				</table>
				</div>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="4" id="tablaDetalle" width="98%" border="0" bordercolor="blue" class="tabla2" style="display: none;margin-left:4px;">	
		<tr>			
			<td width="50%" height="120px">
				<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
					<tr>
						<td>Descripci�n</td>	
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" class="tablagrilla" style="width:100%;border: 1px solid #048BBA">
					<tr>
						<td>
							<textarea id="txtDescripcion" readonly class="cajatexto_1" style="width:400px;height:70px;" onblur="fc_AsignarDescripcionActivo();">
								
							</textarea>
						</td>	
					</tr>
				</table>							
			</td>
			<td width="50%">
				<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
					<tr>
						<td class="">Documentos Relacionados</td>	
					</tr>
				</table>
				<table cellpadding="0" cellspacing="0" style="width:100%; margin-top: 4px" >
					<tr>
						<td>
							<iframe id="iFrameDocumentosRelacionados" name="iFrameDocumentosRelacionados" frameborder="0" height="90px" width="100%" scrolling="no"></iframe>				
						</td>							
					</tr>
				</table>
			</td>				
		</tr>
		<tr>
			<td>
				<iframe id="iFrameNotasDeSeguimiento" name="iFrameNotasDeSeguimiento" frameborder="0" height="120px" width="100%" scrolling="no">
				</iframe>
			</td>
			<td>
				<iframe id="iFrameSolicitudDeRequerimientos" name="iFrameSolicitudDeRequerimientos" frameborder="0" height="120px" width="100%">
				</iframe>
			</td>
		</tr>			
	</table>
	<!-- botoneria -->
	<table><tr height="20px"><td></td></tr></table>
	<table align="center" id="tablaBotones" style="display: none;">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('agregarSol','','${ctx}/images/botones/agregarsol2.jpg',1)">
				<img alt="Agregar Solicitud" src="${ctx}/images/botones/agregarsol1.jpg" id="agregarSol" style="cursor:pointer;" onclick="javascript:fc_AgregarSolicitud();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('asignarCarga','','${ctx}/images/botones/asignarcarga2.jpg',1)">
				<img alt="Asignar Carga de Trabajo" src="${ctx}/images/botones/asignarcarga1.jpg" id="asignarCarga" style="cursor:pointer;" onclick="javascript:fc_AsignarCargaTrabajo();"></a></td>				
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('guiaatencion','','${ctx}/images/botones/guiaatencion2.jpg',1)">
				<img alt="Guia de Atencion" src="${ctx}/images/botones/guiaatencion1.jpg" id="guiaatencion" style="cursor:pointer;" onclick="javascript:fc_GuiaAtencion();"></a></td>				
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('registrarconf','','${ctx}/images/botones/registrarconf2.jpg',1)">
				<img alt="Registrar Conformidad" src="${ctx}/images/botones/registrarconf1.jpg" id="registrarconf" style="cursor:pointer;" onclick="javascript:fc_RegistrarConformidad();"></a></td>				
		</tr>
	</table>
	</div>
</form:form>
</body>
</html>