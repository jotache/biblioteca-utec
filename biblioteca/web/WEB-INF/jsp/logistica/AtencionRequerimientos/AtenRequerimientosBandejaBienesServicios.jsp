<%@ include file="/taglibs.jsp"%>
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	function onLoad(){
		document.getElementById("txhIndiceCerrar").value="";
		
		fc_BloquerTipoReq();
		
		if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteBien").value){
			TablaBien.style.display='inline-block';
	     	TablaServicio.style.display='none';
	     	TablaBandejaBien.style.display='inline-block';
	     	TablaBandejaServicio.style.display='none';
	    }
	    else{
	    	if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value){
	    		TablaServicio.style.display='inline-block';
	         	TablaBien.style.display='none';
	         	TablaBandejaBien.style.display='none';
	     	 	TablaBandejaServicio.style.display='inline-block';
	       	}
        }
        if(document.getElementById("txhMsg").value=="OK"){
        	alert(mstrCerrarRequerimiento);
        	document.getElementById("txhMsg").value="";
        	fc_Buscar();
       	}
       	else{
       		if(document.getElementById("txhMsg").value=="ERROR")
	              alert(mstrNoCerrarRequerimiento);
     	}
	   	document.getElementById("txhIdReq").value="";
	   	document.getElementById("txhMsg").value="";
	}
	//fin onLoad();
	function fc_BloquerTipoReq(){
		if(document.getElementById("txhEstadoComboTipoReq").value == "1"){
		}			
	}
	//***************************
	function fc_Etapa(param){
		
		switch(param){
			case '1':
			  	
			   	document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienConsumible").value ;
			    
			    break;
			case '2':
			   
			    document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienActivo").value ;
			  	
			   	break;	
			   	
			}
		}
	
	function fc_Limpiar()
		{
			document.getElementById("txtNroRequerimiento").value = "";
			document.getElementById("txtFecInicio").value = "";
			document.getElementById("txtFecFinal").value = "";
			document.getElementById("cboCeco").value = "-1";
			document.getElementById("codEstado").value = "-1";
			
			document.getElementById("txtUsuSolicitante").value ="";
			document.getElementById("codTipoServicio").value = "-1";
		}
	
		function fc_Regresar()
		{
			document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhCodSede").value;
		}
		
		function fc_TipoReq(){
		  
		  if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteBien").value)
		  {
		    TablaBien.style.display = 'inline-block';
			TablaServicio.style.display = 'none';
		    TablaBandejaBien.style.display='inline-block';
	        TablaBandejaServicio.style.display='none';
		   
		  }
		  else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
		 	 {
		 	    TablaBien.style.display = 'none';
				TablaServicio.style.display = 'inline-block';
		        TablaBandejaBien.style.display='none';
	            TablaBandejaServicio.style.display='inline-block';
		  	 }
		  }
		  fc_Buscar();
		}

		function fc_Buscar()
		{    srtFechaInicio=document.getElementById("txtFecInicio").value;
		     srtFechaFin=document.getElementById("txtFecFinal").value;
		     var num=0;
		     var srtFecha="0";
		     
		    if( srtFechaInicio!="" && srtFechaFin!="")
		       {  	num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
		          	if(num==1) 	srtFecha="0";
		           else srtFecha="1";
		       }
		    
		    if(srtFecha=="0")
		    	{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteBien").value)
		    	   {
		    	      document.getElementById("txhOperacion").value = "BUSCAR";
					  document.getElementById("frmMain").submit();
				   }
				  else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
				          {  document.getElementById("txhOperacion").value = "BUSCAR";
					 		 document.getElementById("frmMain").submit();
				          }
				      }
		    	}
			else alert(mstrFecha);
			
		}
		
		function fc_SeleccionarRegistro(srtIndex, srtIndReq, srtIndCerrar){
		
		    document.getElementById("txhIdReq").value=srtIndReq;
		    document.getElementById("txhIndiceCerrar").value=srtIndCerrar;
		}
		
		function fc_Atender_Requerimiento(){
			if(document.getElementById("txhIdReq").value!="")
			 {	if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteBien").value)
				{
				    	srtCodRequerimiento=document.getElementById("txhIdReq").value;
						srtCodSede=document.getElementById("txhCodSede").value;
						srtCodUsuario=document.getElementById("txhCodUsuario").value;
					
						location.href="${ctx}/logistica/AtencionRequerimientoBienes.html?txhCodSede="+
						srtCodSede+ "&txhCodUsuario=" +srtCodUsuario+ "&txhCodRequerimiento="+srtCodRequerimiento;
				}
				else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
				{
				   	 	srtCodRequerimiento=document.getElementById("txhIdReq").value;
						srtCodSede=document.getElementById("txhCodSede").value;
						srtCodUsuario=document.getElementById("txhCodUsuario").value;
					
						location.href="${ctx}/logistica/AtencionRequerimientoServicios.html?txhCodSede="+
						srtCodSede+ "&txhCodUsuario=" +srtCodUsuario+ "&txhCodRequerimiento="+srtCodRequerimiento;
				}
				
				}
			 }
			 else alert(mstrSeleccione);
		}
		
		function fc_Cerrar_Requerimiento(){
			  if(document.getElementById("txhIdReq").value!="")
			  { if(document.getElementById("txhIndiceCerrar").value=="1"){
				  if(confirm(mstrSeguroCerrarRequerimiento)){
				    document.getElementById("txhOperacion").value = "CERRAR_REQ";
					document.getElementById("frmMain").submit();
				  }
				}
				else alert(mstrNoCerrarRegistro);
			  }
			  else alert(mstrSeleccione);
		}
		
	function fc_Estado(){}
	function fc_IndPendiente(param){
	//ALQD,23/01/09. NUEVA FUNCION
		if(param=="1")
		  {  if(document.getElementById("txhValSelec1").value=="1")
		    	 document.getElementById("txhValSelec1").value="0";
		     else
		     document.getElementById("txhValSelec1").value=param;
	      }
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/AtenRequerimientosBandejaBienesServicios.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="valConsumible" id="txhValConsumible"/>
<form:hidden path="valActivo" id="txhValActivo"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="consteRadio" id="txhConsteRadio"/>
<form:hidden path="consteBien" id="txhConsteBien"/>
<form:hidden path="consteServicio" id="txhConsteServicio"/>
<form:hidden path="consteBienConsumible" id="txhConsteBienConsumible"/>
<form:hidden path="consteBienActivo" id="txhConsteBienActivo"/>
<form:hidden path="indGrupo" id="txhIndGrupo"/>
<form:hidden path="idReq" id="txhIdReq"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codProveedor" id="txhCodProveedor"/>
<form:hidden path="codPerfil" id="txhCodPerfil"/>
<form:hidden path="codOpciones" id="txhCodOpciones"/>
<form:hidden path="banListaBien" id="txhBanListaBien"/>
<form:hidden path="banListaServicio" id="txhBanListaServicio"/>
<form:hidden path="indiceCerrar" id="txhIndiceCerrar"/>
<form:hidden path="estadoAlmacen" id="txhEstadoAlmacen"/>
<form:hidden path="consteEstadoAlmacen" id="txhConsteEstadoAlmacen"/>
<form:hidden path="indSalPorConfirmar" id="txhValSelec1"/>

<form:hidden path="estadoComboTipoReq" id="txhEstadoComboTipoReq"/>

<table cellpadding="0" cellspacing="0" border="0" width="99%">
	<tr>
		<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
		<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px">
	 <tr>
	 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
	 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="775px" class="opc_combo">
	 		<font style="">Atencion de Requerimientos Bienes - Servicios</font></td>
	 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
	 </tr>
</table>
<table cellpadding="1" cellspacing="0" style="margin-top:6px;width:99% ;margin-left: 9px" 
	background="${ctx}/images/Logistica/back.jpg" class="tabla" border="0" bordercolor="red">
	<tr>
		<td class="" width="12%" nowrap>&nbsp;&nbsp;Tipo Req.:</td>
		<td width="16%"><form:select path="codTipoRequerimiento" id="codTipoRequerimiento" cssClass="combo_o" 
						cssStyle="width:110px" onchange="javascript:fc_TipoReq();" >
						<c:if test="${control.listCodTipoRequerimiento!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listCodTipoRequerimiento}" />
						</c:if>
					</form:select>
		</td>
		<td width="18%" > 
		    <table id="TablaBien" name="TablaBien" bordercolor="blue" border="0" width="100%"
				style="display: none;">
			  <tr>
				<td align="left" width="100%"><input Type="radio" checked name="rdoEtapa"
				    <c:if test="${control.consteRadio==control.consteBienConsumible}"><c:out value=" checked=checked " /></c:if>
					id="gbradio1" onclick="javascript:fc_Etapa('1');">&nbsp;Consumible&nbsp;
					<input Type="radio" name="rdoEtapa"
					<c:if test="${control.consteRadio==control.consteBienActivo}"><c:out value=" checked=checked " /></c:if>
					id="gbradio2" onclick="javascript:fc_Etapa('2');">&nbsp;Activo&nbsp;
				</td>   	
			  </tr>
			</table>
       </td>
		<td class="" width="10%" align="left" nowrap>Nro. Req.:</td>
		<td width="12%">
			<form:input path="txtNroRequerimiento" id="txtNroRequerimiento" cssClass="cajatexto"
	            onkeypress="fc_ValidaNumerosGuion();"
				onblur="fc_ValidaNumeroGuionOnblur(this, 'Nro. Req.');"
				cssStyle="width:90px" maxlength="15" />
		</td>				
		<td class="" width="9%" align="right">Fec. Req.:</td>
		<td width="25%">
		<table width="100%" border="0">
		<tr>
			<td nowrap>
				<form:input path="fecInicio" id="txtFecInicio" cssClass="cajatexto"
					onkeypress="javascript:fc_Slash('frmMain','txtFecInicio','/');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio');"  
					cssStyle="width:70px" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
					align="middle" alt="Calendario" style="cursor:pointer;"></a>
			</td>
			<td nowrap>
				<form:input path="fecFinal" id="txtFecFinal" cssClass="cajatexto"
					onkeypress="javascript:fc_Slash('frmMain','txtFecFinal','/');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal');"  
					cssStyle="width:70px" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
					align="middle" alt="Calendario" style="cursor:pointer;"></a>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="" nowrap>&nbsp;&nbsp;Centro Costo:</td>
		<td colspan="2" >
			<form:select path="codCeco" id="cboCeco" cssClass="cajatexto" 
				cssStyle="width:360px">
				<form:option value="-1">-- Todos --</form:option>
				<c:if test="${control.listCeco!=null}">
				<form:options itemValue="codTecsup" itemLabel="descripcion" 
					items="${control.listCeco}" />
				</c:if>
			</form:select>
		</td>
		<td class="" align="left">U. Solicitante :</td>
		<td colspan="2">
			<form:input path="usuSolicitante" id="txtUsuSolicitante" maxlength="40"
				onkeypress="fc_ValidaTextoEspecial();"
				onblur="fc_ValidaSoloLetrasFinal1(this,'U. Solicitante');" 
				cssClass="cajatexto" size="40" />
		</td>
		<td >
		<table width="100%" border="0">
		<tr>
			<td nowrap width="20%">Nro. Gu�a:</td>
			<td width="80%">
				<form:input path="nroGuia" id="nroGuia" maxlength="15"
					cssClass="cajatexto" size="15" />
			</td>
		</tr>
		</table>
		</td>
	</tr>			
	<tr>
		<td class="">&nbsp;&nbsp;Estado</td>
		<td ><form:select path="codEstado" id="codEstado" cssClass="cajatexto" 
						cssStyle="width:180px" onchange="javascript:fc_Estado();">
			<form:option  value="-1">--Todos--</form:option>
				<c:if test="${control.listaCodEstado!=null}">
				<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
					items="${control.listaCodEstado}" />
				</c:if>
			</form:select>					
		</td>
		<td colspan="4" align="left">
			<table id="TablaServicio" name="TablaServicio" style="display: none;width:100%;">
				<tr>
					<td align="left" nowrap>Tipo Servicio:</td>
					<td>
						<form:select path="codTipoServicio" id="codTipoServicio" cssClass="cajatexto" 
							cssStyle="width:300px">
						<form:option value="-1">-- Todos --</form:option>
						<c:if test="${control.listaCodTipoServicio!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaCodTipoServicio}" />
						</c:if>
						</form:select>
					</td>
				</tr>
			</table>
		</td>
<!-- ALQD,23/01/09. AGREGANDO NUEVO FILTRO DE BUSQUEDA -->
		<td align="right">
			<table width="100%" border="0">
			<tr>
				<td nowrap>
					<input Type="checkbox" name="checkbox1"
				    <c:if test="${control.indSalPorConfirmar==control.consteIndSalPorConfirmar}"><c:out value=" checked=checked " /></c:if>
					id="checkbox1" onclick="javascript:fc_IndPendiente('1');">G.S. Por Confirmar
				</td>
				<td npwrap>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
						 style="cursor: pointer; 60px" 
						 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
				</td>
				<td nowrap>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle" 
						 style="cursor: pointer; margin-right: " onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
				</td>
			</tr>
			</table>
		</td>
	</tr>
</table>
		
		<table cellpadding="0" cellspacing="2"  style="display: none; margin-top: 6px" id="TablaBandejaBien" 
		        name="TablaBandejaBien" width="100%" border="0" bordercolor="red">
			<tr>
				<td>
					<div style="overflow: auto; height:230px;width:100%">
			 		<table cellpadding="0" cellspacing="1" bordercolor="red" border="0"
					 style="border: 1px solid #048BBA;width:97%;margin-left:9px">
					
						<tr>							
							<td class="grilla" width="3%">Sel.</td>
							<td class="grilla" width="12%">Nro. Req.</td>
							<td class="grilla" width="15%">Fecha Req.</td>
							<td class="grilla" width="30%">Centro Costo</td>							
							<td class="grilla" width="20%">Usuario Solicitante</td>							
							<td class="grilla" width="20%">Estado</td>

						</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaBandejaBienes}"  >
					<tr class="tablagrilla">
						<td align="center" class="tablagrilla" style="width: 3%"><input type="radio" name="producto"
								id='valBi<c:out value="${loop.index}"/>'
						onclick="fc_SeleccionarRegistro('<c:out value="${loop.index}"/>',
					    '<c:out value="${lista.idRequerimiento}" />','<c:out value="${lista.indiceCerrar}" />');">
						</td>
						<td align="center" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.nroRequerimiento}" /> 						
						</td>
						<td align="center" class="tablagrilla" style="width: 15%">							
							<c:out value="${lista.fechaEmision}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 30%">
							<c:out value="${lista.dscCecoSolicitante}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.usuSolicitante}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.dscEstado}" /> 							
						</td>
											
					</tr>
					</c:forEach>
					
					<c:if test='${control.banListaBien=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="6" height="20px">No se encontraron registros
						</td>
					</tr>					 		
					</c:if>
											
					</table>
					</div>
				</td>				
			</tr>
		</table>
		
		<table cellpadding="0" cellspacing="2"  style="display: none; margin-top: 6px"  id="TablaBandejaServicio" 
		       name="TablaBandejaServicio" width="100%">
			<tr>
				<td>
				   <div style="overflow: auto; height:230px;width:100%">
			 		<table cellpadding="0" cellspacing="1" class="" bordercolor="red" border="0"
					 style="border: 1px solid #048BBA;width:97%;margin-left:10px">
						<tr>							
							<td class="grilla" style="width:2%;" >Sel.</td>
							<td class="grilla">Nro. Req.</td>
							<td class="grilla">Fecha Req.</td>
							<td class="grilla">Centro Costo</td>							
							<td class="grilla">Usuario Solicitante</td>							
							<td class="grilla">Monto</td>
							<td class="grilla">Estado</td>
							

						</tr>
						<c:forEach varStatus="loop" var="lista" items="${control.listaBandejaServicios}"  >
					<tr class="tablagrilla">
						<td width="2%" align="center"><input type="radio" name="producto"
								id='valSe<c:out value="${loop.index}"/>'
						onclick="fc_SeleccionarRegistro('<c:out value="${loop.index}"/>',
					    '<c:out value="${lista.idRequerimiento}" />','<c:out value="${lista.indiceCerrar}" />');">
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.nroRequerimiento}" /> 						
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">							
							<c:out value="${lista.fechaEmision}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.dscCecoSolicitante}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 14%">
							<c:out value="${lista.usuSolicitante}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.totalRequerimiento}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.dscEstado}" /> 							
						</td>
											
					</tr>
					</c:forEach>
					
					<c:if test='${control.banListaServicio=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="7" height="20px">No se encontraron registros
						</td>
					</tr>					 		
					</c:if>
								
					</table>
					</div>
				</td>				
			</tr>			
		</table>
		
		<table align="center" style="margin-top: 6px;">	
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('atenderReq','','${ctx}/images/botones/atenderreq2.jpg',1)">
						<img alt="Atender Requerimiento" src="${ctx}/images/botones/atenderreq1.jpg" id="atenderReq" style="cursor:pointer;" onclick="fc_Atender_Requerimiento();">
					</a>				
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('cerrarReq','','${ctx}/images/botones/cerrarreq2.jpg',1)">
						<img alt="Cerrar Requerimiento" src="${ctx}/images/botones/cerrarreq1.jpg" id="cerrarReq" style="cursor:pointer;" onclick="fc_Cerrar_Requerimiento();">
					</a>
				</td>	
			</tr>			
		</table>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});

	
	
</script>
</body>
</html>