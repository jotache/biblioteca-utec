<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	function onLoad(){
	
		document.getElementById("txhCodRequerimiento").value="";
		document.getElementById("txhCodBDEstado").value="";
		 if(document.getElementById("txhMsg").value=="ORDEN_CERRADA_OK")
		  { alert(mstrCerrarOrden);
		    document.getElementById("txhMsg").value="";
		    fc_Buscar();
		    
		  }
		 else{ if(document.getElementById("txhMsg").value=="CERRAR_ORDEN_ERROR")
		          alert(mstrNoCerrarOrden);
		 }
		document.getElementById("txhMsg").value=""; 
		
			
		var permisos = $('txhCodPerfil').value;
		if (trim($('txhOperacion').value)=='')
			if(permisos.indexOf('LCOMP')!=-1 && permisos.indexOf('LJLOG')!=-1){				
					document.forms['frmMain']['codEstado'].value = '0002';
					document.forms['frmMain']['codTipoOrden'].value = '0001';
			}else if(permisos.indexOf('LJMAN')!=-1){
					document.forms['frmMain']['codEstado'].value = '0002';
					document.forms['frmMain']['codTipoOrden'].value = '0002';		
			}			
	
	}
	
	function fc_Limpiar()
		{
			document.getElementById("nroOrden").value = "";
			document.getElementById("txtFecInicio").value = "";
			document.getElementById("txtFecFinal").value = "";
			document.getElementById("codEstado").value = "-1";
			document.getElementById("nroRucProveedor").value = "";
			document.getElementById("nombreProveedor").value = "";
			document.getElementById("nroRucComprador").value = "";
			document.getElementById("nombreComprador").value = "";
			document.getElementById("codTipoOrden").value = "-1"; 
		}
	
	function fc_Regresar()
		{
			document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhCodSede").value;
		}
		
	function fc_Buscar()
		{    srtFechaInicio=document.getElementById("txtFecInicio").value;
		     srtFechaFin=document.getElementById("txtFecFinal").value;
		     var num=0;
		     var srtFecha="0";
		     if( srtFechaInicio!="" && srtFechaFin!="")
		       {   
		           num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
		           //num=1: si el primer parametro es mayor al segundo parametro
		           //num=0: si el segundo parametro es mayor o igual al primer parametro
		           //alert(num);
		           if(num==1) 	srtFecha="0";
		           else srtFecha="1";	    
		       }
		     
		    if(srtFecha=="0")   
		    	{	
		    	    document.getElementById("txhOperacion").value = "BUSCAR";
					document.getElementById("frmMain").submit();
		    	}
			else alert(mstrFecha);
			
		}

	function fc_SeleccionarRegistro(srtCodigo, srtCodEstado, srtIndiceApro){
		 
		 document.getElementById("txhCodRequerimiento").value=srtCodigo;
		 document.getElementById("txhCodBDEstado").value=srtCodEstado;
		 document.getElementById("txhIndiceAprobacion").value=srtIndiceApro;
		 
		}

	function verOrden(codOrden){
		
	}
	
	//ALQD,05/02/09. SE A�ADE PARAMETRO DE NUMCOTIZACION
	function fc_Ver_Orden(){
		if(document.getElementById("txhCodRequerimiento").value!="") {		
			srtCodOrden=document.getElementById("txhCodRequerimiento").value;
			srtCodSede=document.getElementById("txhCodSede").value;
			srtCodUsuario=document.getElementById("txhCodUsuario").value;
			srtCodPerfil=document.getElementById("txhCodPerfil").value;
			srtCodOpciones=document.getElementById("txhCodOpciones").value;
			srtBanderaPerfil=document.getElementById("txhBanderaPerfil").value;
			srtCodEstado=document.getElementById("txhCodBDEstado").value;
				
			
			//ALQD,21/10/08. SE MOSTRARA EN VENTANAS DIFERENTES PARA EL COMPRADOR Y APROBADOR					
			if((srtCodPerfil.indexOf("LCOMP")>-1 && srtCodPerfil.indexOf("LJLOG")==-1 && srtCodPerfil.indexOf("LJMAN")==-1 ) || srtCodPerfil.indexOf("LCONS")>-1){
				location.href="${ctx}/logistica/GestOrdenDetalleOrden.html?txhCodSede="+srtCodSede+ "&txhCodUsuario="
				+srtCodUsuario+ "&txhCodPerfil="+srtCodPerfil+ "&txhCodOpciones="+srtCodOpciones+
				"&txhCodOrden="+srtCodOrden+ "&txhBanderaPerfil="+srtBanderaPerfil+ "&txhCodEstado="+srtCodEstado+
				"&txhIndiceAprob="+document.getElementById("txhIndiceAprobacion").value;
			}else{
				location.href="${ctx}/logistica/GestOrdenDetalleOrden_Aprob.html?txhCodSede="+srtCodSede+ "&txhCodUsuario="
				+srtCodUsuario+ "&txhCodPerfil="+srtCodPerfil+ "&txhCodOpciones="+srtCodOpciones+
				"&txhCodOrden="+srtCodOrden+ "&txhBanderaPerfil="+srtBanderaPerfil+ "&txhCodEstado="+srtCodEstado+
				"&txhIndiceAprob="+document.getElementById("txhIndiceAprobacion").value;
			}
		}
		else alert(mstrSeleccione);
	}
	
	function fc_Cerrar_Orden(){
	    
	  if( document.getElementById("txhCodBDEstado").value==document.getElementById("txhCodEstadoAprobado").value)
	  {				
	  		document.getElementById("txhOperacion").value = "CERRAR_ORDEN";
			document.getElementById("frmMain").submit();
	    
	  }
	  else alert(mstrNoCerrarOrdenBandeja);
	}
	
	function fc_Excel(){
	
	dscTipoOrden=frmMain.codTipoOrden.options[frmMain.codTipoOrden.selectedIndex].text.replace("--","");
	dscTipoOrden=dscTipoOrden.replace("--","");
	dscEstado=frmMain.codEstado.options[frmMain.codEstado.selectedIndex].text.replace("--","");
	dscEstado=dscEstado.replace("--","");
	url="?tCodRep=" + fc_Trim(document.getElementById("txhCodTipoReporte").value) +
					"&tCodSede=" + fc_Trim(document.getElementById("txhCodSede").value) +
					"&tCodUsu=" + fc_Trim(document.getElementById("txhCodUsuario").value) + 
					"&tCPerfil=" + fc_Trim(document.getElementById("txhCodPerfil").value) +  
					"&tNOr=" + fc_Trim(document.getElementById("nroOrden").value) +
					"&tFIni=" + fc_Trim(document.getElementById("txtFecInicio").value) + 
					"&tFFin=" + fc_Trim(document.getElementById("txtFecFinal").value) + 
					"&tRucP=" + fc_Trim(document.getElementById("nroRucProveedor").value) + 
					"&tNomP=" + fc_Trim(document.getElementById("nombreProveedor").value) +
					"&tRucC=" + fc_Trim(document.getElementById("nroRucComprador").value) +
					"&tNomC=" + fc_Trim(document.getElementById("nombreComprador").value) +
					"&tCTOr=" + fc_Trim(document.getElementById("codTipoOrden").value) +
					"&tDTOr=" + fc_Trim(dscTipoOrden) +
					"&tCEst=" + fc_Trim(document.getElementById("codEstado").value) +
					"&tDEst=" +  fc_Trim(dscEstado);
	                
	window.open("${ctx}/logistica/reportesLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");
	
	
	}
	
	function fc_Exportar(){
		if(document.getElementById("txhCodRequerimiento").value!="")
	{	
	codRep = "0023";
	url="?tCodRep=" + codRep + 
				"&srtCodOrden=" + fc_Trim(document.getElementById("txhCodRequerimiento").value) +
				"&txhCodSede=" + document.getElementById("txhCodSede").value;

	window.open("${ctx}/logistica/reportesLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");

	}
	else alert(mstrSeleccione);
	}
	
	function fc_repOCxEntregar(){

		srtCodPerfil=document.getElementById("txhCodPerfil").value;
		if (srtCodPerfil.indexOf("LCOMP")>0){
			codRep = "0025";
			url="?tCodRep=" + codRep +
				"&codUsuario=" + fc_Trim(document.getElementById("txhCodUsuario").value);
			window.open("${ctx}/logistica/reportesLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");
		}else{
			alert('Funci�n disponible solo para compradores');
		}
		
	}	
	
	//ALQD,14/07/09.DELEGAR APROBACION A OTRO DIRECTOR
	function fc_Delegar(){
		srtCodAprob=document.getElementById("txhCodUsuario").value;
		param="?prmCodUsuario=" + srtCodAprob+
			"&prmCodSede="+document.getElementById("txhCodSede").value+
			"&prmTipoAprobacion="+"1";
		Fc_Popup("${ctx}/logistica/gestAprobadorOrdenSuplente.html"+param,600,300);
	}
</script>
</head>
<body>
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/GestOrdenBandejaConsultaOrden.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codPerfil" id="txhCodPerfil"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codOpciones" id="txhCodOpciones"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="codRequerimiento" id="txhCodRequerimiento"/>
<form:hidden path="codBDEstado" id="txhCodBDEstado"/>
<form:hidden path="codEstadoAprobado" id="txhCodEstadoAprobado"/>
<form:hidden path="banderaPerfil" id="txhBanderaPerfil"/>
<form:hidden path="banListaBandeja" id="txhBanListaBandeja"/>
<form:hidden path="dscUsuario" id="txhDscUsuario"/>
<form:hidden path="codTipoReporte" id="txhCodTipoReporte"/>
<form:hidden path="nomSede" id="txhNomSede"/>
<form:hidden path="indiceAprobacion" id="txhIndiceAprobacion"/>
	<!-- ALQD,04/03/09. CON LA PROPIEDAD INDPERCOMPRADOR SE CONTROLARA EL PARAMETRO ESTADO PARA INDPERCOMPRADOR=1, SE MOSTRARA TODOS Y PARA 0 EL ESTADO EN APROBACION -->
	<!-- BOTON REGRESAR -->
	<!-- ALQD,11/02/09.REDIMENSIONANDO LAS COLUMNAS PARA DAR MAS TAMA�O AL DET. ESTADO -->
	<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>
	<!-- TITULO -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo"><font style="">Consulta de Ordenes</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<!-- CABECERA -->
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2" >
			<tr>
				<td class="" width="10%">Nro. Orden</td>
				<td width="35%"><form:input path="nroOrden" id="nroOrden" cssClass="cajatexto"
								onkeypress="fc_ValidaNroOrden();" 
								onblur="fc_ValidaNroOrdenOnblur(this, 'Nro. Orden.');"
								cssStyle="width:90px" maxlength="15" />
				</td>				
				<td class="" width="10%">Fec. Emisi�n:</td>
				<td width="30%">
					<form:input path="fecInicio" id="txtFecInicio" cssClass="cajatexto"
								onkeypress="javascript:fc_Slash('frmMain','txtFecInicio','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
								align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>&nbsp;&nbsp;
							<form:input path="fecFinal" id="txtFecFinal" cssClass="cajatexto"
								onkeypress="javascript:fc_Slash('frmMain','txtFecFinal','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
								align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>
				</td>				
							
				<td class="" width="7%">Estado</td>
				<td width="18%"><form:select path="codEstado" id="codEstado" cssClass="cajatexto" 
								cssStyle="width:100px">
								<form:option value="-1">-- Todos --</form:option>
								<c:forEach items="${control.listaEstado}" var="lista">
									<option value="${lista.codTipoTablaDetalle}"
										<c:if test="${control.indPerComprador=='0' && control.codEstado==null}">
											<c:if test="${lista.codTipoTablaDetalle=='0002'}">
											selected</c:if>
										</c:if>
										<c:if test="${control.indPerComprador=='2' && control.codEstado==null}">
											<c:if test="${lista.codTipoTablaDetalle=='0003'}">
											selected</c:if>
										</c:if>
										<c:if test="${control.codEstado!=null}">
											<c:if test="${lista.codTipoTablaDetalle==control.codEstado}">
											selected</c:if>
										</c:if>
									>
									<c:out value="${lista.descripcion}"/>
									</option>
								</c:forEach>
							</form:select>
				</td>
			</tr>
			<tr>
				<td class="">Proveedor</td>
				<td>
					<form:input path="nroRucProveedor" id="nroRucProveedor" cssClass="cajatexto"
								onkeypress="fc_PermiteNumeros();"
								onblur="fc_ValidaNumeroOnBlur(this.id);"
								cssStyle="width:70px" maxlength="10" />&nbsp;
					<form:input path="nombreProveedor" id="nombreProveedor" cssClass="cajatexto"
								onkeypress="fc_ValidaTextoEspecial();"
								onblur="fc_ValidaSoloLetrasFinal1(this,'Proveedor');"
								cssStyle="width:230px" maxlength="50" />
				</td>				
				<td class="">Comprador</td>
				<td colspan="3">
					<form:input path="nroRucComprador" id="nroRucComprador" cssClass="cajatexto"
								onkeypress="fc_PermiteNumeros();"
								onblur="fc_ValidaNumeroOnBlur(this.id);"
								cssStyle="width:70px" maxlength="10" />&nbsp;
					<form:input path="nombreComprador" id="nombreComprador" cssClass="cajatexto"
								onkeypress="fc_ValidaTextoEspecial();"
								onblur="fc_ValidaSoloLetrasFinal1(this, 'Comprador');"
								cssStyle="width:230px" maxlength="50" />
				</td>
			</tr>
			<tr>
				<td class="">Tipo Orden</td>
				<td ><form:select path="codTipoOrden" id="codTipoOrden" cssClass="cajatexto" 
								cssStyle="width:100px">
								<form:option value="-1">-- Todos --</form:option>
								<c:if test="${control.listaCodTipoOrden!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
									items="${control.listaCodTipoOrden}"/>
								</c:if>
					</form:select>
				</td>
				<td colspan="2" align="right" >
				
					<a href="javascript:fc_repOCxEntregar();" >
						Reporte de OCs Pendiente de Entrega
					</a>

				</td>
				<td colspan="2" align="right">	
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
							<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
								 style="cursor: pointer; 60px" 
								 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
							
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
							<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle" 
								 style="cursor: pointer; margin-bottom: " onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgExcel','','${ctx}/images/iconos/exportarex2.jpg',1)">
							<img src="${ctx}/images/iconos/exportarex1.jpg" alt="Exportar" align="middle" 
								 style="cursor: pointer; margin-right" 
								 onclick="javascript:fc_Excel();" id="imgExcel"></a>
					
					</td>
			</tr>
		</table>
<!-- ALQD,04/02/09.SI EL ESTADO ES APROBADO ENTONCES SE MOSTRARA LA FECHA DE APROBACION -->
		<table cellpadding="0" cellspacing="0" width="97%" style="margin-top: 6px; margin-left: 9px;"
			border="0" bordercolor="red">
			<tr>
				<td>
				<div style="overflow: auto; height:300px;width:100%">
					<table cellpadding="0" cellspacing="1" class="" bordercolor="red" border="0"
					 style="border: 1px solid #048BBA;width:98%">
						<tr>
							<td class="grilla" width="4%">Sel.</td>
							<td class="grilla">Nro. Orden.</td>
							<c:if test="${control.codEstado==null || control.codEstado==''}">
							<td class="grilla">Fec.Em./Apr.</td>
							</c:if>
							<c:if test="${control.codEstado!='0003' && control.codEstado!=null && control.codEstado!=''}">
							<td class="grilla">Fec. Emisi�n</td>
							</c:if>
							<c:if test="${control.codEstado=='0003'}">
							<td class="grilla">Fec. Aprob.</td>
							</c:if>
							<td class="grilla">Proveedor</td>
							<td class="grilla">Mon.</td>
							<td class="grilla">Monto</td>
							<td class="grilla">Comprador</td>
							<td class="grilla">Tipo Orden</td>
							<td class="grilla">Caja Chica</td>
							<td class="grilla">Estado</td>
							<td class="grilla">Detalle Estado (Ult.Fec.Aprob.)</td>
						</tr>
							
					<c:forEach varStatus="loop" var="lista" items="${control.listaBandeja}"  >
					<tr class="texto" >
					   <td align="center" class="tablagrilla" style="width: 4%">
							<input type="radio" name="producto"
								id='valBa<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro('<c:out value="${lista.codOrden}" />'
								,'<c:out value="${lista.codEstado}" />','<c:out value="${lista.indiceAprobacion}" />');">
							
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<a href="#" onclick="fc_SeleccionarRegistro('<c:out value="${lista.codOrden}" />','<c:out value="${lista.codEstado}" />','<c:out value="${lista.indiceAprobacion}" />');fc_Ver_Orden();">							
							<c:out value="${lista.dscNroOrden}" />
							</a>
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.fechaEmision}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.dscProveedor}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 3%">
							<c:out value="${lista.dscMoneda}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 6%">
							<c:out value="${lista.dscMonto}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 14%">
							<c:out value="${lista.descripcion}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 6%">
							<c:out value="${lista.dscTipoOrden}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 4%">
							<c:out value="${lista.cajaChica}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.dscEstado}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 25%">
							<c:out value="${lista.detalleCondicion}" />
						</td>
											
					</tr>
					</c:forEach>
					
					<c:if test='${control.banListaBandeja=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="10" height="20px">No se encontraron registros
						</td>
					</tr>
					</c:if>
					
					</table>
				  </div>
				</td>
			</tr>
		</table>
		<br>
		<table><tr height="5px"><td></td></tr></table>
		<table align="center">
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('verOrden','','${ctx}/images/botones/verorden2.jpg',1)">
						<img alt="Ver Orden" src="${ctx}/images/botones/verorden1.jpg" id="verOrden" style="cursor:pointer;" onclick="fc_Ver_Orden();">
					</a>
				<td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('exportar','','${ctx}/images/botones/exportar2.jpg',1)">
						<img alt="Exportar" src="${ctx}/images/botones/exportar1.jpg" id="exportar" style="cursor:pointer;" onclick="fc_Exportar();">
					</a>
				<td>
				<td>
					<c:if test="${control.banderaPerfil=='1' && control.indPerComprador!='2'}">
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('cerrarOrden','','${ctx}/images/botones/cerrarorden2.jpg',1)">
							<img alt="Cerrar Orden" src="${ctx}/images/botones/cerrarorden1.jpg" id="cerrarOrden" style="cursor:pointer;" onclick="fc_Cerrar_Orden();">
						</a>					
					</c:if>
				<td>			
				<c:if test="${control.indDirector=='1'}">
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('delegar','','${ctx}/images/botones/delegar2.jpg',1)">
						<img alt="Delegar Aprobaci�n" src="${ctx}/images/botones/delegar1.jpg" id="delegarAprob" style="cursor:pointer;" onclick="fc_Delegar();">
					</a>
				<td>
				</c:if>
			</tr>
		</table>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
</body>
</html>