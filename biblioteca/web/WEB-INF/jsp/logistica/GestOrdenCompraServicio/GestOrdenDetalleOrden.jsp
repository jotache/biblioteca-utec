<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	function onLoad(){
	//alert(document.getElementById("txhCodEstado").value+">><<"+document.getElementById("txhConsteCodEstadoEnAprobacion").value);
	 	if(document.getElementById("txhMsg").value=="APROBAR_OK"){
	  		alert(mstrAprobarDetalle);
	    	document.getElementById("txhCodBandera").value="1";
	    	document.getElementById("txhMsg").value="";
	    	fc_Regresar();
	  	}
	 	else{ if(document.getElementById("txhMsg").value=="APROBAR_ERROR")
	       { alert(mstrNoAprobarDetalle);
	       }
	       else{ if(document.getElementById("txhMsg").value=="RECHAZAR_OK")
	              { alert(mstrRechazarDetalle);
	                document.getElementById("txhCodBandera").value="1";
	                document.getElementById("txhMsg").value="";
	 				fc_Regresar();
	              }
	              else{ if(document.getElementById("txhMsg").value=="RECHAZAR_ERROR")
	                     { alert(mstrNoRechazarDetalle);
	                     }
	              }
	       }
	 }
	 document.getElementById("txhMsg").value="";
	 subTotal=parseFloat(document.getElementById("txtSubTotal").value);
	 document.getElementById("txtSubTotal").value=redondea(subTotal,2); 
	 
	 subTotal2=parseFloat(document.getElementById("txtIgv").value);
	 document.getElementById("txtIgv").value=redondea(subTotal2,2);
	 
	 subTotal3=parseFloat(document.getElementById("txtMoneda").value);
	 document.getElementById("txtMoneda").value=redondea(subTotal3,2);
	 
	 //ALQD,19/02/09.AGREGADO PARA INDINVERSION EN OC
	 if(document.getElementById("txhIndInversion").value=="1")
		document.getElementById("chkIndInversion").checked=true;

		//MODIFICADO RNAPA 21/08/2008
		indImportacion=document.getElementById("txhIndImportacion").value;
		importeOtros=document.getElementById("txtImporteOtros").value;		
		if(indImportacion=="1"){
			document.getElementById("tdIgv01").style.display="none";
			document.getElementById("tdIgv02").style.display="none";
			document.getElementById("tdOtros01").style.display="";
			document.getElementById("tdOtros02").style.display="";
		
		}		
	}
	
	function redondea(sVal, nDec){ 
	    var n = parseFloat(sVal); 
	    var s = "0.00"; 
	    if (!isNaN(n)){ 
			n = Math.round(n * Math.pow(10, nDec)) / Math.pow(10, nDec); 
			s = String(n); 
			s += (s.indexOf(".") == -1? ".": "") + String(Math.pow(10, nDec)).substr(1); 
			s = s.substr(0, s.indexOf(".") + nDec + 1); 
	    } 
	    return s; 
	}
	
	function fc_Regresar(){
	srtCodSede=document.getElementById("txhCodSede").value;
	srtCodUsuario=document.getElementById("txhCodUsuario").value;
	srtCodPerfil=document.getElementById("txhCodPerfil").value;
	srtCodOpciones=document.getElementById("txhCodOpciones").value;
	
	location.href="${ctx}/logistica/GestOrdenBandejaConsultaOrden.html?txhCodSede="+srtCodSede; /*+
	"&txhCodUsuario="+srtCodUsuario/*+"&txhCodPerfil="+srtCodPerfil+"&txhCodOpciones="+srtCodOpciones;*/
	}
	
	function fc_Aprobar(){
		form = document.forms[0];
		//alert(document.getElementById("txhCodOrden").value);
		if(document.getElementById("txhCodEstado").value==document.getElementById("txhConsteCodEstadoEnAprobacion").value)  
		{ if(confirm(mstrSegurAprobar)){
			form.imgAprobar01.disabled=true;
			document.getElementById("txhOperacion").value = "APROBAR";
		    document.getElementById("frmMain").submit();
		  }else
			  form.imgAprobar01.disabled=false;
		}
		else alert(mstrNoAprobarOrden);
	}
	
	function fc_Rechazar(){
		form = document.forms[0];
					//alert(document.getElementById("txhCodOrden").value);
		if(confirm(mstrSeguroRechazar)){
			form.imgRechazar01.disabled=true;
		    document.getElementById("txhOperacion").value = "RECHAZAR";
			document.getElementById("frmMain").submit();
		}else
			form.imgRechazar01.disabled=false;
		
	}
//ALQD,18/08/08. PARA MOSTRAR LAS COTIZACIONES ASOCIADAS A LA O/C
//ALQD,05/02/09. ENVIANDO EL NUCOTIZACION
	function fc_verCotizacion(){
		var sURL="${ctx}/logistica/verCotizaion.html"
		+ "?prmUsuario=" + document.getElementById("txhCodUsuario").value 
		+ "&prmCodSede=" +  document.getElementById("txhCodSede").value
		+ "&prmCodCotizacion="+document.getElementById("txhCodCotizacion").value 
		+ "&txhCodProveedor="+document.getElementById("txhCodUsuario").value
		+ "&prmCodEstCotizacion="+document.getElementById("txhCodEstCotizacion").value
		+ "&txhCodTipoReq="+document.getElementById("txhCodTipoReq").value
		+ "&txhNumCotizacion="+document.getElementById("txhNumCotizacion").value;
		var opciones = "fullscreen=0" + ",toolbar=0" +
	        ",location=0" + ",status=1" +
	        ",menubar=0" + ",scrollbars=0" +
	        ",resizable=0" + ",width=900" +
	        ",height=600" + ",left=100" + ",top=50"; 
     	var ventana = window.open(sURL,"Cotizaciones",opciones);  
	}
		
	function fc_verAprobacion(){
		//alert(document.getElementById("txhCodOrden").value);
		var sURL="${ctx}/logistica/verAutorizaciones.html"
		+ "?prmCodOrden="+document.getElementById("txhCodOrden").value
		+ "&txhCodTipoReq="+document.getElementById("txhCodTipoReq").value;
		
		var opciones = "fullscreen=0" + ",toolbar=0" +
	        ",location=0" + ",status=1" +
	        ",menubar=0" + ",scrollbars=0" +
	        ",resizable=0" + ",width=500" +
	        ",height=400" + ",left=50" + ",top=80"; 
     	var ventana = window.open(sURL,"Aprobadores",opciones);  
	}

	function fc_verSolicitantes(){
		var sURL="${ctx}/logistica/verSolicitantesOrdenCompra.html"
			+ "?prmCodOrden="+document.getElementById("txhCodOrden").value;			
			var opciones = "fullscreen=0" + ",toolbar=0" +
		        ",location=0" + ",status=1" +
		        ",menubar=0" + ",scrollbars=0" +
		        ",resizable=0" + ",width=730" +
		        ",height=500" + ",left=50" + ",top=80"; 
	     	var ventana = window.open(sURL,"Solicitantes",opciones); 		
	}
	function fc_IndInversion(){
		srtPerfil=document.getElementById("txhCodPerfil").value;
		if(srtPerfil.indexOf("LJLOG")>-1){
			if(document.getElementById("chkIndInversion").checked==true){
				document.getElementById("txhIndInversion").value="1";
			}else{
				document.getElementById("txhIndInversion").value="0";
			}
		}else{
			if(document.getElementById("chkIndInversion").checked==true){
				document.getElementById("chkIndInversion").checked=false;
			}else{
				document.getElementById("chkIndInversion").checked=true;
			}
		}
	}
	
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/GestOrdenDetalleOrden.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codPerfil" id="txhCodPerfil"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codOpciones" id="txhCodOpciones"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="codTipoReq" id="txhCodTipoReq"/>
<form:hidden path="codOrden" id="txhCodOrden"/>
<form:hidden path="codBandera" id="txhCodBandera"/>
<form:hidden path="banderaPerfil" id="txhBanderaPerfil"/>
<form:hidden path="codEstado" id="txhCodEstado"/>
<form:hidden path="consteCodEstadoEnAprobacion" id="txhConsteCodEstadoEnAprobacion"/>
<form:hidden path="banListaBandejaCondicion" id="txhBanListaBandejaCondicion"/>
<form:hidden path="banListaBandeja" id="txhBanListaBandeja"/>
<form:hidden path="indiceAprobacion" id="txhIndiceAprobacion"/>
<form:hidden path="codCotizacion" id="txhCodCotizacion"/>
<form:hidden path="codEstCotizacion" id="txhCodEstCotizacion"/>

<form:hidden path="indImportacion" id="txhIndImportacion"/>
<form:hidden path="numCotizacion" id="txhNumCotizacion"/>
<!-- ALQD,05/02/09. LEYENDO LA PROPIEDAD NUMCOTIZACION -->
<!-- ALQD,19/02/09.A�ADIENDO PROPIEDADES PARA TIPO DE GASTO, INVERSION Y COMENTARIO DE ANULACION -->
<form:hidden path="desOrdenTipoGasto" id="txhDesOrdenTipoGasto"/>
<form:hidden path="desComentario" id="txhDesComentario"/>
<form:hidden path="indInversion" id="txhIndInversion"/>
<div style="overflow: auto; height: 500px;width:100%">
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="width:96%;margin-left:9px; margin-top:7px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="795px" class="opc_combo">
		 		<font style="">Detalle de Orden <c:if test="${control.codTipPago=='0001'}">
		 		- <c:out value="${control.desTipPago}" /></c:if></font>
		 		<font style="" <c:if test="${control.codTipPago=='0001'}">color="red"</c:if>
		 		>
		 		&nbsp;- <c:out value="${control.desEstado}" /></font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
<!-- ALQD,19/02/09.A�ADIENDO UNA CAJA DE TEXTO PARA MOSTRAR EL TIPO DE GASTO 
	Y UN CONTROL PARA EL INDICADOR DE INVERSION -->
	<table class="tabla" background="${ctx}/images/Evaluaciones/back.jpg" border="0" bordercolor="red"
	  	style="width:96%;margin-top:6px;margin-left: 9px; height:65px" cellspacing="2" cellpadding="0">
	  	<tr>
			<td>Nro. Orden :</td>
			<td colspan="4">
				<form:input path="nroOrden" id="nroOrden" cssClass="cajatexto" readonly="true"
					size="15" cssStyle="width:120px" maxlength="15" />
				<form:input path="desOrdenTipoGasto" id="desOrdenTipoGasto" cssClass="cajatexto" readonly="true"
					size="20" cssStyle="width:120px" maxlength="20" />
			</td>
			<td>
				<input type="checkbox" id="chkIndInversion" onclick="fc_IndInversion();"/>&nbsp;<label id="lblIndInversion">Inversi�n</label>
			</td>
			<td>Fec. Emisi�n :</td>
			<td>
				<form:input path="fecEmision" id="fecEmision" cssClass="cajatexto" readonly="true"
					size="6" cssStyle="width:80px" maxlength="10" />
			</td>
		</tr>
		<tr>
			<td>Proveedor :</td>
			<td colspan="5">
				<form:input path="txtProveedorRuc" id="txtProveedorRuc" cssClass="cajatexto" readonly="true"
					size="8" cssStyle="width:80px" maxlength="10" />&nbsp;
				<form:input path="txtProveedorNombre" id="txtProveedorNombre" cssClass="cajatexto" readonly="true"
					cssStyle="width:250px" maxlength="10" />				
			</td>
			<td>Atencion a :</td>
			<td>
				<form:input path="txtAtencion" id="txtAtencion" cssClass="cajatexto" readonly="true"
					size="6" cssStyle="width:250px" maxlength="10" />
			</td>
		</tr>			
		<tr>
			<td style="width: 10%">Moneda :</td>
			<td style="width: 12%">
				<form:input path="txtTipoMoneda" id="txtTipoMoneda" cssClass="cajatexto"
			    	readonly="true"	size="3" cssStyle="width:60px" maxlength="4" />
	    	</td>
	    	<td style="width: 8%" id="tdIgv01">IGV :</td>
	    	<td style="width: 12%" id="tdIgv02">
			    <form:input path="txtIgv" id="txtIgv" cssClass="cajatexto" readonly="true"
					size="4" cssStyle="width:70px; text-align: right;" maxlength="10" />
			</td>
			<td style="width: 8%;display: none" id="tdOtros01">Otros :</td>
	    	<td style="width: 12%;display: none" id="tdOtros02">	    		
			    <form:input path="importeOtros" id="txtImporteOtros" cssClass="cajatexto" readonly="true"
					size="4" cssStyle="width:70px; text-align: right;" maxlength="10" />
			</td>
			<td style="width: 8%">Monto :</td>
			<td style="width: 10%">
				<form:input path="txtMoneda" id="txtMoneda" cssClass="cajatexto" readonly="true"
					size="6" cssStyle="width:80px; text-align: right;" maxlength="10" />
			</td>
			<td style="width: 10%">Comprador :</td>
			<td style="width: 30%">
				<form:input path="txtComprador" id="txtComprador" cssClass="cajatexto" readonly="true"
					size="6" cssStyle="width:250px" maxlength="10" />
			</td>		
		</tr>
	</table>

<div style="overflow: auto; height: 130px;width:97%">
<table class="tabla" border="0" bordercolor="red" 
		style="width:97%;margin-top:6px ;margin-left:9px; height:65px" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2">
			<table cellpadding="0" cellspacing="1" border="0" bordercolor="white" class="" width="100%"
			 style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" >
				<tr>
							
					<td class="grilla">Producto</td>
					<td class="grilla">Unidad de Medida</td>
					<td class="grilla">Cantidad</td>							
					<td class="grilla">Precio</td>
					<td class="grilla">Subtotal</td>					
				</tr>
				<c:forEach varStatus="loop" var="lista" items="${control.listaBandeja}"  >
					<tr class="texto">
					   
						<td align="left" class="tablagrilla" style="width: 40%">							
							&nbsp;&nbsp;<c:out value="${lista.descripcion}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.dscUnidad}" />							
						</td>
						<td align="right" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.cantidad}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.precio}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.subtotal}" />							
						</td>
						
																	
					</tr>
					</c:forEach>
					<c:if test='${control.banListaBandeja=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="10" height="20px">No se encontraron registros
						</td>
					</tr>					 		
	</c:if>					
			</table>
		</td>
				
	</tr>
	<tr>
		<td>
		<%-- ALQD,18/08/08. A�adiendo bot�n que lanza la consulta de la cotizaci�n --%>
		<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgVerCot','','${ctx}/images/botones/vercot2.jpg',1)">
			<img alt="Ver Cotizaciones" src="${ctx}/images/botones/vercot1.jpg" id="imgVerCot" style="cursor:pointer;" onclick="javascript:fc_verCotizacion();">
	    </a>
	    &nbsp;
		<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgVerAprob','','${ctx}/images/botones/veraprob2.gif',1)">
			<img alt="Ver Aprobaciones" src="${ctx}/images/botones/veraprob1.gif" id="imgVerAprob" style="cursor:pointer;" onclick="javascript:fc_verAprobacion();">
	    </a>
	    &nbsp;
		<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgVerSolicitantes','','${ctx}/images/botones/versolicitantes2.gif',1)">
			<img alt="Ver Solicitantes" src="${ctx}/images/botones/versolicitantes1.gif" id="imgVerSolicitantes" style="cursor:pointer;" onclick="javascript:fc_verSolicitantes();">
	    </a>	    


	    </td>
		<td  align="right" class="texto_bold">SubTotal&nbsp;:
		<form:input path="txtSubTotal" id="txtSubTotal" cssClass="cajatexto" readonly="true"
							size="4" cssStyle="width:70px; text-align: right;" maxlength="10" />
		</td>
	</tr>
	<!-- tr><td  align="right" class="texto_bold">IGV&nbsp;:
		<form:input path="txtIgv" id="txtIgv" cssClass="cajatexto" readonly="true"
							size="4" cssStyle="width:70px; text-align: right;" maxlength="10" />
							</td></tr>
	<tr><td  align="right" class="texto_bold">Total&nbsp;:
		<form:input path="txtTotal" id="txtTotal" cssClass="cajatexto" readonly="true"
							size="4" cssStyle="width:70px; text-align: right;" maxlength="10" />
							</td></tr-->
	<tr><td>&nbsp;</td></tr>
</table>
</div>

<table cellpadding="0" cellspacing="0" style="width:96%;margin-top:6px ;margin-left:9px;">
	<tr>
		<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			<td background="${ctx}/images/Evaluaciones/centro.jpg" width="790px" class="opc_combo">
			<font style="">Condiciones de Compra</font></td>
			<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
	</tr>
</table>

<div style="overflow: auto; height: 150px;width:97%">		
<table cellpadding="0" cellspacing="1" border="0" bordercolor="white" class="tabla" 
	style="width:97%;margin-top:6px;margin-left:9px;margin-bottom:5px;border: 1px solid #048BBA" >
	<tr>
							
		<td class="grilla" width="30%">Condici�n </td>
		<td class="grilla" width="70%">Descripci�n</td>							
	</tr>
	<c:forEach varStatus="loop" var="lista" items="${control.listaBandejaCondicion}"  >
		<tr class="texto">
		  <td align="left" class="tablagrilla" style="width: 10%">							
		    &nbsp;&nbsp;<c:out value="${lista.dscCondicion}" />
		  </td>
		  <td align="center" class="tablagrilla" style="width: 10%">
				<textarea class="cajatexto_1" id="mad<c:out value="${loop.index}"/>"
				name="mad<c:out value="${loop.index}"/>" cols="100" rows="3" readonly="readonly"><c:out value="${lista.descripcion}" /></textarea>
		  </td>
																							
		</tr>
	</c:forEach>
	<c:if test='${control.banListaBandejaCondicion=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="10" height="20px">No se encontraron registros
						</td>
					</tr>					 		
	</c:if>				
						
</table>
</div>
	
<table align="center" style="margin-left: 9px; margin-top: 6px;" border="0" bordercolor="red">
	<tr>
		<td>
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar01','','${ctx}/images/botones/regresar2.jpg',1)">
			<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar01" style="cursor:pointer;" onclick="fc_Regresar();"></a>
		</td>
		<td>
		<c:if test="${control.banderaPerfil=='1'}">
		  <c:if test="${control.codEstado==control.consteCodEstadoEnAprobacion}">
			  <c:if test="${control.indiceAprobacion=='1'}">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAprobar01','','${ctx}/images/botones/aprobar2.jpg',1)">
					<img alt="Aprobar" src="${ctx}/images/botones/aprobar1.jpg" id="imgAprobar01" style="cursor:pointer;" onclick="javascript:fc_Aprobar();"></a>
			  </c:if>
		  </c:if>
		</c:if>
		</td>
		<td>
		<c:if test="${control.banderaPerfil=='1'}">
		   <c:if test="${control.codEstado==control.consteCodEstadoEnAprobacion}">
		     <c:if test="${control.indiceAprobacion=='1'}">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRechazar01','','${ctx}/images/botones/rechazar2.jpg',1)">
					<img alt="Rechazar" src="${ctx}/images/botones/rechazar1.jpg" id="imgRechazar01" style="cursor:pointer;" onclick="javascript:fc_Rechazar();"></a>
		     </c:if>
		   </c:if>
		</c:if>
		</td>			
	</tr>
</table>
</div>
</form:form>
</body>
</html>