<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

	function fc_Cerrar(){
		window.close();
	}

	function onLoad(){ 
			
	}
	
	function fc_Exportar(){
	u = "0021";
	url="?tCodRep=" + u + 
			"&CtRe=" + document.getElementById("txhCodTipoReq").value +
			"&txhCodSede=" + document.getElementById("txhCodSede").value +
			"&Cco=" + document.getElementById("txhCodCotizacion").value;
		window.open("/SGA/logistica/reportesLogistica.html"+url,"Reportes","resizable=yes, menubar=yes");
	}

</script>
</head>

<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/logistica/verCondicionesOrdenCompra.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="codOrden" id="txhCodOrden" />
		
	<div style="overflow: auto; height: 400px;width:100%">	
	<table cellpadding="0" cellspacing="0" border="0" style="margin-left: 9px; margin-top: 0px;width: 80%;">
		<tr>
			<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px"
				class="opc_combo"><font valign=bottom>Condiciones de Compra</font></td>
			<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="1" border="0" bordercolor="white" class="tabla" 
	style="width:97%;margin-top:6px;margin-left:9px;margin-bottom:5px;border: 1px solid #048BBA" >
	<tr>
		<td class="grilla" width="30%">Condición </td>
		<td class="grilla" width="70%">Descripción</td>							
	</tr>
	<c:forEach varStatus="loop" var="lista" items="${control.listaBandejaCondicion}"  >
		<tr class="texto">
		  <td align="left" class="tablagrilla" style="width: 10%">							
		    &nbsp;&nbsp;<c:out value="${lista.dscCondicion}" />
		  </td>
		  <td align="center" class="tablagrilla" style="width: 10%">
				<textarea class="cajatexto_1" id="mad<c:out value="${loop.index}"/>"
				name="mad<c:out value="${loop.index}"/>" cols="100" rows="3" readonly="readonly"><c:out value="${lista.descripcion}" /></textarea>
		  </td>
																							
		</tr>
	</c:forEach>
	<c:if test='${control.banListaBandejaCondicion=="0"}'>
		<tr class="tablagrilla">
			<td align="center" colspan="10" height="20px">No se encontraron registros
			</td>
		</tr>					 		
	</c:if>				
						
	</table>
	

	<table align="left" style="width:93%; height: 180; vertical-align: top">
		<tr height="7px">
			<td>
			&nbsp;
			</td>
		</tr>
		<tr>
			<td align="center">
				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('iCerrarCot','','${ctx}/images/botones/cerrar2.jpg',1)">
					<img alt="Cerrar" src="${ctx}/images/botones/cerrar1.jpg" id="iCerrarCot" style="cursor: pointer;" onclick="fc_Cerrar();">
				</a>
			</td>
		</tr>
	</table>
	</div>
	
</form:form>
</body>
