<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

	function fc_Cerrar(){
		window.close();
	}

	function onLoad(){ 
			
	}

	
	function fc_Exportar(){
	u = "0021";
	url="?tCodRep=" + u + 
			"&CtRe=" + document.getElementById("txhCodTipoReq").value +
			"&txhCodSede=" + document.getElementById("txhCodSede").value +
			"&Cco=" + document.getElementById("txhCodCotizacion").value;
		window.open("/SGA/logistica/reportesLogistica.html"+url,"Reportes","resizable=yes, menubar=yes");
	}
	
	
</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/logistica/verSolicitantesOrdenCompra.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="codOrden" id="txhCodOrden" />
		
	<div style="overflow: auto; height: 490px;width:100%">	
	<table cellpadding="0" cellspacing="0" border="0" style="margin-left: 9px; margin-top: 0px;width: 80%;">
		<tr>
			<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px"
				class="opc_combo"><font valign=bottom>Solicitantes de Productos</font></td>
			<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	
	<table cellpadding="1" cellspacing="1" border="0" style="width:710px;margin-top:6px;margin-left:9px;" >
		<tr>
			<td>			
			<table id="tblSolicitantesProd" cellpadding="1" cellspacing="1" style="width:700px; height: 480; vertical-align: top" class="tabla_1A" border="0" bordercolor="white">
						<!-- tr class="grilla" height="5px" valign="middle">
							<td width="15%" align="center" nowrap="nowrap">&nbsp;
							</td>
							<td width="45%" align="center" nowrap="nowrap">Producto
							</td>
							<td width="15%" nowrap="nowrap" align="center">Unidad</td>
							<td align="center" width="20%">Cantidad Solicitada</td>
							<td align="center" width="20%">Cantidad Solicitada</td>
						</tr-->
							
							<c:forEach varStatus="loop" var="lista" items="${control.listaSolicitantesProd}" >
																	
							<tr class=<c:if test="${lista.indTermino=='C'}">
										<c:out value="grilla"/>
									  </c:if>
									  <c:if test="${lista.indTermino=='D'}">
										<c:out value="tablagrilla"/>
									  </c:if>
								style="display: " id='trArea<c:out value="${loop.index}"/>'  name='trArea<c:out value="${loop.index}"/>'>
			
								<!-- icon -->
								<td align="center">								
									<c:if test="${lista.indTermino=='C'}">
										<input id="chkSeleccion<c:out value="${loop.index}"/>" value="" type="hidden"/>
									</c:if>					
									<c:if test="${lista.indTermino=='D'}">										
										<img src="${ctx}/images/iconos/ico_link.gif" />										
									</c:if>
								</td>
								
								<!-- nomprod -->
								<td align="left" id='tdArea<c:out value="${loop.index}"/>1'>
									&nbsp;<c:out value="${lista.descripcion}" />
								</td> 

								<!-- unidad o nomceco -->
								<td align="left">&nbsp;
									<c:if test="${lista.indTermino=='C'}">
										<c:out value="${lista.dscUnidad}" />
									</c:if>
									<c:if test="${lista.indTermino=='D'}">
										<c:out value="${lista.nomCencos}" />
									</c:if>									
								</td>

								<!-- cantsolicitada o usu_solicitante -->
								<td align="left">&nbsp;
									<c:if test="${lista.indTermino=='C'}">
										<c:out value="${lista.cantidad}" />
									</c:if>
									<c:if test="${lista.indTermino=='D'}">
										<c:out value="${lista.usuSolicitante}" />
									</c:if>									
								</td>

								<!-- "" o cantidad solicitada -->
								<td align="right">&nbsp;
									<c:if test="${lista.indTermino=='C'}">
										&nbsp;
									</c:if>
									<c:if test="${lista.indTermino=='D'}">
										<c:out value="${lista.cantidad}" />
									</c:if>									
								</td>
							</tr>
							</c:forEach>				
					</table>

			</td>			
							
		</tr>
	</table>
	

	<table align="left" style="width:93%; height: 180; vertical-align: top">
		<tr height="7px">
			<td>
			&nbsp;
			</td>
		</tr>
		<tr>
			<td align="center">
				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('iCerrarCot','','${ctx}/images/botones/cerrar2.jpg',1)">
					<img alt="Cerrar" src="${ctx}/images/botones/cerrar1.jpg" id="iCerrarCot" style="cursor: pointer;" onclick="fc_Cerrar();">
				</a>
			</td>
		</tr>
	</table>
	</div>
	
</form:form>
</body>
