<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

	function fc_Cerrar(){
		window.close();
	}

	function onLoad(){ 
		//var mensaje = "<%=(( request.getAttribute("msg")==null)?"": request.getAttribute("msg"))%>";
		var mensaje = document.getElementById("txhCadena").value;;
		if(mensaje=="GRABAR_ERROR")
			alert("Error al grabar. Comun�quese con el programador.");
		if(mensaje=="GRABAR_OK")
			alert("Se grabo exitosamente.");
	}
	
	function fc_FlagAprob(param1){
	    val = document.getElementById("chkAprob" + param1).value;
	 	if(val == 0){
			document.getElementById("chkAprob" + param1).value = "1";
		}
		else{
			document.getElementById("chkAprob" + param1).value = "0";
		}
	}

	function fc_FlagSuple(param1){
	    val = document.getElementById("chkSuple" + param1).value;
	 	if(val == 0){
			document.getElementById("chkSuple" + param1).value = "1";
		}	
		else{
			document.getElementById("chkSuple" + param1).value = "0";
		}
	}
	function fc_Grabar(){
		var i = 0;
		cadSeleccion = "";
		oCodSuplente = "";
		oCheck = "";
		while (true)
		{
			oCheck = document.getElementById("chkSuple" + i);
			oCodSuplente = document.getElementById("txhCodSuple" + i);
			if ( oCodSuplente == null ){ break; }
			if(i==0){
				cadSeleccion = oCodSuplente.value + "|" + oCheck.value;
				}
			else{
			cadSeleccion = cadSeleccion + "$" + oCodSuplente.value + "|" + oCheck.value;
			}
			i++;
		}
		document.getElementById("txhCadena").value = cadSeleccion;
		if (confirm(mstrSeguroGrabar)){
			document.getElementById("txhOperacion").value = "GRABAR";
			document.getElementById("frmMain").submit();
		}
	}

</script>
</head>

<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain"
	action="${ctx}/logistica/gestAprobadorOrdenSuplente.html">

	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="codUsuario" id="prmCodUsuario" />
	<form:hidden path="sede" id="prmCodSede" />
	<form:hidden path="msg" id="txhCadena" />
	<form:hidden path="codTipoReporte" id="prmTipoAprobacion" />

	<div style="overflow: auto; height: 500px;width:100%">
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		style="margin-left: 9px; margin-top: 0px;width:100%;">
		<tr>
			<td align="left"><img
				src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px"
				class="opc_combo"><font valign=bottom>Delegar Aprobaci�n de Ordenes de Compra</font></td>
			<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" border="1" style="width:100%;margin-top:6px;margin-left:9px;" align="center">
		<tr>
			<td width="50%" valign="top">
				<!-- BANDEJA -->
				<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA">
					<tr>
						<td class="grilla" width="80%">Delegando A:</td>
						<td class="grilla" width="20%" align="center">Sel.</td>
					</tr>
					
					<c:forEach var="objAprob1" items="${control.listResponsable1}" varStatus="loop">
						<tr class="tablagrilla">
							<td>
								<input type="hidden" id="txhCodSuple${loop.index}" value="${objAprob1.codUsuario}" />
								<c:out value="${objAprob1.dscAprobador}" />
							</td>
							<td align="center">
								<input <c:if test="${objAprob1.obsEstado=='1'}"><c:out value=" checked=checked " /></c:if>
								type="checkbox" id="chkSuple${loop.index}" onclick="javascript:fc_FlagSuple('<c:out value="${loop.index}" />');" value="${objAprob1.obsEstado}"/>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
			<td width="50%" valign="top">
				<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" >
					<tr>
						<td class="grilla" width="80%">Aprobando Por:</td>
						<td class="grilla" width="20%" align="center">Sel.</td>
					</tr>
					
					<c:forEach var="objAprob2" items="${control.listResponsable2}" varStatus="loop">
						<tr class="tablagrilla">
							<td>
								<input type="hidden" id="txhCodAprob${loop.index}" value="${objAprob2.codUsuario}" />
								<c:out value="${objAprob2.dscAprobador}" />
							</td>
							<td align="center">
								<input <c:if test="${objAprob2.obsEstado=='1'}"><c:out value=" checked=checked " /></c:if>
								type="checkbox" disabled id="chkAprob${loop.index}"
								onclick="javascript:fc_FlagAprob('<c:out value="${loop.index}" />');" value="${objAprob2.obsEstado}"/>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
		</tr>
	</table>
	
	
	<table align="center">
		<tr>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('iGrabarDelegar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="iGrabarDelegar" style="cursor: pointer;" onclick="fc_Grabar();">
				</a>
			</td>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('iCerrarDelegar','','${ctx}/images/botones/cerrar2.jpg',1)">
					<img alt="Cerrar" src="${ctx}/images/botones/cerrar1.jpg" id="iCerrarDelegar" style="cursor: pointer;" onclick="fc_Cerrar();">
				</a>
			</td>
		</tr>
	</table>
	</div>

</form:form>
</body>
