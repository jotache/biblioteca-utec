<%@ include file="/taglibs.jsp"%>
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	function onLoad(){
	  if(document.getElementById("txhMsg").value=="OK")
	  {  alert(mstrSeGraboConExito);
	     parent.TablaDocumentoPago.style.display='inline-block';
	     document.getElementById("txhMsg").value="";
	     parent.fc_Buscar_iFrame();
	     
	  }
	  else{ if(document.getElementById("txhMsg").value=="ERROR")
	           alert(mstrProblemaGrabar);
	        else{ if(document.getElementById("txhMsg").value=="FAC_REG")
	                 alert(mstrNroFacturaRegistrado);
	              else{ if(document.getElementById("txhMsg").value=="NRO_GUIA")
	                       alert(mstrNroGuiaRegistrado);
	                    else if(document.getElementById("txhMsg").value=="CANT_FACTURADA")
	                 		 alert(mstrExcedeCantidadDocPago);
	                   }
	            }
	  }
	 	document.getElementById("txhMsg").value="";
	}
	
	function fc_Grabar(){
		form = document.forms[0];
		var ffw=0;
		fc_Encadenar();
		ffw=fc_ValidarFechas();
		if((ffw==1)&&(document.getElementById("nroFactura").value!=""))
		{  if(confirm(mstrSeguroAgregarDocPago)){
			form.imgGrabar01.disabled=true;
			document.getElementById("txhOperacion").value = "GRABAR";
		 	document.getElementById("frmMain").submit();
		   }else
		    	 form.imgGrabar01.disabled=false;
		}
		else{ if(ffw==0) ffw=0;
		      else{
		      		if(document.getElementById("nroFactura").value=="") alert(mstrIngNroFactura);
		      }
		}
	}
	
	function fc_CalcularCantidad(srtIndice){
	 
	 if(document.getElementById("canFac"+srtIndice).value!="")
		{ srtCanSolicitada=document.getElementById("canSol"+srtIndice).value;
		  srtCanFacturada=document.getElementById("canFac"+srtIndice).value;
		  
		  var canFacturada=parseFloat(srtCanFacturada);
		  var canSolicitada=parseFloat(srtCanSolicitada);
		  
		  if(canSolicitada>=canFacturada)
			 {	if(document.getElementById("total").value=="")
				    document.getElementById("total").value="0";
				    
				 srtPrecioUnitario=document.getElementById("preUni"+srtIndice).value;
				 var precioUnitario=parseFloat(srtPrecioUnitario);
				 var suma=precioUnitario*canFacturada;
				
				 document.getElementById("subTotal"+srtIndice).value=suma.toString();
				 Suma_Total();
			 }
		 else{
		       alert("canSolicitada: "+canSolicitada+", canSolicitada: "+canSolicitada);
		       alert(mstrDocPagoExcedeCantidad);
		       document.getElementById("canFac"+srtIndice).focus();
		       
		 }
		}
	 else{
	       document.getElementById("subTotal"+srtIndice).value="";
	       Suma_Total();
	 }
	}
	
	function Suma_Total(){
		srtLong=parseInt(document.getElementById("txhLongitud").value);
	 	var redSun = 0;
	 	var igvDB = parseFloat(document.getElementById("txhIgvDB").value);
	 	//ALQD,28/05/09.SI AFECTO ES 0 => igvDB=0
	 	if(document.getElementById("txhIgvDB").value=="0"){
	 		igvDB=0;
	 	}
	    for ( a = 0; a< srtLong; a++ ) {
	    	if(document.getElementById("subTotal"+a).value!=""){
	    		redSun=parseFloat(document.getElementById("subTotal"+a).value)+redSun;
    		}
   		}
   		indImportacion = document.getElementById("txhIndImportacion").value;
   		if(indImportacion == "1"){
   			//NO SE CALCULA EL IGV SUBTOTAL IGUAL A TOTAL
   			document.getElementById("txtSubTot").value=redSun.toString();
		  	document.getElementById("txtIgv").value = "0.00";
		  	document.getElementById("total").value= redSun.toString();
   		}
   		else{
   			//SE SIGUE CON EL MISMO FLUJO NO SE MODIFICO NADA AQUI
   			document.getElementById("txtSubTot").value=redSun.toString();
	  		document.getElementById("txtIgv").value = (redSun*igvDB).toString();
	  		document.getElementById("total").value= redSun + parseFloat(document.getElementById("txtIgv").value);
   		}
	}
	
	function fc_Encadenar(){
	 srtLong=parseInt(document.getElementById("txhLongitud").value);
	 var cadCantidades="";
	 var cadObservaciones="";
	  for ( a = 0; a< srtLong; a++ ) {
	        
	        if(a==0)
	         {	cadCantidades=document.getElementById("canFac"+a).value ;
	            cadObservaciones=document.getElementById("obs"+a).value ;
	         }
	       	else
	        {cadCantidades=cadCantidades + "|" + document.getElementById("canFac"+a).value;
	         cadObservaciones=cadObservaciones + "|" + document.getElementById("obs"+a).value;
	        }
	        
	  }
	  cadCantidades=fc_Trim(cadCantidades);
	  cadObservaciones=fc_Trim(cadObservaciones);
	
	 document.getElementById("txhCadCantidades").value=cadCantidades;
	 document.getElementById("txhCadObservaciones").value=cadObservaciones;
	}
	
	function fc_ValidarFechas(){
	
	srtFechaBD=document.getElementById("txhFechaBD").value;
	
	srtFechaFin=document.getElementById("txtFecInicio1").value;
	var num=0;
	num=fc_ValidaFechaIniFechaFin(srtFechaBD,srtFechaFin);
	//si es 0 la fecEmision es mayor igual ala fechaBD
	
	var mad=0;
	srtFechaVcto=document.getElementById("txtFecInicio2").value;
	//ALQD,27/05/09.LA FECHA DE VENC. DEBE SER MAYOR O IGUAL A LA FEC.RECEPCION
	//LA FECHA DE RECEP. DEBE SER MAYOR O IGUAL A LA FEC.RECEPCION
	strFecRecepcion=document.getElementById("txtFecFinal2").value;
	var rec=0;
	rec=fc_ValidaFechaIniFechaFin(strFecRecepcion,srtFechaFin);
	if(srtFechaVcto!="")
	   mad=fc_ValidaFechaIniFechaFin(srtFechaVcto,strFecRecepcion);
	else mad=0;
//ALQD,27/05/09.NO SE VALIDARA LA FECHA DE LA GUIA
//	var wild=0;
//	srtFechaFinDos=document.getElementById("txtFecFinal1").value; 
//	wild=fc_ValidaFechaIniFechaFin(srtFechaBD,srtFechaFinDos);
	//si es 0 la fecEmision es mayor igual ala fechaBD
	
	if( (srtFechaFin!="")&&(mad+rec>=0) ) //&&(srtFechaFinDos!="")
	 {  
	        return 1;
	 }
	 else{ if(srtFechaFin=="") alert(mstrFecEmisionNoVacia);
	             else{  
		             if(mad<0) 
			             alert("La fecha de vencimiento debe ser mayor o igual\n a la fecha de recepci�n.");
	                 else{
	                 	if(rec<0)
	                 		alert("La fecha de recepci�n debe ser mayor o igual\n a la fecha de emisi�n.");
	                 }
	             }
	             
	      
	  return 0;
	 }
	 
	}
		
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/GestDocConDocPagosAsociadosIframe.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codPerfil" id="txhCodPerfil"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codOpciones" id="txhCodOpciones"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="codOrden" id="txhCodOrden"/>
<form:hidden path="codDocumento" id="txhCodDocumento"/>
<form:hidden path="longitud" id="txhLongitud"/>
<form:hidden path="cadCantidades" id="txhCadCantidades"/>
<form:hidden path="cadObservaciones" id="txhCadObservaciones"/>
<form:hidden path="fechaBD" id="txhFechaBD"/>
<form:hidden path="igvDB" id="txhIgvDB"/>

<form:hidden path="indImportacion" id="txhIndImportacion"/>
<form:hidden path="indAfectoIGV" id="txhIndAfectoIGV"/>
<!-- ALQD,28/05/09.NUEVO PARAMETRO -->

	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:6px; 
       margin-top: 7px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="750px" class="opc_combo">
		 		<font style="">Documento de Pago</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
<!-- ALQD,25/06/09.NO SE VALIDARA EL INGRESO DEL # DEL DOCUMENTO, LETRAS Y #s onkeypress="fc_ValidaNumerosGuion();" onblur="fc_ValidaNumeroGuionOnblur(this, 'Nro. Factura');" -->
	<table class="tabla" background="${ctx}/images/Evaluaciones/back.jpg" border="0" bordercolor="red"
					style="width:97%;margin-top:6px; margin-left:6px;" cellspacing="2" cellpadding="0">
				<tr>
					<td class="" width="10%">&nbsp;&nbsp;&nbsp;Nro. Factura:</td>
					<td width="15%">
						<form:input path="nroFactura" id="nroFactura" cssClass="cajatexto_o"
								  cssStyle="width:100px; text-align: right;" maxlength="20"/>
					</td>
					<td class="" align="right">Fecha Emisi�n : &nbsp;&nbsp; </td>
					<td ><form:input path="fecInicio1" id="txtFecInicio1" cssClass="cajatexto_o"
								onkeypress="javascript:fc_Slash('frmMain','txtFecInicio1','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio1');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni1','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni1"
								alt="Calendario" style="cursor:pointer;"></a>
					</td>
					<td class="" align="right">Fecha Vencimiento. :</td>
					<td ><form:input path="fecInicio2" id="txtFecInicio2" cssClass="cajatexto"
								onkeypress="javascript:fc_Slash('frmMain','txtFecInicio2','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio2');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni2','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni2"
								alt="Calendario" style="cursor:pointer;"></a>
					</td>
				</tr>
				
				<tr>
					<td class="" width="10%">&nbsp;&nbsp;&nbsp;Nro. Gu�a :</td>
					<td width="15%"><form:input path="nroGuia" id="nroGuia" cssClass="cajatexto_o"
								cssStyle="width:100px; text-align: right;" maxlength="20" />
					</td>
					<td class="" align="right">Fecha Emisi�n : &nbsp;&nbsp;</td>
					<td ><form:input path="fecFinal1" id="txtFecFinal1" cssClass="cajatexto_o"
								onkeypress="javascript:fc_Slash('frmMain','txtFecFinal1','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal1');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin1','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin1"
								alt="Calendario" style="cursor:pointer;"></a>
					</td>
					<td class="" align="right">Fecha Recepci�n : &nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td ><form:input path="fecFinal2" id="txtFecFinal2" cssClass="cajatexto"
								onkeypress="javascript:fc_Slash('frmMain','txtFecFinal2','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal2');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin2','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin2"
								alt="Calendario" style="cursor:pointer;"></a>
					</td>					
				</tr>			
	</table>
		
	<div style="overflow: auto; height: 150px;width:98%">
	<table border="0" cellpadding="0" cellspacing="0" bordercolor="red"
			   style="width:97%;margin-top:6px; margin-left:6px;">
				<tr>
					<td width="100%">
						<table cellpadding="0" cellspacing="1" border="0" bordercolor="white" ID="Table5"
						   style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA">
							<tr>
								<td class="grilla" width="25%">Producto / Servicio</td>
								<td class="grilla" width="9%">Cantidad<br>Solicitada</td>
								<td class="grilla" width="9%">Precio<br>Unitario</td>
								<td class="grilla" width="9%">Cantidad<br>Facturada</td>							
								<td class="grilla" width="5%">Unidad</td>
								<td class="grilla" width="14%">SubTotal<br>Facturado</td>
								<td class="grilla" width="14%">Obs.</td>								
							</tr>
				<c:forEach varStatus="loop" var="lista" items="${control.listaBandejaMedio}"  >
					<tr class="texto">
					   
						<td align="left" class="tablagrilla" style="width: 25%">							
							&nbsp;&nbsp;&nbsp;<c:out value="${lista.nomItem}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 9%">
							
							<input type="text" id="canSol<c:out value="${loop.index}"/>" name="canSol<c:out value="${loop.index}"/>"
							 size="5" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.cantidad}" />" readonly />
						</td>
						<td align="center" class="tablagrilla" style="width: 9%">
							
							<input type="text" id="preUni<c:out value="${loop.index}"/>" name="preUni<c:out value="${loop.index}"/>"
							 size="10" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.preUni}" />" readonly />
						</td>
						<td align="center" class="tablagrilla" style="width: 9%">
						  <input type="text" id="canFac<c:out value="${loop.index}"/>" name="canFac<c:out value="${loop.index}"/>"
							 value="<c:out value="${lista.cantidad}" />"
							 size="10" style="text-align: right;" class="cajatexto" onblur="fc_CalcularCantidad('<c:out value="${loop.index}"/>');" onkeypress="fc_PermiteNumerosPunto();" />							
<!-- ALQD,24/10/08. CORREGIR EL INGRESO DE DECIMALES						  <input type="text" id="canFac<c:out value="${loop.index}"/>" name="canFac<c:out value="${loop.index}"/>"
							 value="<c:out value="${lista.cantidad}" />"
							 size="10" style="text-align: right;" class="cajatexto" onblur="fc_ValidaDecimalOnBlur('canFac<c:out value="${loop.index}"/>','3','2');fc_CalcularCantidad('<c:out value="${loop.index}"/>');" onkeypress="fc_PermiteNumerosPunto();" />
 -->
						</td>

						<!-- jhpr 2008-09-25 -->
						<td align="center" class="tablagrilla" style="width: 5%">
							<input type="text" id="unidad<c:out value="${loop.index}"/>" name="unidad<c:out value="${loop.index}"/>"
							 size="6" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.unidadMedida}" />" readonly />							
						</td>

						<td align="center" class="tablagrilla" style="width: 14%">						
							<input type="text" id="subTotal<c:out value="${loop.index}"/>" name="subTotal<c:out value="${loop.index}"/>"
							 size="10" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.dscSubTotal}" />" readonly  />					
						</td>
						<td align="center" class="tablagrilla" style="width: 14%">
							<textarea cols="40" rows="2" class="cajatexto" maxlength="200" 
							id="obs<c:out value="${loop.index}"/>" name="obs<c:out value="${loop.index}"/>"	></textarea>					
						</td>
							
					</tr>
				</c:forEach>	
						
						</table>
					</td>					
				</tr>
				
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" width="78%" ID="Table9">
							<tr>
								<td width="55%" class="texto_bold" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SUB TOTAL :&nbsp;</td>								
								<td width="10%" align="left">&nbsp;&nbsp;
									<form:input path="subTotal" id="txtSubTot" 
									cssClass="cajatexto_1" cssStyle="width:65px; text-align: right;" readonly="true"/>
								</td>
							</tr>
							<tr id="trIgv">
								<td width="55%" class="texto_bold" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IGV :&nbsp;</td>								
								<td width="10%" align="left" >&nbsp;&nbsp;
									<form:input path="igv" id="txtIgv" 
									cssClass="cajatexto_1" cssStyle="width:65px; text-align: right;" readonly="true"/>
								</td>
							</tr>
							<tr>
								<td width="55%" class="texto_bold" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TOTAL :&nbsp;</td>								
								<td width="10%" align="left">&nbsp;&nbsp;
									<form:input path="total" id="total" cssClass="cajatexto_1"
										readonly="true"
										cssStyle="width:65px; text-align: right;" maxlength="10" />
								</td>																
							</tr>
						</table>	
					</td>
				</tr>
	</table>
	</div>
			
	<table align=center id="botones" style="margin-top: 10px;">
				<tr>
					<td align="center">
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar01','','${ctx}/images/botones/grabar2.jpg',1)">
						<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imgGrabar01" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a>
					</td>
				
				</tr>
	</table>


</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio1",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni1",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal1",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin1",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecInicio2",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni2",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal2",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin2",
		singleClick    :    true
	});
</script>
</body>
</html>