<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
		function onLoad(){
		
		objMsgRe = document.getElementById("txhMsgRe");
		if ( objMsgRe.value == "OK" )
		{	
			objMsgRe.value = "";
			document.getElementById("txhMsgRe").valu="";
			alert(mstrSeGraboConExito2);
			Fc_Regresa();
		}
		else if( objMsgRe.value == "ERROR" )
		{
		objMsgRe.value = "";
		document.getElementById("txhMsgRe").valu="";
		alert(mstrProblemaGrabar2);
		}
		
		objMsgVa = document.getElementById("txhMsgVa");
		if ( objMsgVa.value == "OK" )
		{	
			objMsgVa.value = "";
			document.getElementById("txhMsgVa").valu="";
			alert(mstrSeGraboConExito3);
			Fc_Regresa();
		}
		else if( objMsgVa.value == "ERROR" )
		{
		objMsgVa.value = "";
		document.getElementById("txhMsgVa").valu="";
		alert(mstrProblemaGrabar3);
		}
			if(document.getElementById("txhCant").value>0)  
			   fc_Convertir(document.getElementById("txhCant").value);    
			if(document.getElementById("txtMonto").value!="") 
			   document.getElementById("txtMonto").value=fc_Trim(document.getElementById("txtMonto").value);		   
		   
			if(document.getElementById("txtSubTotal").value!="")
			 { document.getElementById("txtSubTotal").value = redondea(document.getElementById("txtSubTotal").value, 2);
			 }
			if(document.getElementById("txtIgv").value!="")
			 { document.getElementById("txtIgv").value = redondea(document.getElementById("txtIgv").value, 2);
			 }
			if(document.getElementById("txtTot").value!="")
			 { document.getElementById("txtTot").value = redondea(document.getElementById("txtTot").value, 2);
			 }		
		}
		
		function Fc_Regresa(){
		srtCodSede=document.getElementById("txhSede").value;
		location.href="${ctx}/logistica/GestDocAprobarDocumentosPago.html?txhCodSede="+srtCodSede;
		}
		function Fc_Rechazar(){
		if (confirm(mstrSeguroGrabar3)){
		document.getElementById("txhOperacion").value="RECHAZAR";
		document.getElementById("frmMain").submit();
		}
		}
		function Fc_Validar(){
		if (confirm(mstrSeguroGrabar2)){
		document.getElementById("txhOperacion").value="VALIDAR";
		document.getElementById("frmMain").submit();
		}
		}
		
		function redondea(sVal, nDec){ 
	    var n = parseFloat(sVal); 
	    var s = "0.00"; 
	    if (!isNaN(n)){ 
			n = Math.round(n * Math.pow(10, nDec)) / Math.pow(10, nDec); 
			s = String(n); 
			s += (s.indexOf(".") == -1? ".": "") + String(Math.pow(10, nDec)).substr(1); 
			s = s.substr(0, s.indexOf(".") + nDec + 1); 
	    } 
	    return s; 
		}
		
		function fc_Convertir(num){
		
		 for(i=0; i<num;i++)
		 { subTotal3=parseFloat(document.getElementById("preUni"+i).value);
		   document.getElementById("preUni"+i).value=redondea(subTotal3,2);
		   subTotal4=parseFloat(document.getElementById("subTot"+i).value);
		   document.getElementById("subTot"+i).value=redondea(subTotal4,2);
		 }
		}
</script>
</head>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_VerDocumento.html">
	<form:hidden path="operacion" id="txhOperacion"/> 
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="msgRe" id="txhMsgRe"/>
	<form:hidden path="msgVa" id="txhMsgVa"/>
	<form:hidden path="ordeId" id="txhOrdeId"/>
	<form:hidden path="docId" id="txhDocId"/>
	<form:hidden path="sede" id="txhSede"/>
	<form:hidden path="cant" id="txhCant"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>

		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px; margin-top: 7px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="785px" class="opc_combo">
		 		<font style="">
		 			Validar Documento de Pago					 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	   </table>	
	   		
	   <table class="tabla" background="${ctx}/images/Evaluaciones/back.jpg" border="0" bordercolor="red"
					style="width:98%;margin-top:6px; margin-left:9px; height:65px" cellspacing="2" cellpadding="0">
				<tr>
					<td class="" width="10%">&nbsp;&nbsp;&nbsp;Nro. Factura:</td>
					<td width="20%"><form:input path="nroFactura" id="txtNroFactura" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:100px;text-align: right;" /></td>
					<td class="" width="10%">Fecha Emisi�n :</td>
					<td width="20%"><form:input path="fechaEmicion" id="txtFechaEmicion" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:90px;text-align: right;"/>						
					</td>				
					<td class="" width="10%">Fecha Vcto. :</td>
					<td width="30%"><form:input path="fechaVcto" id="txtFechaVcto" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:90px;text-align: right;"/>					
					</td>
				</tr>
				
				<tr>
					<td class="">&nbsp;&nbsp;&nbsp;Nro. Gu�a :</td>
					<td width="20%"><form:input path="nroGuia" id="txtNroGuia" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:100px;text-align: right;" /></td>
					<td class="">Fecha Emisi�n :</td>
					<td ><form:input path="fechaEmicion2" id="txtFechaEmicion2" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:90px;text-align: right;"/>
					</td>
					<td class="">Moneda :</td>
					<td class="">
					<form:label path="moneda" id="lblMoneda" cssClass="texto_dentro" cssStyle="HEIGHT: 14px;width:300px" >${control.moneda}</form:label>
					</td>
				</tr>			
				<tr>
					<td class="">&nbsp;&nbsp;&nbsp;Nro. Orden :</td>
					<td width="20%"><form:input path="nroOrden" id="txtNroOrden" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:100px;text-align: right;" /></td>
					<td class="">Fecha Emisi�n :</td>
					<td ><form:input path="fechaEmicion3" id="txtFechaEmicion3" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:90px;text-align: right;"/></td>
					<td class="">Monto :</td>
					<td ><form:input path="monto" id="txtMonto" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:90px;text-align: right;"/></td>					
				</tr>		
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0"  width="100%" bordercolor="red"
			   style="width:98%;margin-top: 6px; margin-left: 9px;">
				<tr>
					<td width="100%">
						<table cellpadding="0" cellspacing="1" border="0" bordercolor="white" width="100%">
						<tr>
							<td class="grilla">Producto/Servicio</td>
							<td class="grilla">Cantidad Solicitada</td>
							<td class="grilla">Precio Unitario</td>							
							<td class="grilla">Cantidad Facturada</td>
							<td class="grilla">SubTotal Facturado</td>
							<td class="grilla">Obs.</td>							
						</tr>
						<c:forEach varStatus="loop" var="lista" items="${control.listDocumentos}"  >
						<tr class="texto">
							<td align="left" class="tablagrilla" style="width: 20%">							
								&nbsp;&nbsp;&nbsp;<c:out value="${lista.nomItem}" />
							</td>
							<td align="center" class="tablagrilla" style="width: 12%">
							<input type="text" id="canSol<c:out value="${loop.index}"/>" name="canSol<c:out value="${loop.index}"/>"
							size="11" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.cantidad}" />" readonly>
							</td>
							<td align="center" class="tablagrilla" style="width: 12%">
							<input type="text" id="preUni<c:out value="${loop.index}"/>" name="preUni<c:out value="${loop.index}"/>"
							size="11" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.preUni}" />" readonly>
							</td>
							<td align="center" class="tablagrilla" style="width: 12%">
							<input type="text" id="canFac<c:out value="${loop.index}"/>" name="canFac<c:out value="${loop.index}"/>"
							size="11" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.cantFacturada}" />" readonly>
							</td>
							<td align="center" class="tablagrilla" style="width: 12%">
							<input type="text" id="subTot<c:out value="${loop.index}"/>" name="subTot<c:out value="${loop.index}"/>"
							size="11" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.subTotal}" />" readonly>
							</td>
							<td align="center" class="tablagrilla" style="width: 32%">
							 <textarea cols="60" rows="2" readonly="readonly"
							id="obs<c:out value="${loop.index}"/>" name="obs<c:out value="${loop.index}"/>" 
							 class="cajatexto_1">${lista.observacion}</textarea>
							</td>
						</tr>
						</c:forEach>			
					</table>
					</td>					
				</tr>
				
				<tr>
					<td>
						<table cellpadding="0" cellspacing="1" border="0" bordercolor="blue" class="" width="100%"
							ID="Table5" style="margin-top: 10px;">
							<tr>
								<td style="width: 20%"></td>
								<td style="width: 12%"></td>
								<td style="width: 12%"></td>
								<td style="width: 12%" class="texto_bold" align="right">SUB TOTAL:</td>
								<td style="width: 12%" align="center">
									<form:input path="subTotal" id="txtSubTotal" 
										cssClass="cajatexto_1" 
										cssStyle="width:65px; text-align: right;" 
										readonly="true"/>
								</td>
								<td style="width: 32%"></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td class="texto_bold" align="right">IGV:</td>
								<td align="center">
									<form:input path="igv" id="txtIgv" 
										cssClass="cajatexto_1" 
										cssStyle="width:65px; text-align: right;" 
										readonly="true"/>
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td class="texto_bold" align="right">TOTAL:</td>
								<td align="center">
									<form:input path="tot" id="txtTot" 
										cssClass="cajatexto_1" 
										cssStyle="width:65px; text-align: right;" 
										readonly="true"/>
								</td>
							</tr>
						</table>	
					</td>
				</tr>
			</table>
			
			<table align=center id="botones" style="margin-top: 10px;">
				<tr>
					<td align="center">
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnRegrezar','','${ctx}/images/botones/regresar2.jpg',1)">
					<img src="${ctx}/images/botones/regresar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Regresa();" alt="Regrezar" id="btnRegrezar">
					</a>
					
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgValidar','','${ctx}/images/botones/validar2.jpg',1)">
					<img src="${ctx}/images/botones/validar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Validar();" alt="Validar" id="imgValidar">
					</a>
					
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnRechazar','','${ctx}/images/botones/rechazar2.jpg',1)">
						<img src="${ctx}/images/botones/rechazar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Rechazar();" alt="Rechazar" id="btnRechazar">
					</a>
					
					</td>
				</tr>
			</table>
</form:form>

