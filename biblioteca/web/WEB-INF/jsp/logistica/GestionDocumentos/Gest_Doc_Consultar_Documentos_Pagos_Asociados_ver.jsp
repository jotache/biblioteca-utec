<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
		function onLoad(){
		
		objMsgRe = document.getElementById("txhMsgRe");
		if ( objMsgRe.value == "OK" )
		{
			objMsgRe.value = "";
			document.getElementById("txhMsgRe").valu="";
			alert(mstrSeGraboConExito2);
			Fc_Regresa();
		}
		else if( objMsgRe.value == "ERROR" )
		{
		objMsgRe.value = "";
		document.getElementById("txhMsgRe").valu="";
		alert(mstrProblemaGrabar2);
		}
		
		objMsgVa = document.getElementById("txhMsgVa");
		if ( objMsgVa.value == "OK" )
		{
			objMsgVa.value = "";
			document.getElementById("txhMsgVa").valu="";
			alert(mstrSeGraboConExito3);
			Fc_Regresa();
		}
		else if( objMsgVa.value == "ERROR" )
		{
		objMsgVa.value = "";
		document.getElementById("txhMsgVa").valu="";
		alert(mstrProblemaGrabar3);
		}
		if(document.getElementById("txhCant").value>0)
		   fc_Convertir(document.getElementById("txhCant").value);
		if(document.getElementById("txtMonto").value!="")
		   document.getElementById("txtMonto").value=fc_Trim(document.getElementById("txtMonto").value);
		   
		if(document.getElementById("txtSubTot").value!="")
		 { document.getElementById("txtSubTot").value = redondea(document.getElementById("txtSubTot").value, 2);
		 }
		if(document.getElementById("txtIgv").value!="")
		 { document.getElementById("txtIgv").value = redondea(document.getElementById("txtIgv").value, 2);
		 }
		if(document.getElementById("txtTot").value!="")
		 { document.getElementById("txtTot").value = redondea(document.getElementById("txtTot").value, 2);
		 }
		
		}

		//JHPR 2008-09-29
		function exportar(){
			if(document.getElementById("txhIndImportacion").value=="0"){
				u = "0024";
			}
			else
			{
				u = "0027";
			}	
			url="?tCodRep=" + u +
				"&nroDocumento=" + document.getElementById("txhDocId").value +
				"&lugarTrabajo=" + document.getElementById("txhSede").value +
				"&codUsuario=" + document.getElementById("txhCodAlumno").value +
				"&nroOrden=" + document.getElementById("txhOrdeId").value;
			
			window.open("${ctx}/logistica/reportesLogistica.html"+url,"","resizable=yes, menubar=yes");
		}
		
		function Fc_Regresa(){
		srtCodSede=document.getElementById("txhSede").value;
		location.href="${ctx}/logistica/GestDocAprobarDocumentosPago.html?txhCodSede="+srtCodSede;
		}
		function Fc_Rechazar(){
		if (confirm(mstrSeguroGrabar3)){
		document.getElementById("txhOperacion").value="RECHAZAR";
		document.getElementById("frmMain").submit();
		}
		}
		function Fc_Validar(){
		if (confirm(mstrSeguroGrabar2)){
		document.getElementById("txhOperacion").value="VALIDAR";
		document.getElementById("frmMain").submit();
		}
		}
		
		function redondea(sVal, nDec){
	    var n = parseFloat(sVal);
	    var s = "0.00";
	    if (!isNaN(n)){
			n = Math.round(n * Math.pow(10, nDec)) / Math.pow(10, nDec);
			s = String(n);
			s += (s.indexOf(".") == -1? ".": "") + String(Math.pow(10, nDec)).substr(1);
			s = s.substr(0, s.indexOf(".") + nDec + 1);
	    }
	    return s;
		}
		
		function fc_Convertir(num){
		
		 for(i=0; i<num;i++)
		 { subTotal3=parseFloat(document.getElementById("preUni"+i).value);
		   document.getElementById("preUni"+i).value=redondea(subTotal3,2);
		   subTotal4=parseFloat(document.getElementById("subTot"+i).value);
		   document.getElementById("subTot"+i).value=redondea(subTotal4,2);
		 }
		}
</script>
</head>
<!-- ALQD,12/06/09.SE MOSTRARA LOS DATOS DE DOCUMENTOS ASOCIADOS EN LA IMPORTACION -->
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/consultarDocumentoPagoVer.html">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="msgRe" id="txhMsgRe"/>
	<form:hidden path="msgVa" id="txhMsgVa"/>
	<form:hidden path="ordeId" id="txhOrdeId"/>
	<form:hidden path="docId" id="txhDocId"/>
	<form:hidden path="sede" id="txhSede"/>
	<form:hidden path="cant" id="txhCant"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="indImportacion" id="txhIndImportacion"/>

		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px; margin-top: 7px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="785px" class="opc_combo">
		 		<font style="">
		 			Documento de Pago
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	   </table>
	   
	   <table class="tabla" background="${ctx}/images/Evaluaciones/back.jpg" border="0" bordercolor="blue"
					style="width:98%;margin-top:6px; margin-left:9px; height:65px" cellspacing="2" cellpadding="0">
				<tr>
					<td class="" width="10%">&nbsp;&nbsp;&nbsp;Nro. Factura:</td>
					<td width="20%"><form:input path="nroFactura" id="txtNroFactura" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:100px;text-align: right;" /></td>
					<td class="" width="13%">Fecha Emisi�n :</td>
					<td width="20%"><form:input path="fechaEmicion" id="txtFechaEmicion" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:90px;text-align: right;"/>
					</td>
					<td class="" width="10%">Fecha Vcto. :</td>
					<td width="30%"><form:input path="fechaVcto" id="txtFechaVcto" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:90px;text-align: right;"/>
					</td>
				</tr>
				
				<tr>
					<td class="">&nbsp;&nbsp;&nbsp;Nro. Gu�a :</td>
					<td width="20%"><form:input path="nroGuia" id="txtNroGuia" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:100px;text-align: right;" /></td>
					<td class="">Fecha Emisi�n :</td>
					<td ><form:input path="fechaEmicion2" id="txtFechaEmicion2" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:90px;text-align: right;"/>
					</td>
					<td class="">Moneda :</td>
					<td class="">
					<form:label path="moneda" id="lblMoneda" cssClass="texto_dentro" cssStyle="HEIGHT: 14px;width:300px" >${control.moneda}</form:label>
					</td>
				</tr>
				<tr>
					<td class="">&nbsp;&nbsp;&nbsp;Nro. Orden :</td>
					<td width="20%"><form:input path="nroOrden" id="txtNroOrden" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:100px;text-align: right;" /></td>
					<td class="">Fecha Emisi�n :</td>
					<td ><form:input path="fechaEmicion3" id="txtFechaEmicion3" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:90px;text-align: right;"/></td>
					<td class="">Monto :</td>
					<td ><form:input path="monto" id="txtMonto" readonly="true"
					cssClass="cajatexto_1" cssStyle="width:90px;text-align: right;"/></td>
				</tr>
			</table>
			
			<div id="divTabla" style="overflow: auto; height: 330px;width:99%">
			<table border="0" cellpadding="0" cellspacing="0" bordercolor="red"
			   style="width:96%;margin-top: 6px; margin-left: 9px;">
				<tr>
					<td width="100%">
					  <table cellpadding="0" cellspacing="1" border="0" bordercolor="blue" style="width:100%;">
						<tr>
							<td class="grilla">Producto/Servicio</td>
							<td class="grilla">Cantidad Solicitada</td>
							<td class="grilla">Precio Unitario</td>
							<td class="grilla">Cantidad Facturada</td>
							<td class="grilla">Unidad</td>
							<td class="grilla">SubTotal Facturado</td>
							<td class="grilla">Obs.</td>
						</tr>
						<c:forEach varStatus="loop" var="lista" items="${control.listDocumentos}"  >
						<tr class="texto">
						<td align="left" class="tablagrilla" style="width: 20%">
							&nbsp;&nbsp;&nbsp;<c:out value="${lista.nomItem}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 14%">
						<input type="text" id="canSol<c:out value="${loop.index}"/>" name="canSol<c:out value="${loop.index}"/>"
						size="11" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.cantidad}" />" readonly>
						</td>
						<td align="center" class="tablagrilla" style="width: 14%">
						<input type="text" id="preUni<c:out value="${loop.index}"/>" name="preUni<c:out value="${loop.index}"/>"
						size="11" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.preUni}" />" readonly>
						</td>
						<td align="center" class="tablagrilla" style="width: 14%">
						<input type="text" id="canFac<c:out value="${loop.index}"/>" name="canFac<c:out value="${loop.index}"/>"
						size="11" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.cantFacturada}" />" readonly>
						</td>
				
						<!-- unidad medida jhpr 2008-09-25 -->
						<td align="center" class="tablagrilla" style="width: 5%">
						<input type="text" id="unidad<c:out value="${loop.index}"/>" name="unidad<c:out value="${loop.index}"/>"
						size="6" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.unidadMedida}" />" readonly>
						</td>

						<td align="center" class="tablagrilla" style="width: 14%">
						<input type="text" id="subTot<c:out value="${loop.index}"/>" name="subTot<c:out value="${loop.index}"/>"
						size="11" style="text-align: right;" class="cajatexto_1" value="<c:out value="${lista.subTotal}" />" readonly>
						</td>
						<td align="center" class="tablagrilla" style="width: 1%">
						 <textarea cols="50" rows="2" readonly="readonly"
						id="obs<c:out value="${loop.index}"/>" name="obs<c:out value="${loop.index}"/>"
						 class="cajatexto_1">${lista.observacion}</textarea>
						</td>
					</tr>
					</c:forEach>
					
					</table>
					</td>					
				</tr>
				
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" class="" width="100%"
						  ID="Table5" style="margin-top: 10px;">
							<tr>
								<c:if test='${control.indImportacion!="1"}'>
								<td width="50%" class="texto_bold" align="right">SUB TOTAL :</td>
								</c:if>
								<c:if test='${control.indImportacion=="1"}'>
								<td width="50%" class="texto_bold" align="right">FACTURA PROVEEDOR :</td>
								</c:if>
								<td width="50%" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
									<form:input path="subTotal" id="txtSubTot"
									cssClass="cajatexto_1" cssStyle="width:65px; text-align: right;" readonly="true"/>
								</td>
							</tr>
							<tr>
								<c:if test='${control.indImportacion!="1"}'>
								<td width="50%" class="texto_bold" align="right">IGV :</td>
								<td width="50%" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
									<form:input path="igv" id="txtIgv"
									cssClass="cajatexto_1" cssStyle="width:65px; text-align: right;" readonly="true"/>
								</td>
								</c:if>
								<c:if test='${control.indImportacion=="1"}'>
									<td colspan="3">
									<form:hidden path="igv" id="txtIgv"/>
									<display:table name="sessionScope.listDocumentosRelacionados" cellpadding="0" cellspacing="1"
										decorator="com.tecsup.SGA.bean.LogisticaDecorator"
										requestURI="" style="border: 1px solid #048BBA;width:100%">
										<display:column property="numeroSelecciona" title="N�mero" headerClass="grilla" class="tablagrilla" style="align:left;width:250px;text-align:right"/>
										<display:column property="fechaSelecciona" title="Fecha" headerClass="grilla" class="tablagrilla" style="align:left;width:180px;text-align:right"/>
										<display:column property="monedaSelecciona" title="Moneda" headerClass="grilla" class="tablagrilla" style="align:left;width:150px;text-align:right"/>
										<display:column property="montoSelecciona" title="Monto" headerClass="grilla" class="tablagrilla" style="align:right;width:250px;text-align:right"/>
										<display:column property="tcSelecciona" title="T/C" headerClass="grilla" class="tablagrilla" style="align:right;width:150px;text-align:right"/>
										<display:column property="descripcionSelecciona" title="Descripci�n" headerClass="grilla" class="tablagrilla" style="align:left;width:600px;text-align:left"/>
										
										<display:setProperty name="basic.empty.showtable" value="true"  />
										<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>" />
										<display:setProperty name="paging.banner.placement" value="bottom"/>
										<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
										<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
										<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
									</display:table>
									<% request.getSession().removeAttribute("listDocumentosRelacionados"); %>
									</td>
								</c:if>
							</tr>
							<tr>
								<td width="50%" class="texto_bold" align="right">TOTAL :</td>
								<c:if test='${control.indImportacion!="1"}'>
								<td width="50%" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
									<form:input path="tot" id="txtTot"
									cssClass="cajatexto_1" cssStyle="width:65px; text-align: right;" readonly="true"/>
								</td>
								</c:if>
								<c:if test='${control.indImportacion=="1"}'>
								<td width="50%" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
									<form:input path="impOtros" id="txtTot"
									cssClass="cajatexto_1" cssStyle="width:65px; text-align: right;" readonly="true"/>
								</td>
								</c:if>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
			
			<table align=center id="botones" style="margin-top: 10px;">
				<tr>
					<td align="center">

					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnExportar','','${ctx}/images/botones/exportar2.jpg',1)">
						<img src="${ctx}/images/botones/exportar1.jpg" style="cursor:hand;" onclick="javascript:exportar();" alt="Exportar" id="btnExportar">
					</a>
					&nbsp;&nbsp;
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnRechazar','','${ctx}/images/botones/cerrar2.jpg',1)">
						<img src="${ctx}/images/botones/cerrar1.jpg" style="cursor:hand;" onclick="javascript:window.close();" alt="cerrar" id="btnRechazar">
					</a>
					
					</td>
				</tr>
			</table>
</form:form>
