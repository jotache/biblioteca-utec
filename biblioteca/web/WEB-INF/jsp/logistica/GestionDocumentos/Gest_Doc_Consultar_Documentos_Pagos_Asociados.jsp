<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	
	function fc_Ver(codDoc){
		
		url = "?txhCodUsuario="+document.getElementById("txhCodUsuario").value+
		"&txhCodOrden="+document.getElementById("txhCodOrden").value+
		"&txhCodDocumento="+codDoc+
		"&txhCodSede="+document.getElementById("txhCodSede").value;
		
		Fc_Popup("/SGA/logistica/consultarDocumentoPagoVer.html"+url,800,500);
	}
	
	function onLoad(){
		if(document.getElementById("txhMsg").value=="OK"){
			alert(mstrSeGraboConExito);
	     	document.getElementById("txhCodDocumento").value="";
	     	document.getElementById("txhMsg").value="";
	  	}
		else{
			if(document.getElementById("txhMsg").value=="ERROR")
	           	alert(mstrProblemaGrabar);
	      	else{
	      		if(document.getElementById("txhMsg").value=="ENVIAR_OK"){
	      			alert(mstrEnviarDocPago);
	    		  	document.getElementById("txhCodDocumento").value="";
	    		  	document.getElementById("txhMsg").value="";
	            }
	            else{
	            	if(document.getElementById("txhMsg").value=="ENVIAR_ERROR")
	                     alert(mstrNoEnviarDocPago);
               		else{
               			if(document.getElementById("txhMsg").value=="Buscar_iFrame")
	                         TablaDocumentoPago.style.display='inline-block';
                 	}
	            }
	      	}
	  	}
		document.getElementById("txhCodDocumento").value="";
		document.getElementById("txhMsg").value="";
	
		if(document.getElementById("txhIndImportacion").value == "0"){
			//si es Importacion se muestra el boton
			document.getElementById("botonDocumento").style.display="none";
		}
		fc_OcultarBotonDocumento();
		fc_EstadoCerrado();
	}
	function fc_EstadoCerrado(){
		if(document.getElementById("txhCodEstadoOrden").value == "0005"){
			document.getElementById("agregarDocumento").disabled = true;
			document.getElementById("anularDocumento").disabled = true;
			document.getElementById("imgDocumentos").disabled = false;
		}
	}
	function fc_Buscar(){
	    document.getElementById("txhOperacion").value = "BUSCAR";
	    document.getElementById("frmMain").submit();
	}
	
	function fc_Buscar_iFrame(){
	    document.getElementById("txhOperacion").value = "BUSCAR_IFRAME";
	    document.getElementById("frmMain").submit();
	}
	
	function fc_Agregar(){
	
		if(confirm(mstrSeguroAgregarDocPago)){
		     srtCodOrden= document.getElementById("txhCodOrden").value;
		     srtCodDocumento="";
		     srtCodSede=document.getElementById("txhCodSede").value;
		     srtCodUsuario=document.getElementById("txhCodUsuario").value;
		     srtIndImporatcion = document.getElementById("txhIndImportacion").value;
		     //ALQD,27/05/09. NUEVO IND_AFECTOIGV
		     srtIndAfectoIGV = document.getElementById("txhIndAfectoIGV").value;
		     document.getElementById("iFrameDocumentosPago").src="${ctx}/logistica/GestDocConDocPagosAsociadosIframe.html?"+
		      "txhCodOrden="+srtCodOrden + "&txhCodDocumento="+srtCodDocumento+
		      "&txhCodSede="+srtCodSede+"&txhCodUsuario="+srtCodUsuario +
		      "&txhIndImportacion="+srtIndImporatcion +"&txhIndAfectoIGV="+srtIndAfectoIGV;
			 TablaDocumentoPago.style.display='inline-block';
		 }
	
	}
	
	function fc_Cerrar(){
	}
	
	function fc_Regresar(){
	srtCodSede=document.getElementById("txhCodSede").value;
	location.href="${ctx}/logistica/BandejaConsultarOrdenesAprobadas.html?txhCodSede="+srtCodSede;
	}
	
	function fc_SeleccionarRegistro( srtCodDoc, srtCodEstado){
	document.getElementById("txhCodDocumento").value=srtCodDoc;
	document.getElementById("txhCodEstado").value=srtCodEstado;
	}
	
	function fc_Anular(){
	//txhConsteCodEstado ---> conste de estado Pendiente==0001
	if(document.getElementById("txhCodDocumento").value!="")
	{ 	if(document.getElementById("txhCodEstado").value==document.getElementById("txhConsteCodEstado").value ||
	       document.getElementById("txhCodEstado").value==document.getElementById("txhConsteCodEstadoEnviado").value)
			{ if(confirm(mstrSeguroAnularDocPago)){
		      	document.getElementById("txhOperacion").value = "ANULAR";
				document.getElementById("frmMain").submit();
		     }
		    }
		else alert(mstrDocPagoAnular);
	 }
	else alert(mstrSeleccione);
	}
	
	function fc_Enviar(){
	 if(document.getElementById("txhCodDocumento").value!="")
	 {if(document.getElementById("txhCodEstado").value==document.getElementById("txhConsteCodEstado").value)
		  {   if(confirm(mstrSeguroEnviarDocPago)){
			   document.getElementById("txhOperacion").value = "ENVIAR";
		       document.getElementById("frmMain").submit();
		      }
		  }
	   else alert(mstrDocPagoEnviar);
	 }
	 else alert(mstrSeleccione);
	}
	function fc_DocumentosRelacionados(){
		codUsuario = document.getElementById("txhCodUsuario").value;
		codOrden = document.getElementById("txhCodOrden").value;
		codEstadoOrden = document.getElementById("txhOpcionDocRel").value;
		
		Fc_Popup("/SGA/logistica/Documentos_Relacionados.html?txhCodUsuario="+codUsuario+"&txhDocPago="+codOrden+"&txhCodEstadoOrden="+codEstadoOrden,800,500);
	}
	function fc_OcultarBotonDocumento(){
		tamBandeja = document.getElementById("txhTamListaBandejaMedio").value;
		if(tamBandeja > 0){
			for(var i=0; i < tamBandeja; i++){
				codEstado=document.getElementById("txhEstado"+i).value;
				//BASTA QUE SEA UNO DE ESTROS ESTADOS Y SE DESABILITA EL BOTON
				//CONST_DOCPAGO_ESTADO_EN_VALIDA CHAR(4) := '0002';
				//CONST_DOCPAGO_ESTADO_VALIDA CHAR(4) := '0003';
				//CONST_DOCPAGO_ESTADO_RECHAZADA CHAR(4) := '0006';
				if(codEstado == "0002" || codEstado == "0003" || codEstado == "0006"){
					document.getElementById("txhOpcionDocRel").value="1";
				}
				//CONST_DOCPAGO_ESTADO_PENDIENTE CHAR(4) := '0001';
				//CONST_DOCPAGO_ESTADO_ANULADA CHAR(4) := '0005';
				//CONST_DOCPAGO_ESTADO_ENVIADA CHAR(4) := '0007';
			}
		}
		else{
			document.getElementById("imgDocumentos").disabled=false;
		}
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/GestDocConsultarDocumentosPagosAsociados.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codProveedor" id="txhCodProveedor"/>
<form:hidden path="codPerfil" id="txhCodPerfil"/>
<form:hidden path="codOpciones" id="txhCodOpciones"/>
<form:hidden path="codOrden" id="txhCodOrden"/>
<form:hidden path="codEstado" id="txhCodEstado"/>
<form:hidden path="codDocumento" id="txhCodDocumento"/>
<form:hidden path="consteCodEstado" id="txhConsteCodEstado"/>
<form:hidden path="indDocPago" id="txhIndDocPago"/>
<form:hidden path="consteCodEstadoEnviado" id="txhConsteCodEstadoEnviado"/>

<form:hidden path="indImportacion" id="txhIndImportacion"/>
<form:hidden path="tamListaBandejaMedio" id="txhTamListaBandejaMedio"/>
<!-- MODIFICADO RNAPA 03/09/2008 -->
<form:hidden path="codEstadoOrden" id="txhCodEstadoOrden"/>
<form:hidden path="opcionDocRel" id="txhOpcionDocRel"/>
<!-- ALQD,27/05/09.AGREGANDO IND_AFECTOIGV -->
<form:hidden path="indAfectoIGV" id="txhIndAfectoIGV"/>
<!-- ALQD,11/06/09. CAMBIOS PARA OC IMPORTACION -->
<div style="overflow: auto; height: 530px;width:100%">
<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
	style="margin-left:9px; margin-top: 7px;">
	<tr>
		<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		<td background="${ctx}/images/Evaluaciones/centro.jpg" width="760px" class="opc_combo">
			<font style="">Consultar Documentos de Pago Asociados
			<c:if test='${control.indImportacion=="1"}'>&nbsp;- Importaci�n</c:if>
			</font></td>
		<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
	</tr>
</table>

<table class="tabla" background="${ctx}/images/Evaluaciones/back.jpg"  border="0" bordercolor="red"
	style="width:96%;margin-top:6px;margin-left:9px; height:65px" cellspacing="2" cellpadding="0">
	<tr>
		<td class="" width="18%">&nbsp;&nbsp;Nro. Orden :</td>
		<td width="20%">
			<form:input path="nroOrden" id="nroOrden" cssClass="cajatexto_1" readonly="true"
				cssStyle="width:100px; text-align: right;" maxlength="10" />
		</td>
		<td class="" width="12%">Fec. Emisi�n :</td>
		<td width="20%">
			<form:input path="fecEmision" id="fecEmision" cssClass="cajatexto_1" readonly="true"
				cssStyle="width:70px; text-align: right;" maxlength="10"/>&nbsp;
		</td>
		<td class="" width="14%">Moneda :</td>
		<td width="12%">
			<form:input path="txtMoneda" id="txtMoneda" cssClass="cajatexto_1"
				readonly="true" cssStyle="width:80px" maxlength="10" />
		</td>
	</tr>
	<tr>
		<td class="">&nbsp;&nbsp;Proveedor :</td>
		<td colspan="1">
			<form:input path="txtNroRucProveedor" id="txtNroRucProveedor" cssClass="cajatexto_1"
				readonly="true" cssStyle="width:100px; text-align: right;" maxlength="10" />&nbsp;
		</td>
		<td colspan="2">
			<form:input path="txtNombreProveedor" id="txtNombreProveedor" cssClass="cajatexto_1"
				readonly="true" cssStyle="width:185px" maxlength="50" />
	 	</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;Monto Orden :</td>
		<td><form:input path="txtMontoOrden" id="txtMontoOrden" cssClass="cajatexto_1" readonly="true"
				cssStyle="width:80px; text-align: right;" maxlength="10" />
		</td>
		<td>Monto Total de Import. :</td>
		<td>
			<form:input path="importeOtros" id="txtImporteOtros" cssClass="cajatexto_1" readonly="true"
				cssStyle="width:70px; text-align: right;"/>
		</td>
		<td colspan="2">
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgDocumentos','','${ctx}/images/iconos/agregar2.jpg',1)" id="botonDocumento">
				<img src="${ctx}/images/iconos/agregar1.jpg" alt="Agregar Documentos" align="middle"
					style="cursor: pointer; margin-bottom: " onclick="javascript:fc_DocumentosRelacionados();" id="imgDocumentos">
			</a>&nbsp;&nbsp;Agregar documentos de importaci�n
		</td>
	</tr>
	<tr>
		<td class="">&nbsp;&nbsp;Monto Facturado Validado :</td>
		<td>
			<form:input path="txtMontoFacturadoValido" id="txtMontoFacturadoValido" cssClass="cajatexto_1"
				readonly="true"	cssStyle="width:70px; text-align: right;" maxlength="10" />
		</td>
		<td class="">Saldo Validado :</td>
		<td>
			<form:input path="txtSaldoValidado" id="txtSaldoValidado" cssClass="cajatexto_1"
				readonly="true"	cssStyle="width:70px; text-align: right;" maxlength="10" />
		</td>
		<td class="">Saldo Pendiente :</td>
		<td>
			<form:input path="txtSaldoPendiente" id="txtSaldoPendiente" cssClass="cajatexto_1"
				readonly="true"	cssStyle="width:80px; text-align: right;" maxlength="10" />
		</td>
	</tr>
</table>
<!-- BANDEJA  -->
<table cellpadding="0" cellspacing="0" border="0" bordercolor="green" width="100%" style="width:96%;margin-top:6px;margin-left:9px;">
	<tr>
		<td width="97%">
			<div style="overflow: auto; height: 110px">
				<table cellpadding="0" cellspacing="1" border="0" bordercolor="white" style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA">
					<tr>
						<td class="grilla" width="4%">Sel.</td>
						<td class="grilla">Nro. Factura.</td>
						<td class="grilla">Fec. Emisi�n</td>
						<td class="grilla">Fec. Vcto.</td>
						<td class="grilla">Monto</td>
						<td class="grilla">Estado</td>
						<td class="grilla">Detalle Estado</td>
						<td class="grilla">Nro. Gu�a</td>
						<td class="grilla">Ver</td>
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaBandejaMedio}"  >
					<tr class="texto">
					   	<td align="center" class="tablagrilla" style="width: 5%">
							<input type="radio" name="producto"
								id='valBa<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro('<c:out value="${lista.codDocumento}" />',
								'<c:out value="${lista.codEstado}" />');">
							<input type="hidden" id="txhEstado${loop.index}" value="${lista.codEstado}"/>
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.nroDocumento}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.fechaEmision}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.fechaVencimiento}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.monto}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 15%">
							<c:out value="${lista.dscEstado}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 15%">
							<c:out value="${lista.dscDetalleEstado}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.dscNroGuia}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<img src="${ctx}/images/iconos/buscar1.jpg" alt="Ver Detalle" align="middle"
							 style="cursor: pointer;" onclick="javascript:fc_Ver('<c:out value="${lista.codDocumento}" />')" id="imgBuscar">
						</td>
					</tr>
					</c:forEach>
				</table>
			</div>
		</td>
		<td align="right" width="3%">
			<c:if test="${control.codPerfil!='LCONS'}">
				<c:if test="${control.indDocPago=='1'}">
					<img src="${ctx}/images/iconos/agregar_on.gif" onclick="javascript:fc_Agregar();" style="cursor:hand" alt="Agregar Documento" id="agregarDocumento">
					<br>
				</c:if>
				<img src="${ctx}/images/iconos/cerrar_on.gif" alt="Anular Documento" onclick="javascript:fc_Anular();" style="cursor:hand" id="anularDocumento">
			    <br>
			    <img src="${ctx}/images/iconos/modificar_on.gif" onclick="javascript:fc_Enviar();" style="cursor:hand" alt="Enviar Documento">
		    </c:if>
		</td>
	</tr>
</table>
<!-- /BANDEJA  -->
<table align="center" border="0" bordercolor="blue" style="margin-top: 6px;">
	<tr>
		<td align=center>
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar01','','${ctx}/images/botones/regresar2.jpg',1)">
			<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar01" style="cursor:pointer;" onclick="javascript:fc_Regresar();"></a>
		</td>
	</tr>
</table>

<table id="TablaDocumentoPago" name="TablaDocumentoPago" style="display: none;" border="0" bordercolor="red" width="98%">
	<tr>
		<td>
			<iframe id="iFrameDocumentosPago" name="iFrameDocumentosPago" frameborder="0"  height="300px" width="100%">
			</iframe>
		</td>
	</tr>
</table>

</div>
</form:form>
<script language="javascript">
	addDocoff = "/SGA/images/iconos/agregar_off.gif";
	addDocon = "/SGA/images/iconos/agregar_on.gif";
	anuDocon = "/SGA/images/iconos/cerrar_on.gif";
	anuDocoff = "/SGA/images/iconos/cerrar_off.gif";
	if(document.getElementById("txhCodEstadoOrden").value == "0005"){
		document.getElementById("agregarDocumento").src = addDocoff;
		document.getElementById("anularDocumento").src = anuDocoff;
		document.getElementById("imgDocumentos").disabled = false;
	}else{
		document.getElementById("agregarDocumento").src = addDocon;
		document.getElementById("anularDocumento").src = anuDocon;
	}
</script>
</body>
</html>