<%@ include file="/taglibs.jsp"%>

<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	function onLoad(){ 
	if(document.getElementById("txhValSelec1").value=="")
	   document.getElementById("txhValSelec1").value="0";
	if(document.getElementById("txhValSelec2").value=="")
	   document.getElementById("txhValSelec2").value="0";
	   
	   document.getElementById("txhCodOrden").value="";
	   document.getElementById("txhCodEstado").value="";
	
	}
	
	function fc_Regresar()
		{
			document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhCodSede").value;
		}
		
	function fc_RegistroDocumento(){
	if(document.getElementById("txhCodOrden").value!=""){
		srtCodSede=document.getElementById("txhCodSede").value;
		srtCodOrden=document.getElementById("txhCodOrden").value;
		srtCodEstado=document.getElementById("txhCodEstado").value;
		srtCodUsuario=document.getElementById("txhCodUsuario").value;
		srtIndice=document.getElementById("txhIndDocPago").value;
		srtPerfil=document.getElementById("txhCodPerfil").value;
		
		location.href="${ctx}/logistica/GestDocConsultarDocumentosPagosAsociados.html?txhCodSede="+srtCodSede
		+"&txhCodOrden="+srtCodOrden+ "&txhCodEstado="+srtCodEstado+ "&txhCodUsuario="+srtCodUsuario+
		 "&txhIndDocPago="+srtIndice+"&txhCodPerfil="+srtPerfil;
	}
	else alert(mstrSeleccione);
	}
	
	function fc_Etapa1(param){
	if(param=="1")
		{ if(document.getElementById("txhValSelec1").value=="1")
	 	 document.getElementById("txhValSelec1").value="0";
	 	 else
	 	 document.getElementById("txhValSelec1").value=param;
		}
	}
	
	function fc_Etapa2(param)
	{ if(param=="1")
		  {  if(document.getElementById("txhValSelec2").value=="1")
		    	 document.getElementById("txhValSelec2").value="0";
		     else
		     document.getElementById("txhValSelec2").value=param;
	      }
	}
	
	function fc_Etapa3(param){
	//ALQD,21/01/09. NUEVA FUNCION
		if(param=="1")
		  {  if(document.getElementById("txhValSelec3").value=="1")
		    	 document.getElementById("txhValSelec3").value="0";
		     else
		     document.getElementById("txhValSelec3").value=param;
	      }
	}
	
	function fc_Buscar(){
		srtFechaInicio=document.getElementById("txtFecInicio").value;
		srtFechaFin=document.getElementById("txtFecFinal").value;
	    var num=0;
	    var srtFecha="0";
	    if( srtFechaInicio!="" && srtFechaFin!="")
	      {
	          num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
	          //num=1: si el primer parametro es mayor al segundo parametro
	          //num=0: si el segundo parametro es mayor o igual al primer parametro
	          //alert(num);
	          if(num==1) 	srtFecha="0";
	          else srtFecha="1";
	      }
	    
	    if(srtFecha=="0")
	    	{
	    	    document.getElementById("txhOperacion").value = "BUSCAR";
				document.getElementById("frmMain").submit();
	    	}
		else alert(mstrFecha);
	}
	function fc_SeleccionarRegistro(srtCodOrden, srtCodEstado, srtIndice){
	 	document.getElementById("txhCodOrden").value=srtCodOrden;
	  	document.getElementById("txhCodEstado").value=srtCodEstado;
	  	document.getElementById("txhIndDocPago").value=srtIndice;
	}
	
	function fc_Excel(){
	dscTipoOrden= frmMain.codTipoOrden.options[frmMain.codTipoOrden.selectedIndex].text.replace("--","");
	dscTipoOrden=dscTipoOrden.replace("--","");
	url="?tCodRep=" + fc_Trim(document.getElementById("txhCodTipoReporte").value) +
					"&tCodSede=" + fc_Trim(document.getElementById("txhCodSede").value) +
					"&tCodUsu=" + fc_Trim(document.getElementById("txhCodUsuario").value) +
					"&tCPerfil=" + fc_Trim(document.getElementById("txhCodPerfil").value) +
					"&tNOrden=" + fc_Trim(document.getElementById("nroOrden").value) +
					"&tFIni=" + fc_Trim(document.getElementById("txtFecInicio").value) +
					"&tFFin=" + fc_Trim(document.getElementById("txtFecFinal").value) +
					"&tRucP=" + fc_Trim(document.getElementById("nroRucProveedor").value) +
					"&tNomP=" + fc_Trim(document.getElementById("nombreProveedor").value) +
					"&tRucC=" + fc_Trim(document.getElementById("nroRucComprador").value) +
					"&tNomC=" + fc_Trim(document.getElementById("nombreComprador").value) +
					"&tCTOr=" + fc_Trim(document.getElementById("codTipoOrden").value) +
					"&tVa1=" + fc_Trim(document.getElementById("txhValSelec1").value) +
					"&tVa2=" + fc_Trim(document.getElementById("txhValSelec2").value) +
					"&tDTOr=" + fc_Trim(dscTipoOrden)+
					"&tNFact" + fc_Trim(document.getElementById("numFactura").value) +
					"&tVa3=" + fc_Trim(document.getElementById("txhValSelec3").value);
	
	window.open("${ctx}/logistica/reportesLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");
	
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/BandejaConsultarOrdenesAprobadas.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codPerfil" id="txhCodPerfil"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codOpciones" id="txhCodOpciones"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="valSelec1" id="txhValSelec1"/>
<form:hidden path="valSelec2" id="txhValSelec2"/>
<form:hidden path="consteSoloTipoPago" id="txhConsteSoloTipoPago"/>
<form:hidden path="consteSoloDoc" id="txhConsteSoloDoc"/>
<form:hidden path="codOrden" id="txhCodOrden"/>
<form:hidden path="codEstado" id="txhCodEstado"/>
<form:hidden path="indDocPago" id="txhIndDocPago"/>
<form:hidden path="fechaBD" id="txhFechaBD"/>
<form:hidden path="banListaBandeja" id="txhBanListaBandeja"/>
<form:hidden path="codTipoReporte" id="txhCodTipoReporte"/>
<form:hidden path="dscUsuario" id="txhDscUsuario"/>
<form:hidden path="nomSede" id="txhNomSede"/>
<form:hidden path="valSelec3" id="txhValSelec3"/>

<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
</table>
	<!-- title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo"><font style="">Consulta Ordenes Aprobadas</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<!-- BANDEJA -->
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
		cellspacing="2" cellpadding="2" border="0">
		<tr>
			<td class="">Nro. Orden:</td>
			<td><form:input path="nroOrden" id="nroOrden" cssClass="cajatexto"
							onkeypress="fc_ValidaNroOrden();"
							onblur="fc_ValidaNroOrdenOnblur(this, 'Nro. Orden.');"
							cssStyle="width:90px" maxlength="15" />
			</td>
			<td class="">Fec. Emisi�n:</td>
			<td>
				<form:input path="fecInicio" id="txtFecInicio" cssClass="cajatexto"
					onkeypress="javascript:fc_Slash('frmMain','txtFecInicio','/');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio');" 
					cssStyle="width:70px" maxlength="10"/>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
						<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
							align="absmiddle" alt="Calendario" style="cursor:pointer;">
					</a>&nbsp;&nbsp;
				<form:input path="fecFinal" id="txtFecFinal" cssClass="cajatexto"
					onkeypress="javascript:fc_Slash('frmMain','txtFecFinal','/');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal');" 
					cssStyle="width:70px" maxlength="10"/>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
						<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
							align="absmiddle" alt="Calendario" style="cursor:pointer;">
					</a>
			</td>
			<td clas="">Nro. Factura:</td>
			<td><form:input path="numFactura" id="numFactura" cssClass="cajatexto"
					onkeypress="fc_ValidaNroOrden();"
					cssStyle="width:90px" maxlength="15" />
			</td>
		</tr>
		<tr>
			<td class="">Proveedor:</td>
			<td>
				<form:input path="nroRucProveedor" id="nroRucProveedor" cssClass="cajatexto"
					onkeypress="fc_PermiteNumeros();"
					onblur="fc_ValidaNumeroOnBlur(this.id);"
					cssStyle="width:65px" maxlength="11" />&nbsp;
				<form:input path="nombreProveedor" id="nombreProveedor" cssClass="cajatexto"
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaSoloLetrasFinal1(this,'Proveedor');"
					cssStyle="width:190px" maxlength="50" />
			</td>
			<td class="">Comprador:</td>
			<td>
				<form:input path="nroRucComprador" id="nroRucComprador" cssClass="cajatexto"
					onkeypress="fc_PermiteNumeros();"
					onblur="fc_ValidaNumeroOnBlur(this.id);"
					cssStyle="width:65px" maxlength="11" />&nbsp;
				<form:input path="nombreComprador" id="nombreComprador" cssClass="cajatexto"
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaSoloLetrasFinal1(this, 'Comprador');"
					cssStyle="width:190px" maxlength="50" />
			</td>
			<!-- ALQD,21/01/09. PARA MOSTRAR LOS INGRESOS POR CONFIRMAR -->
			<td colspan="2">
				<input Type="checkbox" name="checkbox3"
				    <c:if test="${control.valSelec3==control.consteIngresoPendiente}"><c:out value=" checked=checked " /></c:if>
					id="checkbox3" onclick="javascript:fc_Etapa3('1');">Ver ingresos x confirmar&nbsp;
			</td>
		</tr>
		<tr>
			<td class="" width="10%">Tipo Orden:</td>
			<td width="30%">
				<form:select path="codTipoOrden" id="codTipoOrden" cssClass="cajatexto" 
					cssStyle="width:110px">
					<form:option value="-1">-- Todos --</form:option>
					<c:if test="${control.listaCodTipoOrden!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaCodTipoOrden}"/>
					</c:if>
				</form:select>
			</td>
			<td colspan="3" width="45%">
				<input Type="checkbox" name="checkbox1"
					<c:if test="${control.valSelec1==control.consteSoloTipoPago}"><c:out value=" checked=checked " /></c:if>
					id="checkbox1" onclick="javascript:fc_Etapa1('1');">Solo Tipos de Pago: Caja Chica&nbsp;
				<input Type="checkbox" name="checkbox2"
					<c:if test="${control.valSelec2==control.consteSoloDoc}"><c:out value=" checked=checked " /></c:if>
					id="checkbox2" onclick="javascript:fc_Etapa2('1');">&nbsp;Solo Doc's Sin Envio a Proveedor &nbsp;
				
			</td>
			<td align="right" width="15%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
						 style="cursor: pointer; 60px" 
						 onclick="javascript:fc_Limpiar();" id="imgLimpiar">
				</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle" 
						 style="cursor: pointer; margin-bottom: " onclick="javascript:fc_Buscar();" id="imgBuscar">
				</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgExcel','','${ctx}/images/iconos/exportarex2.jpg',1)">
					<img src="${ctx}/images/iconos/exportarex1.jpg" alt="Exportar" align="middle" 
						 style="cursor: pointer; margin-right" 
						 onclick="javascript:fc_Excel();" id="imgExcel">
				</a>
			</td>
			
		</tr>
	</table>
	<!-- BANDEJA -->
	<table cellpadding="0" cellspacing="0" 
		style="border: 0px solid #048BBA;width:97%;margin-left:9px ;margin-top: 6px">
		<tr>
			<td>
				<div style="overflow: auto; height:300px;width:100%">
					<table cellpadding="0" cellspacing="1" 
						style="border: 1px solid #048BBA;width:98%;">
						<tr>
							<td class="grilla" width="4%">Sel.</td>
							<td class="grilla">Nro. Orden.</td>
							<td class="grilla">Fec. Emisi�n</td>
							<td class="grilla">Proveedor</td>
							<td class="grilla">Moneda</td>
							<td class="grilla">Monto<br>Orden</td>
							<td class="grilla">Monto<br>Facturado</td>
							<td class="grilla">Saldo</td>
							<td class="grilla">Comprador</td>
							<td class="grilla">Tipo<br>Orden</td>
							<td class="grilla">Estado</td>
						</tr>
						<c:forEach varStatus="loop" var="lista" items="${control.listaBandeja}" >
						<tr class="texto">
							<td align="center" class="tablagrilla" style="width: 5%">
							<input type="radio" name="producto"
								id='valBa<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro('<c:out value="${lista.codOrden}" />','<c:out value="${lista.codEstado}" />', '<c:out value="${lista.indDocPago}" />');">
							
							</td>
							<td align="center" class="tablagrilla" style="width: 12%">
								<c:out value="${lista.nroOrden}" />
							</td>
							<td align="center" class="tablagrilla" style="width: 8%">
								<c:out value="${lista.fechaEmision}" />
							</td>
							<td align="left" class="tablagrilla" style="width: 13%">
								<c:out value="${lista.dscProveedor}" />
							</td>
							<td align="center" class="tablagrilla" style="width: 6%">
								<c:out value="${lista.dscMoneda}" />
							</td>
							<td align="right" class="tablagrilla" style="width: 10%">
								<c:out value="${lista.monto}" />
							</td>
							<td align="right" class="tablagrilla" style="width: 10%">
								<c:out value="${lista.montoFacturado}" />
							</td>
							<td align="right" class="tablagrilla" style="width: 10%">
								<c:out value="${lista.montoSaldo}"/>
							</td>
							<td align="left" class="tablagrilla" style="width: 14%">
								<c:out value="${lista.dscComprador}" />	
							</td>
							<td align="left" class="tablagrilla" style="width: 12%">
								<c:out value="${lista.codTipoOrden}" />
							</td>
							<td align="left" class="tablagrilla" style="width: 15%">
								<c:out value="${lista.dscEstado}" />
							</td>
						</tr>
						</c:forEach>
						
						<c:if test='${control.banListaBandeja=="0"}'>
						<tr class="tablagrilla">
							<td align="center" colspan="11" height="20px">No se encontraron registros
							</td>
						</tr>
						</c:if>
						
					</table>
				</div>
			</td>
		</tr>
	</table>
	
	
	<table id="botondevolver" align="center" style="margin-top: 10px">
		<tr>
			<td align=center>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegistrar','','${ctx}/images/botones/registrardoc2.jpg',1)">
					<img alt="Registrar Documento" src="${ctx}/images/botones/registrardoc1.jpg" id="imgRegistrar" style="cursor:pointer;" onclick="fc_RegistroDocumento();">
				</a>
			</td>
		</tr>
	</table>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
</body>
</html>