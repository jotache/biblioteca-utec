<%@ include file="/taglibs.jsp"%>
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	function onLoad(){
	 document.getElementById("txhCodOrden").value="";
	 document.getElementById("txhCodDocumento").value="";
	}
	
	function fc_Regresar()
		{
			document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhCodSede").value;
		}
		
	function fc_Validar_Documento_Pago(){
	  	if(document.getElementById("txhCodDocumento").value!=""){
	  		srtCodSede=document.getElementById("txhCodSede").value;
			srtCodDocumento=document.getElementById("txhCodDocumento").value;
			srtCodUsuario=document.getElementById("txhCodUsuario").value;
		    srtCodOrden=document.getElementById("txhCodOrden").value;
		
		location.href="${ctx}/logistica/log_VerDocumento.html?txhCodSede="+srtCodSede
		+"&txhCodDocumento="+srtCodDocumento+ "&txhCodUsuario="+srtCodUsuario+
		 "&txhCodOrden="+srtCodOrden;
	 }	
	else alert(mstrSeleccione);
	}
	
	 //1 q la fecha inicial es mayor o igual ala fecha final
      //0 es vacia las 2 fechas o la fecha final es mayor q la fecha incial
	function fc_Validar_Fechas(srtFechaInicio, srtFechaFin)
	{
	 var num=0;
	 var srtFecha="0";
	 if( srtFechaInicio!="" && srtFechaFin!="")
	{   
		    num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
		      //alert(num);
		     if(num==1) srtFecha="0";
		     else srtFecha="1";	    
	 }
	 
    return srtFecha;
	}
	
	function fc_Buscar(){
	var fec1=fc_Validar_Fechas(document.getElementById("txtFecInicio1").value,
	document.getElementById("txtFecFinal1").value);
	var fec2=fc_Validar_Fechas(document.getElementById("txtFecInicio2").value,
	document.getElementById("txtFecFinal2").value);
	
	if(fec1=="0" && fec2=="0")
	 { document.getElementById("txhOperacion").value = "BUSCAR";
	   document.getElementById("frmMain").submit();
	 }
	 else{ if(fec1!="0") alert("La fecha inicio de Nro. Factura debe ser menor a la fecha fin.");
	 	   else if(fec2!="0") alert("La fecha inicio de Nro. Orden debe ser menor a la fecha fin.");
	 } 
	
	}

	function fc_SeleccionarRegistro(srtCodOrden, srtCodDocumento){
	 document.getElementById("txhCodOrden").value=srtCodOrden;
	 document.getElementById("txhCodDocumento").value=srtCodDocumento;
	}
	
	function fc_Limpiar(){
	document.getElementById("nroFactura").value="";
	document.getElementById("txtFecInicio1").value="";
	document.getElementById("txtFecFinal1").value="";
	document.getElementById("nroRucProveedor").value="";
	document.getElementById("nombreProveedor").value="";
	document.getElementById("codEstado").value="-1";
	document.getElementById("nroOrden").value="";
	document.getElementById("txtFecInicio2").value="";
	document.getElementById("txtFecFinal2").value="";
	
	}
	/*onkeypress="fc_ValidaNumerosGuion();" validar nroOrden
	 onblur="fc_ValidaNumeroGuionOnblur(this, 'Nro. Orden.');"*/
	 function fc_Excel(){
	 }
	 
	function fc_Estado(){}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/GestDocAprobarDocumentosPago.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codPerfil" id="txhCodPerfil"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codOpciones" id="txhCodOpciones"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="codOrden" id="txhCodOrden"/>
<form:hidden path="codDocumento" id="txhCodDocumento"/>
<form:hidden path="banListaBandeja" id="txhBanListaBandeja"/>

<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
</table>
	<!-- TITULO -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo"><font style="">Consultar Documentos de Pago</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>	
	<!-- CABECERA -->
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2" >
			<tr>
				<td class="">Nro. Factura:</td>
				<td><form:input path="nroFactura" id="nroFactura" cssClass="cajatexto"
								onkeypress="fc_ValidaNumerosGuion();"
								onblur="fc_ValidaNumeroGuionOnblur(this, 'Nro. Factura');"
								cssStyle="width:90px" maxlength="15" />
				</td>
				<td class="">Fec. Emisi�n:</td>
				<td ><form:input path="fecInicio1" id="txtFecInicio1" cssClass="cajatexto"
								onkeypress="javascript:fc_Slash('frmMain','txtFecInicio1','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio1');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni1','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni1"
								alt="Calendario" style="cursor:pointer;"></a>&nbsp;&nbsp;
				<form:input path="fecFinal1" id="txtFecFinal1" cssClass="cajatexto"
								onkeypress="javascript:fc_Slash('frmMain','txtFecFinal1','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal1');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin1','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin1"
								alt="Calendario" style="cursor:pointer;"></a>
				</td>
			</tr>
			<tr>
				<td class="">Proveedor:</td>
				<td>
					<form:input path="nroRucProveedor" id="nroRucProveedor" cssClass="cajatexto"
								onkeypress="fc_PermiteNumeros();"
								onblur="fc_ValidaNumeroOnBlur(this.id);"
								cssStyle="width:65px" maxlength="11" />&nbsp;
					<form:input path="nombreProveedor" id="nombreProveedor" cssClass="cajatexto"
								onkeypress="fc_ValidaTextoEspecial();"
								onblur="fc_ValidaSoloLetrasFinal1(this,'Proveedor');"
								cssStyle="width:250px" maxlength="50" />
				</td>
				<td class="">Estado:</td>
				<td>
					<form:select path="codEstado" id="codEstado" cssClass="cajatexto" cssStyle="width:180px" 
					       onchange="javascript:fc_Estado();">
						<form:option  value="-1">--Todos--</form:option>
							<c:if test="${control.listaCodEstado!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaCodEstado}"/>
							</c:if>
					</form:select>
				</td>
			</tr>
				
			<tr>
				<td class="">Nro. Orden:</td>
				<td><form:input path="nroOrden" id="nroOrden" cssClass="cajatexto"
				                onkeypress="fc_ValidaNroOrden();" 
							    onblur="fc_ValidaNroOrdenOnblur(this, 'Nro. Orden.');"
								cssStyle="width:90px" maxlength="15" />
				</td>
				<td class="">Fec. Emisi�n:</td>
				<td>
					<form:input path="fecInicio2" id="txtFecInicio2" cssClass="cajatexto"
								onkeypress="javascript:fc_Slash('frmMain','txtFecInicio2','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio2');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni2','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni2"
								alt="Calendario" style="cursor:pointer;"></a>&nbsp;&nbsp;
				<form:input path="fecFinal2" id="txtFecFinal2" cssClass="cajatexto"
								onkeypress="javascript:fc_Slash('frmMain','txtFecFinal2','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal2');"  
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin2','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin2"
								alt="Calendario" style="cursor:pointer;"></a>
				</td>
	
				<td align="right">	
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
							<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
								 style="cursor: pointer; 60px" 
								 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
							
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
							<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle" 
								 style="cursor: pointer; margin-bottom: " onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgExcel','','${ctx}/images/iconos/exportarex2.jpg',1)">
							<img src="${ctx}/images/iconos/exportarex1.jpg" alt="Limpiar" align="middle" 
								 style="cursor: pointer; margin-right" 
								 onclick="javascript:fc_Excel();" id="imgExcel"></a>
				</td>
			</tr>	
		</table>		
					
		<table cellpadding="0" cellspacing="0" style="margin-top: 6px;margin-left: 9px;width: 97%;"
		      border="0">
			<tr>
				<td>
					<div style="overflow: auto; height:300px;width:100%">
					<table cellpadding="0" cellspacing="1" style="border: 1px solid #048BBA;width:98%;">
						<tr>
							<td class="grilla" width="4%">Sel.</td>							
							<td class="grilla">Nro. Factura.</td>
							<td class="grilla">Fec. Emisi�n</td>
							<td class="grilla">Proveedor</td>
							<td class="grilla">Moneda</td>
							<td class="grilla">Monto</td>
							<td class="grilla">Fec. Vcto.</td>
							<td class="grilla">Estado</td>
							<td class="grilla">Detalle Estado</td>
							<td class="grilla">Nro. Orden</td>													
						</tr>
						<c:forEach varStatus="loop" var="lista" items="${control.listaBandeja}"  >
						<tr class="texto">
						    <td align="center" class="tablagrilla" style="width: 5%">
								<input type="radio" name="producto"
									id='valBa<c:out value="${loop.index}"/>'
									onclick="fc_SeleccionarRegistro('<c:out value="${lista.codOrden}" />'
									,'<c:out value="${lista.codDocumento}" />');">
							</td>
							<td align="center" class="tablagrilla" style="width: 8%">							
								<c:out value="${lista.nroDocumento}" />
							</td>
							<td align="center" class="tablagrilla" style="width: 9%">
								<c:out value="${lista.fechaEmision}" />
							</td>
							<td align="left" class="tablagrilla" style="width: 10%">
								<c:out value="${lista.dscProveedor}" />
							</td>
							<td align="center" class="tablagrilla" style="width: 5%">
								<c:out value="${lista.dscMoneda}" /> 					
							</td>
							<td align="right" class="tablagrilla" style="width: 7%">
								<c:out value="${lista.monto}" />							
							</td>
							<td align="center" class="tablagrilla" style="width: 9%">
								<c:out value="${lista.fechaVencimiento}" />
							</td>
							<td align="left" class="tablagrilla" style="width: 10%">						
								<c:out value="${lista.dscEstado}" />							
							</td>
							<td align="left" class="tablagrilla" style="width: 15%">
								<c:out value="${lista.dscDetalleEstado}" /> 					
							</td>
							<td align="center" class="tablagrilla" style="width: 15%">
								<c:out value="${lista.nroOrden}" /> 					
							</td>
												
						</tr>
						</c:forEach>
						<c:if test='${control.banListaBandeja=="0"}'>
						<tr class="tablagrilla">
							<td align="center" colspan="10" height="20px">No se encontraron registros
							</td>
						</tr>					 		
						</c:if>	
					</table>
					</div>
				</td>				
			</tr>		
		</table>
		
		<table align="center" style="margin-top: 10px">
			<tr>
				<td align=center>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgVerDoc','','${ctx}/images/botones/verdocumento2.jpg',1)">
						<img alt="Ver Documento" src="${ctx}/images/botones/verdocumento1.jpg" id="imgVerDoc" style="cursor:pointer;" onclick="fc_Validar_Documento_Pago();">
					</a>
				</td>
			</tr>			
		</table>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio1",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni1",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal1",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin1",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecInicio2",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni2",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal2",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin2",
		singleClick    :    true
	});
</script>
</body>
</html>