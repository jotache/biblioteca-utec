<%@ include file="/taglibs.jsp"%>
<%@page import="com.tecsup.SGA.common.CommonSeguridad"%>
<%@page import="com.tecsup.SGA.bean.UsuarioSeguridad"%>

<head>
	
	<script language=javascript>
	function onLoad(){
	}
	
	function fc_Principales(nomMenu)
	{
	
		if ( document.getElementById(nomMenu).style.display == 'none' )
		{
			/*trMenu06.style.display = 'none';
			trMenu05.style.display = 'none';
			trMenu04.style.display = 'none';*/
			
			if ( document.getElementById("trMenu06") != null  )
				document.getElementById("trMenu06").style.display = 'none';
			if ( document.getElementById("trMenu05") != null  )
				document.getElementById("trMenu05").style.display = 'none';
			if ( document.getElementById("trMenu04") != null  )
				document.getElementById("trMenu04").style.display = 'none';
			if ( document.getElementById("trMenu03") != null  )
				document.getElementById("trMenu03").style.display = 'none';
			if ( document.getElementById("trMenu02") != null  )
				document.getElementById("trMenu02").style.display = 'none';
			if ( document.getElementById("trMenu01") != null  )
				document.getElementById("trMenu01").style.display = 'none';
			
			document.getElementById(nomMenu).style.display = 'inline-block';
		}
		else
		{
			document.getElementById(nomMenu).style.display = 'none';
		}
	}
	
	function cambiarSede(){		
		parent.cambiarSede(document.getElementById("cboSede").value);	
	}
	
	function fc_Opciones(opcion)
	{	
	
		strSede = "";
		if ( document.getElementById("cboSede") != null  )
		{
			if ( document.getElementById("cboSede").value == "" )
			{
				alert(mstrDebeSelSede);
				return false;
			}
			strSede =document.getElementById("cboSede").value; 
		}
		
		switch (opcion)
		{			
			case "LOG_ADMSIS":	
			location.href="${ctx}/logistica/log_Mant_Config_Log.html" + 
						"?txhCodAlumno=" + document.getElementById("txhCodUsuario").value + "&txhCodSede=" + strSede;  
				break;
			case "LOG_SREQ":						
				location.href="${ctx}/logistica/bandejaSreqUsuario.html" +
						"?txhCodSede=" + strSede;
				break;
			case "LOG_AREQ":						
				location.href="${ctx}/logistica/bandejaAreqUsuario.html" +
						"?txhCodSede=" + strSede;
				break;
			case "LOG_PENCOT":						
				location.href="${ctx}/logistica/bandejaPenAtencionCotizacion.html" +
						"?txhCodSede=" + strSede;
				break; 
			case "LOG_SOLCOT":						
				location.href="${ctx}/logistica/bandejaSolSolicitudCotizacion.html" +
						"?txhCodSede=" + strSede + "&txhDscSede=" + frmMain.cboSede.options[frmMain.cboSede.selectedIndex].text;
				break; 
			case "LOG_ORDCOM":						
				location.href="${ctx}/logistica/GestOrdenBandejaConsultaOrden.html" +
						"?txhCodSede=" + strSede +
						"&txhNomSede=" + frmMain.cboSede.options[frmMain.cboSede.selectedIndex].text ;
				break;
			case "LOG_DOCPAGO":						
				location.href="${ctx}/logistica/BandejaConsultarOrdenesAprobadas.html" +
						"?txhCodSede=" + strSede + 
						"&txhNomSede=" + frmMain.cboSede.options[frmMain.cboSede.selectedIndex].text ;
				break;
			case "LOG_ADOCPAGO":						
				location.href="${ctx}/logistica/GestDocAprobarDocumentosPago.html" +
						"?txhCodSede=" + strSede ;
				break;
			case "LOG_AREQBS":						
				location.href="${ctx}/logistica/AtenRequerimientosBandejaBienesServicios.html" +
						"?txhCodSede=" + strSede;
				break;
			case "LOG_AREQMA":						
				location.href="${ctx}/logistica/AtenRequerimientosBandejaMantenimiento.html" +
						"?txhCodSede=" + strSede;
				break;
			case "LOG_ALRBS":						
				location.href="${ctx}/logistica/RecepcionBienes.html" +
						"?txhCodSede=" + strSede;
				break;
			case "LOG_ALRBD":						
				location.href="${ctx}/logistica/RecepcionBienesDevueltos.html" +
						"?txhCodSede=" + strSede + "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value;
				break;			
			case "LOG_ALRBD":						
				location.href="${ctx}/logistica/RecepcionBienesDevueltos.html" +
						"?txhCodSede=" + strSede + "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value;
				break;
			case "LOG_ALRBE":						
				location.href="${ctx}/logistica/ActualizarInventarioFisico.html" +
						"?txhCodSede=" + strSede + "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value;
				break;
			case "LOG_CONREP":						
				location.href="${ctx}/logistica/consulta_reportes.html" +
						"?txhCodSede=" + strSede;
				break;
			case "LOG_INTCONT":						
				location.href="${ctx}/logistica/activarInterfazContable.html" +
						"?txhCodSede=" + strSede;
				break;
			case "LOG_SREQ2":						
				//bandejaSreqUsuario.html
				location.href="${ctx}/logistica/bandejaSreqPresupuestado.html" +
						"?txhCodSede=" + strSede;
				break;
		}				
	}
			</script>
	</head>
	<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/menuLogistica.html">
	<form:hidden path="codEval" id="txhCodUsuario"/>	
	<form:hidden path="sedeSelWork" id="txhSedeSelWork"/>
	<table cellSpacing="0" cellPadding="2" style="width: 97%; margin-top: 3px;margin-left:10px;" >
		<tr height="2px" bgcolor="#00A0E4">
			<td></td>
			<td>
			<%--			
			<span style="font-family: Tahoma;">	
			<font size="4" >
				<b>Noticia:</b><br>
				Por motivo de toma de inventario el Almac�n estar� cerrado
				para solicitar materiales los d�as 29 y 30 de diciembre.
			</font>
			</span>
			--%>
			</td>
			<td rowspan="18" align="right" height="280px" width="463px"><img
				src="${ctx}/images/Logistica/opciones-log.jpg">
			</td>
		</tr>
		<%
			UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
			String strPerfiles ="";
			if (usuarioSeguridad!=null) {
				strPerfiles = usuarioSeguridad.getPerfiles();
			} 
			
			if ( strPerfiles.indexOf("LRCECO") > -1 ||
				strPerfiles.indexOf("LDCECO") > -1 ||
				strPerfiles.indexOf("LDADM") > -1 ||
				strPerfiles.indexOf("LGEGE") > -1 ||
				strPerfiles.indexOf("LCOMP") > -1 ||
				strPerfiles.indexOf("LJLOG") > -1 ||
				strPerfiles.indexOf("LALMC") > -1 ||
				strPerfiles.indexOf("LMANTO") > -1 ||
				strPerfiles.indexOf("LUSU") > -1 ||
				strPerfiles.indexOf("LRDP") > -1 ) {
		%>
		<tr bgcolor="#00A0E4" id="trComboSede" style="height: 50px">
			<!--td width="6%" align="left">&nbsp;</td-->
			<td colspan="2" style="FONT-SIZE: 10px;TEXT-TRANSFORM: none;COLOR: #ffffff;FONT-FAMILY: Verdana;TEXT-DECORATION: none;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SEDE :</b>&nbsp;
				<form:select path="codSede" id="cboSede" cssClass="combo_o" onchange="cambiarSede();" 
					cssStyle="width:100px">
					<c:if test="${control.listaSedes!=null}">
						<form:options itemValue="codSede" itemLabel="dscSede"
							items="${control.listaSedes}" />
					</c:if>
				</form:select>
			</td>
		</tr>
		<% } %>
		<%		
		if (
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_SREQ") == 1 ||
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_AREQ") == 1) 
			{
		%>
			<tr bgcolor="#00A0E4" >
				<td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
				<td >
					<a href="javascript:fc_Principales('trMenu01');" class="enlace"
					style="cursor: hand">Solicitud y Aprobaci�n de Requerimientos</a>
				</td>
			</tr>
			<tr id="trMenu01" style="display:none; height: 50px" bgcolor="#00A0E4" >
				<td align="right"></td>
				<td >
					<table cellSpacing="2" cellPadding="2" style="background-color:#00A0E4; height: 50px" >
						<%		
						if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_SREQ") == 1) {
						%>
						<tr>
							<td align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_SREQ');" class="enlace"
								style="cursor: hand">Solicitud de Requerimientos</a></td>
						</tr>
						<%
						}
						%>
						<% if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_AREQ") == 1){ %>						
						<tr>
							<td width="5%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_AREQ');" class="enlace"
								style="cursor: hand">Aprobaci�n de Requerimientos</a></td>
						</tr>
						<% } 
						if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_SREQ") == 1){ %>
						<%-- 						
						<tr>
							<td width="5%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_SREQ2');" class="enlace"
								style="cursor: hand">Solicitud y Aprobaci&oacute;n de lo Presupuestado</a></td>
						</tr>
						--%>
						<% } %>
					</table>
				</td>
			</tr>
		<%
		}
		%>

		<%		
		if (
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_PENCOT") == 1 ||
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_SOLCOT") == 1) 
			{
		%>
			<tr bgcolor="#00A0E4">
				<td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
				<td >
					<a href="javascript:fc_Principales('trMenu02');" class="enlace"
					style="cursor: hand">Cotizaci�n de Bienes y Servicios</a>
				</td>
			</tr>
			<tr id="trMenu02" style="display:none" bgcolor="#00A0E4">
				<td align="right">&nbsp;</td>
				<td >
					<table cellSpacing="2" cellPadding="2" style="WIDTH:100%; background-color:#00A0E4" >
						<%		
						if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_PENCOT") == 1) {
						%>
						<tr>
							<td width="5%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_PENCOT');" class="enlace"
								style="cursor: hand">Pendientes de Atenci�n para Cotizaci�n</a></td>
						</tr>
						<%
						}
						%>
						<%		
						if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_SOLCOT") == 1){
						%>
						<tr>
							<td width="5%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_SOLCOT');" class="enlace"
								style="cursor: hand">Solicitudes de Cotizaci�n</a></td>
						</tr>
						<%
						}
						%>
					</table>
				</td>
			</tr>
		<%
		}
		%>

		<%		
		if (
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_ORDCOM") == 1) 
			{
		%>
			<tr bgcolor="#00A0E4">
				<td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
				<td >
					<a href="javascript:fc_Principales('trMenu03');" class="enlace"
					style="cursor: hand">Gesti�n de Ordenes de Compra / Servicios</a>
				</td>
			</tr>
			<tr id="trMenu03" style="display:none" style="cursor:hand;" bgcolor="#00A0E4">
				<td align="right">&nbsp;</td>
				<td >
					<table cellSpacing="2" cellPadding="2" style="WIDTH:100%; background-color: transparent" >
						<%		
						if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_ORDCOM") == 1) {
						%>
						<tr>
							<td width="5%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_ORDCOM');" class="enlace"
								style="cursor: hand">Consulta y Aprobaci�n de �rdenes</a></td>
						</tr>
						<%
						}
						%>
					</table>
				</td>
			</tr>
		<%
		}
		%>

        <%		
		if (
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_DOCPAG") == 1 ||
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_ADOPAG") == 1) 
			{
		%>
			<tr bgcolor="#00A0E4">
				<td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
				<td >
					<a href="javascript:fc_Principales('trMenu04');" class="enlace"
					style="cursor: hand">Gesti�n de Documentos de Pago</a>
				</td>
			</tr>
			<tr id="trMenu04" style="display:none" bgcolor="#00A0E4">
				<td align="right">&nbsp;</td>
				<td >
					<table cellSpacing="2" cellPadding="2" style="WIDTH:100%; background-color:#00A0E4" >
						<%		
						if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_DOCPAG") == 1) {
						%>
						<tr>
							<td width="5%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_DOCPAGO');" class="enlace"
								style="cursor: hand">Documentos de Pago</a></td>
						</tr>
						<%
						}
						%>
						<%		
						if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_ADOPAG") == 1){
						%>
						<tr>
							<td width="5%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_ADOCPAGO');" class="enlace"
								style="cursor: hand">Aprobar Documentos de Pago</a></td>
						</tr>
						<%
						}
						%>
					</table>
				</td>
			</tr>
		<%
		}
		%>
		
        <%		
		if (
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_ALRBS") == 1 ||
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_ALRBD") == 1 ||
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_ALRBE") == 1)
			{
		%>
			<tr bgcolor="#00A0E4">
				<td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
				<td >
					<a href="javascript:fc_Principales('trMenu06');" class="enlace"
					style="cursor: hand">Almac�n</a>
				</td>
			</tr>
			<tr id="trMenu06" style="display:none" bgcolor="#00A0E4">
				<td align="right">&nbsp;</td>
				<td >
					<table cellSpacing="2" cellPadding="2" style="WIDTH:100%; background-color:#00A0E4" >
						<%		
						if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_ALRBS") == 1) {
						%>
						<tr>
							<td width="5%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_ALRBS');" class="enlace"
								style="cursor: hand">Recepci�n de Bienes Solicitados</a></td>
						</tr>
						<%
						}
						%>
						<%		
						if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_ALRBD") == 1){
						%>
						<tr>
							<td width="5%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_ALRBD');" class="enlace"
								style="cursor: hand">Recepci�n de Bienes Devueltos</a></td>
						</tr>
						<%
						}
						%>
						<%		
						if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_ALRBE") == 1){
						%>
						<tr>
							<td width="5%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_ALRBE');" class="enlace"
								style="cursor: hand">Actualizar Inventario F�sico</a></td>
						</tr>
						<%
						}
						%>
					</table>
				</td>
			</tr>
		<%
		}
		%>
		
        <%		
		if (
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_AREQBS") == 1 ||
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_AREQMA") == 1) 
			{
		%>
			<tr bgcolor="#00A0E4">
				<td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
				<td >
					<a href="javascript:fc_Principales('trMenu05');" class="enlace"
					style="cursor: hand">Atenci�n de Requerimientos</a>
				</td>
			</tr>
			<tr id="trMenu05" style="display:none" bgcolor="#00A0E4">
				<td align="right">&nbsp;</td>
				<td >
					<table cellSpacing="2" cellPadding="2" style="WIDTH:100%; background-color:#00A0E4" >
						<%		
						if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_AREQBS") == 1) {
						%>
						<tr>
							<td width="5%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_AREQBS');" class="enlace"
								style="cursor: hand">Atencion de Requerimientos Bienes - Servicios</a></td>
						</tr>
						<%
						}
						%>
						<%		
						if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_AREQMA") == 1){
						%>
						<tr>
							<td width="5%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
							<td><a href="javascript:fc_Opciones('LOG_AREQMA');" class="enlace"
								style="cursor: hand">Atenci�n de Requerimientos de Mantenimiento</a></td>
						</tr>
						<%
						}
						%>
					</table>
				</td>
			</tr>
		<%
		}
		%>
      
    	<%		
		if (
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_CONREP") == 1) 
			{
		%>
			<tr bgcolor="#00A0E4">
				<td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
				<td >
					<a href="javascript:fc_Opciones('LOG_CONREP');" class="enlace"
					style="cursor: hand">Consultas y Reportes</a>
				</td>
			</tr>
		<%
		}
		%>
    
		<%		
		if (
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_ADMSIS") == 1) 
			{
		%>
			<tr bgcolor="#00A0E4">
				<td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
				<td >
					<a href="javascript:fc_Opciones('LOG_ADMSIS');" class="enlace"
					style="cursor: hand">Administraci�n del Sistema</a>
				</td>
			</tr>
		<%
		}
		%>
		<%		
		if (
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"LOG_INTCONT") == 1) 
			{
		%>
			<tr bgcolor="#00A0E4">
				<td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
				<td >
					<a href="javascript:fc_Opciones('LOG_INTCONT');" class="enlace"
					style="cursor: hand">Interfaz Contable</a>
				</td>
			</tr>
		<%
		}
		%>
		
		<tr bgcolor="#00A0E4" height="5px">
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form:form>
<script type="text/javascript">	
	if (document.getElementById("txhSedeSelWork").value!=''){
		document.getElementById("cboSede").value=document.getElementById("txhSedeSelWork").value;		
	}
</script>

