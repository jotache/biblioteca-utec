<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	java.util.Date ahora = new java.util.Date();
	java.util.Calendar actual = java.util.Calendar.getInstance();
	actual.setTime(ahora);
	String tiempo = String.valueOf(actual.get(java.util.Calendar.YEAR)
			+ "," + actual.get(java.util.Calendar.MONTH) + ","
			+ actual.get(java.util.Calendar.DAY_OF_MONTH) + ","
			+ actual.get(java.util.Calendar.HOUR_OF_DAY) + ","
			+ actual.get(java.util.Calendar.MINUTE) + ","
			+ actual.get(java.util.Calendar.SECOND));
%>

<script type="text/javascript">
	var tiempo=new Date(<%=tiempo%>);
	function mostrarReloj(){
		local=new Date();
		tiempo.setSeconds(tiempo.getSeconds()+1);
		//document.getElementById('reloj').innerHTML="Lima,&nbsp;"+setZeroString(tiempo.getDate())+"-"+setZeroString(tiempo.getMonth()+1)+"-"+tiempo.getFullYear()+"&nbsp;"+setZeroString(tiempo.getHours())+":"+setZeroString(tiempo.getMinutes())+":"+setZeroString(tiempo.getSeconds())+"&nbsp;&nbsp;";	
		//document.getElementById('txtHoraIni').innerHTML="Hr. Inicio:&nbsp;"+setZeroString(tiempo.getHours())+":"+setZeroString(tiempo.getMinutes())+":"+setZeroString(tiempo.getSeconds())+"";
		document.getElementById('txtHoraIni').value=setZeroString(tiempo.getHours())+":"+setZeroString(tiempo.getMinutes())+":"+setZeroString(tiempo.getSeconds());
		setTimeout('mostrarReloj()',990);
	}
	
	function setZeroString(s_valor){
		return (s_valor<10)?'0'+s_valor:s_valor;
	}
</script>