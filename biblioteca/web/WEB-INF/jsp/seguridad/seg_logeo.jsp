<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<script language=javascript>
	<!--
		function onLoad(){
			document.getElementById("txtUsuario").focus();
		}
		
		function fc_Validar(){
			if (fc_Trim(document.getElementById("txtUsuario").value) == ""){
				alert(mstrUsuario);
				document.getElementById("txtUsuario").focus();
				return false;
			}
			
			if(fc_Trim(document.getElementById("txtClave").value) == ""){
				alert(mstrClave);
				document.getElementById("txtClave").focus();
				return false;
			}
			return true;
		}
		
		function fc_Enter(e, param){
			if (e.keyCode == 13){fc_Ingresar(param)}
		}
	
		function fc_Upload(param) {
			switch(param){
				case '1':
					Fc_Popup("${ctx}/reclutamiento/cambio_clave.html",350,165,"SGA")
					break;	
				case '2':
					Fc_Popup("${ctx}/reclutamiento/olvido_clave.html",350,90,"SGA")
					break;		
			}
		}
		
		function fc_Ingresar(param) {
				switch(param){
					case '1':
						if (fc_Validar()){
							document.getElementById('txhOperacion').value = "ACCESO";
							document.getElementById("frmMain").submit();
						}
						break;
					case '2':
						document.getElementById('txhOperacion').value = "REGISTRO";
						location.href="${ctx}/reclutamiento/cuerpo_registro.html?txhIdRec=&txhOperacion=" + document.getElementById('txhOperacion').value;
						break;	
				}
		}
	//-->
	</script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" >
	<form:form name="frmMain" commandName="control" action="${ctx}/logeo.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="idRec" id="txhIdRec"/>
		
		<table cellpadding="0" cellspacing="0" width="700px" class="grilla" align="center" border="0">
			<tr>
				<td width="25%">&nbsp;</td>
				<td width="25%">&nbsp;</td>
				<td width="25%">&nbsp;</td>
				<td width="25%">&nbsp;</td>
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" style="margin-left:150px">
			<tr>
				<td><img src="${ctx}/images/reclutamiento/recluta_portada.gif"></td>
				<td width="329px" valign="top">
					<table class="tabla" height="255px" border="0">
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3" align=center><b>SISTEMA DE RECLUTAMIENTO</b></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="10%">
							<td width="25%">&nbsp;</td>
							<td>Email :</td>
							<td>
								<form:input path="usuario" id="txtUsuario" cssClass="cajatexto_1"/>
							</td>
						</tr>
						<tr height="10%">
							<td>&nbsp;</td>
							<td>Clave :</td>
							<td>
								<form:password path="clave" id="txtClave" cssClass="cajatexto_1" 
									onkeypress="javascript:fc_Enter(event,'1')"/>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><input type="button" class="boton" value="Ingresar" onclick="javascript:fc_Ingresar('1');">&nbsp;
								<input type="button" class="boton" value="Registrarse" onclick="javascript:fc_Ingresar('2');">
							</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td valign="top" width="65%" style="cursor:hand" onclick="javascript:fc_Upload('1');"><u>Cambio de Clave</u></td>
						</tr>

						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td valign="top" width="65%" style="cursor:hand" onclick="javascript:fc_Upload('2');"><u>�Olvido su Clave?</u></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="700px" class="grilla" align="center">
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
	</form:form>
		
	<script type="text/javascript" language="javascript">
		if (document.getElementById("txhTypeMessage").value != ""){
		strMsg = document.getElementById("txhMessage").value;
			switch(document.getElementById("txhTypeMessage").value){
				case 'ERROR':
					if ( strMsg != ""){
						alert(strMsg);
					}
					break;
				case 'PASS':
					if ( strMsg != ""){
						alert(strMsg);
					}
					break;
				case 'OK':
					location.href="${ctx}/reclutamiento/cuerpo_registro.html?txhIdRec=" + document.getElementById('txhIdRec').value + 
									"&txhOperacion="+ document.getElementById('txhOperacion').value;
					break;
			}
		}
		document.getElementById("txhTypeMessage").value = "";
		document.getElementById("txhMessage").value = "";
	</script>	
</body>