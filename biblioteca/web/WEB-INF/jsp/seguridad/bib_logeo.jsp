<%@ include file="/taglibs.jsp"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<head>
	<script type="text/javascript">	
	
	function onLoad(){
			document.getElementById("txtUsuario").focus();
		}
		function fc_Validar(){			
			if (fc_Trim(document.getElementById("txtUsuario").value) == ""){				
				alert('Debe ingresar un usuario.');
				document.getElementById("txtUsuario").focus();
				return false;
			}
			if(fc_Trim(document.getElementById("txtClave").value) == ""){
				alert('Debe ingresar la clave de su cuenta.');
				document.getElementById("txtClave").focus();
				return false;
			}
			return true;
		}
		function fc_Enter(e, param){
			if (e.keyCode == 13){fc_Ingresar()}
		}
		function fc_Ingresar() {
			if (fc_Validar()){				
				$('#txhOperacion').val('ACCESO');
				$('#frmMain').submit();
			}
		}	
		
		
		function OpenPopupCenter(pageURL, title, w, h) {
			var left = (screen.width - w) / 2;
			var top = (screen.height - h) / 4;  // for 25% - devide by 4  |  for 33% - devide by 3		
			title='';			
			var targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
		}
		
		
	</script>
	<title>Sistema de Gesti�n Administrativa - Biblioteca</title>

	<script type="text/javascript"> 
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-16798972-1']);  _gaq.push(['_trackPageview']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="${ctx}/styles/login/login-biblioteca.css" rel="stylesheet" type="text/css" />
	<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.2.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function(){
			$("#cerceve").hide().fadeIn(500);
			$(".show").hide();
			$(".close").click(function(){
				$("#cerceve").hide(500);
				$(".show").fadeIn(500);
			});
			$(".show").click(function(){
				$("#cerceve").fadeIn(500);
				$(".show").hide(500);
			});
		});
	</script>

</head>
<body>

	<div class="show"></div>
	<div id="cerceve">
	
		<div style="background-color:#E6E7E7">
			<img src="${ctx}/images/biblioteca/login/logo-tecsup-fondogris.png" border="0" width="210" height="59" align="middle"  />	    
		</div>
		
		<div class="header">
			<div class="text" style="float:left">
		 	SGA Biblioteca - Iniciar sesi�n</div>
		    <div class="close" style="float:right;margin-right:20px;cursor:pointer;">x</div>
		</div>
		<div class="formbody">
			<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/logeoBiblioteca.html" >
				<form:hidden path="operacion" id="txhOperacion"/>
				<form:hidden path="message" id="txhMessage"/>
				<form:hidden path="typeMessage" id="txhTypeMessage"/>
				<form:hidden path="idRec" id="txhIdRec"/>
				<input type="hidden" id="codSis" value="bib"/>			
		
			<form:input path="usuario" id="txtUsuario" cssClass="text" cssStyle="background:url(${ctx}/images/biblioteca/login/username.png) no-repeat;" />
			
			<form:password path="clave" id="txtClave" cssClass="text" cssStyle="background:url(${ctx}/images/biblioteca/login/password.png) no-repeat;" onkeypress="javascript:fc_Enter(event,'1')"/>
						
			<input type="button" class="submit" value="Ingresar" onClick="javascript:fc_Ingresar();" style="background:url(${ctx}/images/biblioteca/login/login.png) no-repeat;">
			
			
<!-- 			<a style="font-family:arial;color:black;font-size:11px;display:block;"  -->
<!-- 								href="javascript:void(0)"  -->
<!-- 								onclick="OpenPopupCenter('http://www.tecsup.edu.pe/Seguridad/', 'Recuperar Clave', 750, 500)">&iquest;Olvid&eacute; mi contrase&#241;a?</a>			 -->
			
			</form:form>
		</div>
	</div>

		
	<script type="text/javascript">
		
		strMsg = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";		
		if(strMsg!="" && strMsg=="OK"){
			window.location.href="${ctx}/Biblioteca.html";			
		}
		else if ( strMsg == "-1" || strMsg == "-5" || strMsg == "-7"){alert('Usuario o Clave incorrectos.');}
		else if ( strMsg == "-2"){alert('Usuario no est� activo.');}
		else if ( strMsg == "-3"){alert('Usuario est� dentro del periodo de bloqueo.');}
		else if ( strMsg == "-4"){alert('Usuario alcanz� el n�mero m�ximo de intentos fallidos permitidos.');}				
		else if ( strMsg == "-6"){window.location.href="${ctx}/biblioteca/cambiaClaveBiblioteca.html?msg=1";}		
		else if ( strMsg == "NA"){alert('Lo sentimos pero no tiene opciones disponibles en este sistema');} 
		else if ( strMsg == "NH"){alert('Lo sentimos pero no tiene acceso al sistema desde su ubicaci�n');} 
		else if ( parseFloat(strMsg)>0 ){				       
			alert('Falta '+ strMsg +' dias para que tu clave expire\nSe recomienda que cambie su clave.');
			window.location.href="${ctx}/Biblioteca.html";
		}
	</script>
		
</body>