<%@ include file="/taglibs.jsp"%>
<head>
	<style type="text/css">
	.fondoTitulo{
	    FONT-WEIGHT: 100;
	    FONT-SIZE: 12px;
	    TEXT-TRANSFORM: capitalize;
	    COLOR: white;
	    FONT-FAMILY: Arial;    
	    BACKGROUND-COLOR: #7B0F1E;    
	    TEXT-ALIGN: center;
		HEIGHT:30px;	
	}
	</style>
	<script language=javascript>
	<!--
		function onLoad(){
			$("txtUsuario").focus();
		}
		
		function fc_Validar(){
			if (fc_Trim(document.getElementById("txtUsuario").value) == ""){
				alert('Debe ingresar un usuario.');
				$("txtUsuario").focus();
				return false;
			}
			
			if(fc_Trim(document.getElementById("txtClave").value) == ""){
				alert('Debe ingresar la clave de su cuenta.');
				$("txtClave").focus();
				return false;
			}
			return true;
		}
		
		function fc_Enter(e, param){
			if (e.keyCode == 13){fc_Ingresar(param)}
		}
	
		function fc_Upload(param) {
			switch(param){
				case '1':
					Fc_Popup("/SGA/reclutamiento/cambio_clave.html",350,165,"SGA")
					break;	
				case '2':
					Fc_Popup("/SGA/reclutamiento/olvido_clave.html",350,90,"SGA")
					break;		
			}
		}
		
		function fc_Ingresar(param) {
				switch(param){
					case '1':
						if (fc_Validar()){														
							$('txhOperacion').value="ACCESO";
							$('frmMain').submit();
						}
						break;
					case '2':
						$('txhOperacion').value = "REGISTRO";
						location.href="/SGA/reclutamiento/cuerpo_registro.html?txhIdRec=&txhOperacion=" + $('txhOperacion').value+"&codSis=rec";
						break;	
				}
		}
	//-->
	</script>
	<title>Sistema de Gesti�n Administrativa - Reclutamiento</title>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" >

<script type="text/javascript"> 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16798972-1']);  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

	<form:form name="frmMain" id="frmMain" commandName="control" action="/SGA/logeoReclutamiento.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="idRec" id="txhIdRec"/>
		<input type="hidden" id="codSis" value="rec"/>
		<br><br><br><br>
		<table cellpadding="0" cellspacing="0" width="700px" class="fondoTitulo" align="center" border="0">
			<tr>
				<td colspan="4" ><strong>SISTEMA DE  DE RECLUTAMIENTO</strong></td>				
			</tr>            
       	</table>
		
		<table cellpadding="0" cellspacing="0" border="0" width="700px" align="center">
			<tr>
				<td><img src="/SGA/images/reclutamiento/recluta_portada.gif"></td>
				<td width="329px" valign="top">
					<table class="tabla" height="255px" border="0">
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3" align=center><b>&nbsp;</b></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="10%">
							<td width="25%">&nbsp;</td>
							<td>Usuario :</td>
							<td>
								<form:input path="usuario" id="txtUsuario" cssClass="cajatexto_1"/>
							</td>
						</tr>
						<tr height="10%">
							<td>&nbsp;</td>
							<td>Clave :</td>
							<td>
								<form:password path="clave" id="txtClave" cssClass="cajatexto_1" 
									onkeypress="javascript:fc_Enter(event,'1')"/>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><input type="button" class="boton" value="Ingresar" onclick="javascript:fc_Ingresar('1');">&nbsp;
								
							</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td valign="top" width="65%" style="cursor:hand" ></td>
						</tr>

						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td valign="top" width="65%" style="cursor:hand" ></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<table cellpadding="0" cellspacing="0" width="700px" class="fondoTitulo" align="center">    
            <tr>
				<td >&nbsp;</td>
			</tr>
		</table>
		
	</form:form>
		
	<script type="text/javascript" language="javascript">
		
		strMsg = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";		
		if(strMsg!="" && strMsg=="OK"){window.location.href="${ctx}/Reclutamiento.html";}
		else if ( strMsg == "-1" || strMsg == "-5" || strMsg == "-7"){alert('Usuario o Clave incorrectos.');}
		else if ( strMsg == "-2"){alert('Usuario no est� activo.');}
		else if ( strMsg == "-3"){alert('Usuario est� dentro del periodo de bloqueo.');}
		else if ( strMsg == "-4"){alert('Usuario alcanz� el n�mero m�ximo de intentos fallidos permitidos.');}						
		else if (strMsg=="-6"){window.location.href="${ctx}/reclutamiento/cambiaClaveReclutamiento.html?msg=1";}		
		else if ( strMsg == "-9"){alert("No cuenta con un perfil asignado a esta sistema.");}
		else if ( parseFloat(strMsg)>0 ){
			alert('Falta '+ strMsg +' dias para que tu clave expire\nSe recomienda que cambie su clave.');
			window.location.href="/SGA/Reclutamiento.html";
		}
	</script>	
</body>