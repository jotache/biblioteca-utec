<%@ include file="/taglibs.jsp"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<head>
	<script type="text/javascript">
	<!--
		function onLoad(){
			document.getElementById("txtUsuario").focus();
		}
		
		function fc_Validar(){
			if (fc_Trim(document.getElementById("txtUsuario").value) == ""){
				alert(mstrUsuario);
				document.getElementById("txtUsuario").focus();
				return false;
			}
			
			if(fc_Trim(document.getElementById("txtClave").value) == ""){
				alert(mstrDebeIngClave);
				document.getElementById("txtClave").focus();
				return false;
			}
			return true;
		}
		
		function fc_Enter(e, param){
			if (e.keyCode == 13){fc_Ingresar(param)}
		}
	
		function fc_Ingresar(param) {
			if (fc_Validar()){				
				form = document.forms[0];
				form.txhOperacion.value="ACCESO";
				form.submit();
			}
		}
		function OpenPopupCenter(pageURL, title, w, h) {
			var left = (screen.width - w) / 2;
			var top = (screen.height - h) / 4;  // for 25% - devide by 4  |  for 33% - devide by 3
			title = '';
			var targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
		}
		
	//-->
	</script>
	<title>Sistema de Gesti�n Administrativa - Log�stica</title>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" >

<script type="text/javascript"> 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16798972-1']);  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

	<form:form name="frmMain" commandName="control" action="${ctx}/logeoLogistica.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="idRec" id="txhIdUsuario"/>
		<br><br><br><br>
		<table cellpadding="0" cellspacing="0" width="700px" class="grilla" align="center" border="0">
			<tr>
				<td colspan="4" >SISTEMA DE LOG�STICA</td>
				
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" width="700px" align="center">
			<tr>
				<td><img src="${ctx}/images/reclutamiento/recluta_portada.gif"></td>
				<td width="329px" valign="top">
					<table class="tabla" height="255px" border="0">
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3" align=center><b></b></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="10%">
							<td width="25%">&nbsp;</td>
							<td style="color: #048BBB;">Usuario :</td>
							<td>
								<form:input path="usuario" id="txtUsuario" cssClass="cajatexto_1" />
							</td>
						</tr>
						<tr height="10%">
							<td>&nbsp;</td>
							<td style="color: #048BBB;">Clave :</td>
							<td>
								<form:password path="clave" id="txtClave" cssClass="cajatexto_1" 
									onkeypress="javascript:fc_Enter(event,'1')"/>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><input type="button" class="boton" value="Ingresar" onClick="javascript:fc_Ingresar('1');">&nbsp;
								
							</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td><TD align=center colspan="2"><a style="font-family:arial;color:black;font-size:11px;display:block;" href="javascript:void(0)" onclick="OpenPopupCenter('http://www.tecsup.edu.pe/Seguridad/', 'Recuperar Clave', '750', '500')">&iquest;Olvid&oacute; su contrase&#241;a?</a></td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="700px" class="grilla" align="center">
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		
	</form:form>
		
	<script type="text/javascript" language="javascript">
		
		strMsg = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";		
		if(strMsg!="" && strMsg=="OK"){	window.location.href="${ctx}/Logistica.html";}
		else if ( strMsg == "-1"){alert(mstrUsuarioNoReg);}
		else if ( strMsg == "-2"){alert(mstrUsuarioNoActivo);}
		else if ( strMsg == "-3"){alert(mstrUsuarioPerBloqueo);}
		else if ( strMsg == "-4"){alert(mstrUsuarioMaxIntFall);}
		else if ( strMsg == "-5"){alert(mstrClaveIncorrecta);}
		//else if ( strMsg == "-6"){alert(mstrClaveSinVigencia);}
		else if ( strMsg == "-6"){window.location.href="${ctx}/logistica/cambiaClaveLogistica.html?msg=1";}
		else if ( strMsg == "-7"){alert(mstrErrorLogueo);}
		else if ( strMsg == "-9"){alert(mstrUsuarioNoAccess);}
		else if ( parseFloat(strMsg)>0 ){				       
			alert('Falta '+ strMsg +' dias para que tu clave expire\nSe recomienda que cambie su clave.');
			window.location.href="${ctx}/Logistica.html";
		}
	</script>	
</body>