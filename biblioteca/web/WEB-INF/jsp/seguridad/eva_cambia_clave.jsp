<%@ include file="/taglibs.jsp"%>
<!-- JHPR 2008-04-15 para evitar acceder a esta p�gina si no se esta logeado -->
<% if (request.getSession().getAttribute("usuarioClave") == null) { %>
<script type="text/javascript">		
	alert("Su sesi�n ha terminado, favor de volver a ingresar al sistema.");			
	top.parent.location.href = "/SGA/logeoEvaluacion.html";	
</script>
<%}%>
<head>
<style type="text/css">
  .textoClave 
  {
    FONT-SIZE: 12px;    
	HEIGHT: 18px;
	COLOR:#606060;
    FONT-FAMILY: Arial;
  }
</style>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<script language=javascript>
	<!--
		function onLoad(){			
			<%	String ingresando = (String)session.getAttribute("contextoIngreso");
				if(ingresando!=null) {
			%>
				document.getElementById("txtclavenueva").focus();
			<%}else{%>
				document.getElementById("txtclaveactual").focus();
			<%};%>
						
			var strMsgInicial = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";		
			if(strMsgInicial=="1"){alert(mstrPorSegCambiaClave);}						
			
			var strMsgCambioClave = "<%=(( request.getAttribute("mens")==null)?"": request.getAttribute("mens"))%>";
			if (strMsgCambioClave=="-1") {alert(mstrErrClaCaracMinPer);}
			else if ( strMsgCambioClave == "-2"){alert('Tu Clave Nueva tiene menos n�meros que el m�nimo permitido. Intenta otra clave.');}
			else if ( strMsgCambioClave == "-3"){alert('Tu Clave Nueva no puede ser igual que las anteriormente ingresadas. Intenta otra clave');}
			else if ( strMsgCambioClave == "-4"){alert('Hay un problema desconocido en tu cambio de clave. Consulta al Administrador de Sistemas.');}
			//Extra
			else if ( strMsgCambioClave == "-5"){alert('La Clave Actual que ingresaste no es correcta');}
			return true;	
		}
		
		function fc_Validar(){	
			
			<% if(ingresando==null) { %>				
			if (fc_Trim(document.getElementById("txtclaveactual").value) == ""){
				alert('Debes ingresar tu clave actual');				
				document.getElementById("txtclaveactual").focus();
				return false;
			}			
			<% } %>
			
			if(fc_Trim(document.getElementById("txtclavenueva").value) == ""){				
				alert('Debe ingresar la nueva clave');				
				document.getElementById("txtclavenueva").focus();
				return false;
			}			
			if(fc_Trim(document.getElementById("txtclaveconfirma").value) == ""){
				alert('Vuelva a ingresar la nueva clave');				
				document.getElementById("txtclaveconfirma").focus();
				return false;
			}	
			if(!(document.getElementById("txtclavenueva").value==document.getElementById("txtclaveconfirma").value)){				
				alert("Tu Clave Nueva no es igual a tu Clave Confirmada.");				
				document.getElementById("txtclavenueva").value="";
				document.getElementById("txtclaveconfirma").value="";
				document.getElementById("txtclavenueva").focus();
				return false;			
			}	
			return true;			
		}
														
		function fc_Ingresar() {
			if (fc_Validar()) {		
				document.getElementById('txhOperacion').value = "CAMBIAR_CLAVE";					
				document.getElementById("frmMain").submit();
			}				
		}
		
		function fc_Regresar() {
			//Regresar al menu de inicio (menuEvaluaciones).
			window.location.href = "${ctx}/menuEvaluaciones.html";
		}
		
		function fc_CerrarSesion()
		{						
			document.getElementById('txhOperacion').value = "SALIR";
			document.getElementById("frmMain").submit();
		}		
		
	//-->
	</script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" >
		<table cellpadding="0" cellspacing="0" width="989px" border="0" align="center">
			<tr>
				<td><img src="${ctx}/images/cabecera/cabecera_celeste.jpg"></td>
			</tr>
		</table>
		<table border="0" bordercolor="white" cellspacing="0" cellpadding="0" width="989px" align="center">
			<tr>	
				<td valign="middle" width="21px" align="center">
					&nbsp;&nbsp;<img alt="Cerrar Sesi�n" align="middle" src="${ctx}/images/iconos/limpiar1.jpg" onclick="fc_CerrarSesion();" style="cursor: pointer;"/>										
				</td>
				<td width="7px"></td>
				<td>
					<label class="Opciones" onclick="fc_CerrarSesion();" style="cursor: pointer;"><b>Salir</b></label>
				</td>							
			</tr>	
		</table>
		
	<form:form name="frmMain" id="frmMain" commandName="changePasswordCommand" action="${ctx}/evaluaciones/cambiaClaveEvaluacion.html" >		
		<form:hidden path="message" id="txhMessage"/>		
		<form:hidden path="operacion" id="txhOperacion"/>
		
		<table width='750' align=center border=0 cellpadding="0" cellspacing="0">
			<tr><td height='4'></td></tr>
			<tr><td colspan=3 height="1PX" class="fondoceleste"></td></tr>
			<tr>
			<td width="1" class="fondoceleste"></td>
			<td width="748">
	
			<table width='100%' align=center border=0 cellpadding="0" cellspacing="0">
			<tr>
				<td width='8'   height='8' rowspan=3></td>
				<td width='730' height='8'></td>
				<td width='8'   height='8' rowspan=3></td>
			</tr>
			<tr>
				<td>
	
				<table width='100%' align=center border=0 cellpadding="0" cellspacing="0">
				<tr>
					<td class="Tab_off" align="center"><strong>Cambio de Contrase�a</strong></td>
				</tr>
				<tr><td height=10PX></td></tr>
			
				<%	if(ingresando==null) {	%>
				<tr>
					<td class="textoClave">Para modificar su <b>Contrase�a</b>, ingrese la que est&aacute; usando actualmente y la nueva (confirmarla tambi&eacute;n). Luego haga clic en el bot&oacute;n <b>Cambiar</b>.</td>
				</tr>
				<%	} else {	%>
				<tr>
					<td class="textoClave">Por motivos de seguridad le pedimos que modique su contrase�a.</td>
				</tr>			
				<%	}	%>
				<tr><td height=10PX></td></tr>
				<tr>
					<td>
	
					<table width="100%" border="0" cellspacing="0" cellpadding="0" align=center>
			
					<%	if(ingresando==null) {	%>
					<TR>
						<TD width=45% height=20PX class="textoClave" align="right"><b>Contrase�a Actual:</b>&nbsp;</TD>
						<TD width=55% height=20PX>						
							<form:password path="passwordOld" id="txtclaveactual" cssClass="" size="20"/>
						</TD>
					</TR>
					<%	}	%>
					<TR>
						<TD width=45% height=20PX class="textoClave" align="right"><b>Contrase�a Nueva:</b>&nbsp;</TD>
						<TD width=55% height=20PX>						
							<form:password path="passwordNew" id="txtclavenueva" cssClass="" size="20"/>
						</TD>
					</TR>
					<TR>					
						<TD width=45% height=20PX class="textoClave" align="right"><b>Confirma Nueva:</b>&nbsp;</TD>
						<TD width=55% height=20PX>											    
						    <input type="password" id="txtclaveconfirma" size="20" class="">
						</TD>
					</TR>
					<tr><td height=10PX></td></tr>
					<tr>
						<td class="">&nbsp;</td>
						<td><INPUT type="button" name="accion" value="Cambiar" class="boton" onclick="javascript:fc_Ingresar();">
						<%	if(ingresando==null){	%>
							&nbsp;<INPUT type="button" name="accion" value="Regresar" class="boton" onclick="javascript:history.back();">
						<%	}%>
						</TD>
					</tr>
					</table>
	
					</td>
				</tr>
				<tr><td height=10PX></td></tr>
				</table>
	
				</td>
			</tr>
			<tr>	<td height='8'></td></tr>
			</table>
	
			</td>
			<td width="1" class="fondoceleste"></td>
		</tr>
		<tr>	<td colspan=3 height="1PX" class="fondoceleste"></td></tr>
		<tr>	<td height="4PX"></td></tr>
		</table>
	
	</form:form>
			
</body>