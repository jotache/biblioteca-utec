<%@ include file="/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<%	
if(request.getSession().getAttribute("usuarioSeguridad")==null) {%>
<script type="text/javascript">	
	<% if (request.getSession().getAttribute("reingreso")!=null) {%>
		alert("Por favor reingrese al sistema con su nueva clave.");
	<% }else{ %>
		alert("Su sesi�n ha terminado, favor de volver a ingresar al sistema.");
	<% } %>		
	location.href = "/SGA/logeoLogistica.html";	
</script>
<%}else {	
	if ( request.getSession().getAttribute("bandejaLogisticaBean")!= null ){
		 request.getSession().removeAttribute("bandejaLogisticaBean");
		}
 }
%>	

<head>

<link href="${ctx}/styles/cab.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" type="text/javascript">
		function onLoad(){
						
		}	
		function fc_CerrarSesion()
		{
			if ( confirm("�Est� seguro de cerrar sesi�n?") )
			{				
				form = document.forms[0];
				form.txhAccion.value="CERRAR_SESION";
				form.submit();
			}
		}
		function fc_CambiarClave()
		{			 			
			window.location.href = "${ctx}/logistica/cambiaClaveLogistica.html";			
		}
		function cambiarSede(sede){
			//alert(sede);
			form = document.forms[0];
			form.sedeOperacion.value=sede;
			form.txhAccion.value="CAMBIASEDE";
			form.submit();
		}
	</script>	
	
<title>Sistema de Log�stica</title>
</head>
<body>
	<form:form name="frmMain" commandName="control" action="${ctx}/Logistica.html">
		<form:hidden path="accion" id="txhAccion"/>
		<form:hidden path="sedeOperacion" id="sedeOperacion"/>	
		
		<%-- 
		<table cellpadding="0" cellspacing="0" width="989px" border="0" align="center" background="${ctx}/images/cabecera/cabecera_celeste.jpg">
			<tr>
            	<td height="65px" colspan="3">&nbsp;</td>                
            </tr>								
			<tr>	
				<td valign="middle" width="595">	
					&nbsp;
					<span class="textonormal">Bienvenido(a): &nbsp;
						<form:label path="codEval" cssClass="Opciones"><b>${control.codEval}</b></form:label>
					</span>
						&nbsp;
					<label onClick="fc_CerrarSesion();" style="cursor: pointer;">
                        <span class="textomenu" onmouseover="this.className='textolink'" onmouseout="this.className='textomenu'">[Cerrar Sesi&oacute;n]</span>
					</label>
						&nbsp;&nbsp;						
					<label onclick="fc_CambiarClave();" style="cursor: pointer;">
						<span class="textomenu" onmouseover="this.className='textolink'" onmouseout="this.className='textomenu'" >[Cambiar Contrase&ntilde;a]</span>
					</label>
					&nbsp;&nbsp;
					<span class="textonormal">Sede:&nbsp;
						<form:label path="codEval" cssClass="Opciones"><b>							
							<c:if test="${control.sedeOperacion=='L'}">Lima</c:if>
							<c:if test="${control.sedeOperacion=='A'}">Arequipa</c:if>
							<c:if test="${control.sedeOperacion=='T'}">Trujillo</c:if>
						</b></form:label>
					</span>
				</td>				
				<td>													
				</td>
				<td width="5px">
				</td>
			</tr>	
		</table>
		--%>
		<table border="0" cellpadding="0" cellspacing="0" width="80%" align="center" >
		  <tr>
		   <td><img src="${ctx}/images/cabecera/center/spacer.gif" width="14" height="1" border="0" alt="" /></td>
		   <td><img src="${ctx}/images/cabecera/center/spacer.gif" width="700" height="1" border="0" alt="" /></td>
		   <td><img src="${ctx}/images/cabecera/center/spacer.gif" width="1" height="1" border="0" alt="" /></td>
		   <td><img src="${ctx}/images/cabecera/center/spacer.gif" width="274" height="1" border="0" alt="" /></td>
		   <td><img src="${ctx}/images/cabecera/center/spacer.gif" width="1" height="1" border="0" alt="" /></td>
		  </tr>
		
		  <tr>
		   <td align="right"><img name="cabecera_celestecopia_r1_c1" src="${ctx}/images/cabecera/center/cabecera_celeste%20-%20copia_r1_c1.jpg" width="14" height="67" border="0" id="cabecera_celestecopia_r1_c1" alt="" /></td>
		   <td colspan="2" background="${ctx}/images/cabecera/center/cabecera_celeste - copia_r1_c2_2.jpg"></td>
		   <td rowspan="2" align="left"><img name="cabecera_celestecopia_r1_c4" src="${ctx}/images/cabecera/center/cabecera_celeste%20-%20copia_r1_c4.jpg" width="274" height="82" border="0" id="cabecera_celestecopia_r1_c4" alt="LOGO TECSUP" /></td>
		   <td><img src="${ctx}/images/cabecera/center/spacer.gif" width="1" height="67" border="0" alt="" /></td>
		  </tr>
		  <tr>
		   <td colspan="2" height="15px" class="textonormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bienvenido(a):&nbsp;
		   		<form:label path="codEval" cssClass="Opciones"><b>${control.codEval}</b></form:label>
		   		&nbsp;&nbsp;&nbsp;
		        <label onClick="fc_CerrarSesion();" style="cursor: pointer;">
		            <span class="textomenu" onmouseover="this.className='textolink'" onmouseout="this.className='textomenu'">[Cerrar Sesi&oacute;n]</span>
		        </label>
		        &nbsp;&nbsp;						
		        <label onclick="fc_CambiarClave();" style="cursor: pointer;">
		            <span class="textomenu" onmouseover="this.className='textolink'" onmouseout="this.className='textomenu'" >[Cambiar Contrase&ntilde;a]</span>
		        </label>
				&nbsp;&nbsp;
				Sede:&nbsp;
				<form:label path="codEval" cssClass="Opciones"><b>							
					<c:if test="${control.sedeOperacion=='L'}">Lima</c:if>
					<c:if test="${control.sedeOperacion=='A'}">Arequipa</c:if>
					<c:if test="${control.sedeOperacion=='T'}">Trujillo</c:if>
				</b></form:label>
		   </td>
		   <td><img name="cabecera_celestecopia_r2_c3" src="${ctx}/images/cabecera/center/cabecera_celeste%20-%20copia_r2_c3.jpg" width="1" height="15" border="0" id="cabecera_celestecopia_r2_c3" alt="" /></td>
		   <td><img src="${ctx}/images/cabecera/center/spacer.gif" width="1" height="15" border="0" alt="" /></td>
		  </tr>
		</table>
		
		
		
		<%-- 
		<iframe id="Body" name="Body" src="${ctx}/menuLogistica.html" scrolling="auto" 
		frameborder="0" height="532px" width="99%" style="align:center;margin-top:9px"></iframe>
		
		989px
		--%>
		
		<table cellpadding="0" cellspacing="0" width="80%" border="0" align="center">
		  <tr>
		   <td><img src="${ctx}/images/cabecera/center/spacer.gif" width="14" height="1" border="0" alt="" /></td>
		   <td><img src="${ctx}/images/cabecera/center/spacer.gif" width="700" height="1" border="0" alt="" /></td>
		   <td><img src="${ctx}/images/cabecera/center/spacer.gif" width="1" height="1" border="0" alt="" /></td>
		   <td><img src="${ctx}/images/cabecera/center/spacer.gif" width="274" height="1" border="0" alt="" /></td>
		   <td><img src="${ctx}/images/cabecera/center/spacer.gif" width="1" height="1" border="0" alt="" /></td>
		  </tr>		
				
         <tr><td height="10px" colspan="5">&nbsp;</td></tr>
         
         <tr><td align="center" colspan="5">
         <iframe id="Body" name="Body" src="${ctx}/menuLogistica.html" scrolling="auto" 
         frameborder="0" height="532px" width="100%"></iframe>
         </td></tr>
         </table>
		
	</form:form>	
</body>