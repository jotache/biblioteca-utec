<%@ include file="/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<%	if ( request.getSession().getAttribute("usuarioSeguridad") == null ) { %>
<script type="text/javascript">	
	<% if (request.getSession().getAttribute("reingreso")!=null) {%>
		alert("Por favor reingrese al sistema con su nueva clave.");
	<% }else{ %>
		alert("Su sesi�n ha terminado, favor de volver a ingresar al sistema.");
	<% } %>		
	location.href = "/SGA/logeoEvaluacion.html";	
</script>
<%} %>

<head>

<link href="${ctx}/styles/cab.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" type="text/javascript">
		function onLoad(){
						
		}	
		function fc_CerrarSesion(){
			if ( confirm("�Est� seguro de cerrar sesi�n?") ){
				//form = document.forms[0];
				document.forms[0].txhAccion.value="CERRAR_SESION";
				document.forms[0].submit();				
			}
		}
		
		function fc_CambiarClave(){
			//JHPR: 2008-04-10 Cambio de Clave.
			window.location.href = "${ctx}/evaluaciones/cambiaClaveEvaluacion.html";			
		}
	</script>	
</head>

<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/Evaluaciones.html">
		<form:hidden path="accion" id="txhAccion"/>
		<%-- 
		<table cellpadding="0" cellspacing="0" width="100%" style=" margin-left: 0px">
			<tr>
				<td><img src="${ctx}/images/cabecera/cabecera_celeste.jpg"  height="82px" width="989px"></td>
			</tr>
		</table>
		--%>
		<table border="0" cellspacing="0" cellpadding="0" background="${ctx}/images/cabecera/cabecera_celeste.jpg" width="989px" align="center">
			<tr>
            	<td height="65px">&nbsp;
                </td>                
            </tr>			
			<tr>	
				<td valign="middle" width="495">						
					<span class="textonormal">&nbsp;&nbsp;&nbsp;Bienvenido(a): &nbsp;<form:label path="codEval" cssClass="Opciones"><b>${control.codEval}</b></form:label></span>						
					<label onClick="fc_CerrarSesion();" style="cursor: pointer;">
                        <span class="textomenu" onmouseover="this.className='textolink'" onmouseout="this.className='textomenu'">[Cerrar Sesi&oacute;n]</span>
					</label>
						&nbsp;&nbsp;						
					<label onclick="fc_CambiarClave();" style="cursor: pointer;">
						<span class="textomenu" onmouseover="this.className='textolink'" onmouseout="this.className='textomenu'" >[Cambiar Contrase&ntilde;a]</span>
					</label>
				</td>								
				<td>
														
				</td>
				<td width="5px">
				</td>
			</tr>	
		</table>
		
		<%-- 
		<iframe id="Body" name="Body" src="${ctx}/menuEvaluaciones.html" scrolling="auto" 
		frameborder="0" height="550px" width="99%" style="align:center;margin-top:9px"></iframe>
		--%>
		
		 <table cellpadding="0" cellspacing="0" width="989px" border="0" align="center">
         <tr><td height="10px">&nbsp;</td></tr>
         <tr><td align="center">
         <iframe id="Body" name="Body" src="${ctx}/menuEvaluaciones.html" scrolling="auto" 
         frameborder="0" height="550px" width="989px" style="align:center;margin-top:9px"></iframe>
         </td></tr>
         </table>
		
	</form:form>	
</body>