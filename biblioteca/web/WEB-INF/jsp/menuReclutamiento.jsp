<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<head>
	<script type="text/javascript" type="text/javascript">
		function onLoad(){			
		}
		function fc_Perfiles(obj){
			switch(obj){
				case '1':	
						window.location.href = "${ctx}/reclutamiento/rec_mto_conf_inicio.html?txhCodUsuario=" + document.getElementById("txhCodEval").value; 
					break;
				case '2':					
						window.location.href = "${ctx}/reclutamiento/logueo_reclutamiento.html"
											"?txhCodUsuario=" + document.getElementById("txhCodEval").value + 
											"&txhCodPeriodo=" + document.getElementById("txhCodPeriodo").value;
					break;
				case '3':
						window.location.href = "${ctx}/reclutamiento/procesos_bandeja.html" +
											"?txhCodUsuario=" + document.getElementById("txhCodEval").value;
					break;
				case '4':					
						window.location.href = "${ctx}/reclutamiento/bandeja_evaluaciones_evaluador.html" +
											"?txhCodUsuario=" + document.getElementById("txhCodEval").value;					
					break;
				case '5': 					
						window.location.href = "${ctx}/evaluaciones/bandeja_gest_administrativa.html?txhCodPeriodo="+
											document.getElementById('txhCodPeriodo').value + "&txhCodEval="+
											document.getElementById('txhCodEval').value;					
					break;
				case '6':					
					window.location.href = "${ctx}/reclutamiento/log_ConsultasyReportes.html" +
										"?txhCodUsuario=" + document.getElementById("txhCodEval").value;					
					break;
				default:
					break;
			}
		}
		

	</script>
</head>

<body>
<form:form name="frmMain" id="frmMain" action="${ctx}/menuReclutamiento.html" commandName="control">
		
	<form:hidden path="codEval" id="txhCodEval"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		
	<table cellSpacing="0" align=center cellPadding="0" border="1" style="height:108px; width:98%" >
		<tr>
			<td colspan="2"></td>	
			<td rowspan="9" align="right" height="270px" >
				<img src="${ctx}/images/reclutamiento/opciones-reclut.jpg">
			</td>																
		</tr>
		
		<% if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"REC_MANT") == 1) {%>
		<tr bgcolor="#AD222F">
			<td width="10%" align="right">
				<img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand">
			</td>
			<td width="40%">
				&nbsp;<a href="javascript:fc_Perfiles('1');" class="enlace" style="cursor:hand">Mantenimiento y Configuraciones</a>
			</td>
		</tr>		
		
		<%}	%>					
				
			
		<%if (CommonSeguridad.getValidaOpcion(request.getSession()
					.getAttribute("usuarioSeguridad"),"REC_PROC") == 1) {%>
		<tr bgcolor="#AD222F"><td width="10%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
			<td>&nbsp;<a href="javascript:fc_Perfiles('3');" class="enlace" style="cursor:hand">Gestionar Procesos</a></td>
		</tr>
		<%}	%>		

				
		<%if (CommonSeguridad.getValidaOpcion(request.getSession()
					.getAttribute("usuarioSeguridad"),"REC_EVAL") == 1) {%>
		<tr bgcolor="#AD222F"><td width="10%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
			<td>&nbsp;<a href="javascript:fc_Perfiles('4');" class="enlace" style="cursor:hand">Evaluar Postulante</a></td>
		</tr>
		<%}%>			

		<%if (CommonSeguridad.getValidaOpcion(request.getSession()
					.getAttribute("usuarioSeguridad"),"REC_EVAL") == 1) {%>
		<tr bgcolor="#AD222F"><td width="10%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
			<td>&nbsp;<a href="javascript:fc_Perfiles('6');" class="enlace" style="cursor:hand">Consultas y Reportes</a></td>
		</tr>
		<%}%>
		<%if (CommonSeguridad.getValidaOpcion(request.getSession()
				.getAttribute("usuarioSeguridad"),"REC_INFO") == 1) {%>
		<!--tr bgcolor="#AD222F"><td width="10%" align="right">	<img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
			<td>&nbsp;<a href="javascript:fc_Perfiles('5');" class="enlace" style="cursor:hand">Explotación de Información</a></td>
		</tr-->
		<%}%>		
	</table>
</form:form>	
</body>